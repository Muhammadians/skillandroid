package com.skillsquared.app.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.skillsquared.app.R;
import com.skillsquared.app.adapters.ManageServicesDisputedAdapter;
import com.skillsquared.app.adapters.ManageServicesRevisionAdapter;
import com.skillsquared.app.handler.ApplicationHandler;
import com.skillsquared.app.utils.Constants;

public class DisputedFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_complete_sales, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecyclerView recyclerViewCompletedSales = view.findViewById(R.id.recyclerCompletedSales);

        if (Constants.k_ManageOrdersModel != null) {
            if (Constants.k_ManageOrdersModel.getSellerordersfrombuyers() != null) {
                if (Constants.k_ManageOrdersModel.getSellerordersfrombuyers().getAdisputed() != null) {
                    ManageServicesDisputedAdapter buyerRequestAdapter;
                    buyerRequestAdapter = new ManageServicesDisputedAdapter(getActivity(), Constants.k_ManageOrdersModel.getSellerordersfrombuyers().getAdisputed());
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                    recyclerViewCompletedSales.setLayoutManager(linearLayoutManager);
                    recyclerViewCompletedSales.setAdapter(buyerRequestAdapter);
                } else {
                    ApplicationHandler.toast(getResources().getString(R.string.no_data_to_display));
                }
            } else {
                ApplicationHandler.toast(getResources().getString(R.string.no_data_to_display));
            }
        } else {
            ApplicationHandler.toast(getResources().getString(R.string.no_data_to_display));
        }

    }

}