package com.skillsquared.app.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.skillsquared.app.R;
import com.skillsquared.app.adapters.AboutUserReviewAdapter;
import com.skillsquared.app.adapters.GigsUserAdapter;
import com.skillsquared.app.handler.ApplicationHandler;
import com.skillsquared.app.utils.Constants;

public class ReviewsUserFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_review_user, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecyclerView recyclerViewAboutUserReview = view.findViewById(R.id.recyclerViewAboutUserReview);
        TextView tvUserRatingAboutUser = view.findViewById(R.id.tvUserRatingAboutUser);

        if (Constants.k_SellerProfileModel!=null) {

            tvUserRatingAboutUser.setText(String.valueOf(Constants.k_SellerProfileModel.getAvgRating()));

            AboutUserReviewAdapter allChatsAdapter =
                    new AboutUserReviewAdapter(getActivity(), Constants.k_SellerProfileModel.getSellerReviews());
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
            recyclerViewAboutUserReview.setLayoutManager(linearLayoutManager);
            recyclerViewAboutUserReview.setAdapter(allChatsAdapter);
        } else {
            ApplicationHandler.toast(getResources().getString(R.string.no_data_to_display));
      }
    }
}