package com.skillsquared.app.fragments.manageOrdersFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.skillsquared.app.R;
import com.skillsquared.app.adapters.BuyerManageDisputedOrdersAdapter;
import com.skillsquared.app.adapters.BuyerManageManageOrdersAdapter;
import com.skillsquared.app.handler.ApplicationHandler;
import com.skillsquared.app.utils.Constants;

public class AdisputedFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_complete_sales, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecyclerView recyclerViewCompletedSales = view.findViewById(R.id.recyclerCompletedSales);

        if (Constants.k_BuyerManageOrders != null) {
            if (Constants.k_BuyerManageOrders.getBuyerordersmanagement() != null) {
                if (Constants.k_BuyerManageOrders.getBuyerordersmanagement().getAdisputed() != null) {
                    BuyerManageDisputedOrdersAdapter postedApprovedRequestListAdapter
                            = new BuyerManageDisputedOrdersAdapter(getActivity(), Constants.k_BuyerManageOrders.getBuyerordersmanagement().getAdisputed());
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                    recyclerViewCompletedSales.setLayoutManager(linearLayoutManager);
                    recyclerViewCompletedSales.setAdapter(postedApprovedRequestListAdapter);
                } else {
                    ApplicationHandler.toast(getResources().getString(R.string.no_data_to_display));
                }
            } else {
                ApplicationHandler.toast(getResources().getString(R.string.no_data_to_display));
            }
        } else {
            ApplicationHandler.toast(getResources().getString(R.string.no_data_to_display));
        }

    }

}