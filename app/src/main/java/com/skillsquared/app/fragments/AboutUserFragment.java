package com.skillsquared.app.fragments;

import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.skillsquared.app.R;
import com.skillsquared.app.activities.CategoriesGridActivity;
import com.skillsquared.app.activities.GigsActivity;
import com.skillsquared.app.adapters.CategoriesAdapter;
import com.skillsquared.app.adapters.GigsAdapter;
import com.skillsquared.app.adapters.SkillsAdapter;
import com.skillsquared.app.handler.ApplicationHandler;
import com.skillsquared.app.utils.Constants;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

import static org.webrtc.ContextUtils.getApplicationContext;

public class AboutUserFragment extends Fragment {

    TextView tvUserNameAboutUser, tvUserRatingAboutUser, tvLocation,
            tvMemberSince, tvRecentDelivery, tvLang, tvDescripttionAboutUser;
    CircleImageView ivSelectCategory;
    RecyclerView rcSkills;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_about_user, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        init(view);

    }

    private void init(View view) {
        tvUserNameAboutUser = view.findViewById(R.id.tvUserNameAboutUser);
        tvUserRatingAboutUser = view.findViewById(R.id.tvUserRatingAboutUser);
        tvLocation = view.findViewById(R.id.tvLocation);
        tvMemberSince = view.findViewById(R.id.tvResponse);
        tvRecentDelivery = view.findViewById(R.id.tvRecentDelivery);
        ivSelectCategory = view.findViewById(R.id.ivSelectCategory);
        tvLang = view.findViewById(R.id.tvLang);
        rcSkills = view.findViewById(R.id.rcSkills);
        tvDescripttionAboutUser = view.findViewById(R.id.tvDescripttionAboutUser);

        setValues();

    }

    private void setValues() {
        if (Constants.k_SellerProfileModel != null) {
            tvUserNameAboutUser.setText(Constants.k_SellerProfileModel.getProfileinfo().getSellerProfileinfo().getUsername());
            tvUserRatingAboutUser.setText(String.valueOf(Constants.k_SellerProfileModel.getAvgRating()));
            tvLocation.setText(Constants.k_SellerProfileModel.getProfileinfo().getSellerProfileinfo().getLocation());
            tvMemberSince.setText(Constants.k_SellerProfileModel.getProfileinfo().getSellerProfileinfo().getMemberSince());
            tvRecentDelivery.setText(Constants.k_SellerProfileModel.getProfileinfo().getSellerProfileinfo().getRecentDelivery());
            Picasso.get().load(Constants.k_SellerProfileModel.getProfileinfo().getSellerProfileinfo().getFreelancerImage())
                    .placeholder(R.drawable.noimg)
                    .into(ivSelectCategory);
            tvLang.setText(Constants.k_SellerProfileModel.getProfileinfo().getSellerProfileinfo().getLanguage());
            String des = Html.fromHtml(Constants.k_SellerProfileModel.getProfileinfo().getSellerProfileinfo().getDescription()).toString();
            tvDescripttionAboutUser.setText(des);

            if (Constants.k_SellerProfileModel.getSkills()!=null){

                SkillsAdapter customAdapter = new SkillsAdapter(getActivity(), Constants.k_SellerProfileModel.getSkills());
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                rcSkills.setLayoutManager(linearLayoutManager);
                rcSkills.setAdapter(customAdapter);
            }

        } else {
            ApplicationHandler.toast(getResources().getString(R.string.no_data_to_display));
        }
    }

}
