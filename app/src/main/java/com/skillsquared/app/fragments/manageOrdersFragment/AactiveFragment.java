package com.skillsquared.app.fragments.manageOrdersFragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.skillsquared.app.R;
import com.skillsquared.app.adapters.BuyerManageActiveOrdersAdapter;
import com.skillsquared.app.adapters.BuyerManageCustomActiveOrdersAdapter;
import com.skillsquared.app.handler.ApplicationHandler;
import com.skillsquared.app.utils.Constants;

public class AactiveFragment extends Fragment {

    public static final String TAG = AactiveFragment.class.getSimpleName();
    RecyclerView recyclerViewCompletedSales;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_complete_sales, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerViewCompletedSales = view.findViewById(R.id.recyclerCompletedSales);

    }

    @Override
    public void onStart() {
        super.onStart();
        if (getUserVisibleHint()){
            String chooseOrder = getArguments().getString("chooseOrder");
            Log.e(TAG, "onViewCreated: " + chooseOrder);
            setData(chooseOrder);
        }
    }

    private void setData(String param){
        if (Constants.k_BuyerManageOrders != null) {
            if (param.equals("custom")){
                if (Constants.k_BuyerManageOrders.getBuyerordersmanagementCustom().getAactive() != null) {
                    BuyerManageCustomActiveOrdersAdapter postedApprovedRequestListAdapter
                            = new BuyerManageCustomActiveOrdersAdapter(getActivity(), Constants.k_BuyerManageOrders.getBuyerordersmanagementCustom().getAactive());
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                    recyclerViewCompletedSales.setLayoutManager(linearLayoutManager);
                    recyclerViewCompletedSales.setAdapter(postedApprovedRequestListAdapter);
                } else {
                    ApplicationHandler.toast(getResources().getString(R.string.no_data_to_display));
                }
            } else {
                if (Constants.k_BuyerManageOrders.getBuyerordersmanagement() != null) {
                    if (Constants.k_BuyerManageOrders.getBuyerordersmanagement().getAactive() != null) {
                        BuyerManageActiveOrdersAdapter postedApprovedRequestListAdapter
                                = new BuyerManageActiveOrdersAdapter(getActivity(), Constants.k_BuyerManageOrders.getBuyerordersmanagement().getAactive());
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                        recyclerViewCompletedSales.setLayoutManager(linearLayoutManager);
                        recyclerViewCompletedSales.setAdapter(postedApprovedRequestListAdapter);
                    } else {
                        ApplicationHandler.toast(getResources().getString(R.string.no_data_to_display));
                    }
                } else {
                    ApplicationHandler.toast(getResources().getString(R.string.no_data_to_display));
                }
            }
        } else {
            ApplicationHandler.toast(getResources().getString(R.string.no_data_to_display));
        }
    }

//    @Override
//    public void setUserVisibleHint(boolean isVisibleToUser) {
//        super.setUserVisibleHint(isVisibleToUser);
//
//        if (isVisibleToUser && isResumed()){
//            if (getArguments() != null && getArguments().getString("chooseOrder") != null) {
//                String chooseOrder = getArguments().getString("chooseOrder");
//                Log.e(TAG, "onViewCreated: " + chooseOrder);
//            }
//        }
//
//    }
}