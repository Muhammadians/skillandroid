package com.skillsquared.app.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.skillsquared.app.R;
import com.skillsquared.app.adapters.GigsUserAdapter;
import com.skillsquared.app.handler.ApplicationHandler;
import com.skillsquared.app.utils.Constants;

public class GigsUserFragment extends Fragment {

    private RecyclerView recyclerViewGigs;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_gigs_user, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerViewGigs = view.findViewById(R.id.recyclerGigsUser);

        setRecyclerView();

    }

    private void setRecyclerView() {
        if (Constants.k_SellerProfileModel!=null){
            GigsUserAdapter allChatsAdapter =
                    new GigsUserAdapter(getActivity(), Constants.k_SellerProfileModel.getProfileinfo().getGigs().getActiveGigsServices());
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
            recyclerViewGigs.setLayoutManager(linearLayoutManager);
            recyclerViewGigs.setAdapter(allChatsAdapter);
        } else {
            ApplicationHandler.toast(getResources().getString(R.string.no_data_to_display));
        }
    }

}