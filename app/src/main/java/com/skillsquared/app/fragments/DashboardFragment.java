package com.skillsquared.app.fragments;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.skillsquared.app.R;
import com.skillsquared.app.activities.GigsActivity;
import com.skillsquared.app.adapters.GigsHomeAdapter;
import com.skillsquared.app.adapters.ServicesAdapter;
import com.skillsquared.app.adapters.ViewPagerAdapter;
import com.skillsquared.app.handler.ApplicationHandler;
import com.skillsquared.app.handler.RestApi;
import com.skillsquared.app.models.modelsOnHome.FeaturedGigsModel;
import com.skillsquared.app.models.modelsOnHome.GetServicesOnHomeModel;
import com.skillsquared.app.models.modelsOnHome.PopularCategoriesModel;
import com.skillsquared.app.utils.Constants;
import com.skillsquared.app.utils.Loading;
import com.skillsquared.app.utils.SessionManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardFragment extends Fragment {

    private final static String TAG = DashboardFragment.class.getSimpleName();
    private RecyclerView recyclerViewServices, recyclerViewGigs;
    private EditText etSearchField;
    private SessionManager sessionManager;
    private TextView tvNoDataService, tvNoDataGigs, tvSeeAll, tvSeeAllGigs;
    private GetServicesOnHomeModel getServicesOnHomeModel;
    private ViewPager rlAdView;
    private Button btnSearch;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_dashboard, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        sessionManager = new SessionManager(Objects.requireNonNull(getActivity()));
        recyclerViewServices = Objects.requireNonNull(getView()).findViewById(R.id.recyclerViewServices);
        recyclerViewGigs = getView().findViewById(R.id.recyclerViewGigs);
        etSearchField = getView().findViewById(R.id.searchField);
        tvNoDataGigs = getView().findViewById(R.id.tvNoDataToDisplayGigs);
        tvNoDataService = getView().findViewById(R.id.tvNoDataToDisplayServices);
        tvSeeAll = getView().findViewById(R.id.tvSeeAll);
        tvSeeAllGigs = getView().findViewById(R.id.tvSeeAllGigs);
        rlAdView = getView().findViewById(R.id.rlAdView);
        btnSearch = getView().findViewById(R.id.btnSearch);
        Loading.cancel();

        etSearchField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length()>0){
                    btnSearch.setVisibility(View.VISIBLE);
                } else {
                    btnSearch.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        btnSearch.setOnClickListener(view1->{
            if (TextUtils.isEmpty(ApplicationHandler.stringConverter(etSearchField))){
                   etSearchField.setError(getResources().getString(R.string.editTextEmptyFieldError));
                   etSearchField.requestFocus();
            } else {
                etSearchField.setError(null);
                ApplicationHandler.intent(GigsActivity.class, "search", ApplicationHandler.stringConverter(etSearchField));
            }
        });
//        etSearchField.setOnClickListener(view1 -> ApplicationHandler.intent(GigsActivity.class, "cat_id", "15"));

        makeServerCall();

    }

    private void makeServerCall()
    {
        if (getServicesOnHomeModel == null) {
            Loading.show(getActivity(), false, Constants.k_PLEASE_WAIT);
            Log.e(TAG, "makeServerCall: " + sessionManager.getString(Constants.k_ACCESSTOKEN));
            Call<GetServicesOnHomeModel> getServicesOnHomeModelCall = RestApi.getService().getServices(
                    sessionManager.getString(Constants.k_ACCESSTOKEN)
            );
            getServicesOnHomeModelCall.enqueue(new Callback<GetServicesOnHomeModel>() {
                @Override
                public void onResponse(@NonNull Call<GetServicesOnHomeModel> call, @NonNull Response<GetServicesOnHomeModel> response) {
                    Loading.cancel();
                    if (response.isSuccessful()) {
                        getServicesOnHomeModel = response.body();
                        setRecyclerData();
                        setViewPagerBanners();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<GetServicesOnHomeModel> call, @NonNull Throwable t) {
                    Loading.cancel();
                    Log.e(TAG, "onFailure: " + t.getMessage());
//                    ApplicationHandler.toast(t.getMessage());
                    tvNoDataService.setVisibility(View.VISIBLE);
                    tvNoDataService.setText(getResources().getString(R.string.no_data_to_display));
                    tvNoDataGigs.setVisibility(View.VISIBLE);
                    tvNoDataGigs.setText(getResources().getString(R.string.no_data_to_display));
                }
            });
        } else {
            setRecyclerData();
            setViewPagerBanners();
        }
    }

    private void setRecyclerData()
    {
        if (getServicesOnHomeModel != null && getServicesOnHomeModel.getPopularServices() != null) {
            tvNoDataService.setVisibility(View.GONE);
            ServicesAdapter servicesAdapter = new ServicesAdapter(getActivity(), getServicesOnHomeModel.getPopularServices(), tvSeeAll);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
            recyclerViewServices.setLayoutManager(linearLayoutManager);
            recyclerViewServices.setNestedScrollingEnabled(false);
            recyclerViewServices.setHasFixedSize(true);
            recyclerViewServices.setItemViewCacheSize(20);
            recyclerViewServices.setDrawingCacheEnabled(true);
            recyclerViewServices.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
            recyclerViewServices.setAdapter(servicesAdapter);
        } else {
            tvNoDataService.setVisibility(View.VISIBLE);
            tvNoDataService.setText(getResources().getString(R.string.no_data_to_display));
        }

        if (getServicesOnHomeModel != null && getServicesOnHomeModel.getPopularServices() != null) {
            tvNoDataGigs.setVisibility(View.GONE);
            GigsHomeAdapter gigsAdapter = new GigsHomeAdapter(getActivity(), getServicesOnHomeModel.getFeaturedServices(), tvSeeAllGigs);
            LinearLayoutManager linearLayoutManagerGigs = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
            recyclerViewGigs.setLayoutManager(linearLayoutManagerGigs);
            recyclerViewGigs.setNestedScrollingEnabled(false);
            recyclerViewGigs.setHasFixedSize(true);
            recyclerViewGigs.setItemViewCacheSize(20);
            recyclerViewGigs.setDrawingCacheEnabled(true);
            recyclerViewGigs.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
            recyclerViewGigs.setAdapter(gigsAdapter);
        } else {
            tvNoDataGigs.setVisibility(View.VISIBLE);
            tvNoDataGigs.setText(getResources().getString(R.string.no_data_to_display));
        }
    }

    private void setViewPagerBanners()
    {
        ViewPagerAdapter viewPagerAdapter;
        viewPagerAdapter = new ViewPagerAdapter(getActivity(), getServicesOnHomeModel.getBanners());
        rlAdView.setAdapter(viewPagerAdapter);

        rlAdView.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                /*if (position == getServicesOnHomeModel.getBanners().size()-1 && positionOffset == 0 && !isLastPageSwiped) {
                    if (counterPageScroll != 0) {
                        isLastPageSwiped = true;
                        Log.e("page", "last page");
                        rlAdView.setCurrentItem(0, true);
                    }
                    counterPageScroll++;
                } else {
                    counterPageScroll = 0;
                }*/
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    private void filter(String text) {
        List<FeaturedGigsModel> mListFiltered = new ArrayList<>();
        for (FeaturedGigsModel venue : getServicesOnHomeModel.getFeaturedServices()) {
            if (venue.getCategoryName().toLowerCase().contains(text.toLowerCase())) {
                mListFiltered.add(venue);
            }
        }
//        gigsAdapter.filterPopularServices(mListFiltered);
    }

}
