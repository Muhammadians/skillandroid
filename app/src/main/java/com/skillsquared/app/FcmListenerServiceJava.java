package com.skillsquared.app;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Vibrator;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.cometchat.pro.helpers.CometChatHelper;
import com.cometchat.pro.models.BaseMessage;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.JsonObject;
import com.skillsquared.app.activities.HomeActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

public class FcmListenerServiceJava extends FirebaseMessagingService {

    private static final String TAG = "FcmListenerServiceJava";
    String type = "";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
//        Log.e(TAG, "From: " + remoteMessage.getData());

        if (remoteMessage.getData().size()>0){
            sendNotification(remoteMessage);
        }

        if (remoteMessage.getData() != null) {
            try {
                Log.e(TAG, "onMessageReceived: "+remoteMessage.getData());
                Log.e(TAG, "onMessageReceived: notificaation receiver" + remoteMessage.getData().get("alert"));
                if (remoteMessage.getData().get("message")!=null){
                    BaseMessage notification = CometChatHelper.processMessage(new JSONObject(remoteMessage.getData().get("message")));
                    String title = notification.getSender().getName();
                    String body = remoteMessage.getData().get("alert");
                    NotificationHelper.displayNotification(getApplicationContext(), title, body);
                } else {
                    sendNotification(remoteMessage);
                    Log.e(TAG, "notification in else: "+remoteMessage.getData());
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }
    // [END receive_message]


    // [START on_new_token]
    @Override
    public void onNewToken(@NonNull String token) {
        Log.e(TAG, "Refreshed token: " + token);
    }
    // [END on_new_token]

    void sendNotification(RemoteMessage messageBody){
        String message = "", title = "";

            Map<String, String> params = messageBody.getData();
            JSONObject object = new JSONObject(params);

        try {
            title = object.getString("title");
            message = object.getString("message");
            NotificationHelper.displayNotification(getApplicationContext(), title, message);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Intent intent = new Intent(this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this);
        notificationBuilder.setContentTitle(title);
        notificationBuilder.setContentText(message);
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        notificationBuilder.setSound(soundUri);
        notificationBuilder.setSmallIcon(R.drawable.logo);
        notificationBuilder.setLargeIcon(BitmapFactory.decodeResource(this.getResources(), R.drawable.logo));
        notificationBuilder.setAutoCancel(true);
        Vibrator v = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(1000);
        notificationBuilder.setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificationBuilder.build());
    }

}
