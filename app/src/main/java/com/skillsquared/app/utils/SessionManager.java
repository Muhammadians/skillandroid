package com.skillsquared.app.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SessionManager {

    public static final String WRONG_PAIR = "Key-Value pair cannot be blank or null";
    private static final String isLogin = "isLogin";
    private static final String keyName = "userName";
    private static final String keyEmail = "userEmail";
    private static final String key = "key";
    private static final String keyImageUrl = "img_url";
    private static final String PREF_NAME = "sessionPreference";
    private static SharedPreferences.Editor editor;
    private static SharedPreferences pref;
    private static Context context;
    private static int PRIVATE_MODE = 0;

    public SessionManager(Context _contxt) {
        context = _contxt;
        pref = _contxt.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public boolean put(final String key, final String value) {
        if (key == null || key.equals("")) {
            throw new IllegalArgumentException(WRONG_PAIR);
        }
        pref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor.putString(key, value);
        return editor.commit();
    }

    public boolean putBoolean(final String key, final boolean value) {
        if (key == null || key.equals("")) {
            throw new IllegalArgumentException(WRONG_PAIR);
        }
        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor.putBoolean(key, value);
        return editor.commit();
    }

    public static int getInt(final String key) {
        pref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        return pref.getInt(key, 0);
    }

    public void remove(String key) {
        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor.remove(key);
        editor.apply();
    }

    public Boolean getBoolean(final String key) {
        pref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        return pref.getBoolean(key, false);
    }

    public void createLoginSession() {
        editor.putString(isLogin, "true");
        editor.commit();
    }

    public void save(String key, String name) {
        editor.putString(key, name);
        editor.commit();
    }

    public String getString(final String key) {
        pref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        return pref.getString(key, "nothing");
    }

    public void putLatitude(String key) {
        editor.putString(Constants.k_LAT, key);
        editor.commit();
    }

    public void putLongitude(String key) {
        editor.putString(Constants.k_LNG, key);
        editor.commit();
    }

    public void saveUserImageURL(String url) {
        editor.putString(keyImageUrl, url);
        editor.commit();
    }

    public String getUserImageURL() {
        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        String imgUrl = pref.getString(keyImageUrl, "");
        return imgUrl;
    }

    public boolean checkLogin() {
        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        String login = pref.getString(isLogin, "");

        return login.equalsIgnoreCase("true");
    }

    public void clearSession() {
        editor.clear();
        editor.commit();
    }

    public void clearVariable(String name){
        editor.remove(name);
        editor.commit();
    }

}
