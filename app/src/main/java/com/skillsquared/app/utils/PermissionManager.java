package com.skillsquared.app.utils;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.Settings;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.skillsquared.app.handler.ApplicationHandler;

public class PermissionManager {

    private static PermissionManager object;
    private static Activity context;

    private PermissionManager() {

    }

    public static PermissionManager getInstance(Activity acContext) {
        context = acContext;
        if (object == null) {
            return new PermissionManager();
        } else {
            return object;
        }
    }

    public boolean hasPermission(Activity Currentcontext, String Permission) {
        int permissionCheck = ContextCompat.checkSelfPermission(Currentcontext,
                Permission);
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    public void getPermissionifNotAvailble(String[] PERMISSION, int REQ_CODE) {
        ActivityCompat.requestPermissions(context,
                PERMISSION,
                REQ_CODE);
    }

    public void getPermissionifNotAvailble(String PERMISSION, int REQ_CODE) {
        int permissionCheck = ContextCompat.checkSelfPermission(context,
                PERMISSION);
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {

        } else {
            ActivityCompat.requestPermissions(context,
                    new String[]{PERMISSION},
                    REQ_CODE);

        }
    }

    public static void showDialogOK(Activity activity, String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(activity)
                .setCancelable(false)
                .setMessage(message)
                .setPositiveButton("Enable", okListener)
                .setNegativeButton("Cancel", okListener)
                .create()
                .show();
    }

    public static void openSettingsPage(Activity activity){
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", activity.getPackageName(), null);
        intent.setData(uri);
        activity.startActivity(intent);
    }

    public static void requestForPermission(final Activity activity){
        PermissionManager.showDialogOK(activity, "Location services are required for this app",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case DialogInterface.BUTTON_POSITIVE:
                                PermissionManager.openSettingsPage(activity);
                                break;
                            case DialogInterface.BUTTON_NEGATIVE:
                                ApplicationHandler.toast("Please enable location permission from settings");
                                activity.finish();
                                break;
                        }
                    }
                });
    }

}
