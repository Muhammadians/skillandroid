package com.skillsquared.app.utils;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import com.skillsquared.app.R;

import dmax.dialog.SpotsDialog;

public class Loading {
    private static AlertDialog spotsDialog;

    public static boolean isVisible = false;

    public static void show(Context context, boolean cancelable, String message) {
        isVisible = true;
        try {
            spotsDialog = new SpotsDialog.Builder()
                    .setContext(context)
                    .setTheme(R.style.Custom).build();
            spotsDialog.setTitle(message);
            spotsDialog.setCancelable(cancelable);
            spotsDialog.show();
        } catch (Exception e) {
            Log.d("Loading", "show");
        }
    }

    public static void cancel() {
        isVisible = false;
        try {
            if (spotsDialog != null && spotsDialog.isShowing()) {
                spotsDialog.dismiss();
                spotsDialog.cancel();
            }
        } catch (Exception e) {
            Log.d("Loading", "cancel");
        }
    }

    public static void updateMessage(final String message) {
        Runnable changeMessage = new Runnable() {
            @Override
            public void run() {
                spotsDialog.setMessage(message);
            }
        };
        changeMessage.run();
    }

}
