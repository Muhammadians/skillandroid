package com.skillsquared.app.utils;

import com.skillsquared.app.activities.FeaturedGigsDetail;
import com.skillsquared.app.models.becomeFreelancerDetails.GetBecomeFreelanceDetails;
import com.skillsquared.app.models.buyerManageOrders.BuyerManageOrderResponse;
import com.skillsquared.app.models.buyerRequest.BuyerRequest;
import com.skillsquared.app.models.buyerRequestsSeller.Sellerservice;
import com.skillsquared.app.models.gigsDetailsModel.GigsService;
import com.skillsquared.app.models.manageOrders.ManageOrdersModel;
import com.skillsquared.app.models.modelsOnHome.FeaturedGigsModel;
import com.skillsquared.app.models.sellerProfileModel.ActiveGigsService;
import com.skillsquared.app.models.sellerProfileModel.SellerProfileModel;

import java.util.List;

public class Constants {
    public final static String k_LAT = "LATITUDE";
    public final static String k_LNG = "LONGITUDE";
    public final static String k_USER_ID = "userId";
    public final static String k_USERNAME = "userNameNew";
    public final static String k_NAME = "userName";
    public final static String k_USERNAMESKILLSQUARED = "userNameSkillSquared";
    public final static String k_UIDSKILLSQUARED = "uIDSkillSquared";
    public final static String k_EMAIL = "userEmail";
    public final static String k_PHONE = "userPhone";
    public final static String k_ADDRESS = "userAddress";
    public final static String k_IMAGE = "userImage";
    public final static String k_TIMEZONE = "userTimeZone";
    public final static String k_ACCESSTOKEN = "userAccessToken";
    public final static String k_REFERRALURL = "referal_url";
    public final static String k_Freelancer = "freeLancer";
    public final static String k_FreelancerProfile = "freeLancerProfile";
    public final static String k_shareText = "sample share text";
    public final static String k_SERVER_IP = "https://www.skillsquared.com/mobileservices/";
    public final static String k_PLEASE_WAIT = "Please Wait...!!!";
    public static String k_deviceToken;
    public static List<GigsService> k_PopularServiceList = null;
    public static BuyerRequest k_BuyerRequest;
    public static SellerProfileModel k_SellerProfileModel;
    public static ActiveGigsService k_ActiveGigDetail;
    public static ManageOrdersModel k_ManageOrdersModel;
    public static BuyerManageOrderResponse k_BuyerManageOrders;
    public static String k_accessTokenForVideo;
    public static List<Sellerservice> k_SellerservicesList = null;
    public static String k_OfferPrice = null;
    public static String k_RequestIdForServer = null;
    public static String k_BuyerRequestId = null;
    public static FeaturedGigsModel k_FeaturedGigs= null;
    public static String k_LATITUDE;
    public static String k_LONGITUDE;
    public static GetBecomeFreelanceDetails k_becomeFreelanceDetails = null;
    public static boolean k_buyerCustomOrder = false;
    public static boolean k_manageCustomSales = false;
}
