package com.skillsquared.app.adapters;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.skillsquared.app.R;

public class ManageServiceViewHolder extends RecyclerView.ViewHolder {

    ImageView ivMainImageManageService, tvCloseManageService;
    TextView tvHeadingBoldManageService, tvClicksManageService, tvOrderManageService;
    CardView cardViewService;

    public ManageServiceViewHolder(@NonNull View itemView) {
        super(itemView);

        ivMainImageManageService = itemView.findViewById(R.id.ivMainImageManageService);
        tvCloseManageService = itemView.findViewById(R.id.tvCloseManageService);
        tvHeadingBoldManageService = itemView.findViewById(R.id.tvHeadingBoldManageService);
        tvClicksManageService = itemView.findViewById(R.id.tvClicksManageService);
        tvOrderManageService = itemView.findViewById(R.id.tvOrderManageService);
        cardViewService = itemView.findViewById(R.id.cardViewService);

    }
}