package com.skillsquared.app.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.skillsquared.app.R;
import com.skillsquared.app.models.gigsDetailsModel.GigsService;
import com.skillsquared.app.models.gigsDetailsModel.GigsServicesModel;
import com.skillsquared.app.models.gigsDetailsModel.ReviewModel;

import java.util.List;

public class ReviewAdapter extends RecyclerView.Adapter<ReviewViewHolder>  {

    Activity activity;
    List<ReviewModel> list;

    public ReviewAdapter(Activity activity, List<ReviewModel> list) {
        this.activity = activity;
        this.list = list;
    }

    @NonNull
    @Override
    public ReviewViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.review_item, viewGroup, false);
        return new ReviewViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ReviewViewHolder holder, int position) {

        ReviewModel item = list.get(position);

        holder.tvUserNameReview.setText(item.getUsername());
        holder.tvReview.setText(item.getReview());
        holder.ratingBarUserReview.setRating(Float.valueOf(item.getRating()));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

}
