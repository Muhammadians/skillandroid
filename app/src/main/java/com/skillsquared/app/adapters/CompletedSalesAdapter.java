package com.skillsquared.app.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.skillsquared.app.R;
import com.skillsquared.app.models.CompletedSalesModel;

import java.util.ArrayList;

public class CompletedSalesAdapter extends RecyclerView.Adapter<CompletedSalesItemViewHolder>  {

    Activity activity;
    ArrayList<CompletedSalesModel> list;

    public CompletedSalesAdapter(Activity activity, ArrayList<CompletedSalesModel> list) {
        this.activity = activity;
        this.list = list;
    }

    @NonNull
    @Override
    public CompletedSalesItemViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.completed_sales_item, viewGroup, false);
        return new CompletedSalesItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull CompletedSalesItemViewHolder holder, int position) {
        final CompletedSalesModel item = list.get(position);

        holder.tvNameCompletedSales.setText(item.getName());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

}
