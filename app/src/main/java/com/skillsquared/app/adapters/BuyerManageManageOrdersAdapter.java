package com.skillsquared.app.adapters;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.skillsquared.app.R;
import com.skillsquared.app.activities.seller.manageSales.OrderDetailActivity;
import com.skillsquared.app.models.buyerManageOrders.buyerManageOrdersSimple.Aactive;
import com.skillsquared.app.models.buyerManageOrders.buyerManageOrdersSimple.Amanage;
import com.skillsquared.app.utils.SessionManager;
import com.squareup.picasso.Picasso;

import java.util.List;

public class BuyerManageManageOrdersAdapter extends RecyclerView.Adapter<ManageSalesViewHolder> {

    private final static String TAG = BuyerManageActiveOrdersAdapter.class.getSimpleName();
    Activity activity;
    List<Amanage> list;
    SessionManager sessionManager;

    public BuyerManageManageOrdersAdapter(Activity activity, List<Amanage> list) {
        this.activity = activity;
        this.list = list;
    }

    @NonNull
    @Override
    public ManageSalesViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.manage_services_item, viewGroup, false);
        sessionManager = new SessionManager(activity);
        return new ManageSalesViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ManageSalesViewHolder holder, int position) {
        final Amanage item = list.get(position);

        Picasso.get()
                .load(item.getSellerImage())
                .placeholder(R.drawable.noimg)
                .into(holder.ivUserNameManageServices);

        holder.tvDescriptionManageServices.setText(item.getSellerServiceInfo().getTitle());
        holder.tvPriceManageService.setText(item.getRequestPrice() + "$");
        holder.tvUserNameManageServices.setText(item.getSellerName());
        holder.tvStatusManageService.setText(activity.getResources().getString(R.string.active));
        holder.tvStatusManageService.setVisibility(View.GONE);
        holder.tvDateManageServices.setText(item.getSellerServiceInfo().getDelivery());

        holder.itemView.setOnClickListener(view->{
            Intent intent = new Intent(activity, OrderDetailActivity.class);
            intent.putExtra("orderNum", item.getPaymentOrderId());
            intent.putExtra("activity", "buyerManage");
            activity.startActivity(intent);
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

}