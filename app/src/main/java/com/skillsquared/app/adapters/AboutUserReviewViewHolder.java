package com.skillsquared.app.adapters;

import android.media.Image;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.skillsquared.app.R;

public class AboutUserReviewViewHolder extends RecyclerView.ViewHolder {

    ImageView ivReview;
    TextView tvNameReview, tvDescReview, tvReviewRating;

    public AboutUserReviewViewHolder(@NonNull View itemView) {
        super(itemView);

        tvReviewRating = itemView.findViewById(R.id.tvReviewRating);
        tvDescReview = itemView.findViewById(R.id.tvDescReview);
        tvNameReview = itemView.findViewById(R.id.tvNameReview);
        ivReview = itemView.findViewById(R.id.ivReview);

    }
}