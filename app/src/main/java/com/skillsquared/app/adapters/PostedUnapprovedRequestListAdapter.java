package com.skillsquared.app.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.skillsquared.app.R;
import com.skillsquared.app.activities.buyer.postAJob.PostedRequestList;
import com.skillsquared.app.handler.ApplicationHandler;
import com.skillsquared.app.handler.RestApi;
import com.skillsquared.app.models.buyerRequest.Approve;
import com.skillsquared.app.models.buyerRequest.UnApproved;
import com.skillsquared.app.models.postAJob.PostAJobResponse;
import com.skillsquared.app.utils.Constants;
import com.skillsquared.app.utils.Loading;
import com.skillsquared.app.utils.SessionManager;

import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostedUnapprovedRequestListAdapter extends RecyclerView.Adapter<PostedRequestListViewHolder>  {

    Activity activity;
    List<UnApproved> list;
    SessionManager sessionManager;
    RadioButton rbUnapproved;

    public PostedUnapprovedRequestListAdapter(Activity activity, List<UnApproved> list, RadioButton rbUnapproved) {
        this.activity = activity;
        this.list = list;
        this.rbUnapproved = rbUnapproved;
    }

    @NonNull
    @Override
    public PostedRequestListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.posted_request_list_item, viewGroup, false);
        sessionManager = new SessionManager(activity);
        rbUnapproved.setChecked(true);
        return new PostedRequestListViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull PostedRequestListViewHolder holder, int position) {
        final UnApproved item = list.get(position);

        String dummy = item.getCreatedDate();
        List<String> wordList = Arrays.asList(dummy.split(" "));

        holder.tvDate.setText(wordList.get(0));
        holder.tvStatus.setText(item.getStatus());
        holder.tvDescriptionPostedRequest.setText(item.getDescription());
        holder.tvDuration.setText(item.getDelievry());
        holder.tvBudgetList.setText(item.getBudget() + "$");

        holder.tvStatus.setText(activity.getResources().getString(R.string.unapproved));

        holder.ivClosePostedRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Loading.show(activity, false, Constants.k_PLEASE_WAIT);
                Call<PostAJobResponse> call = RestApi.getService().removeBuyerRequest(
                        sessionManager.getString(Constants.k_ACCESSTOKEN),
                        item.getId()
                );

                call.enqueue(new Callback<PostAJobResponse>() {
                    @Override
                    public void onResponse(Call<PostAJobResponse> call, Response<PostAJobResponse> response) {
                        Loading.cancel();
                        if (response.isSuccessful()){
                            if (response.body() != null) {
                                ApplicationHandler.toast(response.body().getMessage());
                                ApplicationHandler.intent(PostedRequestList.class);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<PostAJobResponse> call, Throwable t) {
                        Loading.cancel();
                        ApplicationHandler.toast(t.getMessage());
                    }
                });

            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

}