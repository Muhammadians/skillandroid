package com.skillsquared.app.adapters;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.skillsquared.app.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class ManageSalesViewHolder extends RecyclerView.ViewHolder {

    CircleImageView ivUserNameManageServices;
    TextView tvUserNameManageServices, tvDescriptionManageServices,
            tvStatusManageService, tvPriceManageService, tvDateManageServices;

    public ManageSalesViewHolder(@NonNull View itemView) {
        super(itemView);

        ivUserNameManageServices = itemView.findViewById(R.id.ivUserNameManageServices);
        tvUserNameManageServices = itemView.findViewById(R.id.tvUserNameManageServices);
        tvDescriptionManageServices = itemView.findViewById(R.id.tvDescriptionManageServices);
        tvStatusManageService = itemView.findViewById(R.id.tvStatusManageService);
        tvPriceManageService = itemView.findViewById(R.id.tvPriceManageService);
        tvDateManageServices = itemView.findViewById(R.id.tvDateManageServices);

    }
}