package com.skillsquared.app.adapters;

import android.app.Activity;
import android.content.Intent;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.skillsquared.app.R;
import com.skillsquared.app.activities.seller.ServiceDetailActivity;
import com.skillsquared.app.handler.RestApi;
import com.skillsquared.app.models.FavouriteService;
import com.skillsquared.app.models.signUpModels.SignUpModel;
import com.skillsquared.app.utils.Constants;
import com.skillsquared.app.utils.Loading;
import com.skillsquared.app.utils.SessionManager;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SavedGigsAdapter extends RecyclerView.Adapter<GigsItemViewHolder> {

    private final static String TAG = "GigsAdapter";
    Activity activity;
    List<FavouriteService> list;
    SessionManager sessionManager;

    public SavedGigsAdapter(Activity activity, List<FavouriteService> list) {
        this.activity = activity;
        this.list = list;
    }

    @NonNull
    @Override
    public GigsItemViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.gigs_item, viewGroup, false);
        sessionManager = new SessionManager(activity);
        return new GigsItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull GigsItemViewHolder holder, final int position) {
        FavouriteService item = list.get(position);

        holder.tvHeadingBold.setText(item.getTitle());
        String des = Html.fromHtml(item.getDescription()).toString();
        holder.tvSubHeading.setText(des);
        Picasso.get()
                .load(item.getServiceImage())
                .placeholder(R.drawable.noimg)
                .into(holder.ivMainImage);
        holder.tvPrice.setText("$" + item.getPrice());
        holder.ratingBar.setRating(Float.valueOf(item.getRating()));
        holder.tvCall.setVisibility(View.INVISIBLE);
        holder.tvMessage.setVisibility(View.INVISIBLE);
        holder.ivLike.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_heart));
        holder.ivLike.setOnClickListener(view -> makeFavoriteService(item.getId(), holder.ivLike));

        holder.cardViewGigs.setOnClickListener(view->{
            Intent intent = new Intent(activity, ServiceDetailActivity.class);
            intent.putExtra("mainImage", item.getServiceImage());
            intent.putExtra("title", item.getTitle());
            intent.putExtra("description", item.getDescription());
            intent.putExtra("price", item.getPrice());
            intent.putExtra("deliveryTime", item.getDeliveryTime());
            intent.putExtra("rating", item.getRating());
            activity.startActivity(intent);
        });

    }

    private void makeFavoriteService(String serviceId, ImageView ivLike){
        Loading.show(activity, false, Constants.k_PLEASE_WAIT);

        Call<SignUpModel> favoriteCall = RestApi.getService().makeFavoriteService(
                sessionManager.getString(Constants.k_ACCESSTOKEN),
                serviceId
        );

        favoriteCall.enqueue(new Callback<SignUpModel>() {
            @Override
            public void onResponse(@NonNull Call<SignUpModel> call,@NonNull Response<SignUpModel> response) {
                Loading.cancel();
                if (response.isSuccessful()){
                    SignUpModel signUpModel = response.body();
                    if (signUpModel != null && signUpModel.getMessage().equals("Success: Liked!")) {
                        ivLike.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_heart));
                    } else {
                        ivLike.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_heart_not_liked));
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<SignUpModel> call,@NonNull Throwable t) {
                Loading.cancel();
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

}
