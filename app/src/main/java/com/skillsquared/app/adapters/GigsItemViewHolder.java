package com.skillsquared.app.adapters;

import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.skillsquared.app.R;

public class GigsItemViewHolder extends RecyclerView.ViewHolder {

    ImageView ivProfile, ivLike, ivMainImage;
    TextView tvHeadingBold, tvPrice, tvSubHeading,
            tvCall, tvMessage;
    RatingBar ratingBar;
    CardView cardViewGigs;

    public GigsItemViewHolder(@NonNull View itemView) {
        super(itemView);

        ivProfile = itemView.findViewById(R.id.ivProfile);
        ivLike = itemView.findViewById(R.id.ivLike);
        tvHeadingBold = itemView.findViewById(R.id.tvHeadingBold);
        tvPrice = itemView.findViewById(R.id.tvPrice);
        tvSubHeading = itemView.findViewById(R.id.tvSubHeading);
        tvCall = itemView.findViewById(R.id.tvCall);
        tvMessage = itemView.findViewById(R.id.tvMessage);
        ratingBar = itemView.findViewById(R.id.ratingBar);
        ivMainImage = itemView.findViewById(R.id.ivMainImage);
        cardViewGigs = itemView.findViewById(R.id.cardViewGigs);

    }
}