package com.skillsquared.app.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.skillsquared.app.R;
import com.skillsquared.app.models.AllChatsModel;

import java.util.ArrayList;

public class AllChatsAdapter extends RecyclerView.Adapter<AllChatsViewHolder>  {

    Activity activity;
    ArrayList<AllChatsModel> list;

    public AllChatsAdapter(Activity activity, ArrayList<AllChatsModel> list) {
        this.activity = activity;
        this.list = list;
    }

    @NonNull
    @Override
    public AllChatsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.chats_item, viewGroup, false);
        return new AllChatsViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull AllChatsViewHolder holder, int position) {
        final AllChatsModel item = list.get(position);
        holder.ivProfileImageChat.setImageResource(item.getImage());
        holder.tvUserNameChats.setText(item.getUserName());
        holder.tvUserLastMsgChats.setText(item.getLastMessage());
        holder.tvLastMessageMinutes.setText(item.getLastMessageTime());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

}

