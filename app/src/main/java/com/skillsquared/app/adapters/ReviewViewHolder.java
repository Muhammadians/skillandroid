package com.skillsquared.app.adapters;

import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.skillsquared.app.R;

public class ReviewViewHolder extends RecyclerView.ViewHolder {

    TextView tvUserNameReview, tvReview;
    RatingBar ratingBarUserReview;

    public ReviewViewHolder(@NonNull View itemView) {
        super(itemView);

        tvUserNameReview = itemView.findViewById(R.id.tvUserNameReview);
        tvReview = itemView.findViewById(R.id.tvReview);
        ratingBarUserReview = itemView.findViewById(R.id.ratingBarUserReview);

    }

}