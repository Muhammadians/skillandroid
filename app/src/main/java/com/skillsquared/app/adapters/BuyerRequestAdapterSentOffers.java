package com.skillsquared.app.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.skillsquared.app.R;
import com.skillsquared.app.handler.ApplicationHandler;
import com.skillsquared.app.models.buyerRequestsSeller.SellerSentOffer;
import com.skillsquared.app.models.buyerRequestsSeller.SentOffers;
import com.squareup.picasso.Picasso;

import java.util.Arrays;
import java.util.List;

public class BuyerRequestAdapterSentOffers extends RecyclerView.Adapter<BuyerRequestViewHolder> {

    Activity activity;
    List<SentOffers> list;
    TextView tvPageNumber;
    RadioButton radioButton;

    public BuyerRequestAdapterSentOffers(Activity activity, List<SentOffers> list, TextView tvPageNumber, RadioButton radioButton) {
        this.activity = activity;
        this.list = list;
        this.tvPageNumber = tvPageNumber;
        this.radioButton = radioButton;
        this.radioButton.setChecked(true);
    }

    @NonNull
    @Override
    public BuyerRequestViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.buyer_request_item, viewGroup, false);
        return new BuyerRequestViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull BuyerRequestViewHolder holder, int position) {
        SentOffers item = list.get(position);

        Picasso.get()
                .load(item.getUserImage())
                .placeholder(R.drawable.noimg)
                .into(holder.ivUserImageBuyerRequest);

        holder.tvUserNameBuyerRequest.setText(item.getUserName());

        String dummy = item.getCreatedDate();
        List<String> wordList = Arrays.asList(dummy.split(" "));

        holder.tvDateBuyerRequest.setText(wordList.get(0));
        holder.tvSentOffers.setText(item.getCategory() + " / " + item.getOfferSent());
        holder.tvDurationBuyerRequest.setText(item.getDelievry());
        holder.tvBudgetBuyerRequest.setText(item.getBudget());
        holder.tvDescriptionBuyerRequest.setText(item.getDescription());

        if (item.getApplideOrNot().equals("1")) {
            holder.sendOfferCardView.setCardBackgroundColor(activity.getResources().getColor(R.color.colorGrey));
        } else {
            holder.sendOfferCardView.setCardBackgroundColor(activity.getResources().getColor(R.color.colorPrimary));
            holder.sendOfferCardView.setOnClickListener(view -> ApplicationHandler.toast("Yes"));
        }

        tvPageNumber.setText(list.size()+"/"+(position+1));

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

}
