package com.skillsquared.app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.skillsquared.app.R;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ViewPagerAdapter extends PagerAdapter {

    Context mContext;
    List<String> screenItemList;

    public ViewPagerAdapter(Context mContext, List<String> screenItemList) {
        this.mContext = mContext;
        this.screenItemList = screenItemList;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater inflater= (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layoutScreen = null;
        if (inflater != null) {
            layoutScreen = inflater.inflate(R.layout.view_pager_layout, null);
            ImageView imageView = layoutScreen.findViewById(R.id.ivBanner);

            Picasso.get()
                    .load(screenItemList.get(position))
                    .centerCrop().fit()
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .into(imageView);

            container.addView(layoutScreen);
        }

        return layoutScreen;

    }

    @Override
    public int getCount() {
        return screenItemList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View)object);
    }

}
