package com.skillsquared.app.adapters;

import android.app.Activity;
import android.content.Intent;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.skillsquared.app.R;
import com.skillsquared.app.activities.seller.ServiceDetailActivity;
import com.skillsquared.app.handler.ApplicationHandler;
import com.skillsquared.app.handler.RestApi;
import com.skillsquared.app.models.manageServices.ActiveGig;
import com.skillsquared.app.models.postAJob.PostAJobResponse;
import com.skillsquared.app.utils.Constants;
import com.skillsquared.app.utils.Loading;
import com.skillsquared.app.utils.SessionManager;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ManageServiceAdapter extends RecyclerView.Adapter<ManageServiceViewHolder> {

    Activity activity;
    List<ActiveGig> list;
    private final static String TAG = "Manage Service Adapter";
    private SessionManager sessionManager;

    public ManageServiceAdapter(Activity activity, List<ActiveGig> list) {
        this.activity = activity;
        this.list = list;
    }

    @NonNull
    @Override
    public ManageServiceViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.manage_service_item, viewGroup, false);
        sessionManager = new SessionManager(activity);
        return new ManageServiceViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ManageServiceViewHolder holder, int position) {
        final ActiveGig item = list.get(position);

        Picasso.get()
                .load(item.getServiceImage())
                .placeholder(R.drawable.noimg)
                .into(holder.ivMainImageManageService);

        holder.tvClicksManageService.setText(String.valueOf(item.getTotalClicks()));
        holder.tvOrderManageService.setText(item.getTotalOrders());
        String des = Html.fromHtml(item.getDescription()).toString();
        holder.tvHeadingBoldManageService.setText(item.getTitle());

        holder.cardViewService.setOnClickListener(view->{
            Intent intent = new Intent(activity, ServiceDetailActivity.class);
            intent.putExtra("serviceId", item.getId());
            intent.putExtra("mainImage", item.getServiceImage());
            intent.putExtra("title", item.getTitle());
            intent.putExtra("description", item.getDescription());
            intent.putExtra("price", item.getPrice());
            intent.putExtra("deliveryTime", item.getDeliveryTime());
            intent.putExtra("rating", item.getRating());
            activity.startActivity(intent);
        });

        holder.tvCloseManageService.setOnClickListener(view -> {
            Loading.show(activity, false, Constants.k_PLEASE_WAIT);
            Call<PostAJobResponse> removeService = RestApi.getService().removeService(
                    sessionManager.getString(Constants.k_ACCESSTOKEN),
                    item.getId()
                    );
            removeService.enqueue(new Callback<PostAJobResponse>() {
                @Override
                public void onResponse(@NonNull Call<PostAJobResponse> call,@NonNull Response<PostAJobResponse> response) {
                    Loading.cancel();
                    if (response.isSuccessful()){
                        PostAJobResponse postAJobResponse = response.body();
                        assert postAJobResponse != null;
                        ApplicationHandler.toast(postAJobResponse.getMessage());
                        removeAt(holder.getAdapterPosition());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<PostAJobResponse> call,@NonNull Throwable t) {
                    Loading.cancel();
                    Log.e(TAG, "onFailure: "+t.getMessage());
                    ApplicationHandler.toast(t.getMessage());
                }
            });
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void removeAt(int position) {
        list.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, list.size());
    }

}
