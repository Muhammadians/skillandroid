package com.skillsquared.app.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.skillsquared.app.R;
import com.skillsquared.app.activities.GigsActivity;
import com.skillsquared.app.handler.ApplicationHandler;
import com.skillsquared.app.models.ServicesAndGigsModel;
import com.skillsquared.app.models.modelsOnHome.PopularCategoriesModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesViewHolder>  {

    Activity activity;
    List<PopularCategoriesModel> list;

    public CategoriesAdapter(Activity activity, List<PopularCategoriesModel> list) {
        this.activity = activity;
        this.list = list;
    }

    @NonNull
    @Override
    public CategoriesViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.categories_item, viewGroup, false);
        return new CategoriesViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoriesViewHolder holder, int position) {
        final PopularCategoriesModel item = list.get(position);

        Picasso.get().load(item.getPage_banner()).placeholder(R.drawable.noimg).into(holder.ivCategory);
        holder.tvCategoryName.setText(item.getTitle());

        holder.cardViewGigsItem.setOnClickListener(view -> ApplicationHandler.intent(GigsActivity.class, "cat_id", item.getCat_id()));

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void filterGridCategories(List<PopularCategoriesModel> filteredList){
        list = filteredList;
        notifyDataSetChanged();
    }

}
