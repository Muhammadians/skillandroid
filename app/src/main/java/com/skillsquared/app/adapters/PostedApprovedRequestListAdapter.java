package com.skillsquared.app.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.skillsquared.app.R;
import com.skillsquared.app.activities.buyer.ViewOffers;
import com.skillsquared.app.activities.buyer.postAJob.PostedRequestList;
import com.skillsquared.app.handler.ApplicationHandler;
import com.skillsquared.app.handler.RestApi;
import com.skillsquared.app.models.buyerRequest.Approve;
import com.skillsquared.app.models.postAJob.PostAJobResponse;
import com.skillsquared.app.utils.Constants;
import com.skillsquared.app.utils.Loading;
import com.skillsquared.app.utils.SessionManager;
import java.util.Arrays;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostedApprovedRequestListAdapter extends RecyclerView.Adapter<PostedRequestListViewHolder>  {

    Activity activity;
    List<Approve> list;
    SessionManager sessionManager;
    RadioButton rbActive;

    public PostedApprovedRequestListAdapter(Activity activity, List<Approve> list, RadioButton rbActive) {
        this.activity = activity;
        this.list = list;
        this.rbActive = rbActive;
    }

    @NonNull
    @Override
    public PostedRequestListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.posted_request_list_item, viewGroup, false);
        sessionManager = new SessionManager(activity);
        rbActive.setChecked(true);
        return new PostedRequestListViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull PostedRequestListViewHolder holder, int position) {
        final Approve item = list.get(position);

        String dummy = item.getCreatedDate();
        List<String> wordList = Arrays.asList(dummy.split(" "));

        holder.tvDate.setText(wordList.get(0));
        holder.tvDescriptionPostedRequest.setText(item.getDescription());
        holder.tvDuration.setText(item.getDelievry());
        holder.tvStatus.setText(activity.getResources().getString(R.string.approved));
        holder.cardViewStatus.setCardBackgroundColor(activity.getResources().getColor(R.color.colorPrimaryDark));
        holder.tvOffers.setText("Review Offers ("+item.getTotaloffer()+")");
        holder.tvBudgetList.setText(item.getBudget() + "$");

        holder.cardViewOffersPostedRequest.setOnClickListener(view -> {
            if (item.getTotaloffer()>0){
                ApplicationHandler.intent(ViewOffers.class, "request_id", item.getId());
            } else {
                ApplicationHandler.toast("No Offers Yet...");
            }
        });

        holder.ivClosePostedRequest.setOnClickListener(view -> {
            Loading.show(activity, false, Constants.k_PLEASE_WAIT);
            Call<PostAJobResponse> call = RestApi.getService().removeBuyerRequest(
                    sessionManager.getString(Constants.k_ACCESSTOKEN),
                    item.getId()
            );

            call.enqueue(new Callback<PostAJobResponse>() {
                @Override
                public void onResponse(Call<PostAJobResponse> call, Response<PostAJobResponse> response) {
                    Loading.cancel();
                    if (response.isSuccessful()){
                        if (response.body() != null) {
                            ApplicationHandler.toast(response.body().getMessage());
                            ApplicationHandler.intent(PostedRequestList.class);
                        }
                    }
                }

                @Override
                public void onFailure(Call<PostAJobResponse> call, Throwable t) {
                    Loading.cancel();
                    ApplicationHandler.toast(t.getMessage());
                }
            });

        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

}