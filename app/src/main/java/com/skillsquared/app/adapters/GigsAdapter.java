package com.skillsquared.app.adapters;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cometchat.pro.constants.CometChatConstants;
import com.cometchat.pro.core.CometChat;
import com.cometchat.pro.exceptions.CometChatException;
import com.cometchat.pro.models.User;
import com.skillsquared.app.R;
import com.skillsquared.app.activities.GigsDetailActivity;
import com.skillsquared.app.activities.SignIn;
import com.skillsquared.app.cometChat.Activity.OneToOneChatActivity;
import com.skillsquared.app.cometChat.Contracts.LoginActivityContract;
import com.skillsquared.app.cometChat.Contracts.OneToOneActivityContract;
import com.skillsquared.app.cometChat.Contracts.StringContract;
import com.skillsquared.app.cometChat.Presenters.OneToOneActivityPresenter;
import com.skillsquared.app.cometChat.Utils.CommonUtils;
import com.skillsquared.app.handler.ApplicationHandler;
import com.skillsquared.app.handler.RestApi;
import com.skillsquared.app.models.gigsDetailsModel.GigsService;
import com.skillsquared.app.models.modelsOnHome.PopularCategoriesModel;
import com.skillsquared.app.models.signUpModels.SignUpModel;
import com.skillsquared.app.utils.Constants;
import com.skillsquared.app.utils.Loading;
import com.skillsquared.app.utils.SessionManager;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GigsAdapter extends RecyclerView.Adapter<GigsItemViewHolder>{

    private final static String TAG = GigsAdapter.class.getSimpleName();
    Activity activity;
    List<GigsService> list;
    private String UID;
    AsyncTask<?, ?, ?> runningTask;
    private LoginActivityContract.LoginActivityPresenter loginActivityPresenter;
    private String receiverID;
    private String receiverName;
    private OneToOneActivityContract.OneToOnePresenter oneToOnePresenter;
    private SessionManager sessionManager;
    String cometChatUserName;

    public GigsAdapter(Activity activity, List<GigsService> list) {
        this.activity = activity;
        this.list = list;
    }

    @NonNull
    @Override
    public GigsItemViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.gigs_item, viewGroup, false);
        oneToOnePresenter = new OneToOneActivityPresenter();
        sessionManager = new SessionManager(activity);
        return new GigsItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull GigsItemViewHolder holder, final int position) {
        GigsService item = list.get(position);

        holder.tvHeadingBold.setText(item.getServiceTitle());
        String des = Html.fromHtml(item.getDescription()).toString();
        holder.tvSubHeading.setText(des);
        Picasso.get().load(item.getMedia()).placeholder(R.drawable.noimg).into(holder.ivMainImage);
        holder.tvPrice.setText("$" + item.getPrice());
        receiverName = item.getFreelancerUsername();

        if (item.getServiceRating() != null) {
            String dummy = item.getServiceRating();
            List<String> wordList = Arrays.asList(dummy.split("=>"));
            holder.ratingBar.setRating(Float.valueOf(wordList.get(1)));
        }

        Picasso.get().load(item.getFreelancerImage()).placeholder(R.drawable.noimg).into(holder.ivProfile);

        holder.cardViewGigs.setOnClickListener(view -> {
            Log.e(TAG, "onBindViewHolder: " + position);
            ApplicationHandler.intent(GigsDetailActivity.class, "position", position + "");
        });

        holder.tvMessage.setOnClickListener(view -> {
            UID = "11"+item.getUsername();
            cometChatUserName = item.getFreelancerUsername();
            Log.e(TAG, "onBindViewHolder: "+UID);
            getCometChatUser();
        });

        holder.tvCall.setOnClickListener(view->{
            receiverID = "11"+item.getUsername();
            cometChatUserName = item.getFreelancerUsername();
            makeCall();
        });

        if (item.getLiked()){
            holder.ivLike.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_heart));
        } else {
            holder.ivLike.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_heart_not_liked));
        }

        holder.ivLike.setOnClickListener(view -> makeFavoriteService(item.getId(), holder.ivLike));

    }

    private void getCometChatUser(){
        Loading.show(activity, false, Constants.k_PLEASE_WAIT);
        CometChat.getUser(UID, new CometChat.CallbackListener<User>() {
            @Override
            public void onSuccess(User user) {
                Loading.cancel();
                Log.d(TAG, "User details fetched for user: " + user.toString());

                String contactID = user.getUid();
                String contactName = user.getName();
                String userAvatar = user.getAvatar();

                Intent intent = new Intent(activity, OneToOneChatActivity.class);
                intent.putExtra(StringContract.IntentStrings.USER_ID, contactID);
                intent.putExtra(StringContract.IntentStrings.USER_AVATAR, userAvatar);
                intent.putExtra(StringContract.IntentStrings.USER_NAME, contactName);
                activity.startActivity(intent);
            }

            @Override
            public void onError(CometChatException e) {
                Log.d(TAG, "User details fetching failed with exception: " + e.getMessage());
                ApplicationHandler.cometChatCreateUser(UID, cometChatUserName, activity, "Inbox");
            }
        });
    }

    private void makeCall(){
        com.cometchat.pro.core.Call call = new com.cometchat.pro.core.Call(receiverID, CometChatConstants.RECEIVER_TYPE_USER, CometChatConstants.CALL_TYPE_AUDIO);
        CometChat.initiateCall(call, new CometChat.CallbackListener<com.cometchat.pro.core.Call>() {
            @Override
            public void onSuccess(com.cometchat.pro.core.Call call) {
                CommonUtils.startCallIntent(activity, ((User) call.getCallReceiver()), call.getType(), true, call.getSessionId());
            }

            @Override
            public void onError(CometChatException e) {
                Toast.makeText(activity, e.getMessage(), Toast.LENGTH_SHORT).show();
                ApplicationHandler.cometChatCreateUser(receiverID, cometChatUserName, activity);
                Log.e(TAG, "onError: "+e.getMessage());
            }

        });

    }

    private void makeFavoriteService(String serviceId, ImageView ivLike){
        Loading.show(activity, false, Constants.k_PLEASE_WAIT);

        Call<SignUpModel> favoriteCall = RestApi.getService().makeFavoriteService(
            sessionManager.getString(Constants.k_ACCESSTOKEN),
                serviceId
        );

        favoriteCall.enqueue(new Callback<SignUpModel>() {
            @Override
            public void onResponse(@NonNull Call<SignUpModel> call,@NonNull Response<SignUpModel> response) {
                Loading.cancel();
                if (response.isSuccessful()){
                    SignUpModel signUpModel = response.body();
                    if (signUpModel != null && signUpModel.getMessage().equals("Success: Liked!")) {
                        ivLike.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_heart));
                    } else {
                        ivLike.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_heart_not_liked));
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<SignUpModel> call,@NonNull Throwable t) {
                Loading.cancel();
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void filterGridCategories(List<GigsService> filteredList){
        list = filteredList;
        notifyDataSetChanged();
    }

}
