package com.skillsquared.app.adapters;

import android.app.Activity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.skillsquared.app.R;
import com.skillsquared.app.activities.seller.AboutUserGigsDetailActivity;
import com.skillsquared.app.handler.ApplicationHandler;
import com.skillsquared.app.models.sellerProfileModel.ActiveGigsService;
import com.skillsquared.app.utils.Constants;
import com.squareup.picasso.Picasso;

import java.util.List;

public class GigsUserAdapter extends RecyclerView.Adapter<GigsUserItemViewHolder> {

    Activity activity;
    List<ActiveGigsService> list;

    public GigsUserAdapter(Activity activity, List<ActiveGigsService> list) {
        this.activity = activity;
        this.list = list;
    }

    @NonNull
    @Override
    public GigsUserItemViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.gigs_user_item, viewGroup, false);
        return new GigsUserItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull GigsUserItemViewHolder holder, int position) {
        final ActiveGigsService item = list.get(position);

        holder.tvUserRatingAboutUser.setText(item.getRating());
        holder.tvPriceGigsUser.setText("From $" + item.getPrice());

        String des = Html.fromHtml(item.getDescription()).toString();
        holder.tvDescriptionGigsUser.setText(des);

        Picasso.get().load(item.getServiceImage()).into(holder.ivMainImageGigsUser);

        holder.cardViewGigsDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Constants.k_ActiveGigDetail = null;
                Constants.k_ActiveGigDetail = item;
                ApplicationHandler.intent(AboutUserGigsDetailActivity.class);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

}
