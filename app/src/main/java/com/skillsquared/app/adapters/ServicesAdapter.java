package com.skillsquared.app.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.skillsquared.app.R;
import com.skillsquared.app.activities.CategoriesGridActivity;
import com.skillsquared.app.handler.ApplicationHandler;
import com.skillsquared.app.models.modelsOnHome.PopularCategoriesModel;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ServicesAdapter extends RecyclerView.Adapter<ServicesAndGigsViewHolder>  {

    Activity activity;
    List<PopularCategoriesModel> list;
    private TextView tvSeeAll;
    private boolean listSize = false;

    public ServicesAdapter(Activity activity, List<PopularCategoriesModel> list, TextView tvSeeAll)
    {
        this.activity = activity;
        this.list = list;
        this.tvSeeAll = tvSeeAll;
    }

    @NonNull
    @Override
    public ServicesAndGigsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.services_and_gigs_item, viewGroup, false);
        return new ServicesAndGigsViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ServicesAndGigsViewHolder holder, int position) {
        PopularCategoriesModel item = list.get(position);

        Picasso.get()
                .load(item.getPage_banner())
                .fit().centerCrop()
                .placeholder(R.drawable.noimg)
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .into(holder.ivProduct);
        holder.productName.setText(item.getTitle());

        /*tvSeeAll.setOnClickListener(view->{
            listSize = true;
            notifyDataSetChanged();
        });*/

        holder.rlItem.setOnClickListener(view -> ApplicationHandler.intent(CategoriesGridActivity.class, "cat_id", item.getCat_id()));

    }

    @Override
    public int getItemCount() {
        /*if (listSize){
            return list.size();
        } else {
            return 3;
        }*/

        return list.size();
    }

}
