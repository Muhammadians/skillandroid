package com.skillsquared.app.adapters;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.skillsquared.app.R;
import com.skillsquared.app.activities.seller.manageSales.OrderDetailActivity;
import com.skillsquared.app.models.buyerManageOrders.buyerManageOrdersCustom.Aactive_;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ManageCustomServicesActiveAdapter extends RecyclerView.Adapter<ManageSalesViewHolder> {

    Activity activity;
    List<Aactive_> list;

    public ManageCustomServicesActiveAdapter(Activity activity, List<Aactive_> list) {
        this.activity = activity;
        this.list = list;
    }

    @NonNull
    @Override
    public ManageSalesViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.manage_services_item, viewGroup, false);
        return new ManageSalesViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ManageSalesViewHolder holder, int position) {
        final Aactive_ item = list.get(position);

        Picasso.get()
                .load(item.getSellerImage())
                .placeholder(R.drawable.noimg)
                .into(holder.ivUserNameManageServices);

        holder.tvDescriptionManageServices.setText(item.getSellerServiceInfo().getTitle());
        holder.tvPriceManageService.setText(item.getPrice() + "$");
        holder.tvUserNameManageServices.setText(item.getSellerName());
        holder.tvStatusManageService.setText(activity.getResources().getString(R.string.active));
        holder.tvStatusManageService.setVisibility(View.GONE);
        holder.tvDateManageServices.setText(item.getCreatedOn());

        holder.itemView.setOnClickListener(view -> {
            Intent intent = new Intent(activity, OrderDetailActivity.class);
            intent.putExtra("orderNum", item.getPaymentOrderId());
            intent.putExtra("activity", "active");
            activity.startActivity(intent);
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}