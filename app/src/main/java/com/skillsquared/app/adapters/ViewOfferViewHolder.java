package com.skillsquared.app.adapters;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.skillsquared.app.R;

public class ViewOfferViewHolder extends RecyclerView.ViewHolder {

    ImageView ivUserImageViewOffer, ivCloseViewOffer;
    TextView tvUserNameViewOffer, tvDescriptionViewOffer, tvPrice, tvOrder;
    CardView cardViewOrderViewOffer, cardViewAskQuestionViewOffer;
    RatingBar ratingBar;
    LinearLayout llShowPopup;

    public ViewOfferViewHolder(@NonNull View itemView) {
        super(itemView);

        ivUserImageViewOffer = itemView.findViewById(R.id.ivUserImageViewOffer);
        ivCloseViewOffer = itemView.findViewById(R.id.ivCloseViewOffer);
        tvUserNameViewOffer = itemView.findViewById(R.id.tvUserNameViewOffer);
        ratingBar = itemView.findViewById(R.id.tvUserRatingViewOffer);
        tvDescriptionViewOffer = itemView.findViewById(R.id.tvDescriptionViewOffer);
//        tvPrice = itemView.findViewById(R.id.tvPrice);
        tvOrder = itemView.findViewById(R.id.tvOrder);
        cardViewOrderViewOffer = itemView.findViewById(R.id.cardViewOrderViewOffer);
        cardViewAskQuestionViewOffer = itemView.findViewById(R.id.cardViewAskQuestionViewOffer);
        llShowPopup = itemView.findViewById(R.id.llShowPopup);
    }

}
