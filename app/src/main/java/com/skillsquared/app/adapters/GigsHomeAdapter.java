package com.skillsquared.app.adapters;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.skillsquared.app.R;
import com.skillsquared.app.activities.FeaturedGigsDetail;
import com.skillsquared.app.handler.ApplicationHandler;
import com.skillsquared.app.models.modelsOnHome.FeaturedGigsModel;
import com.skillsquared.app.models.modelsOnHome.PopularCategoriesModel;
import com.skillsquared.app.utils.Constants;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import java.util.List;

public class GigsHomeAdapter extends RecyclerView.Adapter<ServicesAndGigsViewHolder>  {

    private final static String TAG = GigsHomeAdapter.class.getSimpleName();
    Activity activity;
    List<FeaturedGigsModel> list;
    private TextView tvSeeAllGigs;
    private boolean listSize = false;

    public GigsHomeAdapter(Activity activity, List<FeaturedGigsModel> list, TextView tvSeeAllGigs) {
        this.activity = activity;
        this.list = list;
        this.tvSeeAllGigs = tvSeeAllGigs;
    }

    @NonNull
    @Override
    public ServicesAndGigsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.services_and_gigs_item, viewGroup, false);
        return new ServicesAndGigsViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ServicesAndGigsViewHolder holder, int position) {
        final FeaturedGigsModel item = list.get(position);

        Picasso.get()
                .load(item.getMedia())
                .fit().centerCrop()
                .placeholder(R.drawable.noimg)
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .into(holder.ivProduct);
        holder.productName.setText(item.getCategoryName());

        /*tvSeeAllGigs.setOnClickListener(view -> {
            listSize = true;
            notifyDataSetChanged();
        });*/

        holder.rlItem.setOnClickListener(view->{
            Constants.k_FeaturedGigs = item;
            ApplicationHandler.intent(FeaturedGigsDetail.class);
        });
    }

    @Override
    public int getItemCount() {
        /*if (listSize){
            return list.size();
        } else {
            return 3;
        }*/
        return list.size();
    }

    /*public void filterPopularServices(List<FeaturedGigsModel> filteredList){
        listSize = true;
        list = filteredList;
        notifyDataSetChanged();
    }*/

}