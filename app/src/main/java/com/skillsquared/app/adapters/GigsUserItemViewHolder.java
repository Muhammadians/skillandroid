package com.skillsquared.app.adapters;

import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.skillsquared.app.R;

public class GigsUserItemViewHolder extends RecyclerView.ViewHolder {

    ImageView ivMainImageGigsUser, ivHeartGigsUser;
    TextView tvUserRatingAboutUser, tvRatingNumberAboutUser,
            tvDescriptionGigsUser, tvPriceGigsUser;
    CardView cardViewGigsDetail;

    public GigsUserItemViewHolder(@NonNull View itemView) {
        super(itemView);

        ivMainImageGigsUser = itemView.findViewById(R.id.ivMainImageGigsUser);
        ivHeartGigsUser = itemView.findViewById(R.id.ivHeartGigsUser);
        tvUserRatingAboutUser = itemView.findViewById(R.id.tvUserRatingAboutUser);
        tvRatingNumberAboutUser = itemView.findViewById(R.id.tvRatingNumberAboutUser);
        tvDescriptionGigsUser = itemView.findViewById(R.id.tvDescriptionGigsUser);
        tvPriceGigsUser = itemView.findViewById(R.id.tvPriceGigsUser);
        cardViewGigsDetail = itemView.findViewById(R.id.cardViewGigsDetail);

    }
}