package com.skillsquared.app.adapters;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.skillsquared.app.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class ManageOrdersViewHolder extends RecyclerView.ViewHolder {

    CircleImageView ivUserImageManageOrders;
    TextView tvUserNameManageOrders, tvSellerFiles, tvRequestPrice, tvStatusManageOrders;
    CardView cardViewStatusManageOrders;

    public ManageOrdersViewHolder(@NonNull View itemView) {
        super(itemView);

        cardViewStatusManageOrders = itemView.findViewById(R.id.cardViewStatusManageOrders);
        ivUserImageManageOrders = itemView.findViewById(R.id.ivUserImageManageOrders);
        tvUserNameManageOrders = itemView.findViewById(R.id.tvUserNameManageOrders);
        tvSellerFiles = itemView.findViewById(R.id.tvSellerFiles);
        tvRequestPrice = itemView.findViewById(R.id.tvRequestPrice);
        tvStatusManageOrders = itemView.findViewById(R.id.tvStatusManageOrders);

    }
}