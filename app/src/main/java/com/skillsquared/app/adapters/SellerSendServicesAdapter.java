package com.skillsquared.app.adapters;

import android.app.Activity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.skillsquared.app.R;
import com.skillsquared.app.activities.SignUp;
import com.skillsquared.app.activities.seller.BuyerRequest;
import com.skillsquared.app.handler.ApplicationHandler;
import com.skillsquared.app.handler.RestApi;
import com.skillsquared.app.models.buyerRequestsSeller.Sellerservice;
import com.skillsquared.app.models.signUpModels.SignUpModel;
import com.skillsquared.app.models.signUpModels.SignUpUserModel;
import com.skillsquared.app.utils.Constants;
import com.skillsquared.app.utils.Loading;
import com.skillsquared.app.utils.SessionManager;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SellerSendServicesAdapter extends RecyclerView.Adapter<SellerSendServicesAdapter.ViewHolder> {

    private final static String TAG = SellerSendServicesAdapter.class.getSimpleName();
    Activity activity;
    List<Sellerservice> list;
    BottomSheetDialog bottomSheetDialog;
    View view;
    EditText etSellerServiceDesc, etPrice;
    AppCompatSpinner spinnerDeliveryTimeSellerService;
    CardView submitSellerOffer;
    String deliveryTimeVal = null, description, totalOffer;
    String serviceId;
    private SessionManager sessionManager;

    public SellerSendServicesAdapter(Activity activity, List<Sellerservice> list) {
        this.activity = activity;
        this.list = list;
    }

    @NonNull
    @Override
    public SellerSendServicesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_service_item, parent, false);
        initBottomSheet();
        sessionManager = new SessionManager(activity);
        return new SellerSendServicesAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull SellerSendServicesAdapter.ViewHolder holder, int position) {
        Sellerservice item = list.get(position);

        holder.tvUserServiceDecription.setText(item.getTitle());
        Picasso.get()
                .load(item.getServiceImage())
                .placeholder(R.drawable.logo)
                .into(holder.ivUserServiceImage);

        holder.cardViewSellerService.setOnClickListener(view ->
        {
            serviceId = null;
            serviceId = item.getId();
            createBottomSheetDialog();
            bottomSheetDialog.show();
        });

    }

    private void createBottomSheetDialog(){
        if (bottomSheetDialog == null){
            bottomSheetDialog = new BottomSheetDialog(activity);
            bottomSheetDialog.setContentView(view);
        }
    }

    private void getBottomSheetValues(){
        description = ApplicationHandler.stringConverter(etSellerServiceDesc);
        totalOffer = ApplicationHandler.stringConverter(etPrice);

        if (TextUtils.isEmpty(description)){
            etSellerServiceDesc.setError(activity.getResources().getString(R.string.editTextEmptyFieldError));
            etSellerServiceDesc.requestFocus();
            return;
        } else {
            etSellerServiceDesc.setError(null);
        }

        if (TextUtils.isEmpty(totalOffer)){
            etPrice.setError(activity.getResources().getString(R.string.editTextEmptyFieldError));
            etPrice.requestFocus();
            return;
        } else {
            etPrice.setError(null);
        }

        if (deliveryTimeVal == null){
            ApplicationHandler.toast(activity.getResources().getString(R.string.select_delivery_time));
            return;
        } else {
            Log.e(TAG, "description: "+description);
            Log.e(TAG, "total offer: "+totalOffer);
            Log.e(TAG, "delivery time value: "+deliveryTimeVal);
//            ApplicationHandler.toast("Offer Sent");

            sendOfferToServer();
        }

    }

    private void sendOfferToServer(){
        Loading.show(activity, false, Constants.k_PLEASE_WAIT);
        Call<SignUpModel> sendOffer = RestApi.getService().sendOffer(
                sessionManager.getString(Constants.k_ACCESSTOKEN),
                serviceId,
                Constants.k_BuyerRequestId,
                description,
                totalOffer,
                deliveryTimeVal
        );

        sendOffer.enqueue(new Callback<SignUpModel>() {
            @Override
            public void onResponse(@NonNull Call<SignUpModel> call,@NonNull Response<SignUpModel> response) {
                Loading.cancel();

                if (response.isSuccessful()){
                    SignUpModel sendOfferResponse = response.body();
                    if (sendOfferResponse!=null){
                        ApplicationHandler.toast(sendOfferResponse.getMessage());
                        bottomSheetDialog.cancel();
                        ApplicationHandler.intent(BuyerRequest.class);
                    }
                }

            }

            @Override
            public void onFailure(@NonNull Call<SignUpModel> call,@NonNull Throwable t) {
                Loading.cancel();
                ApplicationHandler.toast(t.getMessage());
                Log.e(TAG, "onFailure: "+t.getMessage());
            }
        });

    }

    private void initBottomSheet(){
        view = LayoutInflater.from(activity).inflate(R.layout.seller_service_detail_pop_up, null);

        etSellerServiceDesc = view.findViewById(R.id.etSellerServiceDesc);
        etPrice = view.findViewById(R.id.etPrice);
        spinnerDeliveryTimeSellerService = view.findViewById(R.id.spinnerDeliveryTimeSellerService);
        submitSellerOffer = view.findViewById(R.id.submitSellerOffer);
        setDeliveryTimeDropDown();
        submitSellerOffer.setOnClickListener(view->getBottomSheetValues());
    }

    private void setDeliveryTimeDropDown(){
        ArrayList<String> deliveryTime = new ArrayList<>();
        deliveryTime.add("Select Delivery Time");
        int j = 1;
        for (int i = 0; i <= 29; i++) {
            deliveryTime.add(j+"");
            j++;
        }

        arrayAdapter(deliveryTime, spinnerDeliveryTimeSellerService);

        spinnerDeliveryTimeSellerService.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (!spinnerDeliveryTimeSellerService.getSelectedItem().toString().equals("Select Delivery Time")){
                    deliveryTimeVal = spinnerDeliveryTimeSellerService.getSelectedItem().toString();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private void arrayAdapter(ArrayList<String> array, AppCompatSpinner spinner) {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(activity,
                android.R.layout.simple_spinner_item, array);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvUserServiceDecription;
        CircleImageView ivUserServiceImage;
        CardView cardViewSellerService;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvUserServiceDecription = itemView.findViewById(R.id.tvUserServiceDesc);
            ivUserServiceImage = itemView.findViewById(R.id.ivUserService);
            cardViewSellerService = itemView.findViewById(R.id.sellerServiceCardView);

        }
    }

}
