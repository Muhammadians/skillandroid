package com.skillsquared.app.adapters;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.skillsquared.app.R;

public class CompletedSalesItemViewHolder extends RecyclerView.ViewHolder {

    ImageView ivMainImageCompletedSales;
    TextView tvNameCompletedSales, tvDescriptionCompletedSales,
            tvDateCompletedSales, tvPriceCompletedSales;
    CardView cardViewStatusCompletedSales;

    public CompletedSalesItemViewHolder(@NonNull View itemView) {
        super(itemView);

        ivMainImageCompletedSales = itemView.findViewById(R.id.ivMainImageCompletedSales);
        tvNameCompletedSales = itemView.findViewById(R.id.tvNameCompletedSales);
        tvDescriptionCompletedSales = itemView.findViewById(R.id.tvDescriptionCompletedSales);
        tvDateCompletedSales = itemView.findViewById(R.id.tvDateCompletedSales);
        tvPriceCompletedSales = itemView.findViewById(R.id.tvPriceCompletedSales);
        cardViewStatusCompletedSales = itemView.findViewById(R.id.cardViewStatusCompletedSales);

    }
}