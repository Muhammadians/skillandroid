package com.skillsquared.app.adapters;

import android.media.Image;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.card.MaterialCardView;
import com.skillsquared.app.R;

public class PostedRequestListViewHolder extends RecyclerView.ViewHolder {

    TextView tvDate, tvStatus, tvDescriptionPostedRequest, tvDuration, tvOffers, tvBudgetList;
    MaterialCardView cardViewStatus, cardViewOffersPostedRequest;
    ImageView ivClosePostedRequest;

    public PostedRequestListViewHolder(@NonNull View itemView) {
        super(itemView);

        tvDate = itemView.findViewById(R.id.tvDate);
        tvStatus = itemView.findViewById(R.id.tvStatus);
        tvDescriptionPostedRequest = itemView.findViewById(R.id.tvDescriptionPostedRequest);
        tvDuration = itemView.findViewById(R.id.tvDuration);
        cardViewStatus = itemView.findViewById(R.id.cardViewStatus);
        cardViewOffersPostedRequest = itemView.findViewById(R.id.cardViewOffersPostedRequest);
        ivClosePostedRequest = itemView.findViewById(R.id.ivClosePostedRequest);
        tvOffers = itemView.findViewById(R.id.tvOffers);
        tvBudgetList = itemView.findViewById(R.id.tvBudgetList);

    }

}
