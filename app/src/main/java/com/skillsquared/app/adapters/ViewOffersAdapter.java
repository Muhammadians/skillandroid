package com.skillsquared.app.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.braintreepayments.api.BraintreeFragment;
import com.braintreepayments.api.PayPal;
import com.braintreepayments.api.dropin.DropInRequest;
import com.braintreepayments.api.exceptions.InvalidArgumentException;
import com.braintreepayments.api.models.PayPalRequest;
import com.cometchat.pro.core.CometChat;
import com.cometchat.pro.exceptions.CometChatException;
import com.cometchat.pro.models.User;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.skillsquared.app.R;
import com.skillsquared.app.activities.buyer.ViewOffers;
import com.skillsquared.app.cometChat.Activity.OneToOneChatActivity;
import com.skillsquared.app.cometChat.Contracts.StringContract;
import com.skillsquared.app.handler.ApplicationHandler;
import com.skillsquared.app.handler.RestApi;
import com.skillsquared.app.models.postAJob.PostAJobResponse;
import com.skillsquared.app.models.viewOffers.Asellerrequest;
import com.skillsquared.app.models.viewOffers.SellerInfoModel;
import com.skillsquared.app.utils.Constants;
import com.skillsquared.app.utils.Loading;
import com.skillsquared.app.utils.SessionManager;
import com.squareup.picasso.Picasso;

import java.util.List;

import co.paystack.android.Paystack;
import co.paystack.android.PaystackSdk;
import co.paystack.android.Transaction;
import co.paystack.android.model.Card;
import co.paystack.android.model.Charge;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewOffersAdapter extends RecyclerView.Adapter<ViewOfferViewHolder> {

    private final static String TAG = "View Offers Adapter";
    Activity activity;
    private BottomSheetDialog myDialog;
    List<Asellerrequest> list;
    private SessionManager sessionManager;
    private Dialog paymentDialog;
    private Dialog payStackDialog;
    private Dialog callDialog;
    private String requestId;
    View view;
    BraintreeFragment mBraintreeFragment;
    AppCompatActivity context;
    TextView tvUserNameViewOfferPopUp, tvFreelanceTitleViewOfferPopUp,
            tvUserRatingViewOfferPopUp, tvLocation, tvLanguages, tvDescriptionViewOfferPopUp;

    private Charge charge;
    private String UID;
    String cometChatUserName;

    private String cardNumber, cvv, expiryMonth, expiryYear;

    MyCallBack myCallBack;
    int orderCheck;

    public interface MyCallBack{
        void listenerMethod(String textViewvalue, String requestId);
    }

    public ViewOffersAdapter(Activity activity, List<Asellerrequest> list
            , String requestId, AppCompatActivity context, MyCallBack myCallBack, int OrderCheck) {
        this.activity = activity;
        this.list = list;
        this.requestId = requestId;
        this.context = context;
        this.myCallBack = myCallBack;
        this.orderCheck = OrderCheck;
    }

    @NonNull
    @Override
    public ViewOfferViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_offer_item, viewGroup, false);
        sessionManager = new SessionManager(activity);
        paymentDialog = new Dialog(activity);
        payStackDialog = new Dialog(activity);
        callDialog = new Dialog(activity);

        try {
            mBraintreeFragment = BraintreeFragment.newInstance(context, "sandbox_38tmdkrj_hrrcf2jbtw2kwwjb");
            // mBraintreeFragment is ready to use!
        } catch (InvalidArgumentException e) {
            Log.e(TAG, "onCreateViewHolder: " + e.getMessage());
        }

        return new ViewOfferViewHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewOfferViewHolder holder, int position) {
        final Asellerrequest item = list.get(position);

        holder.tvUserNameViewOffer.setText(item.getUsername());
        holder.tvDescriptionViewOffer.setText(item.getDescription());
        holder.tvOrder.setText("Order "+item.getPrice() + "$");

        holder.llShowPopup.setOnClickListener(view -> showPopUp(item.getFreelancerId()));

        holder.ivCloseViewOffer.setOnClickListener(view -> {
            Loading.show(activity, false, Constants.k_PLEASE_WAIT);
            Call<PostAJobResponse> call = RestApi.getService().removeSellerRequest(
                    sessionManager.getString(Constants.k_ACCESSTOKEN),
                    item.getRequestId()
            );

            call.enqueue(new Callback<PostAJobResponse>() {
                @Override
                public void onResponse(@NonNull Call<PostAJobResponse> call, @NonNull Response<PostAJobResponse> response) {
                    Loading.cancel();
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            ApplicationHandler.toast(response.body().getMessage());
                            ApplicationHandler.intent(ViewOffers.class, "request_id", requestId);
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<PostAJobResponse> call, @NonNull Throwable t) {
                    Loading.cancel();
                    ApplicationHandler.toast(t.getMessage());
                }
            });

        });

        if (orderCheck == 0){
            holder.cardViewOrderViewOffer.setOnClickListener(view ->{
                double price_temp = Double.parseDouble(item.getPrice());
                Constants.k_OfferPrice = String.valueOf(price_temp+3.00);
                Constants.k_RequestIdForServer = item.getRequestId();
                myCallBack.listenerMethod(Constants.k_OfferPrice, item.getRequestId());
                showDialog();
            });
        } else {
            holder.cardViewOrderViewOffer.setCardBackgroundColor(activity.getResources().getColor(R.color.colorGrey));
            holder.cardViewOrderViewOffer.setEnabled(false);
        }

        holder.cardViewAskQuestionViewOffer.setOnClickListener(view ->{
            UID = "11"+item.getUsername();
            cometChatUserName = item.getFreelancer_username();
            getCometChatUser();
        });

    }

    private void showCallDialog(String sellerId) {
        callDialog.setContentView(R.layout.call_dialog_item);
        callDialog.setCancelable(true);

        Window window = callDialog.getWindow();
        if (window != null) {
            window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            WindowManager.LayoutParams wlp = window.getAttributes();
            wlp.gravity = Gravity.CENTER;
            wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
            wlp.dimAmount = 0.3f;
            window.setAttributes(wlp);
        }
        callDialog.show();

        CardView cardViewAudioCall, cardViewVideoCall, cardViewChat;
//
        cardViewAudioCall = callDialog.findViewById(R.id.cardViewAudioCall);
        cardViewVideoCall = callDialog.findViewById(R.id.cardViewVideoCall);
        cardViewChat = callDialog.findViewById(R.id.cardViewChat);

        cardViewAudioCall.setOnClickListener(view -> {
            callDialog.cancel();
//            ApplicationHandler.intent(TwilioVoiceCall.class, "id", sellerId);
        });
        cardViewVideoCall.setOnClickListener(view -> {
            callDialog.cancel();
//            ApplicationHandler.intent(TwilioVideoCall.class, "id", sellerId);
        });
        cardViewChat.setOnClickListener(view -> ApplicationHandler.toast(activity.getResources().getString(R.string.in_progress)));

    }

    private void showPayStackDialog() {
        payStackDialog.setContentView(R.layout.paystack_layout);
        payStackDialog.setCancelable(true);
        paymentDialog.cancel();

        Window window = payStackDialog.getWindow();
        if (window != null) {
            window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            WindowManager.LayoutParams wlp = window.getAttributes();
            wlp.gravity = Gravity.CENTER;
            wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
            wlp.dimAmount = 0.3f;
            window.setAttributes(wlp);
        }

        final EditText etCardNumber = payStackDialog.findViewById(R.id.etCardNumber);
        final EditText etMonth = payStackDialog.findViewById(R.id.etMonth);
        final EditText etYear = payStackDialog.findViewById(R.id.etYear);
        final EditText etCVV = payStackDialog.findViewById(R.id.etCVV);
        CardView payStackDetailsSubmit = payStackDialog.findViewById(R.id.payStackDetailsSubmit);

        payStackDialog.show();

        payStackDetailsSubmit.setOnClickListener(view -> {
            cardNumber = ApplicationHandler.stringConverter(etCardNumber);
            expiryMonth = ApplicationHandler.stringConverter(etMonth);
            expiryYear = ApplicationHandler.stringConverter(etYear);
            cvv = ApplicationHandler.stringConverter(etCVV);

            if (TextUtils.isEmpty(cardNumber)) {
                etCardNumber.setError(activity.getResources().getString(R.string.editTextEmptyFieldError));
                etCardNumber.requestFocus();
                return;
            } else {
                etCardNumber.setError(null);
            }

            if (TextUtils.isEmpty(String.valueOf(expiryMonth))) {
                etMonth.setError(activity.getResources().getString(R.string.editTextEmptyFieldError));
                etMonth.requestFocus();
                return;
            } else {
                etMonth.setError(null);
            }

            if (TextUtils.isEmpty(String.valueOf(expiryYear))) {
                etYear.setError(activity.getResources().getString(R.string.editTextEmptyFieldError));
                etYear.requestFocus();
                return;
            } else {
                etYear.setError(null);
            }

            if (TextUtils.isEmpty(cvv)) {
                etCVV.setError(activity.getResources().getString(R.string.editTextEmptyFieldError));
                etCVV.requestFocus();
                return;
            } else {
                etCVV.setError(null);
            }

            createCard(cardNumber, Integer.parseInt(expiryMonth), Integer.parseInt(expiryYear), cvv);

        });

    }

    private void showDialog() {

        paymentDialog.setContentView(R.layout.payment_method_dialog);
        paymentDialog.setCancelable(true);

        Window window = paymentDialog.getWindow();
        if (window != null) {
            window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            WindowManager.LayoutParams wlp = window.getAttributes();
            wlp.gravity = Gravity.CENTER;
            wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
            wlp.dimAmount = 0.3f;
            window.setAttributes(wlp);
        }
        paymentDialog.show();

        CardView cardViewPaypal, cardViewPaystack;

        cardViewPaypal = paymentDialog.findViewById(R.id.cardViewPaypal);
        cardViewPaystack = paymentDialog.findViewById(R.id.cardViewPayStack);

        cardViewPaypal.setOnClickListener(view -> onBraintreeSubmit());

        cardViewPaystack.setOnClickListener(view -> {
            PaystackSdk.initialize(activity.getApplicationContext());
            showPayStackDialog();
        });

    }

    @SuppressLint("InflateParams")
    private void showPopUp(String id) {

        view = LayoutInflater.from(activity).inflate(R.layout.view_offer_pop_up, null);

        final CircleImageView ivUserImageViewOfferPopUp = view.findViewById(R.id.ivUserImageViewOfferPopUp);
        tvUserNameViewOfferPopUp = view.findViewById(R.id.tvUserNameViewOfferPopUp);
        tvFreelanceTitleViewOfferPopUp = view.findViewById(R.id.tvFreelanceTitleViewOfferPopUp);
        tvUserRatingViewOfferPopUp = view.findViewById(R.id.tvUserRatingViewOfferPopUp);
        tvLocation = view.findViewById(R.id.tvLocation);
        tvLanguages = view.findViewById(R.id.tvLanguages);
        tvDescriptionViewOfferPopUp = view.findViewById(R.id.tvDescriptionViewOfferPopUp);

        if (myDialog == null) {
            myDialog = new BottomSheetDialog(activity);
            myDialog.setContentView(view);
        }

        Loading.show(activity, false, Constants.k_PLEASE_WAIT);
        Call<SellerInfoModel> sellerInfoModelCall = RestApi.getService().getSellerInfo(
                sessionManager.getString(Constants.k_ACCESSTOKEN),
                id
        );

        sellerInfoModelCall.enqueue(new Callback<SellerInfoModel>() {
            @Override
            public void onResponse(@NonNull Call<SellerInfoModel> call, @NonNull Response<SellerInfoModel> response) {
                Loading.cancel();
                if (response.isSuccessful()) {

                    SellerInfoModel sellerInfoModel = response.body();

                    if (sellerInfoModel != null) {
                        tvUserNameViewOfferPopUp.setText(sellerInfoModel.getFreelancerInfo().getUsername());
                        tvFreelanceTitleViewOfferPopUp.setText(sellerInfoModel.getFreelancerInfo().getFreelancerTitle());
                        String des = Html.fromHtml(sellerInfoModel.getFreelancerInfo().getDescription()).toString();
                        tvDescriptionViewOfferPopUp.setText(des);
                        tvLanguages.setText(sellerInfoModel.getFreelancerInfo().getLanguage());
                        tvLocation.setText(sellerInfoModel.getFreelancerInfo().getLocation());
                        tvUserRatingViewOfferPopUp.setText(String.valueOf(sellerInfoModel.getFreelancerInfo().getAvgRating()));

                        Picasso.get()
                                .load(sellerInfoModel.getFreelancerInfo().getUserImage())
                                .placeholder(R.drawable.noimg)
                                .into(ivUserImageViewOfferPopUp);

                        myDialog.show();
                    }

                }

            }

            @Override
            public void onFailure(@NonNull Call<SellerInfoModel> call, @NonNull Throwable t) {
                Loading.cancel();
                ApplicationHandler.toast(t.getMessage());
            }
        });

    }

    public void startBillingAgreement() {
        PayPalRequest request = new PayPalRequest()
                .localeCode("US")
                .billingAgreementDescription("Your agreement description");

        Log.e("paypal", "yes");
        PayPal.requestOneTimePayment(mBraintreeFragment, request);
    }

    private void onBraintreeSubmit() {
        DropInRequest dropInRequest = new DropInRequest()
                .clientToken("sandbox_kttn24dq_63zmsxbxhgp8p9g9");
        activity.startActivityForResult(dropInRequest.getIntent(context), 111);
        paymentDialog.cancel();
    }

    private void createCard(String cardNumber, int month, int year, String cvc) {

        Loading.show(activity, false, Constants.k_PLEASE_WAIT);
        Card card = new Card(cardNumber, month, year, cvc);
        card.isValid();

        if (card.isValid()) {
//            Toast.makeText(activity, "Card is Valid", Toast.LENGTH_LONG).show();
            performCharge(card);
        } else {
            Loading.cancel();
            Toast.makeText(activity, "Card not Valid", Toast.LENGTH_LONG).show();
        }
    }

    private void performCharge(Card card) {
        //create a Charge object
        charge = new Charge();

        //set the card to charge
        charge.setCard(card);

        charge.setEmail("mytestemail@test.com"); //dummy email address

        charge.setAmount(100); //test amount

        PaystackSdk.chargeCard(activity, charge, new Paystack.TransactionCallback() {
            @Override
            public void onSuccess(Transaction transaction) {
                // This is called only after transaction is deemed successful.
                // Retrieve the transaction, and send its reference to your server
                // for verification.
                Loading.cancel();
                String paymentReference = transaction.getReference();
                Toast.makeText(activity, "Transaction Successful! payment reference: "
                        + paymentReference, Toast.LENGTH_LONG).show();
                payStackDialog.cancel();
            }

            @Override
            public void beforeValidate(Transaction transaction) {
                // This is called only before requesting OTP.
                // Save reference so you may send to server. If
                // error occurs with OTP, you should still verify on server.
                Loading.cancel();
                payStackDialog.cancel();
                Log.e(TAG, "beforeValidate: " + transaction.getReference());
            }

            @Override
            public void onError(Throwable error, Transaction transaction) {
                Loading.cancel();
                //handle error here
                Log.e(TAG, "onError: " + error.getMessage());
                Log.e(TAG, "onError: " + transaction.getReference());
            }
        });

    }

    private void getCometChatUser(){
        Loading.show(activity, false, Constants.k_PLEASE_WAIT);
        CometChat.getUser(UID, new CometChat.CallbackListener<User>() {
            @Override
            public void onSuccess(User user) {
                Loading.cancel();
                Log.d(TAG, "User details fetched for user: " + user.toString());

                String contactID = user.getUid();
                String contactName = user.getName();
                String userAvatar = user.getAvatar();

                Intent intent = new Intent(activity, OneToOneChatActivity.class);
                intent.putExtra(StringContract.IntentStrings.USER_ID, contactID);
                intent.putExtra(StringContract.IntentStrings.USER_AVATAR, userAvatar);
                intent.putExtra(StringContract.IntentStrings.USER_NAME, contactName);
                activity.startActivity(intent);
            }

            @Override
            public void onError(CometChatException e) {
                Loading.cancel();
                ApplicationHandler.cometChatCreateUser(UID, cometChatUserName, activity, "Inbox");
                Log.d(TAG, "User details fetching failed with exception: " + e.getMessage());
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

}
