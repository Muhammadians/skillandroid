package com.skillsquared.app.adapters;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.skillsquared.app.R;

public class ServicesAndGigsViewHolder extends RecyclerView.ViewHolder {

    ImageView ivProduct;
    TextView productName;
    RelativeLayout rlItem;

    public ServicesAndGigsViewHolder(@NonNull View itemView) {
        super(itemView);

        ivProduct = itemView.findViewById(R.id.ivService);
        productName = itemView.findViewById(R.id.tvServiceName);
        rlItem = itemView.findViewById(R.id.rlItem);

    }
}
