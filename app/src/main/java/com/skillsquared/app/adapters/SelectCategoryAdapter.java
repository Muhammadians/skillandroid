package com.skillsquared.app.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.skillsquared.app.R;
import com.skillsquared.app.models.AllChatsModel;

import java.util.ArrayList;

public class SelectCategoryAdapter extends RecyclerView.Adapter<SelectCategoryViewHolder>  {

    Activity activity;
    ArrayList<AllChatsModel> list;

    public SelectCategoryAdapter(Activity activity, ArrayList<AllChatsModel> list) {
        this.activity = activity;
        this.list = list;
    }

    @NonNull
    @Override
    public SelectCategoryViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.select_category_item, viewGroup, false);
        return new SelectCategoryViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull SelectCategoryViewHolder holder, int position) {
        AllChatsModel item = list.get(position);

        holder.ivSelectCategory.setImageResource(item.getImage());
        holder.tvHeadingSelectCategory.setText(item.getUserName());
        holder.tvSubHeadingSelectCategory.setText(item.getLastMessage());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

}