package com.skillsquared.app.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.skillsquared.app.R;
import com.skillsquared.app.models.manageOrders.Aactive;
import com.skillsquared.app.models.manageOrders.Arevision;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ManageServicesRevisionAdapter extends RecyclerView.Adapter<ManageSalesViewHolder> {

    Activity activity;
    List<Arevision> list;

    public ManageServicesRevisionAdapter(Activity activity, List<Arevision> list) {
        this.activity = activity;
        this.list = list;
    }

    @NonNull
    @Override
    public ManageSalesViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.manage_services_item, viewGroup, false);
        return new ManageSalesViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ManageSalesViewHolder holder, int position) {
        final Arevision item = list.get(position);

        Picasso.get()
                .load(item.getBuyerImage())
                .placeholder(R.drawable.noimg)
                .into(holder.ivUserNameManageServices);

        holder.tvDescriptionManageServices.setText(item.getSellerServiceInfo());
        holder.tvPriceManageService.setText(item.getPrice() + "$");
        holder.tvUserNameManageServices.setText(item.getBuyerName());
        holder.tvStatusManageService.setText(activity.getResources().getString(R.string.revision));
        holder.tvDateManageServices.setText(item.getDate());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

}
