package com.skillsquared.app.adapters;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.skillsquared.app.R;

public class CategoriesViewHolder extends RecyclerView.ViewHolder {

    ImageView ivCategory;
    TextView tvCategoryName;
    CardView cardViewGigsItem;

    public CategoriesViewHolder(@NonNull View itemView) {
        super(itemView);

        ivCategory = itemView.findViewById(R.id.ivCategory);
        tvCategoryName = itemView.findViewById(R.id.tvCategoryName);
        cardViewGigsItem = itemView.findViewById(R.id.cardViewGigsItem);

    }
}