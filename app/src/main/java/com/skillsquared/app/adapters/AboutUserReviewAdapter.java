package com.skillsquared.app.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.skillsquared.app.R;
import com.skillsquared.app.models.sellerProfileModel.SellerReview;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AboutUserReviewAdapter extends RecyclerView.Adapter<AboutUserReviewViewHolder> {

    Activity activity;
    List<SellerReview> list;

    public AboutUserReviewAdapter(Activity activity, List<SellerReview> list) {
        this.activity = activity;
        this.list = list;
    }

    @NonNull
    @Override
    public AboutUserReviewViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.about_user_review_item, viewGroup, false);
        return new AboutUserReviewViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull AboutUserReviewViewHolder holder, int position) {
        SellerReview item = list.get(position);

        Picasso.get()
                .load(item.getBuyerImage())
                .placeholder(R.drawable.noimg)
                .into(holder.ivReview);

        holder.tvDescReview.setText(item.getReview());
        holder.tvReviewRating.setText(item.getRating());
        holder.tvNameReview.setText(item.getBuyerName());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

}

