package com.skillsquared.app.adapters;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.skillsquared.app.R;

public class AllChatsViewHolder extends RecyclerView.ViewHolder {

    ImageView ivProfileImageChat;
    TextView tvUserNameChats, tvUserLastMsgChats, tvLastMessageMinutes;

    public AllChatsViewHolder(@NonNull View itemView) {
        super(itemView);

        ivProfileImageChat = itemView.findViewById(R.id.ivProfileImageChat);
        tvUserNameChats = itemView.findViewById(R.id.tvUserNameChats);
        tvUserLastMsgChats = itemView.findViewById(R.id.tvUserLastMsgChats);
        tvLastMessageMinutes = itemView.findViewById(R.id.tvLastMessageMinutes);

    }
}
