package com.skillsquared.app.adapters;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.skillsquared.app.R;
import com.skillsquared.app.activities.seller.sendOffer.SellerServices;
import com.skillsquared.app.handler.ApplicationHandler;
import com.skillsquared.app.models.buyerRequestsSeller.BuyerRequestSeller;
import com.skillsquared.app.utils.Constants;
import com.squareup.picasso.Picasso;
import java.util.Arrays;
import java.util.List;

public class BuyerRequestAdapter extends RecyclerView.Adapter<BuyerRequestViewHolder> {

    Activity activity;
    List<BuyerRequestSeller> list;
    TextView tvPageNumber;
    RadioButton radioButton;
    private final static String TAG = BuyerRequestAdapter.class.getSimpleName();

    public BuyerRequestAdapter(Activity activity, List<BuyerRequestSeller> list, TextView tvPageNumber, RadioButton radioButton) {
        this.activity = activity;
        this.list = list;
        this.tvPageNumber = tvPageNumber;
        this.radioButton = radioButton;
        this.radioButton.setChecked(true);
    }

    @NonNull
    @Override
    public BuyerRequestViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.buyer_request_item, viewGroup, false);
        return new BuyerRequestViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull BuyerRequestViewHolder holder, int position) {
        BuyerRequestSeller item = list.get(position);

        Picasso.get()
                .load(item.getUserImage())
                .placeholder(R.drawable.noimg)
                .into(holder.ivUserImageBuyerRequest);

        holder.tvUserNameBuyerRequest.setText(item.getUserName());

        String dummy = item.getCreatedDate();
        List<String> wordList = Arrays.asList(dummy.split(" "));

        holder.tvDateBuyerRequest.setText(wordList.get(0));
        holder.tvSentOffers.setText(item.getCategory() + " / " + item.getOfferSent());
        holder.tvDurationBuyerRequest.setText(item.getDelievry());
        holder.tvBudgetBuyerRequest.setText(item.getBudget()+"$");
        holder.tvDescriptionBuyerRequest.setText(item.getDescription());

        if (item.getApplideOrNot().equals("1")) {
            holder.sendOfferCardView.setCardBackgroundColor(activity.getResources().getColor(R.color.colorGrey));
            holder.sendOfferCardView.setEnabled(false);
        } else {
            holder.sendOfferCardView.setCardBackgroundColor(activity.getResources().getColor(R.color.colorPrimary));
            holder.sendOfferCardView.setOnClickListener(view -> ApplicationHandler.toast("Yes"));
        }

        holder.sendOfferCardView.setOnClickListener(view -> {

            if (holder.sendOfferCardView.isEnabled()){
                Constants.k_BuyerRequestId = null;
                Constants.k_BuyerRequestId = item.getId();
                ApplicationHandler.intent(SellerServices.class);
            } else {
                Log.e(TAG, "onBindViewHolder: not clickable");
            }

        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

}
