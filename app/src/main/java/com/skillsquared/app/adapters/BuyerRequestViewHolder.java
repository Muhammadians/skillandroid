package com.skillsquared.app.adapters;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.skillsquared.app.R;

public class BuyerRequestViewHolder extends RecyclerView.ViewHolder {

    TextView tvUserNameBuyerRequest, tvDateBuyerRequest, tvDescriptionBuyerRequest,
            tvSentOffers, tvDurationBuyerRequest, tvBudgetBuyerRequest;

    ImageView ivUserImageBuyerRequest;

    CardView sendOfferCardView;

    public BuyerRequestViewHolder(@NonNull View itemView) {
        super(itemView);

        tvUserNameBuyerRequest = itemView.findViewById(R.id.tvUserNameBuyerRequest);
        tvDateBuyerRequest = itemView.findViewById(R.id.tvDateBuyerRequest);
        tvDescriptionBuyerRequest = itemView.findViewById(R.id.tvDescriptionBuyerRequest);
        tvSentOffers = itemView.findViewById(R.id.tvSentOffers);
        tvDurationBuyerRequest = itemView.findViewById(R.id.tvDurationBuyerRequest);
        tvBudgetBuyerRequest = itemView.findViewById(R.id.tvBudgetBuyerRequest);
        ivUserImageBuyerRequest = itemView.findViewById(R.id.ivUserImageBuyerRequest);
        sendOfferCardView = itemView.findViewById(R.id.sendOfferCardView);

    }
}