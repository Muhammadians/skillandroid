package com.skillsquared.app.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.skillsquared.app.R;
import com.skillsquared.app.activities.CategoriesGridActivity;
import com.skillsquared.app.handler.ApplicationHandler;
import com.skillsquared.app.models.modelsOnHome.PopularCategoriesModel;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

public class SkillsAdapter extends RecyclerView.Adapter<SkillsAdapter.SkillAdapterViewHolder>  {

    Activity activity;
    List<String> list;

    public SkillsAdapter(Activity activity, List<String> list)
    {
        this.activity = activity;
        this.list = list;
    }

    @NonNull
    @Override
    public SkillsAdapter.SkillAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.skill_layout_item, viewGroup, false);
        return new SkillsAdapter.SkillAdapterViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull SkillsAdapter.SkillAdapterViewHolder holder, int position) {
        String item = list.get(position);
        holder.tvSkillUser.setText(item);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class SkillAdapterViewHolder extends RecyclerView.ViewHolder {

        TextView tvSkillUser;

        SkillAdapterViewHolder(@NonNull View itemView) {
            super(itemView);

            tvSkillUser = itemView.findViewById(R.id.tvSkillUser);

        }
    }

}