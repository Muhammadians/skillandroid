package com.skillsquared.app.adapters;

import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.skillsquared.app.R;

public class SelectCategoryViewHolder extends RecyclerView.ViewHolder {

    ImageView ivSelectCategory;
    TextView tvHeadingSelectCategory, tvSubHeadingSelectCategory;

    public SelectCategoryViewHolder(@NonNull View itemView) {
        super(itemView);

        ivSelectCategory = itemView.findViewById(R.id.ivSelectCategory);
        tvHeadingSelectCategory = itemView.findViewById(R.id.tvHeadingSelectCategory);
        tvSubHeadingSelectCategory = itemView.findViewById(R.id.tvSubHeadingSelectCategory);

    }
}