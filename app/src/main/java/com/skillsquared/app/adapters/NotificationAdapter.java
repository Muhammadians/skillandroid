package com.skillsquared.app.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.skillsquared.app.R;
import com.skillsquared.app.models.NotificationObject;

import java.util.ArrayList;
import java.util.List;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.NotificationViewHolder> {

    Activity activity;
    List<NotificationObject> list;

    public NotificationAdapter(Activity activity, List<NotificationObject> list) {
        this.activity = activity;
        this.list = list;
    }

    @NonNull
    @Override
    public NotificationAdapter.NotificationViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.notifications_item, viewGroup, false);
        return new NotificationAdapter.NotificationViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationAdapter.NotificationViewHolder holder, int position) {
        final NotificationObject item = list.get(position);

        holder.tvNotification.setText(item.getBody());
        holder.tvNotificationDate.setText(item.getCreatedOn());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class NotificationViewHolder extends RecyclerView.ViewHolder {

        TextView tvNotification, tvNotificationDate;

        public NotificationViewHolder(@NonNull View itemView) {
            super(itemView);

            tvNotification = itemView.findViewById(R.id.tvNotification);
            tvNotificationDate = itemView.findViewById(R.id.tvNotificationDate);

        }

    }

}
