package com.skillsquared.app;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;
import android.os.StrictMode;
import android.util.Log;
import android.widget.Toast;

import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

import com.cometchat.pro.core.AppSettings;
import com.cometchat.pro.core.CometChat;
import com.cometchat.pro.exceptions.CometChatException;
import com.google.firebase.iid.FirebaseInstanceId;
import com.skillsquared.app.cometChat.Contracts.StringContract;

import java.util.Objects;

import timber.log.Timber;

public class SkillSquared extends MultiDexApplication {

    private static SkillSquared _instance;

    private static final String TAG = SkillSquared.class.getSimpleName();
    public static final String CHANNEL_ID = "skill_squared";
    private static final String CHANNEL_NAME = "Skill Squared";
    private static final String CHANNEL_DESC = "SkillSquared Notifications";


    public static SkillSquared getAppContext() {
        return _instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        _instance = this;
        MultiDex.install(this);

        createNotificationManager();

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
//        Logger.enableLogs("4ddd5d736cf33ca31a0b4c72ae64b6d5");
        AppSettings appSettings = new AppSettings.AppSettingsBuilder().subscribePresenceForAllUsers().setRegion(StringContract.AppDetails.REGION).build();
        CometChat.init(this, StringContract.AppDetails.APP_ID, appSettings, new CometChat.CallbackListener<String>() {
            @Override
            public void onSuccess(String s) {
//                Toast.makeText(SkillSquared.this, "SetUp Complete", Toast.LENGTH_SHORT).show();
                Log.e(TAG, "onSuccess: SetUp Complete");
            }

            @Override
            public void onError(CometChatException e) {
                Toast.makeText(SkillSquared.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                Timber.d("onError: %s", e.getMessage());
                Log.e(TAG, "onError: " + e.getMessage());
            }

        });

    }

    private void createNotificationManager() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription(CHANNEL_DESC);
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channel);
        }

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        String token = Objects.requireNonNull(task.getResult()).getToken();
                        Log.e(TAG, "createNotificationManager: " + token);
                    } else {
                        Log.e(TAG, "createNotificationManager: " + task.getException().getMessage());
                    }
                });

    }

}
