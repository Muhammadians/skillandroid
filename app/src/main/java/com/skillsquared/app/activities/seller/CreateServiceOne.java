package com.skillsquared.app.activities.seller;

import android.content.Intent;
import android.graphics.Rect;
import android.inputmethodservice.KeyboardView;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.cardview.widget.CardView;

import com.skillsquared.app.R;
import com.skillsquared.app.handler.ApplicationHandler;
import com.skillsquared.app.handler.RestApi;
import com.skillsquared.app.models.postAJob.GetCategoriesModelForPostAJob;
import com.skillsquared.app.models.postAJob.SubCatChildModelForPostAJob;
import com.skillsquared.app.models.postAJob.SubCatModelForPostAJob;
import com.skillsquared.app.models.updateService.UpdateServiceResponse;
import com.skillsquared.app.utils.Constants;
import com.skillsquared.app.utils.Loading;
import com.skillsquared.app.utils.SessionManager;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateServiceOne extends AppCompatActivity {

    private final static String TAG = CreateServiceOne.class.getSimpleName();
    EditText etServiceTitle, etAdditionalInfo;
    AppCompatSpinner spinnerCategories, spinnerCategoryCreateService,
            spinnerSubCategoryCreateService, spinnerSubCategoryChildCreateService;
    LinearLayout llSubCatChildLayoutCreateService, llSubCatLayoutCreateService;
    CardView submitCreateServiceOne;
    RelativeLayout rlCreateServiceOne;
    String cat_id = null;
    String cat_level = null;
    List<GetCategoriesModelForPostAJob> getCategoriesModelForCreateService = null;
    String activityIntent = null;
    SessionManager sessionManager;
    UpdateServiceResponse updateServiceResponse;

    private void init()
    {
        etServiceTitle = findViewById(R.id.etServiceTitle);
        etAdditionalInfo = findViewById(R.id.etAdditionalInfo);
//        spinnerCategories = findViewById(R.id.spinnerCategories);
        submitCreateServiceOne = findViewById(R.id.submitCreateServiceOne);
        spinnerCategoryCreateService = findViewById(R.id.spinnerCategoryCreateService);
        spinnerSubCategoryCreateService = findViewById(R.id.spinnerSubCategoryCreateService);
        spinnerSubCategoryChildCreateService = findViewById(R.id.spinnerSubCategoryChildCreateService);
        llSubCatChildLayoutCreateService = findViewById(R.id.llSubCatChildLayoutCreateService);
        llSubCatLayoutCreateService = findViewById(R.id.llSubCatLayoutCreateService);
        rlCreateServiceOne = findViewById(R.id.rlCreateServiceOne);
        sessionManager = new SessionManager(this);

        activityIntent = getIntent().getStringExtra("editService");

        if (activityIntent != null){
            getServiceDataToEdit();
        } else {
            getCatOptions();
        }

        submitCreateServiceOne.setOnClickListener(view -> checkValues());

    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_service_one);

        init();

        findViewById(R.id.ivBackCreateServiceOne).setOnClickListener(view -> onBackPressed());

    }

    private void getServiceDataToEdit(){
        Loading.show(this, false, Constants.k_PLEASE_WAIT);
        Call<UpdateServiceResponse> getDataForEditService =
                RestApi.getService().updateService(
                        sessionManager.getString(Constants.k_ACCESSTOKEN),
                        activityIntent
                );

        getDataForEditService.enqueue(new Callback<UpdateServiceResponse>() {
            @Override
            public void onResponse(@NotNull Call<UpdateServiceResponse> call, @NotNull Response<UpdateServiceResponse> response) {
                Loading.cancel();
                if (response.isSuccessful()){
                    updateServiceResponse = response.body();

                    if (updateServiceResponse != null){
                        etServiceTitle.setText(updateServiceResponse.getServiceDetail().getTitle());
                        etAdditionalInfo.setText(updateServiceResponse.getServiceDetail().getDescription());
                        if (updateServiceResponse.getCatoptions()!= null) {
                            setListInSpinner(updateServiceResponse.getCatoptions());
                        }
                    }

                }
            }

            @Override
            public void onFailure(@NotNull Call<UpdateServiceResponse> call, @NotNull Throwable t) {
                Loading.cancel();
                Log.e(TAG, "onFailure: "+t.getMessage());
                ApplicationHandler.toast(t.getMessage());
            }
        });
    }

    void getCatOptions()
    {
        Loading.show(this, false, Constants.k_PLEASE_WAIT);
        Call<List<GetCategoriesModelForPostAJob>> getCategoriesModelForPostAJobCall =
                RestApi.getService().getCatForPostJob();

        getCategoriesModelForPostAJobCall.enqueue(new Callback<List<GetCategoriesModelForPostAJob>>() {

            @Override
            public void onResponse(@NonNull Call<List<GetCategoriesModelForPostAJob>> call, @NonNull Response<List<GetCategoriesModelForPostAJob>> response) {
                Loading.cancel();

                if (response.isSuccessful()) {
                    getCategoriesModelForCreateService = response.body();
                    if (getCategoriesModelForCreateService != null) {
                        setListInSpinner(getCategoriesModelForCreateService);
                    }
                }

            }

            @Override
            public void onFailure(@NonNull Call<List<GetCategoriesModelForPostAJob>> call, @NonNull Throwable t) {
                Loading.cancel();
                Log.e(TAG, "onFailure: " + t.getMessage());
                ApplicationHandler.toast(t.getMessage());
            }

        });

    }

    void setListInSpinner(final List<GetCategoriesModelForPostAJob> list)
    {
        ArrayList<String> categoriesList = new ArrayList<>();
        categoriesList.add("Select Category");
        for (int i = 0; i < list.size(); i++) {
            categoriesList.add(list.get(i).getTitle());
        }
        arrayAdapter(categoriesList, spinnerCategoryCreateService);

        if (activityIntent!=null){
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getCatId().equals(updateServiceResponse.getCategoryId())){
                    spinnerCategoryCreateService.setSelection(i+1);
                }
            }
        }

        spinnerCategoryCreateService.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                List<SubCatModelForPostAJob> subList;
                llSubCatLayoutCreateService.setVisibility(View.GONE);
                llSubCatChildLayoutCreateService.setVisibility(View.GONE);
                cat_id = null;
                for (int j = 0; j < list.size(); j++) {
                    if (list.get(j).getTitle().equals(spinnerCategoryCreateService.getSelectedItem().toString())) {
                        if (list.get(j).getSubcat() != null) {
                            subList = list.get(j).getSubcat();
                            setSubList(subList);
                        } else {
                            llSubCatLayoutCreateService.setVisibility(View.GONE);
                            llSubCatChildLayoutCreateService.setVisibility(View.GONE);
                            cat_id = list.get(j).getCatId();
                            cat_level = "1";
                            Log.e(TAG, "onItemSelected: " + cat_id);
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private void setSubList(final List<SubCatModelForPostAJob> list)
    {
        ArrayList<String> subCategoryList = new ArrayList<>();
        subCategoryList.add("Select Subcategory");

        if (list != null) {
            for (int i = 0; i < list.size(); i++) {
                subCategoryList.add(list.get(i).getTitle());
            }
            llSubCatLayoutCreateService.setVisibility(View.VISIBLE);
            arrayAdapter(subCategoryList, spinnerSubCategoryCreateService);

            if (activityIntent!=null){
                for (int i = 0; i < list.size(); i++) {
                    if (list.get(i).getId().equals(updateServiceResponse.getSubcategoryId())){
                        spinnerSubCategoryCreateService.setSelection(i+1);
                    }
                }
            }

            spinnerSubCategoryCreateService.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    cat_id = null;
                    List<SubCatChildModelForPostAJob> subList;
                    for (int j = 0; j < list.size(); j++) {
                        if (list.get(j).getTitle().equals(spinnerSubCategoryCreateService.getSelectedItem().toString())) {
                            if (list.get(j).getSubcatChild() != null) {
                                subList = list.get(j).getSubcatChild();
                                setSubCatChildList(subList);
                            } else {
                                llSubCatChildLayoutCreateService.setVisibility(View.GONE);
                                cat_id = list.get(j).getId();
                                cat_level = "2";
                                Log.e(TAG, "onItemSelected: " + cat_id);
                            }
                        }
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

        }

    }

    private void setSubCatChildList(final List<SubCatChildModelForPostAJob> list)
    {
        ArrayList<String> subCategoryList = new ArrayList<>();
        subCategoryList.add("Select Subcategory Child");
        if (list != null) {
            for (int i = 0; i < list.size(); i++) {
                subCategoryList.add(list.get(i).getTitle());
            }
            llSubCatChildLayoutCreateService.setVisibility(View.VISIBLE);
            arrayAdapter(subCategoryList, spinnerSubCategoryChildCreateService);

            if (activityIntent!=null){
                for (int i = 0; i < list.size(); i++) {
                    if (list.get(i).getId().equals(updateServiceResponse.getSubcategorychildId())){
                        spinnerSubCategoryChildCreateService.setSelection(i+1);
                    }
                }
            }

        }

        spinnerSubCategoryChildCreateService.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                cat_id = null;
                if (list != null) {
                    for (int j = 0; j < list.size(); j++) {
                        if (list.get(j).getTitle().equals(spinnerSubCategoryChildCreateService.getSelectedItem().toString())) {
                            cat_id = list.get(j).getId();
                            cat_level = "3";
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private void arrayAdapter(ArrayList<String> array, AppCompatSpinner spinner)
    {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, array);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    private void checkValues()
    {
        String title = ApplicationHandler.stringConverter(etServiceTitle);
        String description = ApplicationHandler.stringConverter(etAdditionalInfo);

        if (TextUtils.isEmpty(title)) {
            etServiceTitle.setError(getResources().getString(R.string.editTextEmptyFieldError));
            return;
        } else {
            etServiceTitle.setError(null);
        }

        if (cat_id == null) {
            ApplicationHandler.toast("Please select a category...");
            return;
        }

        if (TextUtils.isEmpty(description)) {
            etAdditionalInfo.setError(getResources().getString(R.string.editTextEmptyFieldError));
            return;
        } else {
            etAdditionalInfo.setError(null);
        }

        if (activityIntent!=null){
            Intent intent = new Intent(CreateServiceOne.this, CreateServiceActivity.class);
            intent.putExtra("path", "update");
            intent.putExtra("title", title);
            intent.putExtra("description", description);
            intent.putExtra("category", cat_id);
            intent.putExtra("cat_level", cat_level);
            intent.putExtra("price", updateServiceResponse.getServiceDetail().getPrice());
            intent.putExtra("deliveryTime", updateServiceResponse.getServiceDetail().getDeliveryTime());
            intent.putExtra("additionalInfo", updateServiceResponse.getServiceDetail().getAdditionalInfo());
            intent.putExtra("media", updateServiceResponse.getServiceDetail().getMedia());
            intent.putExtra("serviceId", activityIntent);
            startActivity(intent);
        } else {
            Intent intent = new Intent(CreateServiceOne.this, CreateServiceActivity.class);
            intent.putExtra("path", "new");
            intent.putExtra("title", title);
            intent.putExtra("description", description);
            intent.putExtra("category", cat_id);
            intent.putExtra("cat_level", cat_level);
            startActivity(intent);
        }

    }

    @Override
    public void onBackPressed()
    {
        ApplicationHandler.intent(WhatToDoActivitySeller.class);
    }
}
