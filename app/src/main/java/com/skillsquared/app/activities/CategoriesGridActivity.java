package com.skillsquared.app.activities;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.skillsquared.app.R;
import com.skillsquared.app.adapters.CategoriesAdapter;
import com.skillsquared.app.handler.ApplicationHandler;
import com.skillsquared.app.handler.RestApi;
import com.skillsquared.app.models.CategoriesModel;
import com.skillsquared.app.models.modelsOnHome.PopularCategoriesModel;
import com.skillsquared.app.utils.Constants;
import com.skillsquared.app.utils.Loading;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoriesGridActivity extends AppCompatActivity {

    private final static String TAG = "CategoriesGrid";
    Spinner spinnerPrice;
    RecyclerView recyclerViewCategoriesGrid;
    String cat_id;
    List<PopularCategoriesModel> mList;
    List<PopularCategoriesModel> mListFiltered;
    CategoriesAdapter categoriesAdapter;
    EditText etSearchCategories;

    private void init() {
        spinnerPrice = findViewById(R.id.priceSpinner);
        etSearchCategories = findViewById(R.id.etSearchCategories);

        recyclerViewCategoriesGrid = findViewById(R.id.recyclerViewCategoriesGrid);
        recyclerViewCategoriesGrid.setHasFixedSize(true);

        cat_id = getIntent().getStringExtra("cat_id");

        setRecyclerAdapter();

        etSearchCategories.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                filter(editable.toString());
            }
        });

    }

    private void setRecyclerAdapter() {
        makeServerCall();
    }

    private void makeServerCall(){
        Loading.show(this, false, Constants.k_PLEASE_WAIT);

        Call<CategoriesModel> getSubCategories = RestApi.getService().getSubCategories(cat_id);
        getSubCategories.enqueue(new Callback<CategoriesModel>() {
            @Override
            public void onResponse(@NonNull Call<CategoriesModel> call,@NonNull Response<CategoriesModel> response) {
                Loading.cancel();
                if (response.isSuccessful()){

                    if (response.body() != null) {
                        mList = response.body().getPopularServices();
                        recyclerViewCategoriesGrid.setLayoutManager(new GridLayoutManager(getApplicationContext(), 2));
                        categoriesAdapter = new CategoriesAdapter(CategoriesGridActivity.this, mList);
                        recyclerViewCategoriesGrid.setAdapter(categoriesAdapter);
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<CategoriesModel> call,@NonNull Throwable t) {
                Loading.cancel();
                Log.e(TAG, "onFailure: "+t.getMessage());
                ApplicationHandler.toast(t.getMessage());
            }
        });

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);

        init();

        findViewById(R.id.ivBackGrid).setOnClickListener(view -> onBackPressed());

    }

    @Override
    public void onBackPressed() {
        ApplicationHandler.intent(HomeActivity.class);
    }

    private void filter(String text) {
        mListFiltered = new ArrayList<>();
        for (PopularCategoriesModel venue : mList) {
            if (venue.getTitle().toLowerCase().contains(text.toLowerCase())) {
                mListFiltered.add(venue);
            }
        }
        categoriesAdapter.filterGridCategories(mListFiltered);
    }
}
