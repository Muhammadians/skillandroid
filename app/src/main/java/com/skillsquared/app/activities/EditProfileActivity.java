package com.skillsquared.app.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.skillsquared.app.R;
import com.skillsquared.app.activities.settings.SettingsActivity;
import com.skillsquared.app.handler.ApplicationHandler;
import com.skillsquared.app.handler.FileUtils;
import com.skillsquared.app.handler.RestApi;
import com.skillsquared.app.models.getUserProfile.GetProfileResponse;
import com.skillsquared.app.models.updateUserProfile.UpdateUserProfileResponse;
import com.skillsquared.app.utils.Constants;
import com.skillsquared.app.utils.Loading;
import com.skillsquared.app.utils.SessionManager;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfileActivity extends AppCompatActivity {

    private final static String TAG = EditProfileActivity.class.getSimpleName();
    EditText etUserNameEditProfile, etPhoneEditProfile, etCnicEditProfile, etAddressEditProfile;
    CircleImageView ivUserImageEditProfile;
    Button btnSubmitEditProfile;
    SessionManager sessionManager;
    BottomSheetDialog bottomSheetDialog;
    View view;
    Bitmap bitmap;
    File imageFIle;
    ImageView ivCamera, ivGallery;
    public static final int CAMERA_REQUEST_CODE = 228;
    public static final int GALLERY_REQUEST_CODE = 1128;
    Uri pictureUri;
    String pathPartial, imageFilePath;

    private void init(){
        etUserNameEditProfile = findViewById(R.id.etUserNameEditProfile);
        etPhoneEditProfile = findViewById(R.id.etPhoneEditProfile);
        etCnicEditProfile = findViewById(R.id.etCnicEditProfile);
        ivUserImageEditProfile = findViewById(R.id.ivUserImageEditProfile);
        btnSubmitEditProfile = findViewById(R.id.btnSubmitEditProfile);
        etAddressEditProfile = findViewById(R.id.etAddressEditProfile);
        sessionManager = new SessionManager(this);

        getUserProfile();

        initBottomSheet();
        createBottomSheetDialog();

        ivUserImageEditProfile.setOnClickListener(view-> bottomSheetDialog.show());

        ivCamera.setOnClickListener(view-> requestPermissionsFromUser(1));

        ivGallery.setOnClickListener(view-> requestPermissionsFromUser(2));

    }

    private void requestPermissionsFromUser(int i)
    {
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.CAMERA,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            if (i == 1){
                                invokeCamera();
                            } else {
                                pickImageForGallery();
                            }
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            ApplicationHandler.showSettingsDialog(EditProfileActivity.this);
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(error -> Toast.makeText(getApplicationContext(), "Error occurred! ", Toast.LENGTH_SHORT).show())
                .onSameThread()
                .check();
    }

    private void pickImageForGallery() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, GALLERY_REQUEST_CODE);
    }

    private void initBottomSheet()
    {
        view = LayoutInflater.from(this).inflate(R.layout.camera_gallery_selector, null);

        ivGallery = view.findViewById(R.id.ivGallery);
        ivCamera = view.findViewById(R.id.ivCamera);
    }

    private void createBottomSheetDialog(){
        if (bottomSheetDialog == null){
            bottomSheetDialog = new BottomSheetDialog(this);
            bottomSheetDialog.setContentView(view);
        }
    }

    private void invokeCamera() {

        // get a file reference
        pictureUri = FileProvider.getUriForFile(this, getApplicationContext().getPackageName() + ".provider", createImageFile());
        //String pictureUri = Environment.getExternalStorageDirectory()+ File.separator + "image.jpg";
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        pathPartial = pictureUri.getPath();
        // tell the camera where to save the image.
        intent.putExtra(MediaStore.EXTRA_OUTPUT, pictureUri);
        // tell the camera to request WRITE permission.
        intent.setFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

        startActivityForResult(intent, CAMERA_REQUEST_CODE);

    }

    private File createImageFile() {
        // timestamp makes unique name.
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
        String timestamp = sdf.format(new Date());
        String imageFileName = "IMG_" + timestamp + "_";
        File storageDir =
                getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = null;
        try {
            image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );
//            imageFilePath = FileUtils.getPath(this, pictureUri);
            imageFilePath = image.getAbsolutePath();
        } catch (IOException e) {
            e.printStackTrace();
        }


        return image;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ApplicationHandler.hideStatusBar(this);
        setContentView(R.layout.activity_edit_profile);

        init();

        findViewById(R.id.ivBackEditProfile).setOnClickListener(view->onBackPressed());

        btnSubmitEditProfile.setOnClickListener(view -> checkValues());

    }

    private void getUserProfile(){
        Loading.show(this, false, Constants.k_PLEASE_WAIT);
        Call<GetProfileResponse> getProfileResponseCall = RestApi.getService().getUserDetails(
                sessionManager.getString(Constants.k_ACCESSTOKEN)
        );

        getProfileResponseCall.enqueue(new Callback<GetProfileResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetProfileResponse> call,@NonNull Response<GetProfileResponse> response) {
                Loading.cancel();
                if (response.isSuccessful()){
                    GetProfileResponse getProfileResponse = response.body();

                    if (getProfileResponse!=null){
                        if (getProfileResponse.getStatus()==200){
                            etUserNameEditProfile.setText(getProfileResponse.getUerprofile().get(0).getName());
                            etPhoneEditProfile.setText(getProfileResponse.getUerprofile().get(0).getPhone());
                            etAddressEditProfile.setText(getProfileResponse.getUerprofile().get(0).getAddress());
                            Picasso.get()
                                    .load(getProfileResponse.getUerprofile().get(0).getProfilePic())
                                    .placeholder(R.drawable.noimg)
                                    .into(ivUserImageEditProfile);
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<GetProfileResponse> call,@NonNull Throwable t) {
                Loading.cancel();
                Log.e(TAG, "onFailure: "+t.getMessage());
                ApplicationHandler.toast(t.getMessage());
            }
        });

    }

    private void checkValues(){
        String name = ApplicationHandler.stringConverter(etUserNameEditProfile);
        String phone = ApplicationHandler.stringConverter(etPhoneEditProfile);
        String address = ApplicationHandler.stringConverter(etAddressEditProfile);

        if (TextUtils.isEmpty(name)){
            etUserNameEditProfile.setError(getResources().getString(R.string.editTextEmptyFieldError));
            etUserNameEditProfile.requestFocus();
            return;
        } else {
            etUserNameEditProfile.setError(null);
        }

        if (TextUtils.isEmpty(phone)){
            etPhoneEditProfile.setError(getResources().getString(R.string.editTextEmptyFieldError));
            etPhoneEditProfile.requestFocus();
            return;
        } else {
            etPhoneEditProfile.setError(null);
        }

        if (TextUtils.isEmpty(address)){
            etAddressEditProfile.setError(getResources().getString(R.string.editTextEmptyFieldError));
            etAddressEditProfile.requestFocus();
            return;
        } else {
            etAddressEditProfile.setError(null);
        }

        editUserProfile(name, phone, address);





    }

    private void editUserProfile(String name, String phone, String address) {
        Loading.show(this, false, Constants.k_PLEASE_WAIT);

        RequestBody requestBody;

        if (pictureUri != null && imageFIle != null){
            requestBody = RequestBody.create(MediaType.parse(Objects.requireNonNull(getContentResolver().getType(pictureUri))), imageFIle);
        } else {
            requestBody = null;
        }
        RequestBody nam = RequestBody.create(MediaType.parse("text/plain"), name);
        RequestBody ph = RequestBody.create(MediaType.parse("text/plain"), phone);
        RequestBody add = RequestBody.create(MediaType.parse("text/plain"), address);

        Call<UpdateUserProfileResponse> updateUserProfileResponseCall = RestApi.getService().editUserDetails(
                sessionManager.getString(Constants.k_ACCESSTOKEN),
                nam,
                ph,
                add,
                requestBody
        );

        updateUserProfileResponseCall.enqueue(new Callback<UpdateUserProfileResponse>() {
            @Override
            public void onResponse(@NonNull Call<UpdateUserProfileResponse> call,@NonNull Response<UpdateUserProfileResponse> response) {
                Loading.cancel();

                if (response.isSuccessful()){
                    UpdateUserProfileResponse userProfileResponse = response.body();
                    if (userProfileResponse!=null){
                        if (userProfileResponse.getStatus()==200){

//                            sessionManager.clearVariable(Constants.k_USERNAME);
                            sessionManager.put(Constants.k_NAME, userProfileResponse.getUserobject().getName());
                            sessionManager.put(Constants.k_IMAGE, userProfileResponse.getUserobject().getProfilePic());
                            sessionManager.put(Constants.k_EMAIL, userProfileResponse.getUserobject().getEmail());
                            sessionManager.put(Constants.k_PHONE, userProfileResponse.getUserobject().getPhone());

                            ApplicationHandler.toast(userProfileResponse.getMessage());
                            ApplicationHandler.intent(HomeActivity.class);
                        }
                    }
                }

            }

            @Override
            public void onFailure(@NonNull Call<UpdateUserProfileResponse> call,@NonNull Throwable t) {
                Loading.cancel();
                Log.e(TAG, "onFailure: "+t.getMessage());
                ApplicationHandler.toast(t.getMessage());
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        bitmap = null;
        bottomSheetDialog.cancel();
        if (requestCode == CAMERA_REQUEST_CODE && resultCode == RESULT_OK) {
//            pictureUri = data.getData();
            Log.e(TAG, "imageUri cam: "+pictureUri);

            Log.e(TAG, "image file path cam: "+imageFilePath);
            if (imageFilePath != null) {
                imageFIle = new File(imageFilePath);
            }

            bitmap = BitmapFactory.decodeFile(imageFilePath);

            ivUserImageEditProfile.setImageBitmap(bitmap);

            Log.e(TAG, "image file cam: "+imageFIle);
        } else if (requestCode == GALLERY_REQUEST_CODE && resultCode == RESULT_OK){

            pictureUri = data.getData();
            Log.e(TAG, "imageUri gallery: "+pictureUri);
            imageFilePath = FileUtils.getPath(this, pictureUri);
            Log.e(TAG, "image file path gallery: "+imageFilePath);
            if (imageFilePath != null) {
                imageFIle = new File(imageFilePath);
            }

            bitmap = BitmapFactory.decodeFile(imageFilePath);

            ivUserImageEditProfile.setImageBitmap(bitmap);

            Log.e(TAG, "image file gallery: "+imageFIle);

        }
    }

    @Override
    public void onBackPressed() {
        ApplicationHandler.intent(SettingsActivity.class);
    }
}
