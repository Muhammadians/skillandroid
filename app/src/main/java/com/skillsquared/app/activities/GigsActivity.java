package com.skillsquared.app.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.skillsquared.app.R;
import com.skillsquared.app.adapters.GigsAdapter;
import com.skillsquared.app.handler.ApplicationHandler;
import com.skillsquared.app.handler.RestApi;
import com.skillsquared.app.models.gigsDetailsModel.GigsService;
import com.skillsquared.app.models.gigsDetailsModel.GigsServicesModel;
import com.skillsquared.app.utils.Constants;
import com.skillsquared.app.utils.Loading;
import com.skillsquared.app.utils.SessionManager;
import com.skillsquared.app.utils.SingleShotLocationProvider;
import com.warkiz.widget.IndicatorSeekBar;
import com.warkiz.widget.OnSeekChangeListener;
import com.warkiz.widget.SeekParams;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GigsActivity extends AppCompatActivity implements
        SwipeRefreshLayout.OnRefreshListener {

    private final static String TAG = GigsActivity.class.getSimpleName();
    RecyclerView recyclerViewGigs;
    EditText etSearch;
    ImageView ivSearch;
    String catId;
    AppCompatSpinner priceSpinner;
    SessionManager sessionManager;
    Switch switchButton;
    GigsAdapter gigsAdapter;
    boolean switchButtonVal = false;
    String priceFilterVal = null;
    boolean spinnerItemCheck = false;
    int check = 0;
    GigsServicesModel popularServicesModel;
    List<GigsService> mListFiltered;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private String page;
    private boolean isLoading = false;
    private int pastVisibleItems, visibleItemCount, totalItemCount, previousTotal = 0;
    private int viewThreshold = 10;
    private IndicatorSeekBar seekBarDistance;
    String seekBarValue = "1000";
    TextView distanceBarValue;
    String searchParam;
    String searchValueIntent;
    Button tvFilter;
    int spinnerItemNumber = 0;

    private void init() {

        recyclerViewGigs = findViewById(R.id.rvGigs);
        recyclerViewGigs.setHasFixedSize(true);
        recyclerViewGigs.setItemAnimator(new DefaultItemAnimator());
        etSearch = findViewById(R.id.etSearchGigs);
        ivSearch = findViewById(R.id.ivSearch);
        switchButton = findViewById(R.id.switchButton);
        priceSpinner = findViewById(R.id.priceSpinner);
        seekBarDistance = findViewById(R.id.seekBarDistance);
        catId = getIntent().getStringExtra("cat_id");
        sessionManager = new SessionManager(this);
        mSwipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        distanceBarValue = findViewById(R.id.distanceBarValue);
        tvFilter = findViewById(R.id.tvFilter);
        searchValueIntent = getIntent().getStringExtra("search");
        Loading.cancel();

        if (searchValueIntent != null) {
            catId = null;
            priceFilterVal = null;
            searchParam = searchValueIntent;
            makeServerCall(searchParam, switchButtonVal, priceFilterVal, page,
                    seekBarValue, "", "");
        } else {
            makeServerCall(searchParam, switchButtonVal, priceFilterVal, page,
                    seekBarValue, "", "");
        }

        mSwipeRefreshLayout.setOnRefreshListener(this);

        switchButton.setOnCheckedChangeListener((compoundButton, b) -> {

            if (b) {
                switchButtonVal = true;
                switchButton.setText(getResources().getString(R.string.online));
//                getPermission();
                makeServerCall(searchValueIntent, switchButtonVal, priceFilterVal, page,
                        seekBarValue, Constants.k_LATITUDE, Constants.k_LONGITUDE);
            } else {
                switchButtonVal = false;
                switchButton.setText(getResources().getString(R.string.offline));
//                getPermission();
                makeServerCall(searchValueIntent, switchButtonVal, priceFilterVal, page,
                        seekBarValue, Constants.k_LATITUDE, Constants.k_LONGITUDE);
            }

        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0) {
                    tvFilter.setVisibility(View.VISIBLE);
                } else {
                    tvFilter.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                /*filter(editable.toString());*/
            }
        });

        tvFilter.setOnClickListener(view -> {
            searchParam = ApplicationHandler.stringConverter(etSearch);
            makeServerCall(searchParam, switchButtonVal, priceFilterVal, page,
                    seekBarValue, "", "");
        });

        seekBarDistance.setOnClickListener(view -> getPermission());

    }

    private void getPermission() {
        Dexter.withActivity(GigsActivity.this)
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
//                        getCoordinates();
                        getSeekBarValue();
                        /*if (seekBarValue.equals("1000")) {
                            getSeekBarValue();
                        } else {
                            getCoordinates();
                        }*/
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        if (response.isPermanentlyDenied()) {
                            ApplicationHandler.showSettingsDialog(GigsActivity.this);
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    private void getCoordinates() {
        if (Constants.k_LATITUDE != null && Constants.k_LONGITUDE != null) {
            makeServerCall(searchParam, switchButtonVal, priceFilterVal, page,
                    seekBarValue, Constants.k_LATITUDE, Constants.k_LONGITUDE);
        } else {
            Loading.show(this, false, Constants.k_PLEASE_WAIT);
            SingleShotLocationProvider.requestSingleUpdate(GigsActivity.this,
                    location -> {
                        if (location != null) {
                            Loading.cancel();
                            Log.e("Location", location.latitude + " \n" + location.longitude);
                            Constants.k_LATITUDE = String.valueOf(location.latitude);
                            Constants.k_LONGITUDE = String.valueOf(location.longitude);

                            makeServerCall(searchParam, switchButtonVal, priceFilterVal, page,
                                    seekBarValue, Constants.k_LATITUDE, Constants.k_LONGITUDE);

                        }
                    });
        }
    }

    private void getSeekBarValue() {
        seekBarDistance.setOnSeekChangeListener(new OnSeekChangeListener() {

            @Override
            public void onSeeking(SeekParams seekParams) {
            }

            @Override
            public void onStartTrackingTouch(IndicatorSeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(IndicatorSeekBar seekBar) {
                Log.e(TAG, "onStopTrackingTouch: " + seekBar.getProgress());
                seekBarValue = String.valueOf(seekBar.getProgress());
                distanceBarValue.setText(String.valueOf(seekBar.getProgress()));
                getCoordinates();
            }

        });
    }

    private void makeServerCall(String search, boolean onlineStatus, String filterPrice, String pageNumber,
                                String radiusRange, String lat, String lon) {
        Loading.show(this, false, Constants.k_PLEASE_WAIT);

        Call<GigsServicesModel> popularServicesModelCall;

        if (!onlineStatus && filterPrice == null) {
            popularServicesModelCall = RestApi.getService().getGigsList(
                    sessionManager.getString(Constants.k_ACCESSTOKEN),
                    search,
                    catId,
                    "",
                    "0",
                    pageNumber,
                    radiusRange,
                    lat,
                    lon
            );
        } else if (!onlineStatus) {
            popularServicesModelCall = RestApi.getService().getGigsList(
                    sessionManager.getString(Constants.k_ACCESSTOKEN),
                    search,
                    catId,
                    filterPrice,
                    "0",
                    pageNumber,
                    radiusRange,
                    lat,
                    lon
            );
        } else if (filterPrice == null) {
            popularServicesModelCall = RestApi.getService().getGigsList(
                    sessionManager.getString(Constants.k_ACCESSTOKEN),
                    search,
                    catId,
                    "",
                    "1",
                    pageNumber,
                    radiusRange,
                    lat,
                    lon
            );
        } else {
            popularServicesModelCall = RestApi.getService().getGigsList(
                    sessionManager.getString(Constants.k_ACCESSTOKEN),
                    search,
                    catId,
                    filterPrice,
                    "1",
                    pageNumber,
                    radiusRange,
                    lat,
                    lon
            );
        }

        if (radiusRange != null && radiusRange.equals("0")) {
            radiusRange = "";
            lat = "";
            lon = "";
            popularServicesModelCall = RestApi.getService().getGigsList(
                    sessionManager.getString(Constants.k_ACCESSTOKEN),
                    search,
                    catId,
                    "",
                    "0",
                    pageNumber,
                    radiusRange,
                    lat,
                    lon
            );
        }

        popularServicesModelCall.enqueue(new Callback<GigsServicesModel>() {
            @Override
            public void onResponse(@NonNull Call<GigsServicesModel> call, @NonNull Response<GigsServicesModel> response) {
                Loading.cancel();

                if (response.isSuccessful()) {
                    popularServicesModel = response.body();

                    if (popularServicesModel != null) {
                        if (popularServicesModel.getServices() != null) {
                            Constants.k_PopularServiceList = popularServicesModel.getServices();
                            gigsAdapter = new GigsAdapter(GigsActivity.this, popularServicesModel.getServices());
                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
                            recyclerViewGigs.setLayoutManager(linearLayoutManager);
                            recyclerViewGigs.setAdapter(gigsAdapter);
                        } else {
                            recyclerViewGigs.setAdapter(null);
                            ApplicationHandler.toast(getResources().getString(R.string.no_data_to_display));
                        }

                        if (spinnerItemNumber == 0){
                            if (popularServicesModel.getPriceFilters() != null) {
                                arrayAdapter(popularServicesModel.getPriceFilters(), priceSpinner);
                            }
                            spinnerItemNumber = 1;
                            spinnerItemClickListener();
                        }

                    }

                }

            }

            @Override
            public void onFailure(@NonNull Call<GigsServicesModel> call, @NonNull Throwable t) {
                Loading.cancel();
                Log.e(TAG, "onFailure: " + t.getMessage());
                ApplicationHandler.toast(t.getMessage());
            }

        });

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gigs);

        init();

        findViewById(R.id.ivBackGigs).setOnClickListener(view -> onBackPressed());

    }

    private void arrayAdapter(List<String> array, AppCompatSpinner spinner) {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, array) {

            @NotNull
            public View getView(int position, View convertView, @NonNull ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                ((TextView) v).setTextSize(14);
                ((TextView) v).setTextColor(
                        getResources().getColorStateList(R.color.colorWhite)
                );

                return v;
            }

            public View getDropDownView(int position, View convertView, @NotNull ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                v.setBackgroundResource(R.color.colorPrimaryDark);

                ((TextView) v).setHeight(100);

                ((TextView) v).setTextColor(
                        getResources().getColorStateList(R.color.colorWhite)
                );

                ((TextView) v).setGravity(Gravity.CENTER);

                return v;
            }
        };

        Log.e(TAG, "arrayAdapter: "+array.size());

        spinner.setAdapter(adapter);
    }

    @SuppressLint("ClickableViewAccessibility")
    void spinnerItemClickListener() {
        priceSpinner.setOnTouchListener((view, motionEvent) -> {
            spinnerItemCheck = true;
            priceSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                    if (spinnerItemCheck) {
                        spinnerItemCheck = false;

                        if (priceSpinner.getSelectedItemId() == 0){
                            priceFilterVal = "";
                        } else {
                            priceFilterVal = priceSpinner.getSelectedItem().toString();
                        }

                        makeServerCall(searchParam, switchButtonVal, priceFilterVal, page,
                                seekBarValue, Constants.k_LATITUDE, Constants.k_LONGITUDE);
                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            return false;
        });

    }

    @Override
    public void onBackPressed() {
        ApplicationHandler.intent(HomeActivity.class);
    }

    private void filter(String text) {
        mListFiltered = new ArrayList<>();
        for (GigsService venue : popularServicesModel.getServices()) {
            if (venue.getServiceTitle().toLowerCase().contains(text.toLowerCase())) {
                mListFiltered.add(venue);
            }
        }
        gigsAdapter.filterGridCategories(mListFiltered);
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(() -> mSwipeRefreshLayout.setRefreshing(false), 2000);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Loading.cancel();
    }
}
