package com.skillsquared.app.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.os.Bundle;
import android.view.View;

import com.skillsquared.app.R;
import com.skillsquared.app.activities.buyer.ViewOffers;
import com.skillsquared.app.activities.buyer.WhatToDoActivityBuyer;
import com.skillsquared.app.activities.buyer.postAJob.PostARequest;
import com.skillsquared.app.activities.buyer.postAJob.PostedRequestList;
import com.skillsquared.app.activities.seller.AboutUserActivity;
import com.skillsquared.app.activities.seller.AnalyticsActivity;
import com.skillsquared.app.activities.seller.BuyerRequest;
import com.skillsquared.app.activities.seller.CreateServiceActivity;
import com.skillsquared.app.activities.seller.CreateServiceOne;
import com.skillsquared.app.activities.seller.EarningsActivity;
import com.skillsquared.app.activities.seller.manageSales.ManageSalesActivity;
import com.skillsquared.app.handler.ApplicationHandler;

public class TestScreen extends AppCompatActivity {

    CardView whatToDo, signIn, signUp, home, chats, chatsTwo,
                categories, editProfile, gigs, selectCategory, postARequest, postedRequest,
                viewOffers, analytics, buyerRequest, createService, createServiceOne,
                earningsCardView, aboutUser, manageSales;

    private void init() {
        whatToDo = findViewById(R.id.whatToDo);
        signIn = findViewById(R.id.signIn);
        signUp = findViewById(R.id.signUp);
        home = findViewById(R.id.homeCardView);
        chats = findViewById(R.id.chats);
        chatsTwo = findViewById(R.id.chatsTwo);
        categories = findViewById(R.id.categories);
        editProfile = findViewById(R.id.editProfile);
        gigs = findViewById(R.id.gigs);
        selectCategory = findViewById(R.id.selectCategory);
        postARequest = findViewById(R.id.postARequest);
        postedRequest = findViewById(R.id.postedRequestList);
        viewOffers = findViewById(R.id.viewOffers);
        analytics = findViewById(R.id.analyticsCardView);
        buyerRequest = findViewById(R.id.buyerRequestCard);
        createService = findViewById(R.id.createServiceCard);
        createServiceOne = findViewById(R.id.createServiceOneCard);
        earningsCardView = findViewById(R.id.earningsCardView);
        aboutUser = findViewById(R.id.aboutUserCardView);
        manageSales = findViewById(R.id.manageSalesCardView);

        onClickListeners();

    }

    private void onClickListeners() {
        whatToDo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ApplicationHandler.intent(WhatToDoActivityBuyer.class);
            }
        });

        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ApplicationHandler.intent(SignIn.class);
            }
        });

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ApplicationHandler.intent(SignUp.class);
            }
        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ApplicationHandler.intent(HomeActivity.class);
            }
        });

        chats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ApplicationHandler.intent(MessageActivity.class);
            }
        });

        chatsTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ApplicationHandler.intent(SingleChatActivity.class);
            }
        });

        categories.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ApplicationHandler.intent(CategoriesGridActivity.class);
            }
        });

        editProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ApplicationHandler.intent(EditProfileActivity.class);
            }
        });

        gigs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ApplicationHandler.intent(GigsActivity.class);
            }
        });

        selectCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ApplicationHandler.intent(SelectCategory.class);
            }
        });

        postARequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ApplicationHandler.intent(PostARequest.class);
            }
        });
        postedRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ApplicationHandler.intent(PostedRequestList.class);
            }
        });
        viewOffers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ApplicationHandler.intent(ViewOffers.class);
            }
        });

        analytics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ApplicationHandler.intent(AnalyticsActivity.class);
            }
        });

        buyerRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ApplicationHandler.intent(BuyerRequest.class);
            }
        });

        createService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ApplicationHandler.intent(CreateServiceActivity.class);
            }
        });

        createServiceOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ApplicationHandler.intent(CreateServiceOne.class);
            }
        });

        earningsCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ApplicationHandler.intent(EarningsActivity.class);
            }
        });

        aboutUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ApplicationHandler.intent(AboutUserActivity.class);
            }
        });

        manageSales.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ApplicationHandler.intent(ManageSalesActivity.class);
            }
        });

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_screen);

        init();

    }

}
