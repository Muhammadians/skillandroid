package com.skillsquared.app.activities.seller.manageSales;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;
import com.skillsquared.app.R;
import com.skillsquared.app.activities.seller.WhatToDoActivitySeller;
import com.skillsquared.app.fragments.ActiveSalesFragment;
import com.skillsquared.app.fragments.CompletedSalesFragment;
import com.skillsquared.app.fragments.DeliveredFragment;
import com.skillsquared.app.fragments.DisputedFragment;
import com.skillsquared.app.fragments.RevisionFragment;
import com.skillsquared.app.fragments.manageOrdersFragment.AactiveFragment;
import com.skillsquared.app.handler.ApplicationHandler;
import com.skillsquared.app.handler.RestApi;
import com.skillsquared.app.models.manageOrders.ManageOrdersModel;
import com.skillsquared.app.utils.Constants;
import com.skillsquared.app.utils.Loading;
import com.skillsquared.app.utils.SessionManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ManageSalesActivity extends AppCompatActivity {

    private final static String TAG = ManageSalesActivity.class.getSimpleName();
    FrameLayout fragmentContainer;
    TabLayout tabLayout;
    SessionManager sessionManager;
    ImageView ivFilterManageSales;
    Dialog myDialog;
    RadioButton radioButton, rbBuyerOffer, rbSentOffer;
    RadioGroup rgBuyerRequest, rgPostJobFilter;
    LinearLayout llFilterPostRequest, llBuyerRequestFilter;
    TextView tvSentOffer, tvBuyerRequest;

    void init(){
        fragmentContainer = findViewById(R.id.fragment_container_review_about_user);
        tabLayout = findViewById(R.id.tabLayoutManageSales);
        ImageView ivBackManageSales = findViewById(R.id.ivBackManageSales);
        sessionManager = new SessionManager(this);
        ivFilterManageSales = findViewById(R.id.ivFilterManageSales);
        sessionManager = new SessionManager(this);
        myDialog = new Dialog(this);
        initPopUp();

        ivFilterManageSales.setOnClickListener(view->showPopUp());

        ivBackManageSales.setOnClickListener(view -> onBackPressed());

        getDataFromServer("normal");

        setTabListeners();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ApplicationHandler.hideStatusBar(this);
        setContentView(R.layout.activity_manage_sales);

        init();

    }

    void setTabListeners(){
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0){
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container_review_about_user,
                            new ActiveSalesFragment()).commit();
                } else if (tab.getPosition() == 1){
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container_review_about_user,
                            new DeliveredFragment()).commit();
                } else if (tab.getPosition() == 2){
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container_review_about_user,
                            new CompletedSalesFragment()).commit();
                } else if (tab.getPosition() == 3){
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container_review_about_user,
                            new RevisionFragment()).commit();
                }else {
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container_review_about_user,
                            new DisputedFragment()).commit();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    void getDataFromServer(String param){

        if (Constants.k_ManageOrdersModel!=null){

            if (param.equals("custom")){
                Constants.k_manageCustomSales = true;
            } else {
                Constants.k_manageCustomSales = false;
            }

            setFragment();

        } else {

            Loading.show(this, false, Constants.k_PLEASE_WAIT);
            Call<ManageOrdersModel> manageOrdersModelCall = RestApi.getService().getOrders(
                    sessionManager.getString(Constants.k_ACCESSTOKEN)
            );

            manageOrdersModelCall.enqueue(new Callback<ManageOrdersModel>() {
                @Override
                public void onResponse(@NonNull Call<ManageOrdersModel> call, @NonNull Response<ManageOrdersModel> response) {
                    Loading.cancel();

                    if (response.isSuccessful()) {
                        Constants.k_ManageOrdersModel = response.body();

                    }

                }

                @Override
                public void onFailure(@NonNull Call<ManageOrdersModel> call, @NonNull Throwable t) {
                    Loading.cancel();
                    Log.e(TAG, "onFailure: " + t.getMessage());
                    ApplicationHandler.toast(t.getMessage());
                }
            });
        }
    }

    void initPopUp(){
        myDialog.setContentView(R.layout.post_a_job_filter_pop_up);
        llFilterPostRequest = myDialog.findViewById(R.id.llFilterPostRequest);
        llBuyerRequestFilter = myDialog.findViewById(R.id.llBuyerRequestFilter);
        rgBuyerRequest = myDialog.findViewById(R.id.rgBuyerRequest);
        rgPostJobFilter = myDialog.findViewById(R.id.rgPostJobFilter);
        rbBuyerOffer = myDialog.findViewById(R.id.rbBuyerRequest);
        rbSentOffer = myDialog.findViewById(R.id.rbSellerSentOffer);
        tvBuyerRequest = myDialog.findViewById(R.id.tvBuyerRequest);
        tvSentOffer = myDialog.findViewById(R.id.tvSentOffer);

        llFilterPostRequest.setVisibility(View.GONE);
        rgPostJobFilter.setVisibility(View.GONE);
        llBuyerRequestFilter.setVisibility(View.VISIBLE);
        rgBuyerRequest.setVisibility(View.VISIBLE);
    }

    void showPopUp() {
        myDialog.show();

        tvBuyerRequest.setText("Normal Orders");
        tvSentOffer.setText("Custom Orders");

        rgBuyerRequest.setOnCheckedChangeListener((radioGroup, i) -> {
            int selectedId = radioGroup.getCheckedRadioButtonId();
            radioButton = myDialog.findViewById(selectedId);

            if (Constants.k_BuyerManageOrders != null){

                if (String.valueOf(radioButton.getText()).equals("Buyer Request")){
                    getDataFromServer("normal");
                } else if (String.valueOf(radioButton.getText()).equals("Sent Offer")){
                    getDataFromServer("custom");
                }
            }

            myDialog.cancel();
        });
    }

    private void setFragment(){
        if (Constants.k_ManageOrdersModel != null) {
            if (Constants.k_ManageOrdersModel.getSellerordersfrombuyers() != null) {
                if (Constants.k_ManageOrdersModel.getSellerordersfrombuyers().getAcompleted() != null) {
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container_review_about_user,
                            new ActiveSalesFragment()).commit();
                } else {
                    ApplicationHandler.toast(getResources().getString(R.string.no_data_to_display));
                }
            } else {
                ApplicationHandler.toast(getResources().getString(R.string.no_data_to_display));
            }
        } else {
            ApplicationHandler.toast(getResources().getString(R.string.no_data_to_display));
        }
    }

    @Override
    public void onBackPressed() {
        ApplicationHandler.intent(WhatToDoActivitySeller.class);
    }
}
