package com.skillsquared.app.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.card.MaterialCardView;
import com.skillsquared.app.cometChat.Activity.LoginActivity;
import com.skillsquared.app.handler.ApplicationHandler;
import com.skillsquared.app.R;
import com.skillsquared.app.handler.RestApi;
import com.skillsquared.app.models.signUpModels.SignUpModel;
import com.skillsquared.app.utils.Constants;
import com.skillsquared.app.utils.Loading;
import com.skillsquared.app.utils.SessionManager;

import org.jetbrains.annotations.NotNull;
import org.w3c.dom.Text;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResetPassword extends AppCompatActivity {

    private final static String TAG = ResetPassword.class.getSimpleName();
    ImageView ivBack;
    AutoCompleteTextView etResetEmail;
    TextView backToLogin, tvMessage;
    MaterialCardView resetPassword;
    SessionManager sessionManager;

    private void init() {
        ivBack = findViewById(R.id.ivBack);
        etResetEmail = findViewById(R.id.etEmailReset);
        backToLogin = findViewById(R.id.backToLogin);
        tvMessage = findViewById(R.id.tvMessage);
        resetPassword = findViewById(R.id.resetPassword);
        sessionManager = new SessionManager(this);

        onClickListeners();
    }

    private void onClickListeners() {
        ivBack.setOnClickListener(view -> onBackPressed());
        backToLogin.setOnClickListener(view -> ApplicationHandler.intent(SignIn.class));

        resetPassword.setOnClickListener(view->{
            if (TextUtils.isEmpty(ApplicationHandler.stringConverter(etResetEmail))){
                etResetEmail.setError(getResources().getString(R.string.editTextEmptyFieldError));
                etResetEmail.requestFocus();
            } else {
                etResetEmail.setError(null);

                Loading.show(this, false, Constants.k_PLEASE_WAIT);

                Call<SignUpModel> resetPass = RestApi.getService().resetPassword(
                        ApplicationHandler.stringConverter(etResetEmail)
                );

                resetPass.enqueue(new Callback<SignUpModel>() {
                    @Override
                    public void onResponse(@NotNull Call<SignUpModel> call, @NotNull Response<SignUpModel> response) {
                        Loading.cancel();
                        if (response.isSuccessful()){
                            SignUpModel signUpModel = response.body();
                            if (signUpModel != null) {
                                etResetEmail.setText(null);
                                ApplicationHandler.toast(signUpModel.getMessage());
                                tvMessage.setVisibility(View.VISIBLE);
                                tvMessage.setText(signUpModel.getMessage());
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NotNull Call<SignUpModel> call, @NotNull Throwable t) {
                        Loading.cancel();
                        Log.e(TAG, "onFailure: "+t.getMessage());
                        ApplicationHandler.toast(t.getMessage());
                    }
                });

            }
        });

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ApplicationHandler.hideStatusBar(this);
        setContentView(R.layout.activity_reset_password);

        init();

    }
}
