package com.skillsquared.app.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.skillsquared.app.handler.ApplicationHandler;
import com.skillsquared.app.R;
import com.skillsquared.app.utils.Constants;
import com.skillsquared.app.utils.SessionManager;

public class SplashActivity extends AppCompatActivity {

    private SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ApplicationHandler.hideStatusBar(this);
        setContentView(R.layout.activity_splash);

        sessionManager = new SessionManager(this);
//        sessionManager.put(Constants.k_ACCESSTOKEN, "931821574170175");
        setSplash();
    }

    void setSplash() {
        int SPLASH_DISPLAY_LENGTH = 2000;
        new Handler().postDelayed(() -> {

            if (sessionManager.checkLogin()){
                ApplicationHandler.intent(HomeActivity.class);
            } else {
                Intent mainIntent = new Intent(SplashActivity.this, SignIn.class);
                SplashActivity.this.startActivity(mainIntent);
                SplashActivity.this.finish();
            }
        }, SPLASH_DISPLAY_LENGTH);
    }

}
