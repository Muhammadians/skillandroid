package com.skillsquared.app.activities.seller;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.skillsquared.app.R;
import com.skillsquared.app.adapters.ManageServiceAdapter;
import com.skillsquared.app.adapters.ViewOffersAdapter;
import com.skillsquared.app.handler.ApplicationHandler;
import com.skillsquared.app.handler.RestApi;
import com.skillsquared.app.models.manageServices.ManageServicesModel;
import com.skillsquared.app.models.postAJob.PostAJobResponse;
import com.skillsquared.app.utils.Constants;
import com.skillsquared.app.utils.Loading;
import com.skillsquared.app.utils.SessionManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ManageServicesActivity extends AppCompatActivity {

    private final static String TAG = "Manage Service";
    RecyclerView recyclerViewManageServices;
    private SessionManager sessionManager;
    private ImageView ivBackManageService;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ApplicationHandler.hideStatusBar(this);
        setContentView(R.layout.activity_manage_services);

        recyclerViewManageServices = findViewById(R.id.recyclerViewManageServices);
        ivBackManageService = findViewById(R.id.ivBackManageService);
        sessionManager = new SessionManager(this);

        getDataFromServer();

        ivBackManageService.setOnClickListener(view -> onBackPressed());

    }

    private void getDataFromServer(){

        Loading.show(this, false, Constants.k_PLEASE_WAIT);

        Call<ManageServicesModel> manageServicesModelCall = RestApi.getService().getManageServices(
                sessionManager.getString(Constants.k_ACCESSTOKEN)
        );

        manageServicesModelCall.enqueue(new Callback<ManageServicesModel>() {
            @Override
            public void onResponse(@NonNull Call<ManageServicesModel> call,@NonNull Response<ManageServicesModel> response) {
                Loading.cancel();

                if (response.isSuccessful()){
                    ManageServicesModel manageServicesModel = response.body();

                    if (manageServicesModel != null) {
                        if (manageServicesModel.getActiveGigsServices()!=null){
                            ManageServiceAdapter viewOffersAdapter =
                                    new ManageServiceAdapter(ManageServicesActivity.this, manageServicesModel.getActiveGigsServices());
                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL,false);
                            recyclerViewManageServices.setLayoutManager(linearLayoutManager);
                            recyclerViewManageServices.setAdapter(viewOffersAdapter);
                        } else {
                            ApplicationHandler.toast(getResources().getString(R.string.no_data_to_display));
                        }
                    }

                }

            }

            @Override
            public void onFailure(@NonNull Call<ManageServicesModel> call,@NonNull Throwable t) {
                Loading.cancel();
                Log.e(TAG, "onFailure: "+t.getMessage());
                ApplicationHandler.toast(t.getMessage());
            }
        });

    }

    @Override
    public void onBackPressed() {
        ApplicationHandler.intent(WhatToDoActivitySeller.class);
    }
}
