package com.skillsquared.app.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.skillsquared.app.R;
import com.skillsquared.app.adapters.AllChatsAdapter;
import com.skillsquared.app.adapters.ServicesAdapter;
import com.skillsquared.app.models.AllChatsModel;

import java.util.ArrayList;

public class MessageActivity extends AppCompatActivity {

    RecyclerView recyclerViewChat;
    ArrayList<AllChatsModel> allChatsModelArrayList
            = new ArrayList<AllChatsModel>();

    private void init() {
        recyclerViewChat = findViewById(R.id.recyclerViewChats);

        setRecyclerViewAdapter();
    }

    private void setRecyclerViewAdapter() {
        addDataintoArrayList();
        AllChatsAdapter allChatsAdapter =
                new AllChatsAdapter(this, allChatsModelArrayList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        recyclerViewChat.setLayoutManager(linearLayoutManager);
        recyclerViewChat.setAdapter(allChatsAdapter);
    }

    private void addDataintoArrayList() {

        AllChatsModel allChatsModel = new AllChatsModel(
                1,
                R.drawable.user_profile,
                "Talha Zahid",
                "This is the last message",
                "2 minutes ago"
        );

        AllChatsModel allChatsModelOne = new AllChatsModel(
                2,
                R.drawable.user_profile,
                "Bilal Shahid",
                "This is the last message",
                "5 minutes ago"
        );

        AllChatsModel allChatsModelTwo = new AllChatsModel(
                3,
                R.drawable.user_profile,
                "Zafar Sajjad",
                "This is the last message",
                "15 minutes ago"
        );

        allChatsModelArrayList.add(allChatsModel);
        allChatsModelArrayList.add(allChatsModelOne);
        allChatsModelArrayList.add(allChatsModelTwo);
        allChatsModelArrayList.add(allChatsModel);
        allChatsModelArrayList.add(allChatsModelOne);
        allChatsModelArrayList.add(allChatsModelTwo);
        allChatsModelArrayList.add(allChatsModel);
        allChatsModelArrayList.add(allChatsModelOne);
        allChatsModelArrayList.add(allChatsModelTwo);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);

        init();

    }

}
