package com.skillsquared.app.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.skillsquared.app.R;

public class SingleChatActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_chat);
    }
}
