package com.skillsquared.app.activities.buyer;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.braintreepayments.api.BraintreeFragment;
import com.braintreepayments.api.PayPal;
import com.braintreepayments.api.dropin.DropInActivity;
import com.braintreepayments.api.dropin.DropInRequest;
import com.braintreepayments.api.dropin.DropInResult;
import com.braintreepayments.api.exceptions.InvalidArgumentException;
import com.braintreepayments.api.models.PayPalRequest;
import com.skillsquared.app.R;
import com.skillsquared.app.activities.buyer.postAJob.PostedRequestList;
import com.skillsquared.app.adapters.ViewOffersAdapter;
import com.skillsquared.app.handler.ApplicationHandler;
import com.skillsquared.app.handler.RestApi;
import com.skillsquared.app.models.PlaceOrderResponse;
import com.skillsquared.app.models.paymentResponse.PaypalResponse;
import com.skillsquared.app.models.viewOffers.Asellerrequest;
import com.skillsquared.app.models.viewOffers.ViewOffersModel;
import com.skillsquared.app.utils.Constants;
import com.skillsquared.app.utils.Loading;
import com.skillsquared.app.utils.SessionManager;

import java.util.List;
import java.util.Objects;

import co.paystack.android.Paystack;
import co.paystack.android.PaystackSdk;
import co.paystack.android.Transaction;
import co.paystack.android.model.Card;
import co.paystack.android.model.Charge;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewOffers extends AppCompatActivity implements
        ViewOffersAdapter.MyCallBack{

    private final static String TAG = ViewOffers.class.getSimpleName();
    RecyclerView recyclerViewOffers;
    String request_id;
    String requestIdForSend;
    String price;
    SessionManager sessionManager;
    ImageView ivBackViewOffers;
    AppCompatActivity appCompatActivity;
    ViewOffersAdapter viewOffersAdapter;

    private EditText emailField;

    private void init() {
        recyclerViewOffers = findViewById(R.id.recyclerViewOffers);
        request_id = getIntent().getStringExtra("request_id");
        ivBackViewOffers = findViewById(R.id.ivBackViewOffers);
        appCompatActivity = this;
        sessionManager = new SessionManager(this);

        getDataFromServer();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ApplicationHandler.hideStatusBar(this);
        setContentView(R.layout.activity_view_offers);

        init();

        ivBackViewOffers.setOnClickListener(view -> onBackPressed());

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 111) {
            if (resultCode == RESULT_OK) {
                DropInResult result = null;
                if (data != null) {
                    result = data.getParcelableExtra(DropInResult.EXTRA_DROP_IN_RESULT);
                }
                if (result != null) {
                    Log.e(TAG, "onActivityResult: "+ Objects.requireNonNull(result.getPaymentMethodNonce()).getDescription());
                    Log.e(TAG, "onActivityResult: "+result.getPaymentMethodNonce().getTypeLabel());
                    Log.e(TAG, "onActivityResult: " + Objects.requireNonNull(result.getPaymentMethodNonce()).getNonce());

//                    sendNonceToServer(Objects.requireNonNull(result.getPaymentMethodNonce()).getNonce());
                    placeOrder(Objects.requireNonNull(result.getPaymentMethodNonce()).getNonce());

                }
            } else if (resultCode == RESULT_CANCELED) {
                Log.e(TAG, "onActivityResult: user cancelled");
            } else {
                // handle errors here, an exception may be available in
                if (data != null) {
                    Exception error = (Exception) data.getSerializableExtra(DropInActivity.EXTRA_ERROR);
                    Log.e(TAG, "onActivityResult: "+error);
                }
            }
        }
    }

    private void sendNonceToServer(String nonce){
        if (nonce!=null){
            Loading.show(this, false, Constants.k_PLEASE_WAIT);

            if (Constants.k_OfferPrice==null){
                ApplicationHandler.toast(getResources().getString(R.string.something_went_wrong));
            } else {
                Call<PaypalResponse> getPaypalResponse = RestApi.getService().getPaypalResponse(
                        sessionManager.getString(Constants.k_ACCESSTOKEN),
                        nonce,
                        Constants.k_OfferPrice
                );

                getPaypalResponse.enqueue(new Callback<PaypalResponse>() {
                    @Override
                    public void onResponse(@NonNull Call<PaypalResponse> call, @NonNull Response<PaypalResponse> response) {
                        Loading.cancel();
                        if (response.isSuccessful()) {
                            PaypalResponse paypalResponse = response.body();
                            if (paypalResponse != null && paypalResponse.getSuccess()) {
                                ApplicationHandler.toast(getResources().getString(R.string.order_placed));
                            } else {
                                ApplicationHandler.toast(getResources().getString(R.string.payment_not_successful));
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<PaypalResponse> call, @NonNull Throwable t) {
                        Log.e(TAG, "onFailure: " + t.getMessage());
                        ApplicationHandler.toast("Error" + t.getMessage());
                        Loading.cancel();
                    }
                });
            }
        }
    }

    private void placeOrder(String nonce){
        if (nonce!=null){
            Loading.show(this, false, Constants.k_PLEASE_WAIT);
            if (price==null || requestIdForSend==null){
                ApplicationHandler.toast(getResources().getString(R.string.something_went_wrong));
                Loading.cancel();
            } else {
                Call<PlaceOrderResponse> getPaypalResponse = RestApi.getService().placeOrder(
                        sessionManager.getString(Constants.k_ACCESSTOKEN),
                        nonce,
                        price,
                        requestIdForSend,
                        "paypal"
                );

                getPaypalResponse.enqueue(new Callback<PlaceOrderResponse>() {
                    @Override
                    public void onResponse(@NonNull Call<PlaceOrderResponse> call, @NonNull Response<PlaceOrderResponse> response) {
                        if (response.isSuccessful()) {
                            PlaceOrderResponse paypalResponse = response.body();
                            if (paypalResponse != null) {
                                Loading.cancel();
                                ApplicationHandler.intent(PostedRequestList.class);
//                                getDataFromServer();
                                ApplicationHandler.toast(paypalResponse.getMessage());
                            } else {
                                Loading.cancel();
                                ApplicationHandler.toast(getResources().getString(R.string.something_went_wrong));
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<PlaceOrderResponse> call, @NonNull Throwable t) {
                        Log.e(TAG, "onFailure: " + t.getMessage());
                        ApplicationHandler.toast("Error" + t.getMessage());
                        Loading.cancel();
                    }
                });
            }
        }
    }

    public void listenerMethod(String textViewValue, String requestId){
        requestIdForSend = requestId;
        price = textViewValue;
    }

    private void getDataFromServer() {
        Loading.show(this, false, Constants.k_PLEASE_WAIT);

        Call<ViewOffersModel> viewOffersModelCall = RestApi.getService().getViewOffers(
                sessionManager.getString(Constants.k_ACCESSTOKEN),
                request_id
        );

        viewOffersModelCall.enqueue(new Callback<ViewOffersModel>() {
            @Override
            public void onResponse(@NonNull Call<ViewOffersModel> call, @NonNull Response<ViewOffersModel> response) {
                Loading.cancel();

                if (response.isSuccessful()) {
                    ViewOffersModel viewOffersModel = response.body();

                    if (viewOffersModel != null) {
                        setRecyclerAdapter(viewOffersModel.getAsellerrequest(), viewOffersModel.getOrdercheck());
                    }

                }

            }

            @Override
            public void onFailure(@NonNull Call<ViewOffersModel> call, @NonNull Throwable t) {
                Loading.cancel();
            }
        });

    }

    private void setRecyclerAdapter(List<Asellerrequest> list, int orderCheck) {
        if (list != null) {
            viewOffersAdapter =
                    new ViewOffersAdapter(this, list, request_id, appCompatActivity, this, orderCheck);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            recyclerViewOffers.setLayoutManager(linearLayoutManager);
            recyclerViewOffers.setAdapter(viewOffersAdapter);
        } else {
            ApplicationHandler.toast(getResources().getString(R.string.no_data_to_display));
        }
    }

    @Override
    public void onBackPressed() {
        ApplicationHandler.intent(WhatToDoActivityBuyer.class);
    }

}
