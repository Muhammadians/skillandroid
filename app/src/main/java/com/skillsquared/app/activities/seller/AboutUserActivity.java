package com.skillsquared.app.activities.seller;

import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.tabs.TabLayout;
import com.skillsquared.app.R;
import com.skillsquared.app.activities.HomeActivity;
import com.skillsquared.app.fragments.AboutUserFragment;
import com.skillsquared.app.fragments.GigsUserFragment;
import com.skillsquared.app.fragments.ReviewsUserFragment;
import com.skillsquared.app.handler.ApplicationHandler;
import com.skillsquared.app.handler.RestApi;
import com.skillsquared.app.models.sellerProfileModel.SellerProfileModel;
import com.skillsquared.app.utils.Constants;
import com.skillsquared.app.utils.Loading;
import com.skillsquared.app.utils.SessionManager;

import org.jetbrains.annotations.NotNull;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AboutUserActivity extends AppCompatActivity {

    private final static String TAG = AboutUserActivity.class.getSimpleName();
    FrameLayout fragmentContainer;
    TabLayout tabLayout;
    SessionManager sessionManager;
    ImageView ivBackAboutUser;
    String source;
    TextView tvEditProfile;

    void init() {
        fragmentContainer = findViewById(R.id.fragment_container_about_user);
        tabLayout = findViewById(R.id.tabLayout);
        tvEditProfile = findViewById(R.id.tvEditProfile);
        ivBackAboutUser = findViewById(R.id.ivBackAboutUser);
        sessionManager = new SessionManager(this);

        setTabListeners();

        source = getIntent().getStringExtra("source");

        if (source != null) {
            getFreelancerProfile();
        } else {
            getServerData();
        }

        ivBackAboutUser.setOnClickListener(view -> onBackPressed());

        tvEditProfile.setOnClickListener(view-> new AlertDialog.Builder(AboutUserActivity.this)
                .setTitle("Edit Profile")
                .setMessage("Visit www.skillsquared.com to edit profile")
                .setCancelable(false)
                .setPositiveButton(android.R.string.yes, (dialog, which) -> {
                    //do your task
                    dialog.cancel();
                })
                .setIcon(android.R.drawable.ic_dialog_info)
                .show());

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ApplicationHandler.hideStatusBar(this);
        setContentView(R.layout.activity_about_user);

        init();

    }

    void setTabListeners() {
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container_about_user,
                            new AboutUserFragment()).commit();
                } else if (tab.getPosition() == 1) {
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container_about_user,
                            new GigsUserFragment()).commit();
                } else {
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container_about_user,
                            new ReviewsUserFragment()).commit();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    void getServerData() {
        Loading.show(this, false, Constants.k_PLEASE_WAIT);
        Call<SellerProfileModel> sellerProfileModelCall = RestApi.getService().getUserProfile(
                sessionManager.getString(Constants.k_ACCESSTOKEN)
        );

        sellerProfileModelCall.enqueue(new Callback<SellerProfileModel>() {
            @Override
            public void onResponse(@NonNull Call<SellerProfileModel> call, @NonNull Response<SellerProfileModel> response) {
                Loading.cancel();
                if (response.isSuccessful()) {
                    Constants.k_SellerProfileModel = response.body();
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container_about_user,
                            new AboutUserFragment()).commit();
                } else {
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container_about_user,
                            new AboutUserFragment()).commit();
                }
            }

            @Override
            public void onFailure(@NonNull Call<SellerProfileModel> call, @NonNull Throwable t) {
                Loading.cancel();
                Log.e(TAG, "onFailure: " + t.getMessage());
                ApplicationHandler.toast(t.getMessage());
            }
        });

    }

    void getFreelancerProfile() {
        Loading.show(this, false, Constants.k_PLEASE_WAIT);

        Call<SellerProfileModel> getFreelanceProfile = RestApi.getService().getFreelancerProfile(
                sessionManager.getString(Constants.k_ACCESSTOKEN),
                source
        );

        getFreelanceProfile.enqueue(new Callback<SellerProfileModel>() {
            @Override
            public void onResponse(@NotNull Call<SellerProfileModel> call, @NotNull Response<SellerProfileModel> response) {
                Loading.cancel();

                if (response.isSuccessful()) {
                    Constants.k_SellerProfileModel = response.body();
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container_about_user,
                            new AboutUserFragment()).commit();
                } else {
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container_about_user,
                            new AboutUserFragment()).commit();
                }

            }

            @Override
            public void onFailure(@NotNull Call<SellerProfileModel> call, @NotNull Throwable t) {
                Loading.cancel();
                Log.e(TAG, "onFailure: " + t.getMessage());
                ApplicationHandler.toast(t.getMessage());
            }
        });
    }

    @Override
    public void onBackPressed() {
        Constants.k_SellerProfileModel = null;

        if (source == null) {
            ApplicationHandler.intent(HomeActivity.class);
        } else {
            super.onBackPressed();
        }
    }
}
