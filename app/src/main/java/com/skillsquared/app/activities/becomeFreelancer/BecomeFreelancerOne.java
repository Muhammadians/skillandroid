package com.skillsquared.app.activities.becomeFreelancer;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.skillsquared.app.R;
import com.skillsquared.app.activities.HomeActivity;
import com.skillsquared.app.handler.ApplicationHandler;
import com.skillsquared.app.handler.RestApi;
import com.skillsquared.app.models.becomeFreelancerDetails.Catoption;
import com.skillsquared.app.models.becomeFreelancerDetails.GetBecomeFreelanceDetails;
import com.skillsquared.app.models.becomeFreelancerDetails.GetStates;
import com.skillsquared.app.models.becomeFreelancerDetails.Subcat;
import com.skillsquared.app.models.becomeFreelancerDetails.SubcatChild;
import com.skillsquared.app.utils.Constants;
import com.skillsquared.app.utils.Loading;
import com.skillsquared.app.utils.SessionManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BecomeFreelancerOne extends AppCompatActivity {

    private static String TAG = BecomeFreelancerOne.class.getSimpleName();
    EditText etFreelancerTitleBecomeFreelancer, etAdditionalInfoBecomeFreelancer;
    AppCompatSpinner spinnerCountryBecomeFreelancer, spinnerStateBecomeFreelancer,
            spinnerCityBecomeFreelancer, spinnerCategoryBecomeFreelancer,
            spinnerSubCategoryBecomeFreelancer, spinnerSubCategoryChildBecomeFreelancer;
    LinearLayout llSubCatLayoutBecomeFreelancer, llSubCatChildLayoutBecomeFreelancer;
    String userName, freelancerTitle, additionalInfo;
    private SessionManager sessionManager;
    TextView etUserNameBecomeFreelancer;
    ArrayList<String> countryList = new ArrayList<>();
    List<GetStates> getStates;
    List<GetStates> getCitiesList;
    String getCountry, getState, getCity;
    String cat_id = null;
    String cat_level = null;
    String countryId = "";
    String stateId = "";
    String cityId = "";

    private void init(){
        etUserNameBecomeFreelancer = findViewById(R.id.etUserNameBecomeFreelancer);
        etFreelancerTitleBecomeFreelancer = findViewById(R.id.etFreelancerTitleBecomeFreelancer);
        etAdditionalInfoBecomeFreelancer = findViewById(R.id.etAdditionalInfoBecomeFreelancer);
        spinnerCountryBecomeFreelancer = findViewById(R.id.spinnerCountryBecomeFreelancer);
        spinnerStateBecomeFreelancer = findViewById(R.id.spinnerStateBecomeFreelancer);
        spinnerCityBecomeFreelancer = findViewById(R.id.spinnerCityBecomeFreelancer);
        spinnerCategoryBecomeFreelancer = findViewById(R.id.spinnerCategoryBecomeFreelancer);
        spinnerSubCategoryBecomeFreelancer = findViewById(R.id.spinnerSubCategoryBecomeFreelancer);
        spinnerSubCategoryChildBecomeFreelancer = findViewById(R.id.spinnerSubCategoryChildBecomeFreelancer);
        llSubCatLayoutBecomeFreelancer = findViewById(R.id.llSubCatLayoutBecomeFreelancer);
        llSubCatChildLayoutBecomeFreelancer = findViewById(R.id.llSubCatChildLayoutBecomeFreelancer);
        sessionManager = new SessionManager(this);

        getFieldsData();

        findViewById(R.id.submitBecomeFreelancerOne).setOnClickListener(view-> checkValues());

    }

    private void getFieldsData(){
        Loading.show(this, false, Constants.k_PLEASE_WAIT);
        Call<GetBecomeFreelanceDetails> getBecomeFreelanceDetailsCall = RestApi.getService().getDetails(
                sessionManager.getString(Constants.k_ACCESSTOKEN)
        );

        getBecomeFreelanceDetailsCall.enqueue(new Callback<GetBecomeFreelanceDetails>() {
            @Override
            public void onResponse(@NonNull Call<GetBecomeFreelanceDetails> call,@NonNull Response<GetBecomeFreelanceDetails> response) {
                Loading.cancel();

                if (response.isSuccessful()){
                    Constants.k_becomeFreelanceDetails = response.body();
                    if (Constants.k_becomeFreelanceDetails!=null){
                        setDataInFields();
                    }
                }

            }

            @Override
            public void onFailure(@NonNull Call<GetBecomeFreelanceDetails> call,@NonNull Throwable t) {
                Loading.cancel();
                Log.e(TAG, "onFailure: "+t.getMessage());
                ApplicationHandler.toast(t.getMessage());
            }
        });

    }

    private void setDataInFields(){
        etUserNameBecomeFreelancer.setText(Constants.k_becomeFreelanceDetails.getUserMainInfo().getUsername());

        if (Constants.k_becomeFreelanceDetails.getCountries()!=null){
            for (int i = 0; i < Constants.k_becomeFreelanceDetails.getCountries().size(); i++) {
                countryList.add(Constants.k_becomeFreelanceDetails.getCountries().get(i).getName());
            }
            countryList.add("Select Country");
        }
        Collections.reverse(countryList);

        arrayAdapter(countryList, spinnerCountryBecomeFreelancer);

        if (Constants.k_becomeFreelanceDetails.getCatoptions()!=null){
            setCategorySpinner(Constants.k_becomeFreelanceDetails.getCatoptions());
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_become_freelancer_one);

        init();

        findViewById(R.id.ivBackBecomeFreelancerOne).setOnClickListener(view-> onBackPressed());

    }

    private void checkValues(){
        userName = ApplicationHandler.textViewGetText(etUserNameBecomeFreelancer);
        freelancerTitle = ApplicationHandler.stringConverter(etFreelancerTitleBecomeFreelancer);
        additionalInfo = ApplicationHandler.stringConverter(etAdditionalInfoBecomeFreelancer);

        if (TextUtils.isEmpty(userName)){
            etUserNameBecomeFreelancer.setError(getResources().getString(R.string.editTextEmptyFieldError));
            etUserNameBecomeFreelancer.requestFocus();
            return;
        } else {
            etUserNameBecomeFreelancer.setError(null);
        }

        if (TextUtils.isEmpty(freelancerTitle)){
            etFreelancerTitleBecomeFreelancer.setError(getResources().getString(R.string.editTextEmptyFieldError));
            etFreelancerTitleBecomeFreelancer.requestFocus();
            return;
        } else {
            etFreelancerTitleBecomeFreelancer.setError(null);
        }

        if (TextUtils.isEmpty(additionalInfo)){
            etAdditionalInfoBecomeFreelancer.setError(getResources().getString(R.string.editTextEmptyFieldError));
            etAdditionalInfoBecomeFreelancer.requestFocus();
            return;
        } else {
            etAdditionalInfoBecomeFreelancer.setError(null);
        }

        if (getCity==null){
            ApplicationHandler.toast("Please select Country->State->City");
            return;
        }

        if (cat_id==null){
            ApplicationHandler.toast("Please select category");
            return;
        }

        ApplicationHandler.intent(BecomeFreelancerTwo.class);
        Intent intent = new Intent(this, BecomeFreelancerTwo.class);
        intent.putExtra("username", userName);
        intent.putExtra("freelancerTitle", freelancerTitle);
        intent.putExtra("description", additionalInfo);
        intent.putExtra("cat_id", cat_id);
        intent.putExtra("city", cityId);
        intent.putExtra("state", stateId);
        intent.putExtra("country", countryId);
        startActivity(intent);

    }

    private void arrayAdapter(ArrayList<String> array, AppCompatSpinner spinner)
    {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, array);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        getCountrySpinnerVal();
        getStateSpinnerVal();
        getCitySpinnerVal();

    }

    private void setCategorySpinner(List<Catoption> list){
        ArrayList<String> categoriesList = new ArrayList<>();
        categoriesList.add("Select Category");
        for (int i = 0; i < list.size(); i++) {
            categoriesList.add(list.get(i).getTitle());
        }
        arrayAdapter(categoriesList, spinnerCategoryBecomeFreelancer);

        spinnerCategoryBecomeFreelancer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                List<Subcat> subList;
                llSubCatLayoutBecomeFreelancer.setVisibility(View.GONE);
                llSubCatChildLayoutBecomeFreelancer.setVisibility(View.GONE);
                cat_id = null;
                for (int j = 0; j < list.size(); j++) {
                    if (list.get(j).getTitle().equals(spinnerCategoryBecomeFreelancer.getSelectedItem().toString())) {
                        if (list.get(j).getSubcat() != null) {
                            subList = list.get(j).getSubcat();
                            setSubList(subList);
                        } else {
                            spinnerSubCategoryBecomeFreelancer.setVisibility(View.GONE);
                            spinnerSubCategoryChildBecomeFreelancer.setVisibility(View.GONE);
                            cat_id = list.get(j).getCatId();
                            cat_level = "1";
                            Log.e(TAG, "onItemSelected: " + cat_id);
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void setSubList(List<Subcat> list)
    {
        ArrayList<String> subCategoryList = new ArrayList<>();
        subCategoryList.add("Select Subcategory");

        if (list != null) {
            for (int i = 0; i < list.size(); i++) {
                subCategoryList.add(list.get(i).getTitle());
            }
            llSubCatLayoutBecomeFreelancer.setVisibility(View.VISIBLE);
            arrayAdapter(subCategoryList, spinnerSubCategoryBecomeFreelancer);

            spinnerSubCategoryBecomeFreelancer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    cat_id = null;
                    List<SubcatChild> subList;
                    for (int j = 0; j < list.size(); j++) {
                        if (list.get(j).getTitle().equals(spinnerSubCategoryBecomeFreelancer.getSelectedItem().toString())) {
                            if (list.get(j).getSubcatChild() != null) {
                                subList = list.get(j).getSubcatChild();
                                setSubCatChildList(subList);
                            } else {
                                llSubCatChildLayoutBecomeFreelancer.setVisibility(View.GONE);
                                cat_id = list.get(j).getId();
                                cat_level = "2";
                                Log.e(TAG, "onItemSelected: " + cat_id);
                            }
                        }
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

        }

    }

    private void setSubCatChildList(final List<SubcatChild> list)
    {
        ArrayList<String> subCategoryList = new ArrayList<>();
        subCategoryList.add("Select Subcategory Child");
        if (list != null) {
            for (int i = 0; i < list.size(); i++) {
                subCategoryList.add(list.get(i).getTitle());
            }
            llSubCatChildLayoutBecomeFreelancer.setVisibility(View.VISIBLE);
            arrayAdapter(subCategoryList, spinnerSubCategoryChildBecomeFreelancer);
        }

        spinnerSubCategoryChildBecomeFreelancer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                cat_id = null;
                if (list != null) {
                    for (int j = 0; j < list.size(); j++) {
                        if (list.get(j).getTitle().equals(spinnerSubCategoryChildBecomeFreelancer.getSelectedItem().toString())) {
                            cat_id = list.get(j).getId();
                            cat_level = "3";
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private void getCitySpinnerVal(){
        spinnerCityBecomeFreelancer.setOnTouchListener((view, motionEvent) -> {

            spinnerCityBecomeFreelancer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    getCity = spinnerCityBecomeFreelancer.getSelectedItem().toString();

                    for (int j = 0; j < getCitiesList.size(); j++) {
                        if (getCity.equals(getCitiesList.get(j).getName())){
                            cityId = getCitiesList.get(j).getId();
                        }
                    }

                    Log.e(TAG, "onItemSelected: "+cityId);

                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            return false;
        });
    }

    private void getStateSpinnerVal(){
        spinnerStateBecomeFreelancer.setOnTouchListener((view, motionEvent) -> {

            spinnerStateBecomeFreelancer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    getState = spinnerStateBecomeFreelancer.getSelectedItem().toString();
                    Log.e(TAG, "onItemSelected: "+getState);

                    for (int j = 0; j < getStates.size(); j++) {
                        if (getState.equals(getStates.get(j).getName())){
                            stateId = getStates.get(j).getId();
                        }
                    }

                    Log.e(TAG, "onItemSelected: "+stateId);
                    if (stateId!=null){
                        getCityList(stateId);
                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            return false;
        });
    }

    private void getCountrySpinnerVal(){

        spinnerCountryBecomeFreelancer.setOnTouchListener((view, motionEvent) -> {

            spinnerCountryBecomeFreelancer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    getCountry = spinnerCountryBecomeFreelancer.getSelectedItem().toString();
                    Log.e(TAG, "onItemSelected: "+getCountry);

                    for (int j = 0; j < Constants.k_becomeFreelanceDetails.getCountries().size(); j++) {
                        if (getCountry.equals(Constants.k_becomeFreelanceDetails.getCountries().get(j).getName())){
                            countryId = Constants.k_becomeFreelanceDetails.getCountries().get(j).getId();
                        }
                    }
                    Log.e(TAG, "onItemSelected: "+countryId);
                    if (countryId!=null){
                        getStates(countryId);
                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            return false;
        });

    }

    private void getCityList(String id){
        Loading.show(this, false, Constants.k_PLEASE_WAIT);
        Call<List<GetStates>> getStatesCall = RestApi.getService().getState(
                sessionManager.getString(Constants.k_ACCESSTOKEN),
                "2",
                id
        );

        getStatesCall.enqueue(new Callback<List<GetStates>>() {
            @Override
            public void onResponse(@NonNull Call<List<GetStates>> call,@NonNull Response<List<GetStates>> response) {
                if (response.isSuccessful()){
                    Loading.cancel();
                    getCitiesList = response.body();

                    if (getCitiesList!=null){
                        ArrayList<String> city = new ArrayList<>();
                        for (int i = 0; i < getCitiesList.size(); i++) {
                            city.add(getCitiesList.get(i).getName());
                        }
                        city.add("Select City");
                        spinnerCityBecomeFreelancer.setVisibility(View.VISIBLE);
                        Collections.reverse(city);
                        arrayAdapter(city, spinnerCityBecomeFreelancer);
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<List<GetStates>> call,@NonNull Throwable t) {
                Loading.cancel();
                Log.e(TAG, "onFailure: "+t.getMessage());
                ApplicationHandler.toast(t.getMessage());
            }
        });
    }

    private void getStates(String id){
        Loading.show(this, false, Constants.k_PLEASE_WAIT);
        Call<List<GetStates>> getStatesCall = RestApi.getService().getState(
                sessionManager.getString(Constants.k_ACCESSTOKEN),
                "1",
                id
        );

        getStatesCall.enqueue(new Callback<List<GetStates>>() {
            @Override
            public void onResponse(@NonNull Call<List<GetStates>> call,@NonNull Response<List<GetStates>> response) {
                if (response.isSuccessful()){
                    Loading.cancel();
                    getStates = response.body();

                    if (getStates!=null){
                        ArrayList<String> state = new ArrayList<>();
                        for (int i = 0; i < getStates.size(); i++) {
                            state.add(getStates.get(i).getName());
                        }
                        state.add("Select State");
                        spinnerStateBecomeFreelancer.setVisibility(View.VISIBLE);
                        Collections.reverse(state);
                        arrayAdapter(state, spinnerStateBecomeFreelancer);
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<List<GetStates>> call,@NonNull Throwable t) {
                Loading.cancel();
                Log.e(TAG, "onFailure: "+t.getMessage());
                ApplicationHandler.toast(t.getMessage());
            }
        });

    }

    @Override
    public void onBackPressed() {
        ApplicationHandler.intent(HomeActivity.class);
    }

}
