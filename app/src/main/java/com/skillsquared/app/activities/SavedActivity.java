package com.skillsquared.app.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.skillsquared.app.R;
import com.skillsquared.app.adapters.SavedGigsAdapter;
import com.skillsquared.app.handler.ApplicationHandler;
import com.skillsquared.app.handler.RestApi;
import com.skillsquared.app.models.GetFavServiceModel;
import com.skillsquared.app.utils.Constants;
import com.skillsquared.app.utils.Loading;
import com.skillsquared.app.utils.SessionManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SavedActivity extends AppCompatActivity {

    private final static String TAG = SavedActivity.class.getSimpleName();
    RecyclerView recyclerSaved;
    SessionManager sessionManager;
    TextView tvNoDataSaved;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ApplicationHandler.hideStatusBar(this);
        setContentView(R.layout.activity_saved);

        recyclerSaved = findViewById(R.id.recyclerSaved);
        tvNoDataSaved = findViewById(R.id.tvNoDataSaved);
        sessionManager = new SessionManager(this);

        findViewById(R.id.ivBackSaved).setOnClickListener(view -> onBackPressed());

        makeServerCall();

    }

    private void makeServerCall() {
        Loading.show(this, false, Constants.k_PLEASE_WAIT);
        Call<GetFavServiceModel> getService = RestApi.getService().getFavoriteServices(
                sessionManager.getString(Constants.k_ACCESSTOKEN)
        );

        getService.enqueue(new Callback<GetFavServiceModel>() {
            @Override
            public void onResponse(@NonNull Call<GetFavServiceModel> call, @NonNull Response<GetFavServiceModel> response) {
                Loading.cancel();
                if (response.isSuccessful()) {
                    tvNoDataSaved.setVisibility(View.GONE);
                    GetFavServiceModel getFavServiceModel = response.body();
                    if (getFavServiceModel != null) {
                        if (getFavServiceModel.getFavouriteServices() != null) {
                            SavedGigsAdapter gigsAdapter;
                            gigsAdapter = new SavedGigsAdapter(
                                    SavedActivity.this,
                                    getFavServiceModel.getFavouriteServices()
                            );
                            LinearLayoutManager linearLayoutManager =
                                    new LinearLayoutManager(
                                            getApplicationContext(),
                                            LinearLayoutManager.VERTICAL,
                                            false
                                    );
                            recyclerSaved.setLayoutManager(linearLayoutManager);
                            recyclerSaved.setAdapter(gigsAdapter);
                        } else {
                            tvNoDataSaved.setVisibility(View.VISIBLE);
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<GetFavServiceModel> call, @NonNull Throwable t) {
                Loading.cancel();
                Log.e(TAG, "onFailure: " + t.getMessage());
                ApplicationHandler.toast(t.getMessage());
                tvNoDataSaved.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onBackPressed() {
        ApplicationHandler.intent(HomeActivity.class);
    }
}
