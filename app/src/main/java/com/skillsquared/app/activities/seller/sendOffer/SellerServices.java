package com.skillsquared.app.activities.seller.sendOffer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.skillsquared.app.R;
import com.skillsquared.app.activities.seller.ManageServicesActivity;
import com.skillsquared.app.adapters.ManageServiceAdapter;
import com.skillsquared.app.adapters.SellerSendServicesAdapter;
import com.skillsquared.app.handler.ApplicationHandler;
import com.skillsquared.app.utils.Constants;

public class SellerServices extends AppCompatActivity {

    private final static String TAG = SellerServices.class.getSimpleName();
    RecyclerView recyclerViewUserServices;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seller_services);

        recyclerViewUserServices = findViewById(R.id.recyclerViewUserServices);

        if (Constants.k_SellerservicesList != null){
            SellerSendServicesAdapter viewOffersAdapter =
                    new SellerSendServicesAdapter(SellerServices.this, Constants.k_SellerservicesList);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL,false);
            recyclerViewUserServices.setLayoutManager(linearLayoutManager);
            recyclerViewUserServices.setAdapter(viewOffersAdapter);
        } else {
            ApplicationHandler.toast(getResources().getString(R.string.no_data_to_display));
        }

    }
}
