package com.skillsquared.app.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import com.skillsquared.app.R;
import com.skillsquared.app.adapters.GigsAdapter;
import com.skillsquared.app.adapters.NotificationAdapter;
import com.skillsquared.app.handler.ApplicationHandler;
import com.skillsquared.app.handler.RestApi;
import com.skillsquared.app.models.NotificationResponse;
import com.skillsquared.app.utils.Constants;
import com.skillsquared.app.utils.Loading;
import com.skillsquared.app.utils.SessionManager;

import org.jetbrains.annotations.NotNull;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationsActivity extends AppCompatActivity {

    private final static String TAG = NotificationsActivity.class.getSimpleName();
    RecyclerView recyclerNotifications;
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);

        recyclerNotifications = findViewById(R.id.recyclerNotifications);
        sessionManager = new SessionManager(this);

        Loading.show(this, false, Constants.k_PLEASE_WAIT);

        Call<NotificationResponse> call = RestApi.getService().getNotifications(
                sessionManager.getString(Constants.k_ACCESSTOKEN)
        );

        call.enqueue(new Callback<NotificationResponse>() {
            @Override
            public void onResponse(@NotNull Call<NotificationResponse> call, @NotNull Response<NotificationResponse> response) {
                Loading.cancel();
                if (response.isSuccessful()){
                    NotificationResponse notificationResponse = response.body();

                    if (notificationResponse!=null){
                        if (notificationResponse.getNotifications()!=null){
                            NotificationAdapter notificationAdapter = new NotificationAdapter(NotificationsActivity.this, notificationResponse.getNotifications());
                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
                            recyclerNotifications.setLayoutManager(linearLayoutManager);
                            recyclerNotifications.setAdapter(notificationAdapter);
                        } else {
                            ApplicationHandler.toast(getResources().getString(R.string.no_data_to_display));
                        }
                    } else {
                        ApplicationHandler.toast(getResources().getString(R.string.no_data_to_display));
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<NotificationResponse> call, @NotNull Throwable t) {
                Loading.cancel();
                Log.e(TAG, "onFailure: "+t.getMessage());
                ApplicationHandler.toast(t.getMessage());
            }
        });

    }
}
