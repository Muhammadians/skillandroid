package com.skillsquared.app.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cometchat.pro.constants.CometChatConstants;
import com.cometchat.pro.core.CometChat;
import com.cometchat.pro.exceptions.CometChatException;
import com.cometchat.pro.models.User;
import com.skillsquared.app.R;
import com.skillsquared.app.cometChat.Activity.OneToOneChatActivity;
import com.skillsquared.app.cometChat.Contracts.OneToOneActivityContract;
import com.skillsquared.app.cometChat.Contracts.StringContract;
import com.skillsquared.app.cometChat.Presenters.OneToOneActivityPresenter;
import com.skillsquared.app.cometChat.Utils.CommonUtils;
import com.skillsquared.app.handler.ApplicationHandler;
import com.skillsquared.app.utils.Constants;
import com.skillsquared.app.utils.Loading;
import com.skillsquared.app.utils.SessionManager;
import com.squareup.picasso.Picasso;

public class FeaturedGigsDetail extends AppCompatActivity {

    private final static String TAG = FeaturedGigsDetail.class.getSimpleName();
    ImageView ivFeaturedGigs, ivUserProfileFeaturedGigs, ivCallFreelancer, ivMessageFreelancer;
    TextView tvUserNameFeaturedGigs, tvFeaturedGigsTitle,
            tvFeaturedGigsDescription, tvFeaturedGigCategoryName, tvPriceFeaturedGigs;

    private String UID, cometChatUserName;
    private String receiverID;
    private OneToOneActivityContract.OneToOnePresenter oneToOnePresenter;
    private SessionManager sessionManager;

    private void init(){
        ivFeaturedGigs = findViewById(R.id.ivFeaturedGigs);
        ivUserProfileFeaturedGigs = findViewById(R.id.ivUserProfileFeaturedGigs);
        tvUserNameFeaturedGigs = findViewById(R.id.tvUserNameFeaturedGigs);
        tvFeaturedGigsTitle = findViewById(R.id.tvFeaturedGigsTitle);
        tvFeaturedGigsDescription = findViewById(R.id.tvFeaturedGigsDescription);
        tvFeaturedGigCategoryName = findViewById(R.id.tvFeaturedGigCategoryName);
        tvPriceFeaturedGigs = findViewById(R.id.tvPriceFeaturedGigs);
        ivCallFreelancer = findViewById(R.id.ivCallFreelancer);
        ivMessageFreelancer = findViewById(R.id.ivMessageFreelancer);

        oneToOnePresenter = new OneToOneActivityPresenter();
        sessionManager = new SessionManager(this);

        if (Constants.k_FeaturedGigs != null){

            Picasso.get()
                    .load(Constants.k_FeaturedGigs.getMedia())
                    .placeholder(R.drawable.noimg)
                    .into(ivFeaturedGigs);

            Picasso.get()
                    .load(Constants.k_FeaturedGigs.getUserImage())
                    .placeholder(R.drawable.noimg)
                    .into(ivUserProfileFeaturedGigs);

            String des = Html.fromHtml(Constants.k_FeaturedGigs.getDescription()).toString();

            tvUserNameFeaturedGigs.setText(Constants.k_FeaturedGigs.getServiceUsername());
            tvFeaturedGigsTitle.setText(Constants.k_FeaturedGigs.getServiceTitle());
            tvFeaturedGigsDescription.setText(des);
            tvFeaturedGigCategoryName.setText(Constants.k_FeaturedGigs.getCategoryName());
            tvPriceFeaturedGigs.setText(Constants.k_FeaturedGigs.getPrice());

            ivCallFreelancer.setOnClickListener(view -> {
                receiverID = "11"+Constants.k_FeaturedGigs.getUsername();
                cometChatUserName = Constants.k_FeaturedGigs.getServiceUsername();
                makeCall();
            });

            ivMessageFreelancer.setOnClickListener(view -> {
                UID = "11"+Constants.k_FeaturedGigs.getUsername();
                cometChatUserName = Constants.k_FeaturedGigs.getServiceUsername();
                getCometChatUser();
            });

//            Constants.k_FeaturedGigs = null;

        } else {
            ApplicationHandler.toast(getResources().getString(R.string.no_data_to_display));
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ApplicationHandler.hideStatusBar(this);
        setContentView(R.layout.activity_featured_gigs_detail);

        init();

        findViewById(R.id.ivBackFeaturedGigs).setOnClickListener(view->onBackPressed());

    }

    private void getCometChatUser(){
        Loading.show(FeaturedGigsDetail.this, false, Constants.k_PLEASE_WAIT);
        CometChat.getUser(UID, new CometChat.CallbackListener<User>() {
            @Override
            public void onSuccess(User user) {
                Loading.cancel();
                Log.d(TAG, "User details fetched for user: " + user.toString());

                String contactID = user.getUid();
                String contactName = user.getName();
                String userAvatar = user.getAvatar();

                Intent intent = new Intent(FeaturedGigsDetail.this, OneToOneChatActivity.class);
                intent.putExtra(StringContract.IntentStrings.USER_ID, contactID);
                intent.putExtra(StringContract.IntentStrings.USER_AVATAR, userAvatar);
                intent.putExtra(StringContract.IntentStrings.USER_NAME, contactName);
                startActivity(intent);
            }

            @Override
            public void onError(CometChatException e) {
                Log.d(TAG, "User details fetching failed with exception: " + e.getMessage());
                ApplicationHandler.cometChatCreateUser(UID, cometChatUserName, FeaturedGigsDetail.this, "Inbox");
            }
        });
    }

    private void makeCall(){
        Loading.show(FeaturedGigsDetail.this, false, Constants.k_PLEASE_WAIT);
        com.cometchat.pro.core.Call call = new com.cometchat.pro.core.Call(receiverID, CometChatConstants.RECEIVER_TYPE_USER, CometChatConstants.CALL_TYPE_AUDIO);
        CometChat.initiateCall(call, new CometChat.CallbackListener<com.cometchat.pro.core.Call>() {
            @Override
            public void onSuccess(com.cometchat.pro.core.Call call) {
                Loading.cancel();
                CommonUtils.startCallIntent(FeaturedGigsDetail.this, ((User) call.getCallReceiver()), call.getType(), true, call.getSessionId());
            }

            @Override
            public void onError(CometChatException e) {
                ApplicationHandler.cometChatCreateUser(receiverID, cometChatUserName, FeaturedGigsDetail.this);
                Log.e(TAG, "onError: "+e.getMessage());
            }

        });

    }

}
