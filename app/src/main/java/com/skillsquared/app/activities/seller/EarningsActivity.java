package com.skillsquared.app.activities.seller;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.skillsquared.app.R;
import com.skillsquared.app.activities.HomeActivity;
import com.skillsquared.app.handler.ApplicationHandler;
import com.skillsquared.app.handler.RestApi;
import com.skillsquared.app.models.EarningsModel;
import com.skillsquared.app.models.signUpModels.SignUpModel;
import com.skillsquared.app.utils.Constants;
import com.skillsquared.app.utils.Loading;
import com.skillsquared.app.utils.SessionManager;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EarningsActivity extends AppCompatActivity {

    private final static String TAG = "Earnings Activity";
    TextView tvNetIncome, tvEarnedBonus,
            tvWithdrawn, tvAvailableForWithdraw;
    private SessionManager sessionManager;
    View view;
    BottomSheetDialog bottomSheetDialog;
    CardView cardViewPayStackEarning, cardViewPaypalEarning, cardViewPayoneerEarning;

    private void init()
    {
        tvNetIncome = findViewById(R.id.tvNetIncomeEarnings);
        tvEarnedBonus = findViewById(R.id.tvEarnedBonusEarnings);
        tvWithdrawn = findViewById(R.id.tvWithdrawnEarnings);
        tvAvailableForWithdraw = findViewById(R.id.tvAvailableForWithdrawEarnings);
        cardViewPayStackEarning = findViewById(R.id.cardViewPayStackEarning);
        cardViewPaypalEarning = findViewById(R.id.cardViewPaypalEarning);
        cardViewPayoneerEarning = findViewById(R.id.cardViewPayoneerEarning);
        sessionManager = new SessionManager(this);

        cardViewPayoneerEarning.setOnClickListener(view->ApplicationHandler.toast("We Are Working On It. This Will Be Available Soon."));

        getDataFromServer();

    }

    private void getDataFromServer()
    {
        Loading.show(this, false, Constants.k_PLEASE_WAIT);
        Call<EarningsModel> earningsModelCall = RestApi.getService().getEarnings(
                sessionManager.getString(Constants.k_ACCESSTOKEN)
        );

        earningsModelCall.enqueue(new Callback<EarningsModel>() {
            @Override
            public void onResponse(@NonNull Call<EarningsModel> call,@NonNull Response<EarningsModel> response) {
                Loading.cancel();
                if (response.isSuccessful()){
                    EarningsModel earningsModel = response.body();
                    if (earningsModel != null) {
                        tvNetIncome.setText("$"+earningsModel.getSellerEarnings());
                        tvAvailableForWithdraw.setText("$"+earningsModel.getAvgSellingPrice());
                        tvEarnedBonus.setText("$"+earningsModel.getSellerEarningsCurrentMonth());
                        tvWithdrawn.setText("$"+earningsModel.getTotalCompletedOrders());

                        cardViewPaypalEarning.setOnClickListener(view1 -> showBottomDialog("paypal", earningsModel.getSellerEarnings()));

                        cardViewPayStackEarning.setOnClickListener(view1 -> {
                            if (earningsModel.getPaystack()!=null){
                                showBottomDialog("paystack", earningsModel.getSellerEarnings());
                            } else {
                                ApplicationHandler.intent(BankDetails.class);
                            }
                        });

                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<EarningsModel> call,@NonNull Throwable t) {
                Loading.cancel();
                ApplicationHandler.toast(t.getMessage());
                Log.e(TAG, "onFailure: "+t.getMessage());
            }
        });

    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        ApplicationHandler.hideStatusBar(this);
        setContentView(R.layout.activity_earnings);

        init();

        ImageView ivBackEarnings = findViewById(R.id.ivBackEarnings);
        ivBackEarnings.setOnClickListener(view -> onBackPressed());

    }

    @Override
    public void onBackPressed() {
        ApplicationHandler.intent(WhatToDoActivitySeller.class);
    }

    void showBottomDialog(String title, double availableLimit){
        view = LayoutInflater.from(this).inflate(R.layout.withdrawl_amount, null);

        if (bottomSheetDialog == null) {
            bottomSheetDialog = new BottomSheetDialog(this);
            bottomSheetDialog.setContentView(view);
        }

        EditText etValidEmail = view.findViewById(R.id.etValidEmail);
        EditText etPriceWithdraw = view.findViewById(R.id.etPriceWithdraw);
        Button btnRequestWithdraw = view.findViewById(R.id.btnRequestWithdraw);

        etValidEmail.setText(sessionManager.getString(Constants.k_EMAIL));

        Log.e(TAG, "showBottomDialog: "+availableLimit);

        btnRequestWithdraw.setOnClickListener(view1 -> checkValues(etValidEmail, etPriceWithdraw, availableLimit));

        bottomSheetDialog.show();
    }

    private void checkValues(EditText getEmail, EditText getAmount, double limit){

        String email = ApplicationHandler.stringConverter(getEmail);
        String amount = ApplicationHandler.stringConverter(getAmount);

        if (TextUtils.isEmpty(email)){
            getEmail.setError(getResources().getString(R.string.editTextEmptyFieldError));
            getEmail.requestFocus();
            return;
        } else {
            getEmail.setError(null);
        }

        if (TextUtils.isEmpty(amount)){
            getAmount.setError(getResources().getString(R.string.editTextEmptyFieldError));
            getAmount.requestFocus();
            return;
        } else {
            getAmount.setError(null);

            if (Double.parseDouble(amount)<=limit && limit != 0){
                sendPaystackWithdrawRequest(email, amount);
            } else {
                ApplicationHandler.toast("Requested Amount Is Not Available");
            }

        }

    }

    private void sendPaystackWithdrawRequest(String email, String amount){
        Loading.show(this, false, Constants.k_PLEASE_WAIT);

        Call<SignUpModel> sendPaystackRequest = RestApi.getService().sendPaystackWithdrawRequest(
                sessionManager.getString(Constants.k_ACCESSTOKEN),
                email,
                amount
        );

        sendPaystackRequest.enqueue(new Callback<SignUpModel>() {
            @Override
            public void onResponse(@NonNull Call<SignUpModel> call,@NonNull Response<SignUpModel> response) {
                Loading.cancel();

                if (response.isSuccessful()){
                    SignUpModel signUpModel = response.body();

                    if (signUpModel!=null){
                        ApplicationHandler.toast(signUpModel.getMessage());
                        bottomSheetDialog.cancel();
                    }

                }

            }

            @Override
            public void onFailure(@NonNull Call<SignUpModel> call,@NonNull Throwable t) {
                Loading.cancel();
                Log.e(TAG, "onFailure: "+t.getMessage());
                ApplicationHandler.toast(t.getMessage());
            }
        });

    }

}
