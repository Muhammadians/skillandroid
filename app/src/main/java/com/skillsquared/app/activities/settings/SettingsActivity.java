package com.skillsquared.app.activities.settings;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.cometchat.pro.constants.CometChatConstants;
import com.cometchat.pro.core.CometChat;
import com.cometchat.pro.exceptions.CometChatException;
import com.cometchat.pro.models.User;
import com.google.firebase.messaging.FirebaseMessaging;
import com.skillsquared.app.R;
import com.skillsquared.app.activities.EditProfileActivity;
import com.skillsquared.app.activities.HomeActivity;
import com.skillsquared.app.activities.SignIn;
import com.skillsquared.app.cometChat.Contracts.StringContract;
import com.skillsquared.app.handler.ApplicationHandler;
import com.skillsquared.app.utils.Constants;
import com.skillsquared.app.utils.Loading;
import com.skillsquared.app.utils.SessionManager;

public class SettingsActivity extends AppCompatActivity {

    private final static String TAG = SettingsActivity.class.getSimpleName();
    private TextView tvNotification, tvNotificationSound, tvTerms,
                        tvPrivacy, tvLogout;
    private SessionManager sessionManager;
    private ImageView ivBackSettings;

    private void init(){
//        tvNotification = findViewById(R.id.tvNotifications);
//        tvNotificationSound = findViewById(R.id.tvNotificationSound);
        tvTerms = findViewById(R.id.tvTerms);
        tvPrivacy = findViewById(R.id.tvPrivacy);
        tvLogout = findViewById(R.id.tvLogout);
        ivBackSettings = findViewById(R.id.ivBackSettings);
        sessionManager = new SessionManager(this);

        setOnClickListeners();

    }

    private void setOnClickListeners(){
        /*tvNotification.setOnClickListener(view -> ApplicationHandler.toast("In Process..."));

        tvNotificationSound.setOnClickListener(view -> ApplicationHandler.toast("In Process..."));*/

        tvTerms.setOnClickListener(view -> ApplicationHandler.intent(TermsPrivacy.class, "activity", "terms"));

        tvPrivacy.setOnClickListener(view -> ApplicationHandler.intent(TermsPrivacy.class, "activity", "privacy"));

        tvLogout.setOnClickListener(view-> ApplicationHandler.intent(EditProfileActivity.class));

        ivBackSettings.setOnClickListener(view -> ApplicationHandler.intent(HomeActivity.class));

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ApplicationHandler.hideStatusBar(this);
        setContentView(R.layout.activity_settings);

        init();

    }

    @Override
    public void onBackPressed() {
        ApplicationHandler.intent(HomeActivity.class);
    }
}
