package com.skillsquared.app.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.hbb20.CountryCodePicker;
import com.skillsquared.app.R;
import com.skillsquared.app.handler.ApplicationHandler;
import com.skillsquared.app.handler.RestApi;
import com.skillsquared.app.models.signUpModels.SignUpModel;
import com.skillsquared.app.utils.Constants;
import com.skillsquared.app.utils.Loading;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUp extends AppCompatActivity {

    private static final String TAG = SignUp.class.getSimpleName();
    AutoCompleteTextView etFullNameSignup, etEmailId, etPhoneNumber;
    EditText etPassword, etConfirmPassword;
    CardView cardViewSignUp;
    CountryCodePicker ccp;
    TimeZone timeZone;
    String token;
    TextView tvSignIn;
    CheckBox cbFreelancer;
    int becomeFreelancer;

    void init() {
        etFullNameSignup = findViewById(R.id.etFullNameSignup);
        etEmailId = findViewById(R.id.etEmailId);
        etPhoneNumber = findViewById(R.id.etPhoneNumber);
        etPassword = findViewById(R.id.etPassword);
        etConfirmPassword = findViewById(R.id.etConfirmPassword);
        cardViewSignUp = findViewById(R.id.cardViewSignUp);
        ccp = findViewById(R.id.ccp);
        tvSignIn = findViewById(R.id.tvSignIn);
        timeZone = TimeZone.getDefault();
        cbFreelancer = findViewById(R.id.cbFreelancer);

        cardViewSignUp.setOnClickListener(view -> {
            token = FirebaseInstanceId.getInstance().getToken();
            checkData();
        });

        tvSignIn.setOnClickListener(view -> ApplicationHandler.intent(SignIn.class));

    }

    private String checkNumber(String number) {

        String finalNumber;

        if (!TextUtils.isEmpty(number) && number.charAt(0) == '0') {
            finalNumber = number.substring(1);
        } else {
            finalNumber = number;
        }

        return finalNumber;
    }

    void checkData() {

        String fullName, userEmail, phoneNumber, password, confirmPassword, userTimeZone;

        fullName = ApplicationHandler.stringConverter(etFullNameSignup);
        userEmail = ApplicationHandler.stringConverter(etEmailId);
        phoneNumber = ApplicationHandler.stringConverter(etPhoneNumber);
        password = ApplicationHandler.stringConverter(etPassword);
        confirmPassword = ApplicationHandler.stringConverter(etConfirmPassword);
        userTimeZone = timeZone.getID();

        if (TextUtils.isEmpty(fullName)) {
            etFullNameSignup.setError(getResources().getString(R.string.editTextEmptyFieldError));
            etFullNameSignup.requestFocus();
            return;
        } else {
            etFullNameSignup.setError(null);
        }

        if (TextUtils.isEmpty(userEmail)) {
            etEmailId.setError(getResources().getString(R.string.editTextEmptyFieldError));
            etEmailId.requestFocus();
            return;
        } else {
            if (ApplicationHandler.isEmailValid(userEmail)){
                etEmailId.setError(getResources().getString(R.string.emailFormatError));
                etEmailId.requestFocus();
                return;
            } else {
                etEmailId.setError(null);
            }
        }

        if (TextUtils.isEmpty(phoneNumber)) {
            etPhoneNumber.setError(getResources().getString(R.string.editTextEmptyFieldError));
            etPhoneNumber.requestFocus();
            return;
        } else {
            etPhoneNumber.setError(null);
        }

        if (TextUtils.isEmpty(password)) {
            etPassword.setError(getResources().getString(R.string.editTextEmptyFieldError));
            etPassword.requestFocus();
            return;
        } else {
            if (password.length() < 8) {
                etPassword.setError(getResources().getString(R.string.passwordShortLength));
                etPassword.requestFocus();
                return;
            } else {
                etPassword.setError(null);
            }
        }

        if (TextUtils.isEmpty(confirmPassword)) {
            etConfirmPassword.setError(getResources().getString(R.string.editTextEmptyFieldError));
            etConfirmPassword.requestFocus();
            return;
        } else {
            if (!password.equals(confirmPassword)) {
                etConfirmPassword.setError(getResources().getString(R.string.passwordMismatch));
                etConfirmPassword.requestFocus();
                return;
            } else {
                etConfirmPassword.setError(null);
            }
        }

        if (cbFreelancer.isChecked()){
            becomeFreelancer = 1;
        } else {
            becomeFreelancer = 0;
        }

        Log.e(TAG, "full name: " + fullName);
        Log.e(TAG, "user email: " + userEmail);
        Log.e(TAG, "phone number: " + ccp.getSelectedCountryCodeWithPlus() + checkNumber(ApplicationHandler.stringConverter(etPhoneNumber)));
        Log.e(TAG, "password: " + password);
        Log.e(TAG, "confirm pass: " + confirmPassword);
        Log.e(TAG, "time zone: " + userTimeZone);
        Log.e(TAG, "token: " + token);
        Log.e(TAG, "becomeFreelancer: " + becomeFreelancer);

        makeServerCall(
                fullName,
                userEmail.toLowerCase().replaceAll(" ", ""),
                ccp.getSelectedCountryCodeWithPlus() + checkNumber(ApplicationHandler.stringConverter(etPhoneNumber)),
                password,
                userTimeZone,
                becomeFreelancer
        );

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        init();

    }

    void makeServerCall(String name, String email, String phone, String password, String timeZone, int becomeFreelancer) {
        Loading.show(this, false, Constants.k_PLEASE_WAIT);

        Call<SignUpModel> signUpModelCall = RestApi.getService().signUpCall(
                name,
                email.toLowerCase().replaceAll(" ", ""),
                phone,
                password,
                "android",
                token,
                timeZone,
                becomeFreelancer
        );

        signUpModelCall.enqueue(new Callback<SignUpModel>() {
            @Override
            public void onResponse(@NotNull Call<SignUpModel> call, @NotNull Response<SignUpModel> response) {
                Loading.cancel();

                if (response.isSuccessful()) {
                    SignUpModel signUpModel = response.body();

                    try {
                        if (signUpModel != null) {
                            ApplicationHandler.toast(signUpModel.getMessage());
                            if (signUpModel.getStatus().equals(200)){
                                ApplicationHandler.intent(SignIn.class);
                            }
                        }
                    } catch (Exception e) {
                        Log.e("server error", Objects.requireNonNull(e.getMessage()));
                        ApplicationHandler.toast(e.getMessage());
                    }

                }
            }

            @Override
            public void onFailure(@NotNull Call<SignUpModel> call, @NotNull Throwable t) {
                Loading.cancel();

                Log.e(TAG, "onFailure: " + t.getMessage());
                ApplicationHandler.toast(t.getMessage());

            }
        });

    }

}
