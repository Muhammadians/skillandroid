package com.skillsquared.app.activities.buyer;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.tabs.TabLayout;
import com.skillsquared.app.R;
import com.skillsquared.app.fragments.CompletedSalesFragment;
import com.skillsquared.app.fragments.DeliveredFragment;
import com.skillsquared.app.fragments.DisputedFragment;
import com.skillsquared.app.fragments.RevisionFragment;
import com.skillsquared.app.fragments.manageOrdersFragment.AactiveFragment;
import com.skillsquared.app.fragments.manageOrdersFragment.AcompletedFragment;
import com.skillsquared.app.fragments.manageOrdersFragment.AdisputedFragment;
import com.skillsquared.app.fragments.manageOrdersFragment.AmanageFragment;
import com.skillsquared.app.fragments.manageOrdersFragment.ArevisionFragment;
import com.skillsquared.app.handler.ApplicationHandler;
import com.skillsquared.app.handler.RestApi;
import com.skillsquared.app.models.buyerManageOrders.BuyerManageOrderResponse;
import com.skillsquared.app.models.buyerRequestsSeller.BuyerRequestModel;
import com.skillsquared.app.utils.Constants;
import com.skillsquared.app.utils.Loading;
import com.skillsquared.app.utils.SessionManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ManageOrders extends AppCompatActivity {

    private final static String TAG = ManageOrders.class.getSimpleName();
    private RecyclerView recyclerView;
    private SessionManager sessionManager;
    TextView tvNoDataToDisplayManageOrders;
    FrameLayout fragmentContainer;
    TabLayout tabLayout;
    ImageView ivFilterManageOrders;
    Dialog myDialog;
    RadioButton radioButton, rbBuyerOffer, rbSentOffer;
    RadioGroup rgBuyerRequest, rgPostJobFilter;
    LinearLayout llFilterPostRequest, llBuyerRequestFilter;
    TextView tvSentOffer, tvBuyerRequest;
    String chooseOrdersToDisplay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ApplicationHandler.hideStatusBar(this);
        setContentView(R.layout.activity_manage_orders);

        recyclerView = findViewById(R.id.recyclerViewManageOrders);
        tabLayout = findViewById(R.id.tabLayoutManageOrders);
        fragmentContainer = findViewById(R.id.fragmentContainerManageOrders);
        tvNoDataToDisplayManageOrders = findViewById(R.id.tvNoDataToDisplayManageOrders);
        ivFilterManageOrders = findViewById(R.id.ivFilterManageOrders);
        sessionManager = new SessionManager(this);
        myDialog = new Dialog(this);
        initPopUp();
        chooseOrdersToDisplay = getIntent().getStringExtra("chooseOrder");

        ivFilterManageOrders.setOnClickListener(view->showPopUp());

        findViewById(R.id.ivBackBuyerRequest).setOnClickListener(view->onBackPressed());

        getDataFromServer(chooseOrdersToDisplay);
        setTabListeners();

    }

    void setTabListeners(){
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0){
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainerManageOrders,
                            new AactiveFragment()).commit();
                } else if (tab.getPosition() == 1){
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainerManageOrders,
                            new AmanageFragment()).commit();
                } else if (tab.getPosition() == 2){
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainerManageOrders,
                            new ArevisionFragment()).commit();
                } else if (tab.getPosition() == 3){
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainerManageOrders,
                            new AcompletedFragment()).commit();
                }else {
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainerManageOrders,
                            new AdisputedFragment()).commit();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void getDataFromServer(String param) {

        if (Constants.k_BuyerManageOrders!=null){
/*
            if (param.equals("custom")){
                Constants.k_buyerCustomOrder = true;
            } else {
                Constants.k_buyerCustomOrder = false;
            }*/

            setFragment();

        } else {
            Loading.show(this, false, Constants.k_PLEASE_WAIT);
            Call<BuyerManageOrderResponse> buyerManageOrderResponseCall =
                    RestApi.getService().getBuyerManageOrders(
                            sessionManager.getString(Constants.k_ACCESSTOKEN)
                    );

            buyerManageOrderResponseCall.enqueue(new Callback<BuyerManageOrderResponse>() {
                @Override
                public void onResponse(@NonNull Call<BuyerManageOrderResponse> call, @NonNull Response<BuyerManageOrderResponse> response) {
                    Loading.cancel();
                    if (response.isSuccessful()) {
                        Constants.k_BuyerManageOrders = response.body();
                        setFragment();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<BuyerManageOrderResponse> call, @NonNull Throwable t) {
                    Loading.cancel();
                    tvNoDataToDisplayManageOrders.setVisibility(View.VISIBLE);
                    Log.e(TAG, "onFailure: " + t.getMessage());
                    ApplicationHandler.toast(t.getMessage());
                }
            });
        }
    }

    private void setFragment(){
        if (Constants.k_BuyerManageOrders != null) {
            if (Constants.k_BuyerManageOrders.getBuyerordersmanagement() != null) {
                if (Constants.k_BuyerManageOrders.getBuyerordersmanagement().getAactive() != null) {
                    tvNoDataToDisplayManageOrders.setVisibility(View.GONE);
                    Fragment AactiveFragment = new AactiveFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("chooseOrder", chooseOrdersToDisplay);
                    AactiveFragment.setArguments(bundle);
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fragmentContainerManageOrders,
                            AactiveFragment)
                            .commit();
                }
            }
        }
    }

    void initPopUp(){
        myDialog.setContentView(R.layout.post_a_job_filter_pop_up);
        llFilterPostRequest = myDialog.findViewById(R.id.llFilterPostRequest);
        llBuyerRequestFilter = myDialog.findViewById(R.id.llBuyerRequestFilter);
        rgBuyerRequest = myDialog.findViewById(R.id.rgBuyerRequest);
        rgPostJobFilter = myDialog.findViewById(R.id.rgPostJobFilter);
        rbBuyerOffer = myDialog.findViewById(R.id.rbBuyerRequest);
        rbSentOffer = myDialog.findViewById(R.id.rbSellerSentOffer);
        tvBuyerRequest = myDialog.findViewById(R.id.tvBuyerRequest);
        tvSentOffer = myDialog.findViewById(R.id.tvSentOffer);

        llFilterPostRequest.setVisibility(View.GONE);
        rgPostJobFilter.setVisibility(View.GONE);
        llBuyerRequestFilter.setVisibility(View.VISIBLE);
        rgBuyerRequest.setVisibility(View.VISIBLE);
    }

    void showPopUp() {
        myDialog.show();

        tvBuyerRequest.setText("Normal Orders");
        tvSentOffer.setText("Custom Orders");

        rgBuyerRequest.setOnCheckedChangeListener((radioGroup, i) -> {
            int selectedId = radioGroup.getCheckedRadioButtonId();
            radioButton = myDialog.findViewById(selectedId);

            if (Constants.k_BuyerManageOrders != null){

                if (String.valueOf(radioButton.getText()).equals("Buyer Request")){
                    getDataFromServer("normal");
                } else if (String.valueOf(radioButton.getText()).equals("Sent Offer")){
                    getDataFromServer("custom");
                }
            }

            myDialog.cancel();
        });
    }

}
