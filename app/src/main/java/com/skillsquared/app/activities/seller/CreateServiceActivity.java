package com.skillsquared.app.activities.seller;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.codekidlabs.storagechooser.StorageChooser;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.skillsquared.app.R;
import com.skillsquared.app.handler.ApplicationHandler;
import com.skillsquared.app.handler.RestApi;
import com.skillsquared.app.models.postAJob.PostAJobResponse;
import com.skillsquared.app.utils.Constants;
import com.skillsquared.app.utils.Loading;
import com.skillsquared.app.utils.SessionManager;

import java.io.File;
import java.util.List;
import java.util.Objects;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateServiceActivity extends AppCompatActivity {

    public static final int GALLERY_REQUEST_CODE = 1128;
    private final static String TAG = CreateServiceActivity.class.getSimpleName();
    EditText etPriceCreateService, etDeliveryTimeCreateService,
            etAdditionalInfoCreateService, etServiceBannerCreateService;
//    TextView etServiceBannerCreateService;
    SessionManager sessionManager;
    CardView submitCreateService, serviceBannerCardView;
    String title, description, category, cat_level, price,
            deliveryTime, serviceBanner, additionalInfo, path, priceIntent, deliverTimeIntent,
            additonalInfoIntent, mediaIntent, serviceId;
    String imageFilePath;
    File imageFIle;
    Uri imageUri;

    private void init() {

        sessionManager = new SessionManager(this);

        path = getIntent().getStringExtra("path");

        etPriceCreateService = findViewById(R.id.etPriceCreateService);
        etDeliveryTimeCreateService = findViewById(R.id.etDeliveryTimeCreateService);
        etServiceBannerCreateService = findViewById(R.id.etServiceBannerCreateService);
        etAdditionalInfoCreateService = findViewById(R.id.etAdditionalInfoCreateService);
        submitCreateService = findViewById(R.id.submitCreateService);
        serviceBannerCardView = findViewById(R.id.serviceBannerCardView);

        if (path.equals("new")){
            title = getIntent().getStringExtra("title");
            description = getIntent().getStringExtra("description");
            category = getIntent().getStringExtra("category");
            cat_level = getIntent().getStringExtra("cat_level");
        } else {
            title = getIntent().getStringExtra("title");
            description = getIntent().getStringExtra("description");
            category = getIntent().getStringExtra("category");
            cat_level = getIntent().getStringExtra("cat_level");
            priceIntent = getIntent().getStringExtra("price");
            deliverTimeIntent = getIntent().getStringExtra("deliveryTime");
            additonalInfoIntent = getIntent().getStringExtra("additionalInfo");
            mediaIntent = getIntent().getStringExtra("media");
            serviceId = getIntent().getStringExtra("serviceId");

            etPriceCreateService.setText(priceIntent);
            etDeliveryTimeCreateService.setText(deliverTimeIntent);
            etAdditionalInfoCreateService.setText(additonalInfoIntent);
            etServiceBannerCreateService.setText(mediaIntent);

        }

        submitCreateService.setOnClickListener(view -> checkValues());

        etServiceBannerCreateService.setOnClickListener(view -> requestPermissionsFromUser());

        findViewById(R.id.ivBackCreateService).setOnClickListener(view -> onBackPressed());

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_service);

        init();

    }

    private void checkValues() {
        price = ApplicationHandler.stringConverter(etPriceCreateService);
        deliveryTime = ApplicationHandler.stringConverter(etDeliveryTimeCreateService);
        serviceBanner = ApplicationHandler.stringConverter(etServiceBannerCreateService);
        additionalInfo = ApplicationHandler.stringConverter(etAdditionalInfoCreateService);

        if (TextUtils.isEmpty(price)) {
            etPriceCreateService.setError(getResources().getString(R.string.editTextEmptyFieldError));
            return;
        } else {
            etPriceCreateService.setError(null);
        }

        if (TextUtils.isEmpty(deliveryTime)) {
            etDeliveryTimeCreateService.setError(getResources().getString(R.string.editTextEmptyFieldError));
            return;
        } else {
            etDeliveryTimeCreateService.setError(null);
        }

        if (TextUtils.isEmpty(serviceBanner)){
            etServiceBannerCreateService.setError(getResources().getString(R.string.editTextEmptyFieldError));
            etServiceBannerCreateService.setFocusable(false);
            etServiceBannerCreateService.setCursorVisible(false);
            return;
        } else {
            etServiceBannerCreateService.setError(null);
        }

        if (TextUtils.isEmpty(additionalInfo)) {
            etAdditionalInfoCreateService.setError(getResources().getString(R.string.editTextEmptyFieldError));
            return;
        } else {
            etAdditionalInfoCreateService.setError(null);
        }

        Log.e(TAG, "title: " + title);
        Log.e(TAG, "description: " + description);
        Log.e(TAG, "category: " + category);
        Log.e(TAG, "price: " + price);
        Log.e(TAG, "deliveryTime: " + deliveryTime);
        Log.e(TAG, "additionInfo: " + additionalInfo);
        Log.e(TAG, "cat_level: " + cat_level);

        if (mediaIntent!=null){
            updateService(title, description, category, price, deliveryTime, additionalInfo, cat_level);
        } else {
            sendDataToServer(title, description, category, price, deliveryTime, additionalInfo, cat_level);
        }

    }

    private void sendDataToServer(String title, String description, String category, String price, String deliveryTime, String additionalInfo, String cat_level) {
        Loading.show(this, false, Constants.k_PLEASE_WAIT);

        RequestBody requestBody = RequestBody.create(MediaType.parse(Objects.requireNonNull(getContentResolver().getType(imageUri))), imageFIle);
        RequestBody tit = RequestBody.create(MediaType.parse("text/plain"), title);
        RequestBody cat = RequestBody.create(MediaType.parse("text/plain"), category);
        RequestBody desc = RequestBody.create(MediaType.parse("text/plain"), description);
        RequestBody pric = RequestBody.create(MediaType.parse("text/plain"), price);
        RequestBody delivTime = RequestBody.create(MediaType.parse("text/plain"), deliveryTime);
        RequestBody addInfo = RequestBody.create(MediaType.parse("text/plain"), additionalInfo);
        RequestBody catLevel = RequestBody.create(MediaType.parse("text/plain"), cat_level);

        Call<PostAJobResponse> createService = RestApi.getService().createService(
                sessionManager.getString(Constants.k_ACCESSTOKEN),
                tit,
                cat,
                desc,
                pric,
                delivTime,
                requestBody,
                addInfo,
                catLevel
        );

        createService.enqueue(new Callback<PostAJobResponse>() {
            @Override
            public void onResponse(Call<PostAJobResponse> call, Response<PostAJobResponse> response) {
                Loading.cancel();
                if (response.isSuccessful()) {
                    PostAJobResponse response1 = response.body();
                    if (response1 != null) {
                        ApplicationHandler.toast(response1.getMessage());
                        ApplicationHandler.intent(WhatToDoActivitySeller.class);
                    }
                }
            }

            @Override
            public void onFailure(Call<PostAJobResponse> call, Throwable t) {
                Loading.cancel();
                Log.e(TAG, "onFailure: " + t.getMessage());
                ApplicationHandler.toast(t.getMessage());
            }
        });

    }

    private void updateService(String title, String description, String category, String price, String deliveryTime, String additionalInfo, String cat_level) {
        Loading.show(this, false, Constants.k_PLEASE_WAIT);

        Call<PostAJobResponse> createService = RestApi.getService().updateService(
                sessionManager.getString(Constants.k_ACCESSTOKEN), serviceId, title, category, description, price,
                deliveryTime, "", additionalInfo, cat_level
        );

        createService.enqueue(new Callback<PostAJobResponse>() {
            @Override
            public void onResponse(Call<PostAJobResponse> call, Response<PostAJobResponse> response) {
                Loading.cancel();
                if (response.isSuccessful()) {
                    PostAJobResponse response1 = response.body();
                    if (response1 != null) {
                        ApplicationHandler.toast(response1.getMessage());
                        ApplicationHandler.intent(WhatToDoActivitySeller.class);
                    }
                }
            }

            @Override
            public void onFailure(Call<PostAJobResponse> call, Throwable t) {
                Loading.cancel();
                Log.e(TAG, "onFailure: " + t.getMessage());
                ApplicationHandler.toast(t.getMessage());
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        ApplicationHandler.intent(CreateServiceOne.class);
    }

    private void pickImageForGallery() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, GALLERY_REQUEST_CODE);
    }

    private void requestPermissionsFromUser() {
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
//                            openFilePicker();
                            pickImageForGallery();
//                            ShowFilepicker();
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            ApplicationHandler.showSettingsDialog(CreateServiceActivity.this);
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(error -> Toast.makeText(getApplicationContext(), "Error occurred! ", Toast.LENGTH_SHORT).show())
                .onSameThread()
                .check();
    }

    /**
     * Method that displays the filepicker of the StorageChooser.
     */
    public void ShowFilepicker() {
        // 1. Initialize dialog
        final StorageChooser chooser = new StorageChooser.Builder()
                .withActivity(CreateServiceActivity.this)
                .withFragmentManager(getFragmentManager())
                .withMemoryBar(true)
                .allowCustomPath(true)
                .setType(StorageChooser.FILE_PICKER)
                .build();

        // 2. Retrieve the selected path by the user and show in a toast !
        chooser.setOnSelectListener(path -> {
            String[] separated = path.split("emulated/0");
            etServiceBannerCreateService.setText(separated[1]);
        });

        // 3. Display File Picker !
        chooser.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GALLERY_REQUEST_CODE && resultCode == RESULT_OK) {
            imageUri = data.getData();

            imageFilePath = ApplicationHandler.getFilePathFromURI(CreateServiceActivity.this, imageUri);

            if (imageFilePath != null) {
                imageFIle = new File(imageFilePath);
            }
            etServiceBannerCreateService.setText(imageFIle.getName());
        }
    }

}
