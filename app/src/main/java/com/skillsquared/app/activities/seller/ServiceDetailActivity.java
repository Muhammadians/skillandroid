package com.skillsquared.app.activities.seller;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Html;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.skillsquared.app.R;
import com.skillsquared.app.handler.ApplicationHandler;
import com.skillsquared.app.utils.Constants;
import com.squareup.picasso.Picasso;

public class ServiceDetailActivity extends AppCompatActivity {

    private final static String TAG = ServiceDetailActivity.class.getSimpleName();

    ImageView ivServiceDetail;
    TextView tvServiceDetailTitle, tvServiceDetailDescription, tvPriceServiceDetail,
            tvDeliveryTimeServiceDetail;
    RatingBar tvRatingServiceDetail;
    String serviceId;

    private void init(){
        ivServiceDetail = findViewById(R.id.ivServiceDetail);
        tvServiceDetailTitle = findViewById(R.id.tvServiceDetailTitle);
        tvServiceDetailDescription = findViewById(R.id.tvServiceDetailDescription);
        tvPriceServiceDetail = findViewById(R.id.tvPriceServiceDetail);
        tvDeliveryTimeServiceDetail = findViewById(R.id.tvDeliveryTimeServiceDetail);
        tvRatingServiceDetail = findViewById(R.id.tvRatingServiceDetail);

        String image = getIntent().getStringExtra("mainImage");
        String title = getIntent().getStringExtra("title");
        String description = getIntent().getStringExtra("description");
        String price = getIntent().getStringExtra("price");
        String deliveryTime = getIntent().getStringExtra("deliveryTime");
        String rating = getIntent().getStringExtra("rating");
        serviceId = getIntent().getStringExtra("serviceId");

        Picasso.get()
                .load(image)
                .placeholder(R.drawable.noimg)
                .into(ivServiceDetail);

        tvServiceDetailTitle.setText(title);
        tvPriceServiceDetail.setText("$"+price);
        tvDeliveryTimeServiceDetail.setText(deliveryTime+" Days");
        if (rating != null) {
            tvRatingServiceDetail.setRating(Float.valueOf(rating));
        }

        String des = Html.fromHtml(description).toString();
        tvServiceDetailDescription.setText(des);

        findViewById(R.id.btnEditService).setOnClickListener(view->ApplicationHandler.intent(CreateServiceOne.class,
                "editService", serviceId));

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ApplicationHandler.hideStatusBar(this);
        setContentView(R.layout.activity_service_detail);

        init();

        findViewById(R.id.ivBackServiceDetail).setOnClickListener(view->onBackPressed());

    }
}
