package com.skillsquared.app.activities.seller;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.skillsquared.app.R;
import com.skillsquared.app.handler.ApplicationHandler;
import com.skillsquared.app.handler.RestApi;
import com.skillsquared.app.models.AnalyticsModel;
import com.skillsquared.app.utils.Constants;
import com.skillsquared.app.utils.Loading;
import com.skillsquared.app.utils.SessionManager;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AnalyticsActivity extends AppCompatActivity {

    private final static String TAG = "Analytics Activity";
    private TextView tvTotalEarnings, tvTotalCompleteOrders,
            tvEarnedThisMonth, tvAvgSellingPrice;
    private LineChart chart;

    private SessionManager sessionManager;
    private String analytics;
    final ArrayList<String> xAxisLabel = new ArrayList<>();
    final ArrayList<String> yAxisLabel = new ArrayList<>();

    private void init() {
        tvTotalEarnings = findViewById(R.id.tvTotalEarnings);
        tvTotalCompleteOrders = findViewById(R.id.tvTotalCompleteOrders);
        tvEarnedThisMonth = findViewById(R.id.tvEarnedThisMonth);
        tvAvgSellingPrice = findViewById(R.id.tvAvgSellingPrice);
        sessionManager = new SessionManager(this);

        getDataFromServer();

    }

    private void getDataFromServer() {
        Loading.show(this, false, Constants.k_PLEASE_WAIT);
        Call<AnalyticsModel> analyticsModelCall = RestApi.getService().getAnalytics(
                sessionManager.getString(Constants.k_ACCESSTOKEN)
        );

        analyticsModelCall.enqueue(new Callback<AnalyticsModel>() {
            @Override
            public void onResponse(@NonNull Call<AnalyticsModel> call, @NonNull Response<AnalyticsModel> response) {
                Loading.cancel();
                if (response.isSuccessful()) {
                    AnalyticsModel analyticsModel = response.body();

                    if (analyticsModel != null) {
                        analytics = analyticsModel.getSellerAnalytics();
                        tvTotalEarnings.setText(analyticsModel.getSellerEarnings() + "$");
                        tvAvgSellingPrice.setText(String.valueOf(analyticsModel.getAvgSellingPrice()));
                        tvEarnedThisMonth.setText(analyticsModel.getSellerEarningsCurrentMonth() + "$");
                        tvTotalCompleteOrders.setText(String.valueOf(analyticsModel.getSellerCompltedOrdersCount()));
                        setData();
                        // don't forget to refresh the drawing
                        chart.invalidate();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<AnalyticsModel> call, @NonNull Throwable t) {
                Loading.cancel();
                Log.e(TAG, "onFailure: " + t.getMessage());
                ApplicationHandler.toast(t.getMessage());
            }
        });

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ApplicationHandler.hideStatusBar(this);
        setContentView(R.layout.activity_analytics);

        init();

        findViewById(R.id.ivBackAnalytics).setOnClickListener(view -> ApplicationHandler.intent(WhatToDoActivitySeller.class));

        chart = findViewById(R.id.chart1);
//        chart.setViewPortOffsets(10, 10, 10, 10);
        chart.setExtraOffsets(0, 10, 0, 10);
        chart.setBackgroundColor(getResources().getColor(R.color.primaryColor));
//        chart.setBackgroundColor(Color.rgb(000, 000, 000));

        // no description text
        chart.getDescription().setEnabled(false);

        // enable touch gestures
        chart.setTouchEnabled(false);

        // enable scaling and dragging
        chart.setDragEnabled(false);
        chart.setScaleEnabled(false);

        // if disabled, scaling can be done on x- and y-axis separately
        chart.setPinchZoom(false);

        chart.setDrawGridBackground(false);
        chart.setMaxHighlightDistance(300);

        XAxis x = chart.getXAxis();
        x.setDrawGridLines(false);
        x.setPosition(XAxis.XAxisPosition.BOTTOM);
        x.setAxisLineColor(getResources().getColor(R.color.colorDarkGrey));
        x.setTextColor(getResources().getColor(R.color.colorDarkGrey));
        x.setValueFormatter(new IndexAxisValueFormatter(getXAxisData()));
        x.setLabelCount(11, false);
        x.setLabelRotationAngle(45);

        YAxis y = chart.getAxisLeft();
//        y.setTypeface(tfLight);
        y.setAxisMinimum(0.0f);
        y.setLabelCount(7, true);
        y.setAxisMaximum(30f);
        y.setTextColor(getResources().getColor(R.color.colorDarkGrey));
        y.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        y.setDrawGridLines(false);
        y.setAxisLineColor(getResources().getColor(R.color.colorDarkGrey));
//        y.setStartAtZero(true);

        chart.getAxisRight().setEnabled(false);

        chart.getLegend().setEnabled(false);



        chart.animateXY(2000, 2000);

    }

    //    private void setData(int count, float range) {
    private void setData() {

        ArrayList<Entry> values = new ArrayList<>();

        String[] separated = analytics.split(",");

        for (int i = 0; i < separated.length; i++) {
            float val = (float) (Math.random() * (20 + 1)) + 20;
            values.add(new Entry(i, Float.valueOf(separated[i])));
        }

        LineDataSet set1;

        if (chart.getData() != null &&
                chart.getData().getDataSetCount() > 0) {
            set1 = (LineDataSet) chart.getData().getDataSetByIndex(0);
            set1.setValues(values);
            chart.getData().notifyDataChanged();
            chart.notifyDataSetChanged();
        } else {
            // create a dataset and give it a type
            set1 = new LineDataSet(values, "DataSet 1");

            set1.setMode(LineDataSet.Mode.CUBIC_BEZIER);
            set1.setCubicIntensity(0.2f);
            set1.setDrawFilled(true);
            set1.setDrawCircles(true);
            set1.setLineWidth(1.8f);
            set1.setCircleRadius(4f);
            set1.setCircleColor(getResources().getColor(R.color.colorPrimaryDark));
            set1.setHighLightColor(getResources().getColor(R.color.colorPrimaryDark));
            set1.setColor(getResources().getColor(R.color.colorPrimary));
            set1.setFillColor(getResources().getColor(R.color.colorPrimary));
            set1.setFillAlpha(100);
            set1.setDrawHorizontalHighlightIndicator(false);
            set1.setFillFormatter((dataSet, dataProvider) -> chart.getAxisLeft().getAxisMinimum());

            // create a data object with the data sets
            LineData data = new LineData(set1);
//            data.setValueTypeface(tfLight);
            data.setValueTextSize(9f);
            data.setDrawValues(false);

            // set data
            chart.setData(data);
        }
    }

    public ArrayList<String> getXAxisData() {

        xAxisLabel.add("Jan");
        xAxisLabel.add("Feb");
        xAxisLabel.add("March");
        xAxisLabel.add("April");
        xAxisLabel.add("May");
        xAxisLabel.add("June");
        xAxisLabel.add("July");
        xAxisLabel.add("Aug");
        xAxisLabel.add("Sep");
        xAxisLabel.add("Oct");
        xAxisLabel.add("Nov");
        xAxisLabel.add("Dec");

        return xAxisLabel;
    }

    public ArrayList<String> getYAxisData(){
        yAxisLabel.add("0");
        yAxisLabel.add("5");
        yAxisLabel.add("10");
        yAxisLabel.add("15");
        yAxisLabel.add("20");
        yAxisLabel.add("25");
        yAxisLabel.add("30");
        yAxisLabel.add("Aug");

        return yAxisLabel;
    }

}