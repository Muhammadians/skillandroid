package com.skillsquared.app.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.cometchat.pro.core.CometChat;
import com.cometchat.pro.exceptions.CometChatException;
import com.cometchat.pro.models.User;
import com.google.firebase.iid.FirebaseInstanceId;
import com.skillsquared.app.R;
import com.skillsquared.app.cometChat.Contracts.LoginActivityContract;
import com.skillsquared.app.cometChat.Contracts.StringContract;
import com.skillsquared.app.cometChat.Presenters.LoginAcitivityPresenter;
import com.skillsquared.app.handler.ApplicationHandler;
import com.skillsquared.app.handler.RestApi;
import com.skillsquared.app.models.loginModels.LoginModel;
import com.skillsquared.app.utils.Constants;
import com.skillsquared.app.utils.Loading;
import com.skillsquared.app.utils.SessionManager;

import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

import javax.net.ssl.HttpsURLConnection;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignIn extends AppCompatActivity {

    private static final String TAG = "SignIn";
    ImageView ivBack;
    TextView tvSignUp, tvForgotPassword;
    AutoCompleteTextView etEmailSignIn;
    EditText etPasswordSignIn;
    CardView cardViewSignIn;
    String email, password;
    String token;
    SessionManager sessionManager;
    private int i = 0;
    AsyncTask<?, ?, ?> runningTask;
    private LoginActivityContract.LoginActivityPresenter loginActivityPresenter;

    private void init()
    {
        ivBack = findViewById(R.id.ivBack);
        tvSignUp = findViewById(R.id.tvSignUpSignIn);
        tvForgotPassword = findViewById(R.id.forgotPassword);
        etPasswordSignIn = findViewById(R.id.etPasswordSignIn);
        etEmailSignIn = findViewById(R.id.etEmailSignIn);
        cardViewSignIn = findViewById(R.id.cardViewSignIn);
        sessionManager = new SessionManager(this);
        loginActivityPresenter = new LoginAcitivityPresenter();

        onClickListener();
    }

    private void onClickListener()
    {

        ivBack.setOnClickListener(view -> onBackPressed());

        tvForgotPassword.setOnClickListener(view -> ApplicationHandler.intent(ResetPassword.class));

        tvSignUp.setOnClickListener(view -> ApplicationHandler.intent(SignUp.class));

        cardViewSignIn.setOnClickListener(view -> checkData());

    }

    void checkData()
    {
        token = FirebaseInstanceId.getInstance().getToken();
        email = ApplicationHandler.stringConverter(etEmailSignIn);
        password = ApplicationHandler.stringConverter(etPasswordSignIn);

        if (TextUtils.isEmpty(email)) {
            etEmailSignIn.setError(getResources().getString(R.string.editTextEmptyFieldError));
            etEmailSignIn.requestFocus();
            return;
        } else {
            if (ApplicationHandler.isEmailValid(email)){
                etEmailSignIn.setError(getResources().getString(R.string.emailFormatError));
                etEmailSignIn.requestFocus();
                return;
            } else {
                etEmailSignIn.setError(null);
            }
        }

        if (TextUtils.isEmpty(password)) {
            etPasswordSignIn.setError(getResources().getString(R.string.editTextEmptyFieldError));
            etPasswordSignIn.requestFocus();
            return;
        } else {
            etPasswordSignIn.setError(null);
        }

        Log.e(TAG, "email: " + email.toLowerCase().replaceAll(" ", ""));
        Log.e(TAG, "password: " + password);

        makeServerCall(email.toLowerCase().replaceAll(" ", ""), password);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        init();

    }

    private void makeServerCall(String checkEmail, String password)
    {

        Loading.show(this, false, Constants.k_PLEASE_WAIT);

        Call<LoginModel> loginModelCall = RestApi.getService().loginCall(
                checkEmail.toLowerCase().replaceAll(" ", ""),
                password,
                "android",
                token
        );

        loginModelCall.enqueue(new Callback<LoginModel>() {
            @Override
            public void onResponse(@NonNull Call<LoginModel> call, @NonNull Response<LoginModel> response) {
//                Loading.cancel();

                if (response.isSuccessful()) {
                    LoginModel loginModel = response.body();
                    try {
                        if (loginModel != null && loginModel.getStatus() == 200) {
                            sessionManager.put(Constants.k_USER_ID, loginModel.getSkillSquaredUser().getUserId());
                            sessionManager.put(Constants.k_USERNAMESKILLSQUARED, loginModel.getSkillSquaredUser().getUsername());
                            sessionManager.put(Constants.k_NAME, loginModel.getSkillSquaredUser().getName());
                            sessionManager.put(Constants.k_EMAIL, loginModel.getSkillSquaredUser().getEmail());
                            sessionManager.put(Constants.k_PHONE, loginModel.getSkillSquaredUser().getPhone());
                            sessionManager.put(Constants.k_ADDRESS, loginModel.getSkillSquaredUser().getAddress());
                            sessionManager.put(Constants.k_IMAGE, loginModel.getSkillSquaredUser().getImage());
                            sessionManager.put(Constants.k_TIMEZONE, loginModel.getSkillSquaredUser().getTimezone());
                            sessionManager.put(Constants.k_ACCESSTOKEN, loginModel.getSkillSquaredUser().getAccessToken());
                            sessionManager.put(Constants.k_REFERRALURL, loginModel.getSkillSquaredUser().getReferalUrl());
                            sessionManager.putBoolean(Constants.k_Freelancer, loginModel.getFreelancer());
                            sessionManager.putBoolean(Constants.k_FreelancerProfile, loginModel.getFreelancerProfileSet());
                            sessionManager.createLoginSession();

                            ApplicationHandler.cometChatCreateUser(
                                    sessionManager.getString(Constants.k_USERNAMESKILLSQUARED),
                                    sessionManager.getString(Constants.k_NAME),
                                    HomeActivity.class,
                                    loginModel.getMessage());
                        } else {
                            if (loginModel != null) {
                                Loading.cancel();
                                ApplicationHandler.toast(loginModel.getMessage());
                            }
                        }
                    } catch (Exception e) {
                        Loading.cancel();
                        Log.e("server error", Objects.requireNonNull(e.getMessage()));
                        ApplicationHandler.toast(e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<LoginModel> call, @NonNull Throwable t) {
                Loading.cancel();
                Log.e(TAG, "onFailure: " + t.getMessage());
                ApplicationHandler.toast(getResources().getString(R.string.username_password_incorrect));
            }
        });

    }

    @Override
    public void onBackPressed()
    {
        if (i == 0) {
            i++;
            ApplicationHandler.toast(getResources().getString(R.string.back_press));
        } else {
            Intent a = new Intent(Intent.ACTION_MAIN);
            a.addCategory(Intent.CATEGORY_HOME);
            a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(a);
        }
    }

    /*private void createUser(String UID, String name)
    {
        String createUserEndpoint = "https://api-%s.cometchat.io/v2.0/users";
        try {
            URL url = new URL(String.format(createUserEndpoint, StringContract.AppDetails.REGION));
            HttpsURLConnection httpsURLConnection = (HttpsURLConnection) url.openConnection();
            httpsURLConnection.setReadTimeout(10000);
            httpsURLConnection.setConnectTimeout(15000);
            httpsURLConnection.setRequestMethod("POST");
            httpsURLConnection.setDoInput(true);
            httpsURLConnection.setDoOutput(true);

            // Adding the necessary headers
            httpsURLConnection.setRequestProperty("appid", StringContract.AppDetails.APP_ID);
            httpsURLConnection.setRequestProperty("apikey", StringContract.AppDetails.API_KEY);
            httpsURLConnection.setRequestProperty("Content-Type", "application/json");
            httpsURLConnection.setRequestProperty("Accept", "application/json");

            // Creating the JSON with post params
            JSONObject userData = new JSONObject();
            userData.put("uid", UID);
            userData.put("name", name);

            Log.e(TAG, "createUser: "+userData);

            OutputStream outputStream = new BufferedOutputStream(httpsURLConnection.getOutputStream());
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream, StandardCharsets.UTF_8));
            writer.write(userData.toString());
            writer.flush();
            writer.close();
            outputStream.close();

            int responseCode = httpsURLConnection.getResponseCode();
            String responseMessage = httpsURLConnection.getResponseMessage();
            Log.e(TAG, "Response Code : " + responseCode);
            Log.e(TAG, "Response Message : " + responseMessage);
            String result = "";
            InputStream inputStream;
            if (responseCode >= 400 && responseCode <= 499) {
                inputStream = httpsURLConnection.getErrorStream();
                Log.e(TAG, "createUser: "+inputStream);
            } else {
                inputStream = httpsURLConnection.getInputStream();
                Log.e(TAG, "createUser: "+inputStream);
            }
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String inputLine;
            while ((inputLine = bufferedReader.readLine()) != null) {
                result += inputLine;
            }
            if (responseCode == 200) {
                Log.e(TAG, "Create User Success Response : " + result);

                sessionManager.put(Constants.k_UIDSKILLSQUARED, "123" +
                        sessionManager.getString(Constants.k_USERNAMESKILLSQUARED));
                Loading.show(this, false, Constants.k_PLEASE_WAIT);
                loginActivityPresenter.Login(this, sessionManager.getString(Constants.k_UIDSKILLSQUARED));

            } else {
                if (sessionManager.getString(Constants.k_UIDSKILLSQUARED).equals("nothing")){
                    sessionManager.put(Constants.k_UIDSKILLSQUARED, "123"+sessionManager.getString(Constants.k_USERNAMESKILLSQUARED));
                    Loading.show(this, false, Constants.k_PLEASE_WAIT);
                    loginActivityPresenter.Login(this, sessionManager.getString(Constants.k_UIDSKILLSQUARED));
                } else {
                    Log.e(TAG, "Create User Error Response : " + result);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    class MyAsyncTask extends AsyncTask<Void, Void, Void>
    {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {

            /*createUser(
                    *//*sessionManager.getString(Constants.k_USER_ID)*//*"ss" +
                            sessionManager.getString(Constants.k_USERNAMESKILLSQUARED),
                    sessionManager.getString(Constants.k_NAME));*/

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
        }
    }

}
