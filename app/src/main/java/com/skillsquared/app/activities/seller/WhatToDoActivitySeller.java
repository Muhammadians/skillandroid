package com.skillsquared.app.activities.seller;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.skillsquared.app.R;
import com.skillsquared.app.activities.HomeActivity;
import com.skillsquared.app.activities.seller.manageSales.ManageSalesActivity;
import com.skillsquared.app.handler.ApplicationHandler;

public class WhatToDoActivitySeller extends AppCompatActivity {

    CardView cardViewCreateService, cardViewManageService, cardViewBuyerRequests,
            cardViewPlaceOrder, cardViewAnalytics, cardViewEarnings;

    private void init(){
        cardViewCreateService = findViewById(R.id.cardViewCreateService);
        cardViewManageService = findViewById(R.id.cardViewManageService);
        cardViewBuyerRequests = findViewById(R.id.cardViewBuyerRequests);
        cardViewPlaceOrder = findViewById(R.id.cardViewPlaceOrder);
        cardViewAnalytics = findViewById(R.id.cardViewAnalytics);
        cardViewEarnings = findViewById(R.id.cardViewEarnings);

        setOnClickListeners();

    }

    private void setOnClickListeners(){
        cardViewCreateService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ApplicationHandler.intent(CreateServiceOne.class);
            }
        });

        cardViewManageService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ApplicationHandler.intent(ManageSalesActivity.class);
            }
        });

        cardViewBuyerRequests.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ApplicationHandler.intent(BuyerRequest.class);
            }
        });

        cardViewPlaceOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ApplicationHandler.intent(ManageServicesActivity.class);
            }
        });

        cardViewAnalytics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ApplicationHandler.intent(AnalyticsActivity.class);
            }
        });

        cardViewEarnings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ApplicationHandler.intent(EarningsActivity.class);
            }
        });

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ApplicationHandler.hideStatusBar(this);
        setContentView(R.layout.activity_what_to_do_seller);

        ImageView ivBackWhatToDoSeller = findViewById(R.id.ivBackWhatToDoSeller);
        ivBackWhatToDoSeller.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        init();

    }

    @Override
    public void onBackPressed() {
        ApplicationHandler.intent(HomeActivity.class);
    }
}
