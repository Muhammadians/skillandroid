package com.skillsquared.app.activities.buyer.postAJob;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.skillsquared.app.R;
import com.skillsquared.app.activities.HomeActivity;
import com.skillsquared.app.activities.buyer.WhatToDoActivityBuyer;
import com.skillsquared.app.activities.seller.WhatToDoActivitySeller;
import com.skillsquared.app.adapters.PostedApprovedRequestListAdapter;
import com.skillsquared.app.adapters.PostedPendingRequestListAdapter;
import com.skillsquared.app.adapters.PostedUnapprovedRequestListAdapter;
import com.skillsquared.app.handler.ApplicationHandler;
import com.skillsquared.app.handler.RestApi;
import com.skillsquared.app.models.PostedRequestListModel;
import com.skillsquared.app.models.buyerRequest.BuyerRequest;
import com.skillsquared.app.utils.Constants;
import com.skillsquared.app.utils.Loading;
import com.skillsquared.app.utils.SessionManager;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostedRequestList extends AppCompatActivity {

    private final static String TAG = "PostedRequestList";
    RecyclerView recyclerViewPostedRequests;
    ImageView ivFilter, ivBackPostedRequestList;
    Dialog myDialog;
    RadioButton radioButton, rbActive, rbPending, rbUnapproved;
    RadioGroup radioGroup;
    SessionManager sessionManager;
    CardView cardViewSubmitYourRequest;

    private void init() {
        recyclerViewPostedRequests = findViewById(R.id.recyclerViewPostedRequest);
        ivFilter = findViewById(R.id.ivFilterPostRequest);
        cardViewSubmitYourRequest = findViewById(R.id.cardViewSubmitYourRequest);
        ivBackPostedRequestList = findViewById(R.id.ivBackPostedRequestList);
        sessionManager = new SessionManager(this);

        myDialog = new Dialog(this);

        initDialogFields();

        ivFilter.setOnClickListener(view -> showPopUp());

        cardViewSubmitYourRequest.setOnClickListener(view -> ApplicationHandler.intent(PostARequest.class));

        ivBackPostedRequestList.setOnClickListener(view -> ApplicationHandler.intent(WhatToDoActivityBuyer.class));

    }

    void initDialogFields(){
        myDialog.setContentView(R.layout.post_a_job_filter_pop_up);
        radioGroup = myDialog.findViewById(R.id.rgPostJobFilter);
        rbActive = myDialog.findViewById(R.id.rbActive);
        rbPending = myDialog.findViewById(R.id.rbPending);
        rbUnapproved = myDialog.findViewById(R.id.rbUnapproved);
    }

    void showPopUp() {
        myDialog.show();

        radioGroup.setOnCheckedChangeListener((radioGroup, i) -> {
            int selectedId = radioGroup.getCheckedRadioButtonId();
            radioButton = myDialog.findViewById(selectedId);
            setRecyclerViewPostedRequests(radioButton.getText()+"");
            myDialog.cancel();
            Log.e("radio", radioButton.getText()+"");
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        makeServerCall();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ApplicationHandler.hideStatusBar(this);
        setContentView(R.layout.activity_posted_request_list);

        init();

    }

    private void makeServerCall(){

        Loading.show(this, false, Constants.k_PLEASE_WAIT);

        Call<BuyerRequest> buyerRequestCall = RestApi.getService().getBuyerRequest(
                sessionManager.getString(Constants.k_ACCESSTOKEN)
        );

        buyerRequestCall.enqueue(new Callback<BuyerRequest>() {
            @Override
            public void onResponse(@NonNull Call<BuyerRequest> call,@NonNull Response<BuyerRequest> response) {
                Loading.cancel();
                if (response.isSuccessful()){
                    Constants.k_BuyerRequest = response.body();
                    setRecyclerViewPostedRequests("Active");
                }
            }

            @Override
            public void onFailure(@NonNull Call<BuyerRequest> call,@NonNull Throwable t) {
                Loading.cancel();
                Log.e(TAG, "onFailure: "+t.getMessage());
                ApplicationHandler.toast(t.getMessage());
            }
        });

    }

    void setRecyclerViewPostedRequests(String filter){

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL,false);
        recyclerViewPostedRequests.setLayoutManager(linearLayoutManager);

        PostedApprovedRequestListAdapter postedRequestListAdapter;
        PostedPendingRequestListAdapter postedPendingRequestListAdapter;
        PostedUnapprovedRequestListAdapter postedUnapprovedRequestListAdapter;

        if (Constants.k_BuyerRequest != null) {
            switch (filter) {
                case "Active":
                    if (Constants.k_BuyerRequest.getBuyerrequests().getApprove()!=null){
                        postedRequestListAdapter = new PostedApprovedRequestListAdapter(this,
                                Constants.k_BuyerRequest.getBuyerrequests().getApprove(),
                                rbActive);
                        recyclerViewPostedRequests.setAdapter(postedRequestListAdapter);
                    } else {
                        ApplicationHandler.toast(getResources().getString(R.string.no_data_to_display));
                    }
                    break;
                case "Pending":
                    if (Constants.k_BuyerRequest.getBuyerrequests().getPending()!=null){
                        postedPendingRequestListAdapter = new PostedPendingRequestListAdapter(this,
                                Constants.k_BuyerRequest.getBuyerrequests().getPending(),
                                rbPending);
                        recyclerViewPostedRequests.setAdapter(postedPendingRequestListAdapter);
                    } else {
                        ApplicationHandler.toast(getResources().getString(R.string.no_data_to_display));
                    }
                    break;
                case "Unapproved":
                    if (Constants.k_BuyerRequest.getBuyerrequests().getUnapproved()!=null){
                        postedUnapprovedRequestListAdapter = new PostedUnapprovedRequestListAdapter(this,
                                Constants.k_BuyerRequest.getBuyerrequests().getUnapproved(),
                                rbUnapproved);
                        recyclerViewPostedRequests.setAdapter(postedUnapprovedRequestListAdapter);
                    } else {
                        ApplicationHandler.toast(getResources().getString(R.string.no_data_to_display));
                    }
                    break;
            }
        }
    }

    @Override
    public void onBackPressed() {
        ApplicationHandler.intent(HomeActivity.class);
    }
}
