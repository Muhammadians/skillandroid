package com.skillsquared.app.activities.seller;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.graphics.Color;
import android.media.Image;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.skillsquared.app.R;
import com.skillsquared.app.activities.HomeActivity;
import com.skillsquared.app.adapters.BuyerRequestAdapter;
import com.skillsquared.app.adapters.BuyerRequestAdapterSentOffers;
import com.skillsquared.app.adapters.ViewOffersAdapter;
import com.skillsquared.app.handler.ApplicationHandler;
import com.skillsquared.app.handler.RestApi;
import com.skillsquared.app.models.buyerRequestsSeller.BuyerRequestModel;
import com.skillsquared.app.utils.Constants;
import com.skillsquared.app.utils.Loading;
import com.skillsquared.app.utils.SessionManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BuyerRequest extends AppCompatActivity {

    private final static String TAG = BuyerRequest.class.getSimpleName();
    private RecyclerView recyclerViewBuyerRequests;
    private RecyclerView recyclerViewSellerRequests;
    private SessionManager sessionManager;
    private TextView tvNoDataToDisplayBuyerRequest, tvPageNumber;
    ImageView ivBackBuyerRequest;
    RadioButton radioButton, rbBuyerOffer, rbSentOffer;
    RadioGroup rgBuyerRequest, rgPostJobFilter;
    BuyerRequestModel buyerRequestModel;
    Dialog myDialog;
    LinearLayout llFilterPostRequest, llBuyerRequestFilter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ApplicationHandler.hideStatusBar(this);
        setContentView(R.layout.activity_buyer_request);
        sessionManager = new SessionManager(this);

        recyclerViewBuyerRequests = findViewById(R.id.recyclerViewBuyerRequests);
        recyclerViewSellerRequests = findViewById(R.id.recyclerViewSellerRequests);
        ImageView ivFilterBuyerRequest = findViewById(R.id.ivFilterBuyerRequest);
        ivBackBuyerRequest = findViewById(R.id.ivBackBuyerRequest);
        tvNoDataToDisplayBuyerRequest = findViewById(R.id.tvNoDataToDisplayBuyerRequest);
        tvPageNumber = findViewById(R.id.tvPageNumber);
        myDialog = new Dialog(this);

        getDataFromServer();

        initPopUp();

        ivFilterBuyerRequest.setOnClickListener(view -> showPopUp());

        ivBackBuyerRequest.setOnClickListener(view -> onBackPressed());

    }

    private void getDataFromServer(){
        Loading.show(this, false, Constants.k_PLEASE_WAIT);

        Call<BuyerRequestModel> buyerRequestModelCall = RestApi.getService().getBuyerRequests(
                sessionManager.getString(Constants.k_ACCESSTOKEN)
        );

        buyerRequestModelCall.enqueue(new Callback<BuyerRequestModel>() {
            @Override
            public void onResponse(@NonNull Call<BuyerRequestModel> call,@NonNull Response<BuyerRequestModel> response) {
                Loading.cancel();
                Log.e(TAG, "onResponse: yes");
                if (response.isSuccessful()){
                    Log.e(TAG, "onResponse: response success");
                    buyerRequestModel = response.body();
                    if (buyerRequestModel != null && buyerRequestModel.getBuyerRequests().getBuyerequest().size() > 0) {
                        Log.e(TAG, "onResponse: list size greater than 0");
                        tvNoDataToDisplayBuyerRequest.setVisibility(View.GONE);
                        Constants.k_SellerservicesList = buyerRequestModel.getSellerservices();
                        setRecyclerViewData("buyer");
                    } else {
                        Log.e(TAG, "onResponse: list size is 0");
                        tvNoDataToDisplayBuyerRequest.setVisibility(View.VISIBLE);
                    }
                } else {
                    Log.e(TAG, "onResponse: response not successfull");
                    tvNoDataToDisplayBuyerRequest.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onFailure(@NonNull Call<BuyerRequestModel> call,@NonNull Throwable t) {
                Loading.cancel();
                Log.e(TAG, "onFailure: "+t.getMessage());
                ApplicationHandler.toast(t.getMessage());
                tvNoDataToDisplayBuyerRequest.setVisibility(View.VISIBLE);
            }
        });

    }

    void setRecyclerViewData(String val){
        Log.e(TAG, "setRecyclerViewData: "+val);
        if (buyerRequestModel!= null) {
            tvPageNumber.setText(null);
            tvNoDataToDisplayBuyerRequest.setVisibility(View.GONE);
            if (val.equals("buyer") && buyerRequestModel.getBuyerRequests().getBuyerequest() != null){
                BuyerRequestAdapter buyerRequestAdapter;
                recyclerViewBuyerRequests.setVisibility(View.VISIBLE);
                recyclerViewSellerRequests.setVisibility(View.GONE);
                buyerRequestAdapter = new BuyerRequestAdapter(BuyerRequest.this, buyerRequestModel.getBuyerRequests().getBuyerequest(), tvPageNumber, rbBuyerOffer);
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.HORIZONTAL,false);
                recyclerViewBuyerRequests.setLayoutManager(linearLayoutManager);
                recyclerViewBuyerRequests.setAdapter(buyerRequestAdapter);
                recyclerViewBuyerRequests.addOnScrollListener(new RecyclerView.OnScrollListener() {
                    @Override
                    public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                        super.onScrollStateChanged(recyclerView, newState);
                    }

                    @Override
                    public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                        super.onScrolled(recyclerView, dx, dy);
                        int visibleItemCount = linearLayoutManager.getChildCount();
                        int totalItemCount = linearLayoutManager.getItemCount();
                        int firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition();
                        final int lastItem = firstVisibleItemPosition + visibleItemCount;

                        tvPageNumber.setText(lastItem+"/"+totalItemCount);
                    }
                });
            } else if (val.equals("seller") && buyerRequestModel.getSentOffers() != null){
                BuyerRequestAdapterSentOffers buyerRequestAdapter;
                recyclerViewSellerRequests.setVisibility(View.VISIBLE);
                recyclerViewBuyerRequests.setVisibility(View.GONE);
                buyerRequestAdapter = new BuyerRequestAdapterSentOffers(BuyerRequest.this, buyerRequestModel.getSentOffers(), tvPageNumber, rbSentOffer);
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.HORIZONTAL,false);
                recyclerViewSellerRequests.setLayoutManager(linearLayoutManager);
                recyclerViewSellerRequests.setAdapter(buyerRequestAdapter);
                recyclerViewSellerRequests.addOnScrollListener(new RecyclerView.OnScrollListener() {
                    @Override
                    public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                        super.onScrollStateChanged(recyclerView, newState);
                    }

                    @Override
                    public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                        super.onScrolled(recyclerView, dx, dy);
                        int visibleItemCount = linearLayoutManager.getChildCount();
                        int totalItemCount = linearLayoutManager.getItemCount();
                        int firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition();
                        final int lastItem = firstVisibleItemPosition + visibleItemCount;

                        tvPageNumber.setText(lastItem+"/"+totalItemCount);

                    }
                });
            } else {
                tvNoDataToDisplayBuyerRequest.setVisibility(View.VISIBLE);
            }
        } else {
            tvNoDataToDisplayBuyerRequest.setVisibility(View.VISIBLE);
        }
    }

    void initPopUp(){
        myDialog.setContentView(R.layout.post_a_job_filter_pop_up);
        llFilterPostRequest = myDialog.findViewById(R.id.llFilterPostRequest);
        llBuyerRequestFilter = myDialog.findViewById(R.id.llBuyerRequestFilter);
        rgBuyerRequest = myDialog.findViewById(R.id.rgBuyerRequest);
        rgPostJobFilter = myDialog.findViewById(R.id.rgPostJobFilter);
        rbBuyerOffer = myDialog.findViewById(R.id.rbBuyerRequest);
        rbSentOffer = myDialog.findViewById(R.id.rbSellerSentOffer);

        llFilterPostRequest.setVisibility(View.GONE);
        rgPostJobFilter.setVisibility(View.GONE);
        llBuyerRequestFilter.setVisibility(View.VISIBLE);
        rgBuyerRequest.setVisibility(View.VISIBLE);
    }

    void showPopUp() {
        myDialog.show();

        rgBuyerRequest.setOnCheckedChangeListener((radioGroup, i) -> {
            int selectedId = radioGroup.getCheckedRadioButtonId();
            radioButton = myDialog.findViewById(selectedId);

            if (buyerRequestModel != null){

                if (String.valueOf(radioButton.getText()).equals("Buyer Request")){
                    setRecyclerViewData("buyer");
                } else if (String.valueOf(radioButton.getText()).equals("Sent Offer")){
                    setRecyclerViewData("seller");
                }
            }

            myDialog.cancel();
            Log.e("radio", radioButton.getText()+"");
        });
    }

    @Override
    public void onBackPressed() {
        ApplicationHandler.intent(WhatToDoActivitySeller.class);
    }
}
