package com.skillsquared.app.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.skillsquared.app.R;
import com.skillsquared.app.handler.ApplicationHandler;
import com.skillsquared.app.handler.RestApi;
import com.skillsquared.app.models.signUpModels.SignUpModel;
import com.skillsquared.app.utils.Constants;
import com.skillsquared.app.utils.Loading;
import com.skillsquared.app.utils.SessionManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SupportActivity extends AppCompatActivity {

    private final static String TAG = SupportActivity.class.getSimpleName();
    EditText etSubjectSupport, etDescriptionSupport;
    Button btnSubmitSupport;
    private SessionManager sessionManager;

    private void init(){

        etSubjectSupport = findViewById(R.id.etSubjectSupport);
        etDescriptionSupport = findViewById(R.id.etDescriptionSupport);
        btnSubmitSupport = findViewById(R.id.btnSubmitSupport);
        sessionManager = new SessionManager(this);

        btnSubmitSupport.setOnClickListener(view -> checkValues());

        findViewById(R.id.ivBackSupport).setOnClickListener(view->onBackPressed());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ApplicationHandler.hideStatusBar(this);
        setContentView(R.layout.activity_support);

        init();

    }

    @Override
    public void onBackPressed() {
        ApplicationHandler.intent(HomeActivity.class);
    }

    private void checkValues(){
        String subject = ApplicationHandler.stringConverter(etSubjectSupport);
        String description = ApplicationHandler.stringConverter(etDescriptionSupport);

        if (TextUtils.isEmpty(subject)){
            etSubjectSupport.setError(getResources().getString(R.string.editTextEmptyFieldError));
            etSubjectSupport.requestFocus();
            return;
        } else {
            etSubjectSupport.setError(null);
        }

        if (TextUtils.isEmpty(description)){
            etDescriptionSupport.setError(getResources().getString(R.string.editTextEmptyFieldError));
            etDescriptionSupport.requestFocus();
            return;
        } else {
            etDescriptionSupport.setError(null);
        }

        sendDataToServer(subject, description);
    }

    private void sendDataToServer(String subject, String description) {
        Loading.show(this, false, Constants.k_PLEASE_WAIT);

        Call<SignUpModel> contactSupport = RestApi.getService().contactSupport(
                sessionManager.getString(Constants.k_ACCESSTOKEN),
                sessionManager.getString(Constants.k_NAME),
                sessionManager.getString(Constants.k_EMAIL),
                subject,
                description
        );

        contactSupport.enqueue(new Callback<SignUpModel>() {
            @Override
            public void onResponse(@NonNull Call<SignUpModel> call,@NonNull Response<SignUpModel> response) {
                Loading.cancel();

                if (response.isSuccessful()){
                    SignUpModel contactSupportResponse = response.body();

                    if (contactSupportResponse != null) {
                        Toast.makeText(SupportActivity.this, contactSupportResponse.getMessage(), 2000).show();
                        ApplicationHandler.intent(HomeActivity.class);
                    }
//                    ApplicationHandler.toast(contactSupportResponse.getMessage());

                }

            }

            @Override
            public void onFailure(@NonNull Call<SignUpModel> call,@NonNull Throwable t) {
                Loading.cancel();
                ApplicationHandler.toast(t.getMessage());
                Log.e(TAG, "onFailure: "+t.getMessage());
            }
        });

    }

}
