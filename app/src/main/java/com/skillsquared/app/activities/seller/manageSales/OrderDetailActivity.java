package com.skillsquared.app.activities.seller.manageSales;

import android.Manifest;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.card.MaterialCardView;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.skillsquared.app.R;
import com.skillsquared.app.activities.SignUp;
import com.skillsquared.app.activities.buyer.ManageOrders;
import com.skillsquared.app.activities.seller.CreateServiceActivity;
import com.skillsquared.app.handler.ApplicationHandler;
import com.skillsquared.app.handler.FileUtils;
import com.skillsquared.app.handler.RestApi;
import com.skillsquared.app.models.buyerOrderDetail.BuyerOrderDetailResponse;
import com.skillsquared.app.models.orderDetail.OrderDetailResponse;
import com.skillsquared.app.models.signUpModels.SignUpModel;
import com.skillsquared.app.utils.Constants;
import com.skillsquared.app.utils.Loading;
import com.skillsquared.app.utils.SessionManager;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderDetailActivity extends AppCompatActivity {

    String orderNum;
    String imageFilePath;
    File imageFIle;
    Uri imageUri;
    public static final int GALLERY_REQUEST_CODE = 1128;
    private final static String TAG = OrderDetailActivity.class.getSimpleName();
    TextView tvOrderNumber, tvDescriptionOrderDetail,
            tvBuyerName, tvPriceOrderDetail, tvFile, tvOrderStatus, tvDownload;
    Button btnDeliverWork, btnDispute, btnClose,
            btnSubmit, btnSubmitDeliverWork, btnCloseDeliverWork, btnRevise;
    SessionManager sessionManager;
    private BottomSheetDialog myDialog;
    private BottomSheetDialog dialogDeliverWork;
    View view;
    View viewDeliverWork;
    AppCompatSpinner spinnerDisputeReason;
    EditText etRefund, etOtherDisputeReason, etDescriptionDeliverWork;
    OrderDetailResponse orderDetailResponse;
    LinearLayout llOther;
    String activityFrom, orderId;
    String disputeReason, requestRefund, otherDispute, descriptionDeliverWork, fileName;
    MaterialCardView cardViewDeliveryNote;
    BuyerOrderDetailResponse buyerOrderDetailResponse;

    private void init() {
        tvOrderNumber = findViewById(R.id.tvOrderNumber);
        tvDownload = findViewById(R.id.tvDownload);
        tvOrderStatus = findViewById(R.id.tvOrderStatus);
        cardViewDeliveryNote = findViewById(R.id.cardViewDeliveryNote);
        tvDescriptionOrderDetail = findViewById(R.id.tvDescriptionOrderDetail);
        tvBuyerName = findViewById(R.id.tvBuyerName);
        tvPriceOrderDetail = findViewById(R.id.tvPriceOrderDetail);
        btnDeliverWork = findViewById(R.id.btnDeliverWork);
        btnDispute = findViewById(R.id.btnDispute);
        spinnerDisputeReason = findViewById(R.id.spinnerDisputeReason);
        btnClose = findViewById(R.id.btnClose);
        btnSubmit = findViewById(R.id.btnSubmit);
        etRefund = findViewById(R.id.etRefund);
        btnRevise = findViewById(R.id.btnRevise);
        sessionManager = new SessionManager(this);

        if (activityFrom.equals("delivered")){
            btnDeliverWork.setVisibility(View.GONE);
            btnDispute.setVisibility(View.GONE);
            getFieldsData();
        }

        if (activityFrom.equals("active")){
            getFieldsData();
            cardViewDeliveryNote.setVisibility(View.GONE);
        }

        if (activityFrom.equals("buyerManage")){
            Log.e(TAG, "init: check");
            btnDeliverWork.setText(getResources().getString(R.string.completed));
            btnRevise.setVisibility(View.VISIBLE);
            getBuyerOrderDetail();
            btnDeliverWork.setOnClickListener(view -> buyerChangeOrderStatus("completed"));
            btnRevise.setOnClickListener(view->buyerChangeOrderStatus("revision"));
        } else {
            btnRevise.setVisibility(View.GONE);
            btnDeliverWork.setOnClickListener(view -> bottomSheetDeliverWork());
        }

        btnDispute.setOnClickListener(view -> bottomSheetDialog());

        tvDownload.setOnClickListener(view->{
            if (activityFrom.equals("buyerManage")){
                if (buyerOrderDetailResponse!=null){
                    if (buyerOrderDetailResponse.getDeliverNote()!=null){
                        if (buyerOrderDetailResponse.getDeliverNote().getFile()!=null){
                            ApplicationHandler.downloadFile(buyerOrderDetailResponse.getDeliverNote().getFile(), this);
                        }
                    }
                }
            } else {
                if (orderDetailResponse!=null){
                    if (orderDetailResponse.getDeliverNote()!=null){
                        if (orderDetailResponse.getDeliverNote().getFile()!=null){
                            ApplicationHandler.downloadFile(orderDetailResponse.getDeliverNote().getFile(), this);
                        }
                    }
                }
            }
        });

    }

    private void bottomSheetDeliverWork(){
        viewDeliverWork = LayoutInflater.from(this).inflate(R.layout.deliver_work_layout, null);

        tvFile = viewDeliverWork.findViewById(R.id.tvFile);
        etDescriptionDeliverWork = viewDeliverWork.findViewById(R.id.etDescriptionDeliverWork);
        btnCloseDeliverWork = viewDeliverWork.findViewById(R.id.btnCloseDeliverWork);
        btnSubmitDeliverWork = viewDeliverWork.findViewById(R.id.btnSubmitDeliverWork);

        if (dialogDeliverWork == null) {
            dialogDeliverWork = new BottomSheetDialog(this);
            dialogDeliverWork.setContentView(viewDeliverWork);
        }

        tvFile.setOnClickListener(view->requestPermissionsFromUser());

        dialogDeliverWork.show();

        btnCloseDeliverWork.setOnClickListener(view -> dialogDeliverWork.cancel());

        btnSubmitDeliverWork.setOnClickListener(view -> checkDataForDeliverWork());

    }

    private void bottomSheetDialog() {
        view = LayoutInflater.from(this).inflate(R.layout.dispute_layout, null);

        spinnerDisputeReason = view.findViewById(R.id.spinnerDisputeReason);
        etRefund = view.findViewById(R.id.etRefund);
        btnClose = view.findViewById(R.id.btnClose);
        btnSubmit = view.findViewById(R.id.btnSubmit);
        llOther = view.findViewById(R.id.llOther);
        etOtherDisputeReason = view.findViewById(R.id.etOtherDisputeReason);

        if (myDialog == null) {
            myDialog = new BottomSheetDialog(this);
            myDialog.setContentView(view);
        }

        myDialog.show();

        btnClose.setOnClickListener(view -> myDialog.cancel());

        setDisputeDropdown();

        btnSubmit.setOnClickListener(view -> checkDataForDispute());

    }

    private void setDisputeDropdown() {
        ArrayList<String> disputeList = new ArrayList<>();
        disputeList.add("Choose dispute reason");
        if (activityFrom.equals("buyerManage")){
            for (int i = 0; i < buyerOrderDetailResponse.getDisputereason().size(); i++) {
                disputeList.add(buyerOrderDetailResponse.getDisputereason().get(i).getTitle());
            }
        } else {
            for (int i = 0; i < orderDetailResponse.getDisputereason().size(); i++) {
                disputeList.add(orderDetailResponse.getDisputereason().get(i).getTitle());
            }
        }

        arrayAdapter(disputeList, spinnerDisputeReason);

    }

    private void arrayAdapter(ArrayList<String> array, AppCompatSpinner spinner) {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, array);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        if (activityFrom.equals("buyerManage")){
            spinnerDisputeReason.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    if (spinnerDisputeReason.getSelectedItemId() != 0) {

                        Log.e(TAG, "onItemSelected: " + spinnerDisputeReason.getSelectedItem().toString());
                        disputeReason = buyerOrderDetailResponse.getDisputereason().get(
                                (int) spinnerDisputeReason.getSelectedItemId() - 1
                        ).getId();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        } else {
            spinnerDisputeReason.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    if (spinnerDisputeReason.getSelectedItemId() != 0) {

                        Log.e(TAG, "onItemSelected: " + spinnerDisputeReason.getSelectedItem().toString());
                        disputeReason = orderDetailResponse.getDisputereason().get(
                                (int) spinnerDisputeReason.getSelectedItemId() - 1
                        ).getId();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        }

    }

    private void checkDataForDispute() {
        requestRefund = ApplicationHandler.stringConverter(etRefund);
        otherDispute = ApplicationHandler.stringConverter(etOtherDisputeReason);

        if (TextUtils.isEmpty(requestRefund)){
            etRefund.setError(getResources().getString(R.string.editTextEmptyFieldError));
            etRefund.requestFocus();
            return;
        } else {
            etRefund.setError(null);
        }

        if (disputeReason.equals("")){
            ApplicationHandler.toast("Select Dispute Reason");
            return;
        }

        Log.e(TAG, "order number: " + orderNum);
        Log.e(TAG, "dispute reason: " + disputeReason);
        Log.e(TAG, "request refund: " + requestRefund);
        Log.e(TAG, "other dispute: " + otherDispute);

        if (activityFrom.equals("buyerManage")){
            generateDisputeFromBuyer();
        } else {
            generateDispute();
        }

    }

    private void generateDisputeFromBuyer(){
        Loading.show(this, false, Constants.k_PLEASE_WAIT);

        Call<SignUpModel> call = RestApi.getService().generateDisputeFromBuyer(
                sessionManager.getString(Constants.k_ACCESSTOKEN),
                disputeReason,
                requestRefund,
                orderId,
                "normal"
        );

        call.enqueue(new Callback<SignUpModel>() {
            @Override
            public void onResponse(@NotNull Call<SignUpModel> call, @NotNull Response<SignUpModel> response) {
                Loading.cancel();
                if (response.isSuccessful()){
                    SignUpModel signUpModel = response.body();
                    if (signUpModel!=null){
                        ApplicationHandler.toast(signUpModel.getMessage());
                        myDialog.cancel();
                        ApplicationHandler.intent(ManageOrders.class);
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<SignUpModel> call, @NotNull Throwable t) {
                Loading.cancel();
                Log.e(TAG, "onFailure: " + t.getMessage());
                ApplicationHandler.toast(t.getMessage());
            }
        });
    }

    private void generateDispute(){
        Loading.show(this, false, Constants.k_PLEASE_WAIT);

        Call<SignUpModel> call = RestApi.getService().generateDispute(
                sessionManager.getString(Constants.k_ACCESSTOKEN),
                orderId,
                disputeReason,
                requestRefund,
                orderDetailResponse.getSellerorderdetail().get(0).getOrdertype()
        );

        call.enqueue(new Callback<SignUpModel>() {
            @Override
            public void onResponse(@NotNull Call<SignUpModel> call, @NotNull Response<SignUpModel> response) {
                Loading.cancel();
                if (response.isSuccessful()){
                    SignUpModel signUpModel = response.body();
                    if (signUpModel!=null){
                        ApplicationHandler.toast(signUpModel.getMessage());
                        myDialog.cancel();
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<SignUpModel> call, @NotNull Throwable t) {
                Loading.cancel();
                Log.e(TAG, "onFailure: " + t.getMessage());
                ApplicationHandler.toast(t.getMessage());
            }
        });
    }

    private void checkDataForDeliverWork(){
        descriptionDeliverWork = ApplicationHandler.stringConverter(etDescriptionDeliverWork);
        fileName = ApplicationHandler.textViewGetText(tvFile);

        if (TextUtils.isEmpty(descriptionDeliverWork)){
            etDescriptionDeliverWork.setError(getResources().getString(R.string.editTextEmptyFieldError));
            etDescriptionDeliverWork.requestFocus();
            return;
        } else {
            etDescriptionDeliverWork.setError(null);
        }

        Log.e(TAG, "checkDataForDeliverWork: "+getResources().getString(R.string.choose_file));
        if (fileName.equals(getResources().getString(R.string.choose_file))){
            ApplicationHandler.toast("Please Select A File");
            return;
        } else {
            Log.e(TAG, "descriptionDeliverWork: "+descriptionDeliverWork);
            Log.e(TAG, "fileName: "+fileName);

            deliverWorkToServer(descriptionDeliverWork);

        }

    }

    private void deliverWorkToServer(String description){

        Loading.show(this, false, Constants.k_PLEASE_WAIT);

        RequestBody requestBody = RequestBody.create(MediaType.parse(Objects.requireNonNull(getContentResolver().getType(imageUri))), imageFIle);
        RequestBody desc = RequestBody.create(MediaType.parse("text/plain"), description);
        RequestBody order = RequestBody.create(MediaType.parse("text/plain"), orderId);

        Call<SignUpModel> call = RestApi.getService().deliverWork(
                sessionManager.getString(Constants.k_ACCESSTOKEN),
                order,
                desc,
                requestBody
        );

        call.enqueue(new Callback<SignUpModel>() {
            @Override
            public void onResponse(@NotNull Call<SignUpModel> call, @NotNull Response<SignUpModel> response) {
                Loading.cancel();

                if (response.isSuccessful()){
                    SignUpModel responseResult = response.body();
                    if (responseResult!=null){
                        ApplicationHandler.toast(responseResult.getMessage());
                        ApplicationHandler.intent(ManageSalesActivity.class);
                    }
                }

            }

            @Override
            public void onFailure(@NotNull Call<SignUpModel> call, @NotNull Throwable t) {
                Loading.cancel();
                Log.e(TAG, "onFailure: "+t.getMessage());
                ApplicationHandler.toast(t.getMessage());
            }
        });

    }

    private void buyerChangeOrderStatus(String status){
        Loading.show(this, false, Constants.k_PLEASE_WAIT);

        Call<SignUpModel> call = null;

        if(status.equals("completed")){
            call = RestApi.getService().buyerChangeOrderStatus(
                    sessionManager.getString(Constants.k_ACCESSTOKEN),
                    orderId,
                    "3"
            );
        }

        if (status.equals("revision")){
            call = RestApi.getService().buyerChangeOrderStatus(
                    sessionManager.getString(Constants.k_ACCESSTOKEN),
                    orderId,
                    "5"
            );
        }

        if (call!=null) {
            call.enqueue(new Callback<SignUpModel>() {
                @Override
                public void onResponse(@NotNull Call<SignUpModel> call, @NotNull Response<SignUpModel> response) {
                    Loading.cancel();
                    if (response.isSuccessful()) {
                        SignUpModel signUpModel = response.body();
                        if (signUpModel != null) {
                            ApplicationHandler.toast(signUpModel.getMessage());
                            myDialog.cancel();
                            ApplicationHandler.intent(ManageOrders.class);
                        }
                    }
                }

                @Override
                public void onFailure(@NotNull Call<SignUpModel> call, @NotNull Throwable t) {
                    Loading.cancel();
                    Log.e(TAG, "onFailure: " + t.getMessage());
                    ApplicationHandler.toast(t.getMessage());
                }
            });
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);

        orderNum = getIntent().getStringExtra("orderNum");
        activityFrom = getIntent().getStringExtra("activity");

        init();

    }

    private String getType(){
        String type;

        if (Constants.k_buyerCustomOrder){
            type="custom";
        } else {
            type="normal";
        }

        return type;
    }

    private void getFieldsData() {
        Loading.show(this, false, Constants.k_PLEASE_WAIT);
        Call<OrderDetailResponse> call = RestApi.getService()
                .getOrderDetail(
                        sessionManager.getString(Constants.k_ACCESSTOKEN),
                        orderNum);

        call.enqueue(new Callback<OrderDetailResponse>() {
            @Override
            public void onResponse(@NotNull Call<OrderDetailResponse> call, @NotNull Response<OrderDetailResponse> response) {
                Loading.cancel();
                if (response.isSuccessful()) {
                    orderDetailResponse = response.body();

                    if (orderDetailResponse != null) {
                        if (orderDetailResponse.getSellerorderdetail() != null) {
                            tvOrderNumber.setText(orderDetailResponse.getSellerorderdetail().get(0).getOrderNo());
                            tvDescriptionOrderDetail.setText(orderDetailResponse.getSellerorderdetail().get(0).getOfferDescription());
                            tvBuyerName.setText(orderDetailResponse.getSellerorderdetail().get(0).getPayerName());
                            tvPriceOrderDetail.setText(orderDetailResponse.getSellerorderdetail().get(0).getPrice());
                            orderId = orderDetailResponse.getSellerorderdetail().get(0).getOrderId();
                        }

                        if (orderDetailResponse.getOrderStatus()!=null){
                            tvOrderStatus.setText(orderDetailResponse.getOrderStatus());
                        }

                    }

                }
            }

            @Override
            public void onFailure(@NotNull Call<OrderDetailResponse> call, @NotNull Throwable t) {
                Loading.cancel();
                Log.e(TAG, "onFailure: " + t.getMessage());
                ApplicationHandler.toast(t.getMessage());
            }
        });
    }

    private void getBuyerOrderDetail(){
        Loading.show(this, false, Constants.k_PLEASE_WAIT);
        Call<BuyerOrderDetailResponse> call = RestApi.getService()
                .getBuyerOrderDetail(
                        sessionManager.getString(Constants.k_ACCESSTOKEN),
                        orderNum
                ,getType());

        call.enqueue(new Callback<BuyerOrderDetailResponse>() {
            @Override
            public void onResponse(@NotNull Call<BuyerOrderDetailResponse> call, @NotNull Response<BuyerOrderDetailResponse> response) {
                Loading.cancel();
                if (response.isSuccessful()) {
                    buyerOrderDetailResponse = response.body();

                    if (buyerOrderDetailResponse != null) {
                        if (buyerOrderDetailResponse.getSellerorderdetail() != null) {
                            tvOrderNumber.setText(buyerOrderDetailResponse.getSellerorderdetail().get(0).getOrderNo());
                            tvDescriptionOrderDetail.setText(buyerOrderDetailResponse.getSellerorderdetail().get(0).getOfferDescription());
                            tvBuyerName.setText(buyerOrderDetailResponse.getSellerorderdetail().get(0).getPayerName());
                            tvPriceOrderDetail.setText(buyerOrderDetailResponse.getSellerorderdetail().get(0).getAmount());
                            orderId = buyerOrderDetailResponse.getSellerorderdetail().get(0).getOrderderid();
                        }

                        if (buyerOrderDetailResponse.getOrderStatus()!=null){
                            tvOrderStatus.setText(buyerOrderDetailResponse.getOrderStatus());
                        }

                    }

                }
            }

            @Override
            public void onFailure(@NotNull Call<BuyerOrderDetailResponse> call, @NotNull Throwable t) {
                Loading.cancel();
                Log.e(TAG, "onFailure: " + t.getMessage());
                ApplicationHandler.toast(t.getMessage());
            }
        });
    }

    private void requestPermissionsFromUser() {
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            pickImageForGallery();
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            ApplicationHandler.showSettingsDialog(OrderDetailActivity.this);
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(error -> Toast.makeText(getApplicationContext(), "Error occurred! ", Toast.LENGTH_SHORT).show())
                .onSameThread()
                .check();
    }

    private void pickImageForGallery() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, GALLERY_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GALLERY_REQUEST_CODE && resultCode == RESULT_OK) {
            Log.e(TAG, "image file gallery: "+imageFIle);

            imageUri = data.getData();
            imageFilePath = FileUtils.getPath(this, imageUri);

            if (imageFilePath != null) {
                imageFIle = new File(imageFilePath);
            }
            tvFile.setText(imageFIle.getName());
            tvFile.setTextColor(getResources().getColor(R.color.colorBlack));
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

}
