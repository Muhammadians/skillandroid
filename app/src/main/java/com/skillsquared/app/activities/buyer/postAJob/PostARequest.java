package com.skillsquared.app.activities.buyer.postAJob;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.card.MaterialCardView;
import com.skillsquared.app.R;
import com.skillsquared.app.SkillSquared;
import com.skillsquared.app.activities.HomeActivity;
import com.skillsquared.app.activities.buyer.ViewOffers;
import com.skillsquared.app.activities.buyer.WhatToDoActivityBuyer;
import com.skillsquared.app.handler.ApplicationHandler;
import com.skillsquared.app.handler.RestApi;
import com.skillsquared.app.models.postAJob.GetCategoriesModelForPostAJob;
import com.skillsquared.app.models.postAJob.PostAJobResponse;
import com.skillsquared.app.models.postAJob.SubCatChildModelForPostAJob;
import com.skillsquared.app.models.postAJob.SubCatModelForPostAJob;
import com.skillsquared.app.utils.Constants;
import com.skillsquared.app.utils.Loading;
import com.skillsquared.app.utils.SessionManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostARequest extends AppCompatActivity {

    private final static String TAG = PostARequest.class.getSimpleName();
    MaterialCardView cardViewPostARequest, btnOk;
    Dialog myDialog;
    EditText etDescription, etBudgetPostRequest;
    AppCompatSpinner spinnerCategoryPostRequest,
            spinnerSubCategoryPostRequest,
            spinnerSubCategoryChildPostRequest,
            spinnerDeliveryTimePostRequest;
    LinearLayout llSubCatLayout, llSubCatChildLayout;
    private TextView tvCounter;
    List<GetCategoriesModelForPostAJob> getCategoriesModelForPostAJobs = null;
    String cat_id = null;
    int deliveryTimeVal = 0;
    private SessionManager sessionManager;

    void init() {
        cardViewPostARequest = findViewById(R.id.submitPostARequest);
        etDescription = findViewById(R.id.etDescription);
        spinnerCategoryPostRequest = findViewById(R.id.spinnerCategoryPostRequest);
        spinnerSubCategoryPostRequest = findViewById(R.id.spinnerSubCategoryPostRequest);
        spinnerSubCategoryChildPostRequest = findViewById(R.id.spinnerSubCategoryChildPostRequest);
        spinnerDeliveryTimePostRequest = findViewById(R.id.spinnerDeliveryTimePostRequest);
        etBudgetPostRequest = findViewById(R.id.etBudgetPostRequest);
        llSubCatLayout = findViewById(R.id.llSubCatLayout);
        llSubCatChildLayout = findViewById(R.id.llSubCatChildLayout);
        tvCounter = findViewById(R.id.tvCounter);
        ImageView ivBackPostRequest = findViewById(R.id.ivBackPostRequest);
        myDialog = new Dialog(this);
        sessionManager = new SessionManager(this);
        setDeliveryTimeDropDown();

        ivBackPostRequest.setOnClickListener(view -> onBackPressed());

        final TextWatcher txwatcher = new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                int tick = start + after;
                if (tick <= 3000) {
                    int remaining = 3000 - tick;

                    if (remaining==3000){
                        tvCounter.setText(getResources().getString(R.string._0_3000));
                    } else {
                        tvCounter.setText(remaining + "/3000");
                    }
                }
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            public void afterTextChanged(Editable s) {
            }

        };

        etDescription.addTextChangedListener(txwatcher);

        getCatOptions();

        cardViewPostARequest.setOnClickListener(view -> checkValues());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_arequest);

        init();

    }

    void getCatOptions() {
        Loading.show(this, false, Constants.k_PLEASE_WAIT);
        Call<List<GetCategoriesModelForPostAJob>> getCategoriesModelForPostAJobCall =
                RestApi.getService().getCatForPostJob();

        getCategoriesModelForPostAJobCall.enqueue(new Callback<List<GetCategoriesModelForPostAJob>>() {
            @Override
            public void onResponse(@NonNull Call<List<GetCategoriesModelForPostAJob>> call,@NonNull Response<List<GetCategoriesModelForPostAJob>> response) {
                Loading.cancel();

                if (response.isSuccessful()) {
                    getCategoriesModelForPostAJobs = response.body();
                    if (getCategoriesModelForPostAJobs != null) {
                        setListInSpinner(getCategoriesModelForPostAJobs);
                    }
                }

            }

            @Override
            public void onFailure(@NonNull Call<List<GetCategoriesModelForPostAJob>> call,@NonNull Throwable t) {
                Loading.cancel();
                Log.e(TAG, "onFailure: " + t.getMessage());
                ApplicationHandler.toast(t.getMessage());
            }
        });

    }

    void showPopUp() {
        myDialog.setContentView(R.layout.post_a_job_success_popup);
        Objects.requireNonNull(myDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myDialog.setCancelable(false);
        myDialog.show();

        btnOk = myDialog.findViewById(R.id.cardViewOkay);
        btnOk.setOnClickListener(view -> {
            myDialog.cancel();
            ApplicationHandler.intent(HomeActivity.class);
        });
    }

    private void arrayAdapter(ArrayList<String> array, AppCompatSpinner spinner) {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, array);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    void setListInSpinner(final List<GetCategoriesModelForPostAJob> list) {
        ArrayList<String> categoriesList = new ArrayList<>();
        categoriesList.add("Select Category");
        for (int i = 0; i < list.size(); i++) {
            categoriesList.add(list.get(i).getTitle());
        }
        arrayAdapter(categoriesList, spinnerCategoryPostRequest);

        spinnerCategoryPostRequest.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                List<SubCatModelForPostAJob> subList;
                llSubCatChildLayout.setVisibility(View.GONE);
                llSubCatLayout.setVisibility(View.GONE);
                cat_id = null;
                for (int j = 0; j < list.size(); j++) {
                    if (list.get(j).getTitle().equals(spinnerCategoryPostRequest.getSelectedItem().toString())) {
                        if (list.get(j).getSubcat() != null) {
                            subList = list.get(j).getSubcat();
                            setSubList(subList);
                        } else {
                            llSubCatLayout.setVisibility(View.GONE);
                            llSubCatChildLayout.setVisibility(View.GONE);
                            cat_id = list.get(j).getCatId();
                            Log.e(TAG, "onItemSelected: " + cat_id);
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private void setSubList(final List<SubCatModelForPostAJob> list) {
        ArrayList<String> subCategoryList = new ArrayList<>();
        subCategoryList.add("Select Subcategory");
        if (list != null) {
            for (int i = 0; i < list.size(); i++) {
                subCategoryList.add(list.get(i).getTitle());
            }
            llSubCatLayout.setVisibility(View.VISIBLE);
            arrayAdapter(subCategoryList, spinnerSubCategoryPostRequest);

            spinnerSubCategoryPostRequest.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    cat_id = null;
                    List<SubCatChildModelForPostAJob> subList;
                    for (int j = 0; j < list.size(); j++) {
                        if (list.get(j).getTitle().equals(spinnerSubCategoryPostRequest.getSelectedItem().toString())) {
                            if (list.get(j).getSubcatChild() != null) {
                                subList = list.get(j).getSubcatChild();
                                setSubCatChildList(subList);
                            } else {
                                llSubCatChildLayout.setVisibility(View.GONE);
                                cat_id = list.get(j).getId();
                                Log.e(TAG, "onItemSelected: " + cat_id);
                            }
                        }
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

        }

    }

    private void setSubCatChildList(final List<SubCatChildModelForPostAJob> list) {
        ArrayList<String> subCategoryList = new ArrayList<>();
        subCategoryList.add("Select Subcategory Child");
        if (list != null) {
            for (int i = 0; i < list.size(); i++) {
                subCategoryList.add(list.get(i).getTitle());
            }
            llSubCatChildLayout.setVisibility(View.VISIBLE);
            arrayAdapter(subCategoryList, spinnerSubCategoryChildPostRequest);
        }

        spinnerSubCategoryChildPostRequest.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                cat_id = null;
                if (list != null) {
                    for (int j = 0; j < list.size(); j++) {
                        if (list.get(j).getTitle().equals(spinnerSubCategoryChildPostRequest.getSelectedItem().toString())) {
                            cat_id = list.get(j).getId();
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private void setDeliveryTimeDropDown(){
        ArrayList<String> deliveryTime = new ArrayList<>();
        deliveryTime.add("Select Delivery Time");
        int j = 1;
        for (int i = 0; i <= 29; i++) {
            deliveryTime.add(j+"");
            j++;
        }

        arrayAdapter(deliveryTime, spinnerDeliveryTimePostRequest);

        spinnerDeliveryTimePostRequest.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (!spinnerDeliveryTimePostRequest.getSelectedItem().toString().equals("Select Delivery Time")){
                    deliveryTimeVal = Integer.parseInt(spinnerDeliveryTimePostRequest.getSelectedItem().toString());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private void checkValues(){

        if (TextUtils.isEmpty(ApplicationHandler.stringConverter(etDescription))){
            etDescription.setError(getResources().getString(R.string.editTextEmptyFieldError));
            etDescription.requestFocus();
            return;
        } else {
            etDescription.setError(null);
        }

        if (cat_id==null){
            ApplicationHandler.toast("Please select a category...");
            return;
        }

        if (deliveryTimeVal == 0){
            TextView errorText = (TextView)spinnerDeliveryTimePostRequest.getSelectedView();
            errorText.setError("");
            errorText.setTextColor(Color.RED);//just to highlight that this is an error
            errorText.setText(getResources().getString(R.string.deliveryTimeError));//changes the selected item text to this
            return;
        }

        if (TextUtils.isEmpty(ApplicationHandler.stringConverter(etBudgetPostRequest))){
            etBudgetPostRequest.setError(getResources().getString(R.string.editTextEmptyFieldError));
            etBudgetPostRequest.requestFocus();
            return;
        } else {
            etBudgetPostRequest.setError(null);
        }

        Log.e(TAG, "checkValues: "+ApplicationHandler.stringConverter(etDescription));
        Log.e(TAG, "checkValues: "+cat_id);
        Log.e(TAG, "checkValues: "+deliveryTimeVal);
        Log.e(TAG, "checkValues: "+ApplicationHandler.stringConverter(etBudgetPostRequest));

        postDataToServer();

    }

    void postDataToServer(){
        Loading.show(this, false, Constants.k_PLEASE_WAIT);
        Call<PostAJobResponse> postAJobResponseCall = RestApi.getService().createPost(
                sessionManager.getString(Constants.k_ACCESSTOKEN),
                cat_id,
                ApplicationHandler.stringConverter(etDescription),
                Integer.parseInt(ApplicationHandler.stringConverter(etBudgetPostRequest)),
                deliveryTimeVal + " Days"
        );

        postAJobResponseCall.enqueue(new Callback<PostAJobResponse>() {
            @Override
            public void onResponse(@NonNull Call<PostAJobResponse> call,@NonNull Response<PostAJobResponse> response) {
                Loading.cancel();
                if (response.isSuccessful()){
                    PostAJobResponse postAJobResponse = response.body();
                    if (postAJobResponse != null) {
                        showPopUp();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<PostAJobResponse> call,@NonNull Throwable t) {
                Loading.cancel();
                Log.e(TAG, "onFailure: "+t.getMessage());
                ApplicationHandler.toast(t.getMessage());
            }
        });

    }

    @Override
    public void onBackPressed() {
        ApplicationHandler.intent(WhatToDoActivityBuyer.class);
    }
}
