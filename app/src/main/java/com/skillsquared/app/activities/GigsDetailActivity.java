package com.skillsquared.app.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.abdulhakeem.seemoretextview.SeeMoreTextView;
import com.braintreepayments.api.BraintreeFragment;
import com.braintreepayments.api.dropin.DropInActivity;
import com.braintreepayments.api.dropin.DropInRequest;
import com.braintreepayments.api.dropin.DropInResult;
import com.braintreepayments.api.exceptions.InvalidArgumentException;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.skillsquared.app.R;
import com.skillsquared.app.activities.buyer.postAJob.PostedRequestList;
import com.skillsquared.app.activities.seller.AboutUserActivity;
import com.skillsquared.app.adapters.ReviewAdapter;
import com.skillsquared.app.cometChat.CustomView.CircleImageView;
import com.skillsquared.app.handler.ApplicationHandler;
import com.skillsquared.app.handler.RestApi;
import com.skillsquared.app.models.PlaceOrderResponse;
import com.skillsquared.app.models.saveCustomOrder.SaveCustomOrderResponse;
import com.skillsquared.app.models.sellerProfileModel.SellerProfileModel;
import com.skillsquared.app.utils.Constants;
import com.skillsquared.app.utils.ExpandableTextView;
import com.skillsquared.app.utils.Loading;
import com.skillsquared.app.utils.SessionManager;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import co.paystack.android.Paystack;
import co.paystack.android.PaystackSdk;
import co.paystack.android.Transaction;
import co.paystack.android.model.Card;
import co.paystack.android.model.Charge;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GigsDetailActivity extends AppCompatActivity {

    private final static String TAG = GigsDetailActivity.class.getSimpleName();
    ImageView ivGigsDetails;
    CircleImageView ivUserProfile;
    TextView tvUserNameGigsDetail, tvGigsTitle, tvReviewNum, tvPriceGigsDetail;
    ExpandableTextView tvGigsDescription;
    RatingBar tvRatingGigsDetail;
    RecyclerView recycleReviews;
    String pos;
    SessionManager sessionManager;
    Button btnCustomOrder;
    BottomSheetDialog bsCustomOrder;
    View viewBottomSheet;
    EditText etSellerServiceDesc, etPrice;
    CardView submitSellerOffer;
    AppCompatSpinner spinnerDeliveryTimeSellerService;
    String deliveryTimeVal = null, description, totalOffer;
    int position = 0;
    private Dialog paymentDialog;
    private Dialog payStackDialog;
    private Charge charge;
    private String cardNumber, cvv, expiryMonth, expiryYear;
    BraintreeFragment mBraintreeFragment;
    SaveCustomOrderResponse customOrderResponse;

    void init() {

        pos = getIntent().getStringExtra("position");
        sessionManager = new SessionManager(this);

        if (pos != null) {
            position = Integer.parseInt(pos);
        }

        ivGigsDetails = findViewById(R.id.ivGigsDetails);
        ivUserProfile = findViewById(R.id.ivUserProfile);
        tvUserNameGigsDetail = findViewById(R.id.tvUserNameGigsDetail);
        tvGigsTitle = findViewById(R.id.tvGigsTitle);
        tvGigsDescription = findViewById(R.id.tvGigsDescription);
        tvReviewNum = findViewById(R.id.tvReviewNum);
        tvRatingGigsDetail = findViewById(R.id.tvRatingGigsDetail);
        tvPriceGigsDetail = findViewById(R.id.tvPriceGigsDetail);
        btnCustomOrder = findViewById(R.id.btnCustomOrder);
        recycleReviews = findViewById(R.id.recycleReviews);

        paymentDialog = new Dialog(this);
        payStackDialog = new Dialog(this);

        try {
            mBraintreeFragment = BraintreeFragment.newInstance(this, "sandbox_38tmdkrj_hrrcf2jbtw2kwwjb");
            // mBraintreeFragment is ready to use!
        } catch (InvalidArgumentException e) {
            Log.e(TAG, "onCreateViewHolder: " + e.getMessage());
        }

        recycleReviews.setNestedScrollingEnabled(false);

        final int freelanceId = Constants.k_PopularServiceList.get(position).getFreelancerId();

        String des = Html.fromHtml(Constants.k_PopularServiceList.get(position).getDescription()).toString();

        Picasso.get().load(Constants.k_PopularServiceList.get(position).getMedia()).placeholder(R.drawable.noimg).into(ivGigsDetails);
        Picasso.get().load(Constants.k_PopularServiceList.get(position).getFreelancerImage()).placeholder(R.drawable.noimg).into(ivUserProfile);
        tvUserNameGigsDetail.setText(Constants.k_PopularServiceList.get(position).getFreelancerUsername());
        tvGigsTitle.setText(Constants.k_PopularServiceList.get(position).getServiceTitle());
        tvGigsDescription.setText(des);
        tvPriceGigsDetail.setText("$"+Constants.k_PopularServiceList.get(position).getPrice());
        btnCustomOrder.setOnClickListener(view->{

            if (bsCustomOrder == null){
                bsCustomOrder = new BottomSheetDialog(this);
                bsCustomOrder.setContentView(viewBottomSheet);
            }

            bsCustomOrder.show();

        });
        if (Constants.k_PopularServiceList.get(position).getServiceRating()!=null){
            String dummy = Constants.k_PopularServiceList.get(position).getServiceRating();
            List<String> wordList = Arrays.asList(dummy.split("=>"));
            tvRatingGigsDetail.setRating(Float.valueOf(wordList.get(1)));
        }

        if (Constants.k_PopularServiceList.get(position).getReviews()!=null){
            tvReviewNum.setText(Constants.k_PopularServiceList.get(position).getReviews().size()+" Reviews");
            ReviewAdapter reviewAdapter = new ReviewAdapter(GigsDetailActivity.this, Constants.k_PopularServiceList.get(position).getReviews());
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
            recycleReviews.setLayoutManager(linearLayoutManager);
            recycleReviews.setAdapter(reviewAdapter);
        } else {
            tvReviewNum.setText("0 Reviews");
        }

        ivUserProfile.setOnClickListener(view-> ApplicationHandler.intent(AboutUserActivity.class, "source", String.valueOf(freelanceId)));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ApplicationHandler.hideStatusBar(this);
        setContentView(R.layout.activity_gigs_detail);

        init();
        initBottomSheet();

        findViewById(R.id.ivBackGigsDetail).setOnClickListener(view->onBackPressed());

    }

    private void initBottomSheet(){
        viewBottomSheet = LayoutInflater.from(this).inflate(R.layout.seller_service_detail_pop_up, null);

        etSellerServiceDesc = viewBottomSheet.findViewById(R.id.etSellerServiceDesc);
        etPrice = viewBottomSheet.findViewById(R.id.etPrice);
        spinnerDeliveryTimeSellerService = viewBottomSheet.findViewById(R.id.spinnerDeliveryTimeSellerService);
        submitSellerOffer = viewBottomSheet.findViewById(R.id.submitSellerOffer);
        setDeliveryTimeDropDown();
        submitSellerOffer.setOnClickListener(view->getBottomSheetValues());
    }

    private void setDeliveryTimeDropDown(){
        ArrayList<String> deliveryTime = new ArrayList<>();
        deliveryTime.add("Select Delivery Time");
        int j = 1;
        for (int i = 0; i <= 29; i++) {
            deliveryTime.add(j+" days");
            j++;
        }

        arrayAdapter(deliveryTime, spinnerDeliveryTimeSellerService);

        spinnerDeliveryTimeSellerService.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (!spinnerDeliveryTimeSellerService.getSelectedItem().toString().equals("Select Delivery Time")){
                    String delivery = spinnerDeliveryTimeSellerService.getSelectedItem().toString();
                    String[] splitted = delivery.split(" ");
                    deliveryTimeVal = splitted[0];
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private void arrayAdapter(ArrayList<String> array, AppCompatSpinner spinner) {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, array);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    private void getBottomSheetValues(){
        description = ApplicationHandler.stringConverter(etSellerServiceDesc);
        totalOffer = ApplicationHandler.stringConverter(etPrice);

        if (TextUtils.isEmpty(description)){
            etSellerServiceDesc.setError(getResources().getString(R.string.editTextEmptyFieldError));
            etSellerServiceDesc.requestFocus();
            return;
        } else {
            etSellerServiceDesc.setError(null);
        }

        if (TextUtils.isEmpty(totalOffer)){
            etPrice.setError(getResources().getString(R.string.editTextEmptyFieldError));
            etPrice.requestFocus();
            return;
        } else {
            etPrice.setError(null);
        }

        if (deliveryTimeVal == null){
            ApplicationHandler.toast(getResources().getString(R.string.select_delivery_time));
            return;
        } else {
            Log.e(TAG, "description: "+description);
            Log.e(TAG, "total offer: "+totalOffer);
            Log.e(TAG, "delivery time value: "+deliveryTimeVal);

            saveCustomOrder(description, totalOffer, deliveryTimeVal);
        }
    }

    private void saveCustomOrder(String description, String price, String deliveryTimeVal){
        Loading.show(this, false, Constants.k_PLEASE_WAIT);
        Call<SaveCustomOrderResponse> call = RestApi.getService().saveCustomOrder(
            sessionManager.getString(Constants.k_ACCESSTOKEN),
                Constants.k_PopularServiceList.get(position).getId(),
                price,
                deliveryTimeVal,
                description
        );

        call.enqueue(new Callback<SaveCustomOrderResponse>() {
            @Override
            public void onResponse(@NotNull Call<SaveCustomOrderResponse> call, @NotNull Response<SaveCustomOrderResponse> response) {
                Loading.cancel();
                if (response.isSuccessful()){
                    customOrderResponse = response.body();
                    if (customOrderResponse!=null){
                        bsCustomOrder.cancel();
                        showDialog();
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<SaveCustomOrderResponse> call, @NotNull Throwable t) {
                Loading.cancel();
                Log.e(TAG, "onFailure: "+t.getMessage());
                ApplicationHandler.toast(t.getMessage());
            }
        });

    }

    private void showDialog() {

        paymentDialog.setContentView(R.layout.payment_method_dialog);
        paymentDialog.setCancelable(true);

        Window window = paymentDialog.getWindow();
        if (window != null) {
            window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            WindowManager.LayoutParams wlp = window.getAttributes();
            wlp.gravity = Gravity.CENTER;
            wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
            wlp.dimAmount = 0.3f;
            window.setAttributes(wlp);
        }
        paymentDialog.show();

        CardView cardViewPaypal, cardViewPaystack;

        cardViewPaypal = paymentDialog.findViewById(R.id.cardViewPaypal);
        cardViewPaystack = paymentDialog.findViewById(R.id.cardViewPayStack);

        cardViewPaypal.setOnClickListener(view -> onBraintreeSubmit());

        cardViewPaystack.setOnClickListener(view -> {
            PaystackSdk.initialize(getApplicationContext());
            showPayStackDialog();
        });

    }

    private void onBraintreeSubmit() {
        DropInRequest dropInRequest = new DropInRequest()
                .clientToken("sandbox_kttn24dq_63zmsxbxhgp8p9g9");
        startActivityForResult(dropInRequest.getIntent(this), 111);
        paymentDialog.cancel();
    }

    private void createCard(String cardNumber, int month, int year, String cvc) {

        Loading.show(this, false, Constants.k_PLEASE_WAIT);
        Card card = new Card(cardNumber, month, year, cvc);
        card.isValid();

        if (card.isValid()) {
//            Toast.makeText(activity, "Card is Valid", Toast.LENGTH_LONG).show();
            performCharge(card);
        } else {
            Loading.cancel();
            Toast.makeText(this, "Card not Valid", Toast.LENGTH_LONG).show();
        }
    }

    private void performCharge(Card card) {
        //create a Charge object
        charge = new Charge();

        //set the card to charge
        charge.setCard(card);

        charge.setEmail("mytestemail@test.com"); //dummy email address

        charge.setAmount(100); //test amount

        PaystackSdk.chargeCard(this, charge, new Paystack.TransactionCallback() {
            @Override
            public void onSuccess(Transaction transaction) {
                // This is called only after transaction is deemed successful.
                // Retrieve the transaction, and send its reference to your server
                // for verification.
                Loading.cancel();
                String paymentReference = transaction.getReference();
                Toast.makeText(GigsDetailActivity.this, "Transaction Successful! payment reference: "
                        + paymentReference, Toast.LENGTH_LONG).show();
                payStackDialog.cancel();
            }

            @Override
            public void beforeValidate(Transaction transaction) {
                // This is called only before requesting OTP.
                // Save reference so you may send to server. If
                // error occurs with OTP, you should still verify on server.
                Loading.cancel();
                payStackDialog.cancel();
                Log.e(TAG, "beforeValidate: " + transaction.getReference());
            }

            @Override
            public void onError(Throwable error, Transaction transaction) {
                Loading.cancel();
                //handle error here
                Log.e(TAG, "onError: " + error.getMessage());
                Log.e(TAG, "onError: " + transaction.getReference());
            }
        });

    }

    private void showPayStackDialog() {
        payStackDialog.setContentView(R.layout.paystack_layout);
        payStackDialog.setCancelable(true);
        paymentDialog.cancel();

        Window window = payStackDialog.getWindow();
        if (window != null) {
            window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            WindowManager.LayoutParams wlp = window.getAttributes();
            wlp.gravity = Gravity.CENTER;
            wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
            wlp.dimAmount = 0.3f;
            window.setAttributes(wlp);
        }

        final EditText etCardNumber = payStackDialog.findViewById(R.id.etCardNumber);
        final EditText etMonth = payStackDialog.findViewById(R.id.etMonth);
        final EditText etYear = payStackDialog.findViewById(R.id.etYear);
        final EditText etCVV = payStackDialog.findViewById(R.id.etCVV);
        CardView payStackDetailsSubmit = payStackDialog.findViewById(R.id.payStackDetailsSubmit);

        payStackDialog.show();

        payStackDetailsSubmit.setOnClickListener(view -> {
            cardNumber = ApplicationHandler.stringConverter(etCardNumber);
            expiryMonth = ApplicationHandler.stringConverter(etMonth);
            expiryYear = ApplicationHandler.stringConverter(etYear);
            cvv = ApplicationHandler.stringConverter(etCVV);

            if (TextUtils.isEmpty(cardNumber)) {
                etCardNumber.setError(getResources().getString(R.string.editTextEmptyFieldError));
                etCardNumber.requestFocus();
                return;
            } else {
                etCardNumber.setError(null);
            }

            if (TextUtils.isEmpty(String.valueOf(expiryMonth))) {
                etMonth.setError(getResources().getString(R.string.editTextEmptyFieldError));
                etMonth.requestFocus();
                return;
            } else {
                etMonth.setError(null);
            }

            if (TextUtils.isEmpty(String.valueOf(expiryYear))) {
                etYear.setError(getResources().getString(R.string.editTextEmptyFieldError));
                etYear.requestFocus();
                return;
            } else {
                etYear.setError(null);
            }

            if (TextUtils.isEmpty(cvv)) {
                etCVV.setError(getResources().getString(R.string.editTextEmptyFieldError));
                etCVV.requestFocus();
                return;
            } else {
                etCVV.setError(null);
            }

            createCard(cardNumber, Integer.parseInt(expiryMonth), Integer.parseInt(expiryYear), cvv);

        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 111) {
            if (resultCode == RESULT_OK) {
                DropInResult result = null;
                if (data != null) {
                    result = data.getParcelableExtra(DropInResult.EXTRA_DROP_IN_RESULT);
                }
                if (result != null) {
                    Log.e(TAG, "onActivityResult: "+ Objects.requireNonNull(result.getPaymentMethodNonce()).getDescription());
                    Log.e(TAG, "onActivityResult: "+result.getPaymentMethodNonce().getTypeLabel());
                    Log.e(TAG, "onActivityResult: " + Objects.requireNonNull(result.getPaymentMethodNonce()).getNonce());

                    placeOrder(Objects.requireNonNull(result.getPaymentMethodNonce()).getNonce());

                }
            } else if (resultCode == RESULT_CANCELED) {
                Log.e(TAG, "onActivityResult: user cancelled");
            } else {
                // handle errors here, an exception may be available in
                if (data != null) {
                    Exception error = (Exception) data.getSerializableExtra(DropInActivity.EXTRA_ERROR);
                    Log.e(TAG, "onActivityResult: "+error);
                }
            }
        }
    }

    private void placeOrder(String nonce){
        if (nonce!=null){
            Loading.show(this, false, Constants.k_PLEASE_WAIT);
                double price_temp = Double.parseDouble(totalOffer);
                String price = String.valueOf(price_temp+3.00);

                Call<PlaceOrderResponse> getPaypalResponse = RestApi.getService().placeCustomOrder(
                        sessionManager.getString(Constants.k_ACCESSTOKEN),
                        nonce,
                        price,
                        customOrderResponse.getOrderObject().getCustomorderid(),
                        "paypal"
                );

                getPaypalResponse.enqueue(new Callback<PlaceOrderResponse>() {
                    @Override
                    public void onResponse(@NonNull Call<PlaceOrderResponse> call, @NonNull Response<PlaceOrderResponse> response) {
                        if (response.isSuccessful()) {
                            PlaceOrderResponse paypalResponse = response.body();
                            if (paypalResponse != null) {
                                Loading.cancel();
                                ApplicationHandler.toast(paypalResponse.getMessage());
                            } else {
                                Loading.cancel();
                                ApplicationHandler.toast(getResources().getString(R.string.something_went_wrong));
                            }
                        } else {
                            Loading.cancel();
                            ApplicationHandler.toast(response.message());
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<PlaceOrderResponse> call, @NonNull Throwable t) {
                        Log.e(TAG, "onFailure: " + t.getMessage());
                        ApplicationHandler.toast("Error" + t.getMessage());
                        Loading.cancel();
                    }
                });
        }
    }

}
