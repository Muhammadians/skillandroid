package com.skillsquared.app.activities.seller;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Html;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.skillsquared.app.R;
import com.skillsquared.app.handler.ApplicationHandler;
import com.skillsquared.app.utils.Constants;
import com.squareup.picasso.Picasso;

import java.util.Arrays;
import java.util.List;

public class AboutUserGigsDetailActivity extends AppCompatActivity {

    TextView tvDateAboutUser, tvUserNameAboutUserGigsDetail,
            tvAboutUserDescription, tvAdditionalInfoAboutUser;
    ImageView ivAboutUserGigs, ivUserProfileAboutUser;
    RatingBar tvRatingAboutUserDetail;

    private void init(){
        tvDateAboutUser = findViewById(R.id.tvDateAboutUser);
        tvUserNameAboutUserGigsDetail = findViewById(R.id.tvUserNameAboutUserGigsDetail);
        tvAboutUserDescription = findViewById(R.id.tvAboutUserDescription);
        tvAdditionalInfoAboutUser = findViewById(R.id.tvAdditionalInfoAboutUser);
        ivAboutUserGigs = findViewById(R.id.ivAboutUserGigs);
        ivUserProfileAboutUser = findViewById(R.id.ivUserProfileAboutUser);
        tvRatingAboutUserDetail = findViewById(R.id.tvRatingAboutUserDetail);

        setData();

    }

    private void setData(){

        String dummy = Constants.k_ActiveGigDetail.getCreatedOn();
        List<String> wordList = Arrays.asList(dummy.split(" "));
        tvDateAboutUser.setText(wordList.get(0));

        tvUserNameAboutUserGigsDetail.setText(Constants.k_SellerProfileModel.getProfileinfo().getSellerProfileinfo().getUsername());

        String des = Html.fromHtml(Constants.k_ActiveGigDetail.getDescription()).toString();

        tvAboutUserDescription.setText(des);
        tvAdditionalInfoAboutUser.setText(Constants.k_ActiveGigDetail.getAdditionalInfo());

        Picasso.get()
                .load(Constants.k_ActiveGigDetail.getServiceImage())
                .placeholder(R.drawable.noimg)
                .into(ivAboutUserGigs);

        Picasso.get()
                .load(Constants.k_SellerProfileModel.getProfileinfo().getSellerProfileinfo().getFreelancerImage())
                .placeholder(R.drawable.noimg)
                .into(ivUserProfileAboutUser);

        tvRatingAboutUserDetail.setRating(Float.valueOf(Constants.k_ActiveGigDetail.getRating()));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_user_gigs_detail);

        init();

    }

    @Override
    public void onBackPressed() {
        ApplicationHandler.intent(WhatToDoActivitySeller.class);
    }
}
