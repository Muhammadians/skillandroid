package com.skillsquared.app.activities.seller;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;

import com.skillsquared.app.R;
import com.skillsquared.app.handler.ApplicationHandler;
import com.skillsquared.app.handler.RestApi;
import com.skillsquared.app.models.payStack.PayStackBankList;
import com.skillsquared.app.models.signUpModels.SignUpModel;
import com.skillsquared.app.utils.Constants;
import com.skillsquared.app.utils.Loading;
import com.skillsquared.app.utils.SessionManager;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BankDetails extends AppCompatActivity {

    private final static String TAG = BankDetails.class.getSimpleName();
    AppCompatSpinner spinnerBankName;
    EditText etBankCode, etAccountName, etAccountNumber;
    Button btnSubmitBankInfo;
    private SessionManager sessionManager;
    String bankName = null;
    int check = 0;
    PayStackBankList payStackBankList = null;
    String bankCode = null;

    private void init(){

        spinnerBankName = findViewById(R.id.spinnerBankName);
        etBankCode = findViewById(R.id.etBankCode);
        etAccountName = findViewById(R.id.etAccountName);
        etAccountNumber = findViewById(R.id.etAccountNumber);
        btnSubmitBankInfo = findViewById(R.id.btnSubmitBankInfo);
        sessionManager = new SessionManager(this);

        findViewById(R.id.ivBackBankDetails).setOnClickListener(view -> ApplicationHandler.intent(EarningsActivity.class));

        getBankList();

        btnSubmitBankInfo.setOnClickListener(view->checkValues());

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ApplicationHandler.hideStatusBar(this);
        setContentView(R.layout.activity_bank_details);

        init();

    }

    private void getBankList()
    {
        Loading.show(this, false, Constants.k_PLEASE_WAIT);

        Call<PayStackBankList> getBankList = RestApi.getService().getBankList(
                sessionManager.getString(Constants.k_ACCESSTOKEN)
        );

        getBankList.enqueue(new Callback<PayStackBankList>() {
            @Override
            public void onResponse(@NonNull Call<PayStackBankList> call,@NonNull Response<PayStackBankList> response) {
                Loading.cancel();

                if (response.isSuccessful()){
                    payStackBankList = response.body();

                    if (payStackBankList != null) {
                        ArrayList<String> bankNames = new ArrayList<>();
                        bankNames.add("Select Bank");
                        for (int i = 0; i < payStackBankList.getPaystackbanklistings().getData().size(); i++) {
                            bankNames.add(payStackBankList.getPaystackbanklistings().getData().get(i).getName());
                        }
                        arrayAdapter(bankNames, spinnerBankName);

                        spinnerBankName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                                if(++check > 1) {
                                    bankName = spinnerBankName.getSelectedItem().toString();
                                    Log.e(TAG, "onItemSelected: "+bankName);
                                    for (int j = 0; j < payStackBankList.getPaystackbanklistings().getData().size(); j++) {
                                        if (bankName.equals(payStackBankList.getPaystackbanklistings().getData().get(j).getName())){
                                            bankCode = payStackBankList.getPaystackbanklistings().getData().get(j).getCode();
                                        } /*else {
                                            ApplicationHandler.toast("Please select valid bank");
                                        }*/
                                    }
                                }

                                etBankCode.setText(bankCode);

                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });

                    }

                }

            }

            @Override
            public void onFailure(@NonNull Call<PayStackBankList> call,@NonNull Throwable t) {
                Loading.cancel();
                ApplicationHandler.toast(t.getMessage());
                Log.e(TAG, "onFailure: "+t.getMessage());
            }
        });

    }

    private void arrayAdapter(ArrayList<String> array, AppCompatSpinner spinner)
    {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, array);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    private void checkValues(){
        String bankCode = ApplicationHandler.stringConverter(etBankCode);
        String accountName = ApplicationHandler.stringConverter(etAccountName);
        String accountNumber = ApplicationHandler.stringConverter(etAccountNumber);

        if (TextUtils.isEmpty(bankCode)){
            etBankCode.setError(getResources().getString(R.string.editTextEmptyFieldError));
            etBankCode.requestFocus();
            return;
        } else {
            etBankCode.setError(null);
        }

        if (TextUtils.isEmpty(accountName)){
            etAccountName.setError(getResources().getString(R.string.editTextEmptyFieldError));
            etAccountName.requestFocus();
            return;
        } else {
            etAccountName.setError(null);
        }

        if (TextUtils.isEmpty(accountNumber)){
            etAccountNumber.setError(getResources().getString(R.string.editTextEmptyFieldError));
            etAccountNumber.requestFocus();
            return;
        } else {
            etAccountNumber.setError(null);
        }

        if (bankName == null){
            ApplicationHandler.toast("Please Select Bank Name");
        } else {

            Log.e(TAG, "bank name: "+ bankName);
            Log.e(TAG, "bank code: "+ bankCode);
            Log.e(TAG, "account name: "+ accountName);
            Log.e(TAG, "account number: "+ accountNumber);

            /*ApplicationHandler.toast("Added Successfully");
            ApplicationHandler.intent(EarningsActivity.class);*/

            sendDataToServer(bankName, bankCode, accountName, accountNumber);

        }

    }

    private void sendDataToServer(String bankName, String bankCode, String accountName, String accountNumber) {
        Loading.show(this, false, Constants.k_PLEASE_WAIT);

        Call<SignUpModel> saveBankDetails = RestApi.getService().addBankDetails(
                sessionManager.getString(Constants.k_ACCESSTOKEN),
                accountName,
                bankName,
                bankCode,
                accountNumber
        );

        saveBankDetails.enqueue(new Callback<SignUpModel>() {
            @Override
            public void onResponse(@NonNull Call<SignUpModel> call,@NonNull Response<SignUpModel> response) {
                Loading.cancel();

                if (response.isSuccessful()){
                    SignUpModel saveBankDetailResponse = response.body();

                    ApplicationHandler.toast(saveBankDetailResponse.getMessage());
                    ApplicationHandler.intent(EarningsActivity.class);

                }

            }

            @Override
            public void onFailure(@NonNull Call<SignUpModel> call,@NonNull Throwable t) {
                Loading.cancel();
                ApplicationHandler.toast(t.getMessage());
                Log.e(TAG, "onFailure: "+t.getMessage());
            }
        });

    }

}
