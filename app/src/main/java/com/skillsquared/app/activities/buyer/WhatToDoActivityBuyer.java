package com.skillsquared.app.activities.buyer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.app.Dialog;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.skillsquared.app.activities.HomeActivity;
import com.skillsquared.app.activities.buyer.postAJob.PostARequest;
import com.skillsquared.app.activities.buyer.postAJob.PostedRequestList;
import com.skillsquared.app.handler.ApplicationHandler;
import com.skillsquared.app.R;

import co.paystack.android.PaystackSdk;

public class WhatToDoActivityBuyer extends AppCompatActivity {

    CardView postAjob, browseJobs, manage_orders_Card, cardViewNormalOrder, cardViewCustomOrder;
    private Dialog chooseOrderDialog;

    private void init() {
        postAjob = findViewById(R.id.post_a_job_card);
        browseJobs = findViewById(R.id.browse_jobs_card);
        manage_orders_Card = findViewById(R.id.manage_orders_Card);
        chooseOrderDialog = new Dialog(this);

        onClickListeners();
    }

    private void onClickListeners() {
        postAjob.setOnClickListener(view -> ApplicationHandler.intent(PostARequest.class));

        browseJobs.setOnClickListener(view -> ApplicationHandler.intent(PostedRequestList.class));

        manage_orders_Card.setOnClickListener(view -> {
            showDialog();
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ApplicationHandler.hideStatusBar(this);
        setContentView(R.layout.activity_what_to_do);

        init();

    }

    private void showDialog() {

        chooseOrderDialog.setContentView(R.layout.choose_order_to_display);
        chooseOrderDialog.setCancelable(true);

        Window window = chooseOrderDialog.getWindow();
        if (window != null) {
            window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            WindowManager.LayoutParams wlp = window.getAttributes();
            wlp.gravity = Gravity.CENTER;
            wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
            wlp.dimAmount = 0.3f;
            window.setAttributes(wlp);
        }
        chooseOrderDialog.show();

        cardViewNormalOrder = chooseOrderDialog.findViewById(R.id.cardViewNormalOrder);
        cardViewCustomOrder = chooseOrderDialog.findViewById(R.id.cardViewCustomOrder);

        cardViewNormalOrder.setOnClickListener(view->ApplicationHandler.intent(ManageOrders.class, "chooseOrder", "normal"));
        cardViewCustomOrder.setOnClickListener(view->ApplicationHandler.intent(ManageOrders.class, "chooseOrder", "custom"));

    }

    @Override
    public void onBackPressed() {
        ApplicationHandler.intent(HomeActivity.class);
    }
}
