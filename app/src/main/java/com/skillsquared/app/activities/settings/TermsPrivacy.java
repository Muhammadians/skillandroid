package com.skillsquared.app.activities.settings;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.widget.TextView;

import com.skillsquared.app.R;
import com.skillsquared.app.activities.HomeActivity;
import com.skillsquared.app.handler.ApplicationHandler;
import com.skillsquared.app.handler.RestApi;
import com.skillsquared.app.models.TermsPrivacyModel;
import com.skillsquared.app.models.signUpModels.SignUpModel;
import com.skillsquared.app.utils.Constants;
import com.skillsquared.app.utils.Loading;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TermsPrivacy extends AppCompatActivity {

    private final static String TAG = TermsPrivacy.class.getSimpleName();
    TextView tvDescTerms;
    String activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ApplicationHandler.hideStatusBar(this);
        setContentView(R.layout.activity_terms_privacy);

        activity = getIntent().getStringExtra("activity");

        tvDescTerms = findViewById(R.id.tvDescTerms);

        findViewById(R.id.ivBackTermsPrivacy).setOnClickListener(view -> onBackPressed());

        if (activity.equals("terms")){
            Loading.show(this, false, Constants.k_PLEASE_WAIT);
            Call<TermsPrivacyModel> getTerms = RestApi.getService().getTerms();

            getTerms.enqueue(new Callback<TermsPrivacyModel>() {
                @Override
                public void onResponse(@NonNull Call<TermsPrivacyModel> call,@NonNull Response<TermsPrivacyModel> response) {
                    Loading.cancel();
                    if (response.isSuccessful()){
                        TermsPrivacyModel signUpModel = response.body();
                        if (signUpModel != null) {
                            String desc = Html.fromHtml(signUpModel.getData()).toString();
                            tvDescTerms.setText(desc);
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<TermsPrivacyModel> call,@NonNull Throwable t) {
                    Loading.cancel();
                    Log.e(TAG, "onFailure: "+t.getMessage());
                    ApplicationHandler.toast(t.getMessage());
                }
            });
        } else {
            Loading.show(this, false, Constants.k_PLEASE_WAIT);
            Call<TermsPrivacyModel> getTerms = RestApi.getService().getPrivacyPolicy();

            getTerms.enqueue(new Callback<TermsPrivacyModel>() {
                @Override
                public void onResponse(@NonNull Call<TermsPrivacyModel> call,@NonNull Response<TermsPrivacyModel> response) {
                    Loading.cancel();
                    if (response.isSuccessful()){
                        TermsPrivacyModel signUpModel = response.body();
                        if (signUpModel != null) {
                            String desc = Html.fromHtml(signUpModel.getData()).toString();
                            tvDescTerms.setText(desc);
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<TermsPrivacyModel> call,@NonNull Throwable t) {
                    Loading.cancel();
                    Log.e(TAG, "onFailure: "+t.getMessage());
                    ApplicationHandler.toast(t.getMessage());
                }
            });
        }

    }

    @Override
    public void onBackPressed() {
        ApplicationHandler.intent(SettingsActivity.class);
    }
}
