package com.skillsquared.app.activities.becomeFreelancer;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;

import com.google.android.material.card.MaterialCardView;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.skillsquared.app.R;
import com.skillsquared.app.activities.HomeActivity;
import com.skillsquared.app.activities.SignIn;
import com.skillsquared.app.activities.SplashActivity;
import com.skillsquared.app.activities.seller.CreateServiceActivity;
import com.skillsquared.app.activities.seller.WhatToDoActivitySeller;
import com.skillsquared.app.handler.ApplicationHandler;
import com.skillsquared.app.handler.FileUtils;
import com.skillsquared.app.handler.RestApi;
import com.skillsquared.app.models.SaveBecomeFreelancerResponse;
import com.skillsquared.app.models.UploadFileResponse;
import com.skillsquared.app.models.postAJob.PostAJobResponse;
import com.skillsquared.app.models.signUpModels.SignUpModel;
import com.skillsquared.app.utils.Constants;
import com.skillsquared.app.utils.Loading;
import com.skillsquared.app.utils.SessionManager;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class BecomeFreelancerTwo extends AppCompatActivity {

    private final static String TAG = BecomeFreelancerTwo.class.getSimpleName();
    public static final int GALLERY_REQUEST_CODE = 1128;
    private SessionManager sessionManager;
    AppCompatSpinner spinnerLanguage, spinnerAvailability;
    TextView etFileUpload;
    String imageFilePath;
    File imageFIle;
    Uri imageUri;
    String getLanguage, getAvailability;
    String userName, freelancerTitle, description,
            cat_id, city = null, state = null, country = null, facebook = null,
            linkedIn = null, website = null, twitter = null, instagram = null;
    ArrayList<String> mListAvailability = new ArrayList<>();
    EditText etFbUrl, etlinkedInUrl, etWebUrl, etTwitterUrl, etInstaUrl;

    private void init() {
        sessionManager = new SessionManager(this);
        spinnerLanguage = findViewById(R.id.spinnerLanguage);
        spinnerAvailability = findViewById(R.id.spinnerAvailability);
        etFileUpload = findViewById(R.id.etFileUpload);
        etFbUrl = findViewById(R.id.etFbUrl);
        etlinkedInUrl = findViewById(R.id.etlinkedInUrl);
        etWebUrl = findViewById(R.id.etWebUrl);
        etTwitterUrl = findViewById(R.id.etTwitterUrl);
        etInstaUrl = findViewById(R.id.etInstaUrl);

        mListAvailability.add("Select Your Availability");
        mListAvailability.add("Full Time");
        mListAvailability.add("Part Time");

        arrayAdapter(Constants.k_becomeFreelanceDetails.getLanguages(), spinnerLanguage);
        arrayAdapter(mListAvailability, spinnerAvailability);

        etFileUpload.setOnClickListener(view -> requestPermissionsFromUser());

        userName = getIntent().getStringExtra("username");
        freelancerTitle = getIntent().getStringExtra("freelancerTitle");
        description = getIntent().getStringExtra("description");
        cat_id = getIntent().getStringExtra("cat_id");
        city = getIntent().getStringExtra("city");
        state = getIntent().getStringExtra("state");
        country = getIntent().getStringExtra("country");

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ApplicationHandler.hideStatusBar(this);
        setContentView(R.layout.activity_become_freelancer_two);

        init();

        findViewById(R.id.ivBackBecomeFreelancerTwo).setOnClickListener(view -> onBackPressed());
        findViewById(R.id.submitBecomeFreelancerTwo).setOnClickListener(view -> checkValues());
//        findViewById(R.id.submitBecomeFreelancerTwo).setOnClickListener(view -> uploadFile());

    }

    private void pickImageForGallery() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, GALLERY_REQUEST_CODE);
    }

    private void requestPermissionsFromUser() {
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            pickImageForGallery();
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            ApplicationHandler.showSettingsDialog(BecomeFreelancerTwo.this);
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(error -> Toast.makeText(getApplicationContext(), "Error occurred! ", Toast.LENGTH_SHORT).show())
                .onSameThread()
                .check();
    }

    void setSplash() {
        Loading.show(this, false, Constants.k_PLEASE_WAIT);
        int SPLASH_DISPLAY_LENGTH = 2000;
        new Handler().postDelayed(() -> {

            Loading.cancel();
            ApplicationHandler.toast("Request Submitted Successfully...!!!");
            ApplicationHandler.intent(HomeActivity.class);
        }, SPLASH_DISPLAY_LENGTH);
    }

    private void arrayAdapter(List<String> array, AppCompatSpinner spinner) {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, array);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        getLanguage();
        getAvailability();
    }

    private void checkValues(){

        facebook = ApplicationHandler.stringConverter(etFbUrl);
        linkedIn = ApplicationHandler.stringConverter(etlinkedInUrl);
        website = ApplicationHandler.stringConverter(etWebUrl);
        twitter = ApplicationHandler.stringConverter(etTwitterUrl);
        instagram = ApplicationHandler.stringConverter(etInstaUrl);


        if (getLanguage==null){
            ApplicationHandler.toast("Please select a language...");
            return;
        }

        if (getAvailability==null){
            ApplicationHandler.toast("Please select your availability...");
            return;
        }

        if (getAvailability.equals("Select Your Availability")){
            ApplicationHandler.toast("Please select your availability...");
            return;
        }

        String file = etFileUpload.getText().toString();

        if (TextUtils.isEmpty(file)){
            ApplicationHandler.toast("Please select a file...");
            return;
        }

//        setSplash();

        Log.e(TAG, "checkValues: "+userName);
        Log.e(TAG, "checkValues: "+freelancerTitle);
        Log.e(TAG, "checkValues: "+description);
        Log.e(TAG, "checkValues: "+cat_id);
        Log.e(TAG, "checkValues: "+city);
        Log.e(TAG, "checkValues: "+state);
        Log.e(TAG, "checkValues: "+country);
        Log.e(TAG, "checkValues: "+getLanguage);
        Log.e(TAG, "checkValues: "+getAvailability);
        Log.e(TAG, "checkValues: "+file);
        Log.e(TAG, "checkValues: "+facebook);
        Log.e(TAG, "checkValues: "+linkedIn);
        Log.e(TAG, "checkValues: "+website);
        Log.e(TAG, "checkValues: "+twitter);
        Log.e(TAG, "checkValues: "+instagram);

        uploadFile();
        
    }

    private void uploadFile(){
        Loading.show(this, false, Constants.k_PLEASE_WAIT);

        RequestBody requestBodyFile = RequestBody.create(MediaType.parse("image/*"), imageFIle);

        Call<UploadFileResponse> uploadFileResponseCall = RestApi.getService().uploadFile(
                sessionManager.getString(Constants.k_ACCESSTOKEN),
                requestBodyFile
        );

        uploadFileResponseCall.enqueue(new Callback<UploadFileResponse>() {
            @Override
            public void onResponse(@NonNull Call<UploadFileResponse> call,@NonNull Response<UploadFileResponse> response) {
                Loading.cancel();
                if (response.isSuccessful()){
                    UploadFileResponse uploadFileResponse = response.body();

                    if (uploadFileResponse != null) {
                        Log.e(TAG, "onResponse: "+uploadFileResponse.getUploadedFile());

                        String fileName = uploadFileResponse.getUploadedFile();

                        if (fileName!=null){
                            sendDataToServer(userName, freelancerTitle, description, cat_id, city, state, country, getLanguage,
                                    getAvailability,fileName,facebook,linkedIn,website,twitter,instagram);
                        } else {
                            ApplicationHandler.toast(getResources().getString(R.string.something_went_wrong));
                        }

                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<UploadFileResponse> call,@NonNull Throwable t) {
                Loading.cancel();
                ApplicationHandler.toast(t.getMessage());
                Log.e(TAG, "onFailure: "+t.getMessage());
            }
        });

    }

    private void sendDataToServer(String userName, String freelancerTitle, String description,
                                  String cat_id, String cityParam, String stateParam, String countryParam, String getLanguage,
                                  String getAvailability, String fileParam, String facebookParam, String linkedInParam,
                                  String websiteParam, String twitterParam, String instagramParam) {
        Loading.show(this, false, Constants.k_PLEASE_WAIT);

        /*RequestBody requestBodyFile = RequestBody.create(MediaType.parse("image/*"), imageFIle);

        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), userName);
        RequestBody freelance = RequestBody.create(MediaType.parse("text/plain"), freelancerTitle);
        RequestBody desc = RequestBody.create(MediaType.parse("text/plain"), description);
        RequestBody catId = RequestBody.create(MediaType.parse("text/plain"), cat_id);
        RequestBody city = RequestBody.create(MediaType.parse("text/plain"), cityParam);
        RequestBody state = RequestBody.create(MediaType.parse("text/plain"), stateParam);
        RequestBody country = RequestBody.create(MediaType.parse("text/plain"), countryParam);
        RequestBody lang = RequestBody.create(MediaType.parse("text/plain"), getLanguage);
        RequestBody availability = RequestBody.create(MediaType.parse("text/plain"), getAvailability);
        RequestBody facebook = RequestBody.create(MediaType.parse("text/plain"), facebookParam);
        RequestBody linkedIn = RequestBody.create(MediaType.parse("text/plain"), linkedInParam);
        RequestBody website = RequestBody.create(MediaType.parse("text/plain"), websiteParam);
        RequestBody twitter = RequestBody.create(MediaType.parse("text/plain"), twitterParam);
        RequestBody instagram = RequestBody.create(MediaType.parse("text/plain"), instagramParam);*/

        Call<SaveBecomeFreelancerResponse> createService = RestApi.getService().saveBecomeFreelancer(
                sessionManager.getString(Constants.k_ACCESSTOKEN),
                freelancerTitle,
                cat_id,
                getAvailability,
                description,
                countryParam,
                stateParam,
                cityParam,
                fileParam,
                facebookParam,
                linkedInParam,
                websiteParam,
                twitterParam,
                instagramParam,
                userName,
                getLanguage
        );

        createService.enqueue(new Callback<SaveBecomeFreelancerResponse>() {
            @Override
            public void onResponse(@NonNull Call<SaveBecomeFreelancerResponse> call,@NonNull Response<SaveBecomeFreelancerResponse> response) {
                Loading.cancel();
                if (response.isSuccessful()) {
                    SaveBecomeFreelancerResponse response1 = response.body();
                    if (response1 != null) {
                        ApplicationHandler.toast(response1.getMessage());

                        sessionManager.put(Constants.k_USER_ID, response1.getSkillSquaredUser().getUserId());
                        sessionManager.put(Constants.k_USERNAMESKILLSQUARED, response1.getSkillSquaredUser().getUsername());
                        sessionManager.put(Constants.k_NAME, response1.getSkillSquaredUser().getName());
                        sessionManager.put(Constants.k_EMAIL, response1.getSkillSquaredUser().getEmail());
                        sessionManager.put(Constants.k_PHONE, response1.getSkillSquaredUser().getPhone());
                        sessionManager.put(Constants.k_ADDRESS, response1.getSkillSquaredUser().getAddress());
                        sessionManager.put(Constants.k_IMAGE, response1.getSkillSquaredUser().getImage());
                        sessionManager.put(Constants.k_TIMEZONE, response1.getSkillSquaredUser().getTimezone());
                        sessionManager.putBoolean(Constants.k_Freelancer, response1.getSkillSquaredUser().getFreelancer());
                        sessionManager.putBoolean(Constants.k_FreelancerProfile, response1.getSkillSquaredUser().getFreelancerProfileSet());

                        ApplicationHandler.intent(HomeActivity.class);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<SaveBecomeFreelancerResponse> call,@NonNull Throwable t) {
                Loading.cancel();
                Log.e(TAG, "onFailure: " + t.getMessage());
                ApplicationHandler.toast(t.getMessage());
            }
        });
    }

    private void getLanguage(){

        spinnerLanguage.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                spinnerLanguage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        getLanguage = spinnerLanguage.getSelectedItem().toString();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

                return false;
            }
        });

    }

    private void getAvailability(){

        spinnerAvailability.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                spinnerAvailability.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        getAvailability = spinnerAvailability.getSelectedItem().toString();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

                return false;
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GALLERY_REQUEST_CODE && resultCode == RESULT_OK) {
            imageUri = data.getData();

            Log.e(TAG, "imageUri: "+imageUri);

            imageFilePath = FileUtils.getPath(this, imageUri);

            Log.e(TAG, "image file path: "+imageFilePath);

            if (imageFilePath != null) {
                    imageFIle = new File(imageFilePath);
            }

            Log.e(TAG, "image file: "+imageFIle);

            etFileUpload.setText(imageFIle.getName());
        }
    }

    @Override
    public void onBackPressed() {
        ApplicationHandler.intent(BecomeFreelancerOne.class);
    }

}
