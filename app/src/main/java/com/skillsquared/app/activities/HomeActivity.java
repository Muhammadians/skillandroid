package com.skillsquared.app.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.cometchat.pro.constants.CometChatConstants;
import com.cometchat.pro.core.CometChat;
import com.cometchat.pro.exceptions.CometChatException;
import com.cometchat.pro.models.User;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.skillsquared.app.R;
import com.skillsquared.app.activities.becomeFreelancer.BecomeFreelancerOne;
import com.skillsquared.app.activities.buyer.WhatToDoActivityBuyer;
import com.skillsquared.app.activities.seller.AboutUserActivity;
import com.skillsquared.app.activities.seller.WhatToDoActivitySeller;
import com.skillsquared.app.activities.settings.SettingsActivity;
import com.skillsquared.app.cometChat.Activity.CometChatActivity;
import com.skillsquared.app.cometChat.Activity.LoginActivity;
import com.skillsquared.app.cometChat.Contracts.LoginActivityContract;
import com.skillsquared.app.cometChat.Contracts.StringContract;
import com.skillsquared.app.fragments.DashboardFragment;
import com.skillsquared.app.fragments.SearchFragment;
import com.skillsquared.app.fragments.SettingsFragment;
import com.skillsquared.app.handler.ApplicationHandler;
import com.skillsquared.app.utils.Constants;
import com.skillsquared.app.utils.Loading;
import com.skillsquared.app.utils.SessionManager;
import com.squareup.picasso.Picasso;

import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

public class HomeActivity extends AppCompatActivity implements
        NavigationView.OnNavigationItemSelectedListener {

    private final static String TAG = HomeActivity.class.getSimpleName();
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mToggle;
    private SessionManager sessionManager;
    FrameLayout fragmentContainer;
    Toolbar toolbar;
    CircleImageView navUserImage;
    TextView nav_home, nav_Buyer, nav_Seller, navUserName, nav_inbox,
            nav_notification, nav_saved, nav_setting, nav_support, navBuyerOnly,
            nav_logOut, navBecomeFreelancer, navBuyerBecomeFreelancer, navBecomeFreelancerTest;
    LinearLayout llBuyerSeller, llBuyerBecomeFreelancer;
    User user = CometChat.getLoggedInUser();
    int i = 0;

    private void init() {

        sessionManager = new SessionManager(this);

        fragmentContainer = findViewById(R.id.fragment_container);
        llBuyerSeller = findViewById(R.id.llBuyerSeller);
        llBuyerBecomeFreelancer = findViewById(R.id.llBuyerBecomFreelancer);
        navBuyerOnly = findViewById(R.id.navBuyerOnly);
        navBecomeFreelancer = findViewById(R.id.navBecomeFreelancer);
        navBecomeFreelancerTest = findViewById(R.id.navBecomeFreelancerTest);
        mDrawerLayout = findViewById(R.id.drawer);
        toolbar = findViewById(R.id.toolbar);
        nav_support = findViewById(R.id.nav_support);
        nav_setting = findViewById(R.id.nav_setting);
        nav_saved = findViewById(R.id.nav_saved);
        nav_notification = findViewById(R.id.nav_notification);
        nav_home = findViewById(R.id.nav_home);
        nav_Buyer = findViewById(R.id.nav_Buyer);
        nav_Seller = findViewById(R.id.nav_Seller);
        navUserImage = findViewById(R.id.ivUserImageHome);
        navUserName = findViewById(R.id.tvUserNameHome);
        nav_inbox = findViewById(R.id.nav_inbox);
        nav_logOut = findViewById(R.id.nav_logOut);
        navBuyerBecomeFreelancer = findViewById(R.id.nav_BuyerFreelancer);

        if (sessionManager.getBoolean(Constants.k_Freelancer)) {
            if (sessionManager.getBoolean(Constants.k_FreelancerProfile)) {
                llBuyerSeller.setVisibility(View.VISIBLE);
                llBuyerBecomeFreelancer.setVisibility(View.GONE);
                navBuyerOnly.setVisibility(View.GONE);
            } else {
                llBuyerSeller.setVisibility(View.GONE);
                llBuyerBecomeFreelancer.setVisibility(View.VISIBLE);
                navBuyerOnly.setVisibility(View.GONE);
            }
        } else {
            llBuyerSeller.setVisibility(View.GONE);
            llBuyerBecomeFreelancer.setVisibility(View.GONE);
            navBuyerOnly.setVisibility(View.VISIBLE);
        }

        View.OnClickListener onClickListener = view -> ApplicationHandler.intent(WhatToDoActivityBuyer.class);
        navBuyerOnly.setOnClickListener(onClickListener);
        navBuyerBecomeFreelancer.setOnClickListener(onClickListener);

        navUserName.setText(sessionManager.getString(Constants.k_NAME));
        Picasso.get().load(sessionManager.getString(Constants.k_IMAGE))
                .placeholder(R.drawable.user_profile).into(navUserImage);

        setOnClickListeners();

        setSupportActionBar(toolbar);

        mToggle = new ActionBarDrawerToggle(
                this,
                mDrawerLayout,
                toolbar,
                R.string.open,
                R.string.close);
        mDrawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();

        NavigationView navigationView = findViewById(R.id.navView);
        navigationView.setNavigationItemSelectedListener(this);

//        temporaryLogout();

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                new DashboardFragment()).commit();

    }

    void setOnClickListeners() {
        nav_home.setOnClickListener(view -> {
            if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                mDrawerLayout.closeDrawer(GravityCompat.START);
            }
        });

        nav_Buyer.setOnClickListener(view -> {
            if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                mDrawerLayout.closeDrawer(GravityCompat.START);
                ApplicationHandler.intent(WhatToDoActivityBuyer.class);
            }
        });

        nav_Seller.setOnClickListener(view -> {
            if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                mDrawerLayout.closeDrawer(GravityCompat.START);
                ApplicationHandler.intent(WhatToDoActivitySeller.class);
            }
        });

        nav_inbox.setOnClickListener(view -> {
            Loading.show(this, false, Constants.k_PLEASE_WAIT);
            ApplicationHandler.cometChatLogin(sessionManager.getString(Constants.k_USERNAMESKILLSQUARED), CometChatActivity.class, "Inbox");
        });

        nav_notification.setOnClickListener(view -> ApplicationHandler.intent(NotificationsActivity.class));

        nav_saved.setOnClickListener(view -> ApplicationHandler.intent(SavedActivity.class));

        nav_setting.setOnClickListener(view -> ApplicationHandler.intent(SettingsActivity.class));

        nav_support.setOnClickListener(view -> ApplicationHandler.intent(SupportActivity.class));

        navUserName.setOnClickListener(view -> ApplicationHandler.intent(AboutUserActivity.class));

        nav_logOut.setOnClickListener(view -> logout());

        navBecomeFreelancer.setOnClickListener(view -> ApplicationHandler.intent(BecomeFreelancerOne.class));

        navBecomeFreelancerTest.setOnClickListener(view -> ApplicationHandler.intent(BecomeFreelancerOne.class));

    }

    private void temporaryLogout() {
        CometChat.logout(new CometChat.CallbackListener<String>() {
            @Override
            public void onSuccess(String s) {
                Loading.cancel();

                Log.e(TAG, "onSuccess: " + s);

                /*if (user!=null){
                    if (user.getUid()!=null){
                        FirebaseMessaging.getInstance().unsubscribeFromTopic(StringContract.AppDetails.APP_ID + "_" + CometChatConstants.RECEIVER_TYPE_USER + "_" + user.getUid());
                    }
                }
                sessionManager.clearSession();
                ApplicationHandler.intent(SignIn.class);*/
            }

            @Override
            public void onError(CometChatException e) {
                Loading.cancel();
                Log.e(TAG, "onError: " + e.getMessage());
            }
        });
    }

    private void logout() {
        Loading.show(this, false, Constants.k_PLEASE_WAIT);
        CometChat.logout(new CometChat.CallbackListener<String>() {
            @Override
            public void onSuccess(String s) {
                Loading.cancel();
                if (user != null) {
                    if (user.getUid() != null) {
                        FirebaseMessaging.getInstance().unsubscribeFromTopic(StringContract.AppDetails.APP_ID + "_" + CometChatConstants.RECEIVER_TYPE_USER + "_" + user.getUid());
                    }
                }
                sessionManager.clearSession();
                ApplicationHandler.intent(SignIn.class);
            }

            @Override
            public void onError(CometChatException e) {
                Loading.cancel();
                Log.e(TAG, "onError: " + e.getMessage());
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        init();

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (mToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            if (i == 0) {
                i++;
                ApplicationHandler.toast(getResources().getString(R.string.back_press));
            } else {
                Intent a = new Intent(Intent.ACTION_MAIN);
                a.addCategory(Intent.CATEGORY_HOME);
                a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(a);
            }
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        switch (menuItem.getItemId()) {
            case R.id.navDashboard:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new DashboardFragment()).commit();
                break;
            case R.id.navSearch:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new SearchFragment()).commit();
                break;
            case R.id.navSettings:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new SettingsFragment()).commit();
                break;
        }

        mDrawerLayout.closeDrawer(GravityCompat.START);

        return true;
    }

}
