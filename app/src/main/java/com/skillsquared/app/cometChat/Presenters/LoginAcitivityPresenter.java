package com.skillsquared.app.cometChat.Presenters;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.cometchat.pro.core.CometChat;
import com.cometchat.pro.exceptions.CometChatException;
import com.cometchat.pro.models.User;
import com.skillsquared.app.cometChat.Base.Presenter;
import com.skillsquared.app.cometChat.Contracts.LoginActivityContract;
import com.skillsquared.app.cometChat.Contracts.StringContract;
import com.skillsquared.app.utils.Loading;

import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import javax.net.ssl.HttpsURLConnection;

public class LoginAcitivityPresenter extends Presenter<LoginActivityContract.LoginActivityView> implements
        LoginActivityContract.LoginActivityPresenter {

    private static final String TAG = LoginAcitivityPresenter.class.getSimpleName();

    @Override
    public void Login(Context context, String uid) {

        /*CometChat.login(uid, StringContract.AppDetails.API_KEY, new CometChat.CallbackListener<User>() {
            @Override
            public void onSuccess(User user) {
                Log.d(TAG, "onSuccess: " + user.getUid());
                getBaseView().startCometChatActivity();
            }

            @Override
            public void onError(CometChatException e) {
                e.printStackTrace();
                Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                Log.d(TAG, "onError: " + e.getMessage());
            }

        });*/

        CometChat.login(uid, StringContract.AppDetails.API_KEY, new CometChat.CallbackListener<User>() {
            @Override
            public void onSuccess(User user) {
                Log.d(TAG, "onSuccess: " + user.getUid());
                Loading.cancel();
//                getBaseView().startCometChatActivity();
            }

            @Override
            public void onError(CometChatException e) {
                e.printStackTrace();
                Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                Loading.cancel();
                Log.d(TAG, "onError: " + e.getMessage());
            }

        });

    }

    @Override
    public void loginCheck() {

        try {
            if (CometChat.getLoggedInUser() != null) {
                if (isViewAttached())
                    getBaseView().startCometChatActivity();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
