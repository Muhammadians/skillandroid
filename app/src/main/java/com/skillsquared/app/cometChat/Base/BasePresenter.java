package com.skillsquared.app.cometChat.Base;

public interface BasePresenter<V> {

    void attach(V baseView);

    void detach();

}
