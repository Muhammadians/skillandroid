package com.skillsquared.app.cometChat.Contracts;

import android.content.Context;

import com.cometchat.pro.models.GroupMember;
import com.skillsquared.app.cometChat.Adapter.GroupMemberListAdapter;
import com.skillsquared.app.cometChat.Base.BasePresenter;

import java.util.HashMap;

public interface BannedMemberListContract {

    interface BannedMemberListView {

        void setAdapter(HashMap<String, GroupMember> list);
    }

    interface BannedMemberListPresenter extends BasePresenter<BannedMemberListView> {

        void initMemberList(String groupId, int limit, Context context);

        void reinstateUser(String uid, String groupId, GroupMemberListAdapter groupMemberListAdapter);

        void refresh(String GUID, int LIMIT, Context context);

        void addGroupEventListener(String listenerId, String groupId, GroupMemberListAdapter groupMemberListAdapter);
    }


}