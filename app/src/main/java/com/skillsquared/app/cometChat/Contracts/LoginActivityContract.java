package com.skillsquared.app.cometChat.Contracts;

import android.content.Context;

import com.skillsquared.app.cometChat.Base.BasePresenter;

public interface LoginActivityContract {

    interface LoginActivityView {

        void startCometChatActivity();
    }

    interface LoginActivityPresenter extends BasePresenter<LoginActivityView> {

        void Login(Context context, String uid);

        void loginCheck();
    }
}
