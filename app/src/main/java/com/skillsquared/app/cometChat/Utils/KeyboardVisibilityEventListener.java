package com.skillsquared.app.cometChat.Utils;

public interface KeyboardVisibilityEventListener {
    void onVisibilityChanged(boolean var1);
}