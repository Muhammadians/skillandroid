package com.skillsquared.app.cometChat.Helper;

public interface OnTopReachedListener {
    void onTopReached(int pos);
}
