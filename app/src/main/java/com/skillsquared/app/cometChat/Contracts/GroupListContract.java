package com.skillsquared.app.cometChat.Contracts;

import android.app.ProgressDialog;
import android.content.Context;

import com.cometchat.pro.models.Group;
import com.skillsquared.app.cometChat.Adapter.GroupListAdapter;
import com.skillsquared.app.cometChat.Base.BasePresenter;
import com.skillsquared.app.cometChat.Base.BaseView;

import java.util.HashMap;

public interface GroupListContract {


    interface GroupView extends BaseView {

        void setGroupAdapter(HashMap<String, Group> groupList);

        void groupjoinCallback(Group group);

        void setFilterGroup(HashMap<String, Group> groups);
    }

    interface GroupPresenter extends BasePresenter<GroupView> {
        void initGroupView();

        void joinGroup(Context context, Group group, ProgressDialog progressDialog, GroupListAdapter groupListAdapter);

        void refresh();

        void searchGroup(String s);

        void deleteGroup(Context context, String guid, GroupListAdapter groupListAdapter);
    }
}
