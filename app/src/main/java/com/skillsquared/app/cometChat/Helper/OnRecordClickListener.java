package com.skillsquared.app.cometChat.Helper;

import android.view.View;

public interface OnRecordClickListener {
    void onClick(View v);
}