package com.skillsquared.app.cometChat.Contracts;

import android.content.Context;

import com.skillsquared.app.cometChat.Base.BasePresenter;

public interface CallActivityContract {

    interface CallActivityView {

    }

    interface CallActivityPresenter extends BasePresenter<CallActivityView> {

        void removeCallListener(String listener);

        void addCallListener(Context context, String listener);

    }
}
