package com.skillsquared.app.cometChat.Contracts;

import android.content.Context;

import com.cometchat.pro.models.Group;
import com.skillsquared.app.cometChat.Base.BasePresenter;
import com.skillsquared.app.cometChat.Base.BaseView;


public interface CreateGroupActivityContract {

    interface CreateGroupView extends BaseView {

    }

    interface CreateGroupPresenter extends BasePresenter<CreateGroupView> {

        void createGroup(Context context, Group group);

    }
}
