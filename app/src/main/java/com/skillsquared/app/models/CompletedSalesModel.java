package com.skillsquared.app.models;

public class CompletedSalesModel {

    int id;
    int imagePath;
    String name;
    String description;
    String date;
    String status;
    int price;

    public CompletedSalesModel(int id, int imagePath, String name, String description, String date, String status, int price) {
        this.id = id;
        this.imagePath = imagePath;
        this.name = name;
        this.description = description;
        this.date = date;
        this.status = status;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getImagePath() {
        return imagePath;
    }

    public void setImagePath(int imagePath) {
        this.imagePath = imagePath;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
