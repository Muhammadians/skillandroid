package com.skillsquared.app.models.manageOrders;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Aactive {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("user_duartion_qoute")
    @Expose
    private String userDuartionQoute;
    @SerializedName("seller_offer_request_id")
    @Expose
    private String sellerOfferRequestId;
    @SerializedName("seller_id")
    @Expose
    private String sellerId;
    @SerializedName("request_price")
    @Expose
    private String requestPrice;
    @SerializedName("payment_order_id")
    @Expose
    private String paymentOrderId;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("service_title")
    @Expose
    private String serviceTitle;
    @SerializedName("buyer_name")
    @Expose
    private String buyerName;
    @SerializedName("buyer_image")
    @Expose
    private String buyerImage;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getUserDuartionQoute() {
        return userDuartionQoute;
    }

    public void setUserDuartionQoute(String userDuartionQoute) {
        this.userDuartionQoute = userDuartionQoute;
    }

    public String getSellerOfferRequestId() {
        return sellerOfferRequestId;
    }

    public void setSellerOfferRequestId(String sellerOfferRequestId) {
        this.sellerOfferRequestId = sellerOfferRequestId;
    }

    public String getSellerId() {
        return sellerId;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

    public String getRequestPrice() {
        return requestPrice;
    }

    public void setRequestPrice(String requestPrice) {
        this.requestPrice = requestPrice;
    }

    public String getPaymentOrderId() {
        return paymentOrderId;
    }

    public void setPaymentOrderId(String paymentOrderId) {
        this.paymentOrderId = paymentOrderId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getServiceTitle() {
        return serviceTitle;
    }

    public void setServiceTitle(String serviceTitle) {
        this.serviceTitle = serviceTitle;
    }

    public String getBuyerName() {
        return buyerName;
    }

    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    public String getBuyerImage() {
        return buyerImage;
    }

    public void setBuyerImage(String buyerImage) {
        this.buyerImage = buyerImage;
    }

}