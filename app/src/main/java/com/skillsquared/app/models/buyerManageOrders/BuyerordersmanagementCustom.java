package com.skillsquared.app.models.buyerManageOrders;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.skillsquared.app.models.buyerManageOrders.buyerManageOrdersCustom.Aactive_;
import com.skillsquared.app.models.buyerManageOrders.buyerManageOrdersCustom.Acompleted_;
import com.skillsquared.app.models.buyerManageOrders.buyerManageOrdersCustom.Adisputed_;
import com.skillsquared.app.models.buyerManageOrders.buyerManageOrdersCustom.Amanage_;
import com.skillsquared.app.models.buyerManageOrders.buyerManageOrdersCustom.Arevision_;

import java.util.List;

public class BuyerordersmanagementCustom {

    @SerializedName("aactive")
    @Expose
    private List<Aactive_> aactive = null;
    @SerializedName("amanage")
    @Expose
    private List<Amanage_> amanage = null;
    @SerializedName("arevsion")
    @Expose
    private List<Arevision_> arevsion = null;
    @SerializedName("acompleted")
    @Expose
    private List<Acompleted_> acompleted = null;
    @SerializedName("adisputed")
    @Expose
    private Adisputed_ adisputed;

    public List<Aactive_> getAactive() {
        return aactive;
    }

    public void setAactive(List<Aactive_> aactive) {
        this.aactive = aactive;
    }

    public List<Amanage_> getAmanage() {
        return amanage;
    }

    public void setAmanage(List<Amanage_> amanage) {
        this.amanage = amanage;
    }

    public List<Arevision_> getArevsion() {
        return arevsion;
    }

    public void setArevsion(List<Arevision_> arevsion) {
        this.arevsion = arevsion;
    }

    public List<Acompleted_> getAcompleted() {
        return acompleted;
    }

    public void setAcompleted(List<Acompleted_> acompleted) {
        this.acompleted = acompleted;
    }

    public Adisputed_ getAdisputed() {
        return adisputed;
    }

    public void setAdisputed(Adisputed_ adisputed) {
        this.adisputed = adisputed;
    }

}
