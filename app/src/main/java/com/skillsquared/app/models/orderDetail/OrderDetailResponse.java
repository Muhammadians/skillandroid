package com.skillsquared.app.models.orderDetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrderDetailResponse {

    @SerializedName("sellerorderdetail")
    @Expose
    private List<Sellerorderdetail> sellerorderdetail = null;
    @SerializedName("disputedisexit")
    @Expose
    private Integer disputedisexit;
    @SerializedName("orderstatusbeforesellersubmisson")
    @Expose
    private String orderstatusbeforesellersubmisson;
    @SerializedName("buyerdisputedmessagesandresponse")
    @Expose
    private List<Object> buyerdisputedmessagesandresponse = null;
    @SerializedName("disputereason")
    @Expose
    private List<Disputereason> disputereason = null;
    @SerializedName("processing_fee_type")
    @Expose
    private String processingFeeType;
    @SerializedName("orderStatus")
    @Expose
    private String orderStatus;
    @SerializedName("deliverNote")
    @Expose
    private DeliverNote deliverNote;
    @SerializedName("deliverdornot")
    @Expose
    private boolean deliverdornot;

    public List<Sellerorderdetail> getSellerorderdetail() {
        return sellerorderdetail;
    }

    public void setSellerorderdetail(List<Sellerorderdetail> sellerorderdetail) {
        this.sellerorderdetail = sellerorderdetail;
    }

    public Integer getDisputedisexit() {
        return disputedisexit;
    }

    public void setDisputedisexit(Integer disputedisexit) {
        this.disputedisexit = disputedisexit;
    }

    public String getOrderstatusbeforesellersubmisson() {
        return orderstatusbeforesellersubmisson;
    }

    public void setOrderstatusbeforesellersubmisson(String orderstatusbeforesellersubmisson) {
        this.orderstatusbeforesellersubmisson = orderstatusbeforesellersubmisson;
    }

    public List<Object> getBuyerdisputedmessagesandresponse() {
        return buyerdisputedmessagesandresponse;
    }

    public void setBuyerdisputedmessagesandresponse(List<Object> buyerdisputedmessagesandresponse) {
        this.buyerdisputedmessagesandresponse = buyerdisputedmessagesandresponse;
    }

    public List<Disputereason> getDisputereason() {
        return disputereason;
    }

    public void setDisputereason(List<Disputereason> disputereason) {
        this.disputereason = disputereason;
    }

    public String getProcessingFeeType() {
        return processingFeeType;
    }

    public void setProcessingFeeType(String processingFeeType) {
        this.processingFeeType = processingFeeType;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public DeliverNote getDeliverNote() {
        return deliverNote;
    }

    public void setDeliverNote(DeliverNote deliverNote) {
        this.deliverNote = deliverNote;
    }

    public boolean getDeliverdornot() {
        return deliverdornot;
    }

    public void setDeliverdornot(boolean deliverdornot) {
        this.deliverdornot = deliverdornot;
    }

}
