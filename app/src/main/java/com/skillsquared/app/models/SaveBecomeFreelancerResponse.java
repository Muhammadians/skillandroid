package com.skillsquared.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SaveBecomeFreelancerResponse {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("identityFile")
    @Expose
    private String identityFile;
    @SerializedName("skillSquaredUser")
    @Expose
    private SaveBecomeFreelancerUser skillSquaredUser;
    @SerializedName("data_user")
    @Expose
    private String dataUser;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getIdentityFile() {
        return identityFile;
    }

    public void setIdentityFile(String identityFile) {
        this.identityFile = identityFile;
    }

    public String getDataUser() {
        return dataUser;
    }

    public void setDataUser(String dataUser) {
        this.dataUser = dataUser;
    }

    public SaveBecomeFreelancerUser getSkillSquaredUser() {
        return skillSquaredUser;
    }

    public void setSkillSquaredUser(SaveBecomeFreelancerUser skillSquaredUser) {
        this.skillSquaredUser = skillSquaredUser;
    }
}
