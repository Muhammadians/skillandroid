package com.skillsquared.app.models.updateUserProfile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateUserProfileResponse {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("userobject")
    @Expose
    private Userobject userobject;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Userobject getUserobject() {
        return userobject;
    }

    public void setUserobject(Userobject userobject) {
        this.userobject = userobject;
    }

}
