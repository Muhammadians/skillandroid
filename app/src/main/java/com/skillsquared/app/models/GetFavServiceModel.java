package com.skillsquared.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetFavServiceModel {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("favouriteServices")
    @Expose
    private List<FavouriteService> favouriteServices = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<FavouriteService> getFavouriteServices() {
        return favouriteServices;
    }

    public void setFavouriteServices(List<FavouriteService> favouriteServices) {
        this.favouriteServices = favouriteServices;
    }

}
