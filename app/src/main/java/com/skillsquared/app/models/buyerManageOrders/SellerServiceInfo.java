package com.skillsquared.app.models.buyerManageOrders;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SellerServiceInfo {

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("delivery")
    @Expose
    private String delivery;
    @SerializedName("service_id")
    @Expose
    private String serviceId;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDelivery() {
        return delivery;
    }

    public void setDelivery(String delivery) {
        this.delivery = delivery;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

}
