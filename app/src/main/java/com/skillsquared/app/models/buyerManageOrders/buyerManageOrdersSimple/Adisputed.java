package com.skillsquared.app.models.buyerManageOrders.buyerManageOrdersSimple;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.skillsquared.app.models.buyerManageOrders.SellerServiceInfo;

public class Adisputed {

    @SerializedName("user_duartion_qoute")
    @Expose
    private String userDuartionQoute;
    @SerializedName("seller_offer_request_id")
    @Expose
    private String sellerOfferRequestId;
    @SerializedName("seller_id")
    @Expose
    private String sellerId;
    @SerializedName("request_price")
    @Expose
    private String requestPrice;
    @SerializedName("payment_order_id")
    @Expose
    private String paymentOrderId;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("sellerfiles")
    @Expose
    private String sellerfiles;
    @SerializedName("seller_service_info")
    @Expose
    private SellerServiceInfo sellerServiceInfo;
    @SerializedName("seller_name")
    @Expose
    private String sellerName;
    @SerializedName("seller_image")
    @Expose
    private String sellerImage;

    public String getUserDuartionQoute() {
        return userDuartionQoute;
    }

    public void setUserDuartionQoute(String userDuartionQoute) {
        this.userDuartionQoute = userDuartionQoute;
    }

    public String getSellerOfferRequestId() {
        return sellerOfferRequestId;
    }

    public void setSellerOfferRequestId(String sellerOfferRequestId) {
        this.sellerOfferRequestId = sellerOfferRequestId;
    }

    public String getSellerId() {
        return sellerId;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

    public String getRequestPrice() {
        return requestPrice;
    }

    public void setRequestPrice(String requestPrice) {
        this.requestPrice = requestPrice;
    }

    public String getPaymentOrderId() {
        return paymentOrderId;
    }

    public void setPaymentOrderId(String paymentOrderId) {
        this.paymentOrderId = paymentOrderId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSellerfiles() {
        return sellerfiles;
    }

    public void setSellerfiles(String sellerfiles) {
        this.sellerfiles = sellerfiles;
    }

    public SellerServiceInfo getSellerServiceInfo() {
        return sellerServiceInfo;
    }

    public void setSellerServiceInfo(SellerServiceInfo sellerServiceInfo) {
        this.sellerServiceInfo = sellerServiceInfo;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public String getSellerImage() {
        return sellerImage;
    }

    public void setSellerImage(String sellerImage) {
        this.sellerImage = sellerImage;
    }

}
