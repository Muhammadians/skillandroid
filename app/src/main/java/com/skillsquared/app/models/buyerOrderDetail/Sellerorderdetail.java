package com.skillsquared.app.models.buyerOrderDetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Sellerorderdetail {

    @SerializedName("orderderid")
    @Expose
    private String orderderid;
    @SerializedName("buyer_id")
    @Expose
    private String buyerId;
    @SerializedName("service_id")
    @Expose
    private String serviceId;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("duration")
    @Expose
    private String duration;
    @SerializedName("orderNo")
    @Expose
    private String orderNo;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("payment_id")
    @Expose
    private String paymentId;
    @SerializedName("offer_description")
    @Expose
    private String offerDescription;
    @SerializedName("payer_name")
    @Expose
    private String payerName;
    @SerializedName("service_detail")
    @Expose
    private String serviceDetail;

    public String getOrderderid() {
        return orderderid;
    }

    public void setOrderderid(String orderderid) {
        this.orderderid = orderderid;
    }

    public String getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(String buyerId) {
        this.buyerId = buyerId;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getOfferDescription() {
        return offerDescription;
    }

    public void setOfferDescription(String offerDescription) {
        this.offerDescription = offerDescription;
    }

    public String getPayerName() {
        return payerName;
    }

    public void setPayerName(String payerName) {
        this.payerName = payerName;
    }

    public String getServiceDetail() {
        return serviceDetail;
    }

    public void setServiceDetail(String serviceDetail) {
        this.serviceDetail = serviceDetail;
    }

}
