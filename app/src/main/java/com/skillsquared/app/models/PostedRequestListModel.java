package com.skillsquared.app.models;

public class PostedRequestListModel {

    int id;
    String date;
    String status;
    String description;
    String duration;

    public PostedRequestListModel(int id, String date, String status, String description, String duration) {
        this.id = id;
        this.date = date;
        this.status = status;
        this.description = description;
        this.duration = duration;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }
}
