package com.skillsquared.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AnalyticsModel {

    @SerializedName("seller_complted_orders_count")
    @Expose
    private Integer sellerCompltedOrdersCount;
    @SerializedName("seller_earnings_current_month")
    @Expose
    private String sellerEarningsCurrentMonth;
    @SerializedName("seller_earnings")
    @Expose
    private String sellerEarnings;
    @SerializedName("seller_analytics")
    @Expose
    private String sellerAnalytics;
    @SerializedName("avgSellingPrice")
    @Expose
    private Integer avgSellingPrice;

    public Integer getSellerCompltedOrdersCount() {
        return sellerCompltedOrdersCount;
    }

    public void setSellerCompltedOrdersCount(Integer sellerCompltedOrdersCount) {
        this.sellerCompltedOrdersCount = sellerCompltedOrdersCount;
    }

    public String getSellerEarningsCurrentMonth() {
        return sellerEarningsCurrentMonth;
    }

    public void setSellerEarningsCurrentMonth(String sellerEarningsCurrentMonth) {
        this.sellerEarningsCurrentMonth = sellerEarningsCurrentMonth;
    }

    public String getSellerEarnings() {
        return sellerEarnings;
    }

    public void setSellerEarnings(String sellerEarnings) {
        this.sellerEarnings = sellerEarnings;
    }

    public String getSellerAnalytics() {
        return sellerAnalytics;
    }

    public void setSellerAnalytics(String sellerAnalytics) {
        this.sellerAnalytics = sellerAnalytics;
    }

    public Integer getAvgSellingPrice() {
        return avgSellingPrice;
    }

    public void setAvgSellingPrice(Integer avgSellingPrice) {
        this.avgSellingPrice = avgSellingPrice;
    }

}
