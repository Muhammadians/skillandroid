package com.skillsquared.app.models.gigsDetailsModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GigsServicesModel {

    @SerializedName("services")
    @Expose
    private List<GigsService> services = null;
    @SerializedName("category_filters")
    @Expose
    private List<CategoryFilterModel> categoryFilters = null;
    @SerializedName("price_filters")
    @Expose
    private List<String> priceFilters = null;

    public List<GigsService> getServices() {
        return services;
    }

    public void setServices(List<GigsService> services) {
        this.services = services;
    }

    public List<CategoryFilterModel> getCategoryFilters() {
        return categoryFilters;
    }

    public void setCategoryFilters(List<CategoryFilterModel> categoryFilters) {
        this.categoryFilters = categoryFilters;
    }

    public List<String> getPriceFilters() {
        return priceFilters;
    }

    public void setPriceFilters(List<String> priceFilters) {
        this.priceFilters = priceFilters;
    }
}


