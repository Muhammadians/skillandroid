package com.skillsquared.app.models.payStack;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PayStackBankList {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("paystack_user_info")
    @Expose
    private PaystackUserInfo paystackUserInfo;
    @SerializedName("paystackbanklistings")
    @Expose
    private Paystackbanklistings paystackbanklistings;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public PaystackUserInfo getPaystackUserInfo() {
        return paystackUserInfo;
    }

    public void setPaystackUserInfo(PaystackUserInfo paystackUserInfo) {
        this.paystackUserInfo = paystackUserInfo;
    }

    public Paystackbanklistings getPaystackbanklistings() {
        return paystackbanklistings;
    }

    public void setPaystackbanklistings(Paystackbanklistings paystackbanklistings) {
        this.paystackbanklistings = paystackbanklistings;
    }

}
