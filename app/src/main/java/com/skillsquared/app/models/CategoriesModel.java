package com.skillsquared.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.skillsquared.app.models.modelsOnHome.PopularCategoriesModel;

import java.util.List;

public class CategoriesModel {

    @Expose
    @SerializedName("object")
    private List<PopularCategoriesModel> popularServices = null;

    public CategoriesModel(List<PopularCategoriesModel> popularServices) {
        this.popularServices = popularServices;
    }

    public List<PopularCategoriesModel> getPopularServices() {
        return popularServices;
    }

    public void setPopularServices(List<PopularCategoriesModel> popularServices) {
        this.popularServices = popularServices;
    }
}
