package com.skillsquared.app.models;

public class GigsUserItemModel {

    int id;
    int imagePath;
    double rating;
    int numberOfComments;
    String description;
    int payment;

    public GigsUserItemModel(int id, int imagePath, double rating, int numberOfComments, String description, int payment) {
        this.id = id;
        this.imagePath = imagePath;
        this.rating = rating;
        this.numberOfComments = numberOfComments;
        this.description = description;
        this.payment = payment;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getImagePath() {
        return imagePath;
    }

    public void setImagePath(int imagePath) {
        this.imagePath = imagePath;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public int getNumberOfComments() {
        return numberOfComments;
    }

    public void setNumberOfComments(int numberOfComments) {
        this.numberOfComments = numberOfComments;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPayment() {
        return payment;
    }

    public void setPayment(int payment) {
        this.payment = payment;
    }
}
