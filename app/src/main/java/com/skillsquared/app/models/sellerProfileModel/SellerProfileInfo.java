package com.skillsquared.app.models.sellerProfileModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SellerProfileInfo {

    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("freelancer_title")
    @Expose
    private String freelancerTitle;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("language")
    @Expose
    private String language;
    @SerializedName("memberSince")
    @Expose
    private String memberSince;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("recent_delivery")
    @Expose
    private String recentDelivery;
    @SerializedName("freelancer_image")
    @Expose
    private String freelancerImage;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFreelancerTitle() {
        return freelancerTitle;
    }

    public void setFreelancerTitle(String freelancerTitle) {
        this.freelancerTitle = freelancerTitle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getMemberSince() {
        return memberSince;
    }

    public void setMemberSince(String memberSince) {
        this.memberSince = memberSince;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getRecentDelivery() {
        return recentDelivery;
    }

    public void setRecentDelivery(String recentDelivery) {
        this.recentDelivery = recentDelivery;
    }

    public String getFreelancerImage() {
        return freelancerImage;
    }

    public void setFreelancerImage(String freelancerImage) {
        this.freelancerImage = freelancerImage;
    }

}
