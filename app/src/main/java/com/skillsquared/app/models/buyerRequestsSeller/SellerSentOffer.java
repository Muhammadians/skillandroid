package com.skillsquared.app.models.buyerRequestsSeller;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SellerSentOffer {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("offer_sent")
    @Expose
    private String offerSent;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("user_name")
    @Expose
    private String userName;
    @SerializedName("budget")
    @Expose
    private String budget;
    @SerializedName("delievry")
    @Expose
    private String delievry;
    @SerializedName("created_date")
    @Expose
    private String createdDate;
    @SerializedName("require_doc")
    @Expose
    private String requireDoc;
    @SerializedName("user_image")
    @Expose
    private String userImage;
    @SerializedName("totaloffer")
    @Expose
    private String totaloffer;
    @SerializedName("applide_or_not")
    @Expose
    private String applideOrNot;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOfferSent() {
        return offerSent;
    }

    public void setOfferSent(String offerSent) {
        this.offerSent = offerSent;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getBudget() {
        return budget;
    }

    public void setBudget(String budget) {
        this.budget = budget;
    }

    public String getDelievry() {
        return delievry;
    }

    public void setDelievry(String delievry) {
        this.delievry = delievry;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getRequireDoc() {
        return requireDoc;
    }

    public void setRequireDoc(String requireDoc) {
        this.requireDoc = requireDoc;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getTotaloffer() {
        return totaloffer;
    }

    public void setTotaloffer(String totaloffer) {
        this.totaloffer = totaloffer;
    }

    public String getApplideOrNot() {
        return applideOrNot;
    }

    public void setApplideOrNot(String applideOrNot) {
        this.applideOrNot = applideOrNot;
    }

}
