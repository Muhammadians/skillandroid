package com.skillsquared.app.models.becomeFreelancerDetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetBecomeFreelanceDetails {

    @SerializedName("countries")
    @Expose
    private List<Country> countries = null;
    @SerializedName("catoptions")
    @Expose
    private List<Catoption> catoptions = null;
    @SerializedName("languages")
    @Expose
    private List<String> languages = null;
    /*@SerializedName("loc_data")
    @Expose
    private LocData locData;*/
    @SerializedName("userMainInfo")
    @Expose
    private UserMainInfo userMainInfo;
    /*@SerializedName("userFreelancerInfo")
    @Expose
    private UserFreelancerInfo userFreelancerInfo;*/
    @SerializedName("address")
    @Expose
    private String address;

    public List<Country> getCountries() {
        return countries;
    }

    public void setCountries(List<Country> countries) {
        this.countries = countries;
    }

    public List<Catoption> getCatoptions() {
        return catoptions;
    }

    public void setCatoptions(List<Catoption> catoptions) {
        this.catoptions = catoptions;
    }

    /*public LocData getLocData() {
        return locData;
    }

    public void setLocData(LocData locData) {
        this.locData = locData;
    }*/

    public List<String> getLanguages() {
        return languages;
    }

    public void setLanguages(List<String> languages) {
        this.languages = languages;
    }

    public UserMainInfo getUserMainInfo() {
        return userMainInfo;
    }

    public void setUserMainInfo(UserMainInfo userMainInfo) {
        this.userMainInfo = userMainInfo;
    }

    /*public UserFreelancerInfo getUserFreelancerInfo() {
        return userFreelancerInfo;
    }

    public void setUserFreelancerInfo(UserFreelancerInfo userFreelancerInfo) {
        this.userFreelancerInfo = userFreelancerInfo;
    }*/

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

}
