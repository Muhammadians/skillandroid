package com.skillsquared.app.models.gigsDetailsModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReviewModel {

    @Expose
    @SerializedName("review")
    String review;

    @Expose
    @SerializedName("rating")
    String rating;

    @Expose
    @SerializedName("name")
    String name;

    @Expose
    @SerializedName("image")
    String image;

    @Expose
    @SerializedName("username")
    String username;

    public ReviewModel(String review, String rating, String name, String image, String username) {
        this.review = review;
        this.rating = rating;
        this.name = name;
        this.image = image;
        this.username = username;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
