package com.skillsquared.app.models.buyerRequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BuyerRequest {

    @SerializedName("buyerrequests")
    @Expose
    private BuyerRequestModel buyerrequests;

    public BuyerRequestModel getBuyerrequests() {
        return buyerrequests;
    }

    public void setBuyerrequests(BuyerRequestModel buyerrequests) {
        this.buyerrequests = buyerrequests;
    }

}
