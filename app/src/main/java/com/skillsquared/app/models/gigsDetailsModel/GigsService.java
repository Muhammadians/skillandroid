package com.skillsquared.app.models.gigsDetailsModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GigsService {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("service_title")
    @Expose
    private String serviceTitle;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("uniquekey")
    @Expose
    private String uniquekey;
    @SerializedName("freelancer_image")
    @Expose
    private String freelancerImage;
    @SerializedName("freelancer_username")
    @Expose
    private String freelancerUsername;
    @SerializedName("servicekeyword")
    @Expose
    private String servicekeyword;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("media")
    @Expose
    private String media;
    @SerializedName("service_rating")
    @Expose
    private String serviceRating;
    @SerializedName("reviews")
    @Expose
    private List<ReviewModel> reviews = null;
    @SerializedName("liked")
    @Expose
    private Boolean liked;
    @SerializedName("freelancer_user_id")
    @Expose
    private int freelancerId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getServiceTitle() {
        return serviceTitle;
    }

    public void setServiceTitle(String serviceTitle) {
        this.serviceTitle = serviceTitle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUniquekey() {
        return uniquekey;
    }

    public void setUniquekey(String uniquekey) {
        this.uniquekey = uniquekey;
    }

    public String getFreelancerImage() {
        return freelancerImage;
    }

    public void setFreelancerImage(String freelancerImage) {
        this.freelancerImage = freelancerImage;
    }

    public String getFreelancerUsername() {
        return freelancerUsername;
    }

    public void setFreelancerUsername(String freelancerUsername) {
        this.freelancerUsername = freelancerUsername;
    }

    public String getServicekeyword() {
        return servicekeyword;
    }

    public void setServicekeyword(String servicekeyword) {
        this.servicekeyword = servicekeyword;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getMedia() {
        return media;
    }

    public void setMedia(String media) {
        this.media = media;
    }

    public String getServiceRating() {
        return serviceRating;
    }

    public void setServiceRating(String serviceRating) {
        this.serviceRating = serviceRating;
    }

    public List<ReviewModel> getReviews() {
        return reviews;
    }

    public void setReviews(List<ReviewModel> reviews) {
        this.reviews = reviews;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Boolean getLiked() {
        return liked;
    }

    public void setLiked(Boolean liked) {
        this.liked = liked;
    }

    public int getFreelancerId() {
        return freelancerId;
    }

    public void setFreelancerId(int freelancerId) {
        this.freelancerId = freelancerId;
    }
}
