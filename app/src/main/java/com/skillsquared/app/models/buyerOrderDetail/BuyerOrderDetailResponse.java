package com.skillsquared.app.models.buyerOrderDetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BuyerOrderDetailResponse {

    @SerializedName("sellerorderdetail")
    @Expose
    private List<Sellerorderdetail> sellerorderdetail = null;
    @SerializedName("orderstatusbeforesellersubmisson")
    @Expose
    private String orderstatusbeforesellersubmisson;
    @SerializedName("disputedisexit")
    @Expose
    private Integer disputedisexit;
    @SerializedName("processing_fee_type")
    @Expose
    private String processingFeeType;
    @SerializedName("disputereason")
    @Expose
    private List<Disputereason> disputereason = null;
    @SerializedName("status")
    @Expose
    private Status status;
    @SerializedName("review")
    @Expose
    private Boolean review;
    @SerializedName("orderStatus")
    @Expose
    private String orderStatus;
    @SerializedName("deliverNote")
    @Expose
    private DeliverNote deliverNote;

    public List<Sellerorderdetail> getSellerorderdetail() {
        return sellerorderdetail;
    }

    public void setSellerorderdetail(List<Sellerorderdetail> sellerorderdetail) {
        this.sellerorderdetail = sellerorderdetail;
    }

    public String getOrderstatusbeforesellersubmisson() {
        return orderstatusbeforesellersubmisson;
    }

    public void setOrderstatusbeforesellersubmisson(String orderstatusbeforesellersubmisson) {
        this.orderstatusbeforesellersubmisson = orderstatusbeforesellersubmisson;
    }

    public Integer getDisputedisexit() {
        return disputedisexit;
    }

    public void setDisputedisexit(Integer disputedisexit) {
        this.disputedisexit = disputedisexit;
    }

    public String getProcessingFeeType() {
        return processingFeeType;
    }

    public void setProcessingFeeType(String processingFeeType) {
        this.processingFeeType = processingFeeType;
    }

    public List<Disputereason> getDisputereason() {
        return disputereason;
    }

    public void setDisputereason(List<Disputereason> disputereason) {
        this.disputereason = disputereason;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Boolean getReview() {
        return review;
    }

    public void setReview(Boolean review) {
        this.review = review;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public DeliverNote getDeliverNote() {
        return deliverNote;
    }

    public void setDeliverNote(DeliverNote deliverNote) {
        this.deliverNote = deliverNote;
    }

}
