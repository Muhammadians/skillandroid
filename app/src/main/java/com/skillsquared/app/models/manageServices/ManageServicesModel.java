package com.skillsquared.app.models.manageServices;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ManageServicesModel {

    @SerializedName("inactive_gigs_services")
    @Expose
    private Object inactiveGigsServices;
    @SerializedName("active_gigs_services")
    @Expose
    private List<ActiveGig> activeGigsServices = null;
    @SerializedName("pending_gigs_services")
    @Expose
    private Object pendingGigsServices;

    public Object getInactiveGigsServices() {
        return inactiveGigsServices;
    }

    public void setInactiveGigsServices(Object inactiveGigsServices) {
        this.inactiveGigsServices = inactiveGigsServices;
    }

    public List<ActiveGig> getActiveGigsServices() {
        return activeGigsServices;
    }

    public void setActiveGigsServices(List<ActiveGig> activeGigsServices) {
        this.activeGigsServices = activeGigsServices;
    }

    public Object getPendingGigsServices() {
        return pendingGigsServices;
    }

    public void setPendingGigsServices(Object pendingGigsServices) {
        this.pendingGigsServices = pendingGigsServices;
    }


}
