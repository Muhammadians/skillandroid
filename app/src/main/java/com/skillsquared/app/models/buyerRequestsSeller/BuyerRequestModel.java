package com.skillsquared.app.models.buyerRequestsSeller;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BuyerRequestModel {

    @SerializedName("buyer_requests")
    @Expose
    private BuyerRequestOffers buyerRequests;
    @SerializedName("sent_offers")
    @Expose
    private List<SentOffers> sentOffers = null;
    @SerializedName("sellerservices")
    @Expose
    private List<Sellerservice> sellerservices = null;

    public BuyerRequestOffers getBuyerRequests() {
        return buyerRequests;
    }

    public void setBuyerRequests(BuyerRequestOffers buyerRequests) {
        this.buyerRequests = buyerRequests;
    }

    public List<SentOffers> getSentOffers() {
        return sentOffers;
    }

    public void setSentOffers(List<SentOffers> sentOffers) {
        this.sentOffers = sentOffers;
    }

    public List<Sellerservice> getSellerservices() {
        return sellerservices;
    }

    public void setSellerservices(List<Sellerservice> sellerservices) {
        this.sellerservices = sellerservices;
    }

}
