package com.skillsquared.app.models.buyerManageOrders;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.skillsquared.app.models.buyerManageOrders.buyerManageOrdersSimple.Aactive;
import com.skillsquared.app.models.buyerManageOrders.buyerManageOrdersSimple.Acompleted;
import com.skillsquared.app.models.buyerManageOrders.buyerManageOrdersSimple.Adisputed;
import com.skillsquared.app.models.buyerManageOrders.buyerManageOrdersSimple.Amanage;
import com.skillsquared.app.models.buyerManageOrders.buyerManageOrdersSimple.Arevision;

import java.util.List;

public class BuyerOrdersManagement {

    @SerializedName("aactive")
    @Expose
    private List<Aactive> aactive = null;
    @SerializedName("amanage")
    @Expose
    private List<Amanage> amanage = null;
    @SerializedName("arevsion")
    @Expose
    private List<Arevision> arevsion = null;
    @SerializedName("acompleted")
    @Expose
    private List<Acompleted> acompleted = null;
    @SerializedName("adisputed")
    @Expose
    private List<Adisputed> adisputed;

    public List<Aactive> getAactive() {
        return aactive;
    }

    public void setAactive(List<Aactive> aactive) {
        this.aactive = aactive;
    }

    public List<Amanage> getAmanage() {
        return amanage;
    }

    public void setAmanage(List<Amanage> amanage) {
        this.amanage = amanage;
    }

    public List<Arevision> getArevsion() {
        return arevsion;
    }

    public void setArevsion(List<Arevision> arevsion) {
        this.arevsion = arevsion;
    }

    public List<Acompleted> getAcompleted() {
        return acompleted;
    }

    public void setAcompleted(List<Acompleted> acompleted) {
        this.acompleted = acompleted;
    }

    public List<Adisputed> getAdisputed() {
        return adisputed;
    }

    public void setAdisputed(List<Adisputed> adisputed) {
        this.adisputed = adisputed;
    }

}
