package com.skillsquared.app.models.postAJob;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SubCatModelForPostAJob {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("subcat_child")
    @Expose
    private List<SubCatChildModelForPostAJob> subcatChild = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<SubCatChildModelForPostAJob> getSubcatChild() {
        return subcatChild;
    }

    public void setSubcatChild(List<SubCatChildModelForPostAJob> subcatChild) {
        this.subcatChild = subcatChild;
    }

}
