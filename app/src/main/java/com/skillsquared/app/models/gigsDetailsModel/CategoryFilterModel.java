package com.skillsquared.app.models.gigsDetailsModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CategoryFilterModel {

    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("category_title")
    @Expose
    private String categoryTitle;
    @SerializedName("data_count")
    @Expose
    private String dataCount;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryTitle() {
        return categoryTitle;
    }

    public void setCategoryTitle(String categoryTitle) {
        this.categoryTitle = categoryTitle;
    }

    public String getDataCount() {
        return dataCount;
    }

    public void setDataCount(String dataCount) {
        this.dataCount = dataCount;
    }

}
