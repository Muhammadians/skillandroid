package com.skillsquared.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NotificationResponse {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("notifications")
    @Expose
    private List<NotificationObject> notifications = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<NotificationObject> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<NotificationObject> notifications) {
        this.notifications = notifications;
    }

}
