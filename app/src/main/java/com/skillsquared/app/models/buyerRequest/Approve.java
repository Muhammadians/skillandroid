package com.skillsquared.app.models.buyerRequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Approve {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("buyer_id")
    @Expose
    private String buyerId;
    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("budget")
    @Expose
    private String budget;
    @SerializedName("delievry")
    @Expose
    private String delievry;
    @SerializedName("require_doc")
    @Expose
    private Boolean requireDoc;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("created_date")
    @Expose
    private String createdDate;
    @SerializedName("assign_to_seller_offer_request_id")
    @Expose
    private Object assignToSellerOfferRequestId;
    @SerializedName("totaloffer")
    @Expose
    private int totaloffer;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(String buyerId) {
        this.buyerId = buyerId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBudget() {
        return budget;
    }

    public void setBudget(String budget) {
        this.budget = budget;
    }

    public String getDelievry() {
        return delievry;
    }

    public void setDelievry(String delievry) {
        this.delievry = delievry;
    }

    public Boolean getRequireDoc() {
        return requireDoc;
    }

    public void setRequireDoc(Boolean requireDoc) {
        this.requireDoc = requireDoc;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public Object getAssignToSellerOfferRequestId() {
        return assignToSellerOfferRequestId;
    }

    public void setAssignToSellerOfferRequestId(Object assignToSellerOfferRequestId) {
        this.assignToSellerOfferRequestId = assignToSellerOfferRequestId;
    }

    public int getTotaloffer() {
        return totaloffer;
    }

    public void setTotaloffer(int totaloffer) {
        this.totaloffer = totaloffer;
    }

}
