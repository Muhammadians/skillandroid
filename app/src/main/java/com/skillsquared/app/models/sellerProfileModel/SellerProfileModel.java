package com.skillsquared.app.models.sellerProfileModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SellerProfileModel {

    @SerializedName("page_title")
    @Expose
    private String pageTitle;
    @SerializedName("freelancer_user_id")
    @Expose
    private String freelancerUserId;
    @SerializedName("avgRating")
    @Expose
    private Integer avgRating;
    @SerializedName("profileinfo")
    @Expose
    private Profileinfo profileinfo;
    @SerializedName("skills")
    @Expose
    private List<String> skills = null;
    @SerializedName("SellerReviews")
    @Expose
    private List<SellerReview> sellerReviews = null;

    public String getPageTitle() {
        return pageTitle;
    }

    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }

    public String getFreelancerUserId() {
        return freelancerUserId;
    }

    public void setFreelancerUserId(String freelancerUserId) {
        this.freelancerUserId = freelancerUserId;
    }

    public Integer getAvgRating() {
        return avgRating;
    }

    public void setAvgRating(Integer avgRating) {
        this.avgRating = avgRating;
    }

    public Profileinfo getProfileinfo() {
        return profileinfo;
    }

    public void setProfileinfo(Profileinfo profileinfo) {
        this.profileinfo = profileinfo;
    }

    public List<String> getSkills() {
        return skills;
    }

    public void setSkills(List<String> skills) {
        this.skills = skills;
    }

    public List<SellerReview> getSellerReviews() {
        return sellerReviews;
    }

    public void setSellerReviews(List<SellerReview> sellerReviews) {
        this.sellerReviews = sellerReviews;
    }

}
