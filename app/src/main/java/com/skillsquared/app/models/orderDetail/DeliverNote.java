package com.skillsquared.app.models.orderDetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeliverNote {

    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("delivered_time")
    @Expose
    private String deliveredTime;
    @SerializedName("file")
    @Expose
    private String file;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDeliveredTime() {
        return deliveredTime;
    }

    public void setDeliveredTime(String deliveredTime) {
        this.deliveredTime = deliveredTime;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

}
