package com.skillsquared.app.models.viewOffers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Asellerrequest {

    @SerializedName("freelancer_id")
    @Expose
    private String freelancerId;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("freelancer_title")
    @Expose
    private String freelancerTitle;
    @SerializedName("request_id")
    @Expose
    private String requestId;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("service_id")
    @Expose
    private String serviceId;
    @SerializedName("freelancerimage")
    @Expose
    private String freelancerimage;
    @SerializedName("duration")
    @Expose
    private String duration;
    @SerializedName("frellancer_location")
    @Expose
    private String frellancerLocation;
    @SerializedName("seller_id")
    @Expose
    private String sellerId;
    @SerializedName("freelancer_skills")
    @Expose
    private String freelancerSkills;
    @SerializedName("freelancer_username")
    @Expose
    private String freelancer_username;

    public String getFreelancer_username() {
        return freelancer_username;
    }

    public void setFreelancer_username(String freelancer_username) {
        this.freelancer_username = freelancer_username;
    }

    public String getFreelancerId() {
        return freelancerId;
    }

    public void setFreelancerId(String freelancerId) {
        this.freelancerId = freelancerId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFreelancerTitle() {
        return freelancerTitle;
    }

    public void setFreelancerTitle(String freelancerTitle) {
        this.freelancerTitle = freelancerTitle;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getFreelancerimage() {
        return freelancerimage;
    }

    public void setFreelancerimage(String freelancerimage) {
        this.freelancerimage = freelancerimage;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getFrellancerLocation() {
        return frellancerLocation;
    }

    public void setFrellancerLocation(String frellancerLocation) {
        this.frellancerLocation = frellancerLocation;
    }

    public String getSellerId() {
        return sellerId;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

    public String getFreelancerSkills() {
        return freelancerSkills;
    }

    public void setFreelancerSkills(String freelancerSkills) {
        this.freelancerSkills = freelancerSkills;
    }

}
