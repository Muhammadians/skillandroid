package com.skillsquared.app.models.buyerManageOrders;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BuyerManageOrderResponse {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("buyerordersmanagement")
    @Expose
    private BuyerOrdersManagement buyerordersmanagement;
    @SerializedName("buyerordersmanagement_custom")
    @Expose
    private BuyerordersmanagementCustom buyerordersmanagementCustom;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public BuyerOrdersManagement getBuyerordersmanagement() {
        return buyerordersmanagement;
    }

    public void setBuyerordersmanagement(BuyerOrdersManagement buyerordersmanagement) {
        this.buyerordersmanagement = buyerordersmanagement;
    }

    public BuyerordersmanagementCustom getBuyerordersmanagementCustom() {
        return buyerordersmanagementCustom;
    }

    public void setBuyerordersmanagementCustom(BuyerordersmanagementCustom buyerordersmanagementCustom) {
        this.buyerordersmanagementCustom = buyerordersmanagementCustom;
    }

}
