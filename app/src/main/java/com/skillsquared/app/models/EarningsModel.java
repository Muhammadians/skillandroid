package com.skillsquared.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EarningsModel {

    @SerializedName("seller_earnings")
    @Expose
    private Integer sellerEarnings;
    @SerializedName("avgSellingPrice")
    @Expose
    private Double avgSellingPrice;
    @SerializedName("seller_earnings_current_month")
    @Expose
    private Integer sellerEarningsCurrentMonth;
    @SerializedName("total_completed_orders")
    @Expose
    private Integer totalCompletedOrders;
    @SerializedName("paystack")
    @Expose
    private String paystack;

    public Integer getSellerEarnings() {
        return sellerEarnings;
    }

    public void setSellerEarnings(Integer sellerEarnings) {
        this.sellerEarnings = sellerEarnings;
    }

    public Double getAvgSellingPrice() {
        return avgSellingPrice;
    }

    public void setAvgSellingPrice(Double avgSellingPrice) {
        this.avgSellingPrice = avgSellingPrice;
    }

    public Integer getSellerEarningsCurrentMonth() {
        return sellerEarningsCurrentMonth;
    }

    public void setSellerEarningsCurrentMonth(Integer sellerEarningsCurrentMonth) {
        this.sellerEarningsCurrentMonth = sellerEarningsCurrentMonth;
    }

    public Integer getTotalCompletedOrders() {
        return totalCompletedOrders;
    }

    public void setTotalCompletedOrders(Integer totalCompletedOrders) {
        this.totalCompletedOrders = totalCompletedOrders;
    }

    public String getPaystack() {
        return paystack;
    }

    public void setPaystack(String paystack) {
        this.paystack = paystack;
    }

}
