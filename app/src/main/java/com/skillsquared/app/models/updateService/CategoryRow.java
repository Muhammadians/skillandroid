package com.skillsquared.app.models.updateService;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CategoryRow {

    @SerializedName("menu_id")
    @Expose
    private String menuId;
    @SerializedName("sub_menu_id")
    @Expose
    private String subMenuId;

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public String getSubMenuId() {
        return subMenuId;
    }

    public void setSubMenuId(String subMenuId) {
        this.subMenuId = subMenuId;
    }

}
