package com.skillsquared.app.models.updateService;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.skillsquared.app.models.postAJob.GetCategoriesModelForPostAJob;
import com.skillsquared.app.models.postAJob.SubCatChildModelForPostAJob;

import java.util.List;

public class UpdateServiceResponse {

    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("subcategory_id")
    @Expose
    private String subcategoryId;
    @SerializedName("subcategorychild_id")
    @Expose
    private String subcategorychildId;
    @SerializedName("service_detail")
    @Expose
    private ServiceDetail serviceDetail;
    @SerializedName("catoptions")
    @Expose
    private List<GetCategoriesModelForPostAJob> catoptions = null;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getSubcategoryId() {
        return subcategoryId;
    }

    public void setSubcategoryId(String subcategoryId) {
        this.subcategoryId = subcategoryId;
    }

    public String getSubcategorychildId() {
        return subcategorychildId;
    }

    public void setSubcategorychildId(String subcategorychildId) {
        this.subcategorychildId = subcategorychildId;
    }

    public ServiceDetail getServiceDetail() {
        return serviceDetail;
    }

    public void setServiceDetail(ServiceDetail serviceDetail) {
        this.serviceDetail = serviceDetail;
    }

    public List<GetCategoriesModelForPostAJob> getCatoptions() {
        return catoptions;
    }

    public void setCatoptions(List<GetCategoriesModelForPostAJob> catoptions) {
        this.catoptions = catoptions;
    }

}
