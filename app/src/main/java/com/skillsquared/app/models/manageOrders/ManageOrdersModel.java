package com.skillsquared.app.models.manageOrders;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.skillsquared.app.models.buyerManageOrders.BuyerordersmanagementCustom;

public class ManageOrdersModel {

    @SerializedName("sellerordersfrombuyers")
    @Expose
    private SellerordersfrombuyersCustom sellerordersfrombuyers;
    @SerializedName("sellerordersfrombuyers_custom")
    @Expose
    private BuyerordersmanagementCustom sellerordersfrombuyersCustom;

    public SellerordersfrombuyersCustom getSellerordersfrombuyers() {
        return sellerordersfrombuyers;
    }

    public void setSellerordersfrombuyers(SellerordersfrombuyersCustom sellerordersfrombuyers) {
        this.sellerordersfrombuyers = sellerordersfrombuyers;
    }

    public BuyerordersmanagementCustom getSellerordersfrombuyersCustom() {
        return sellerordersfrombuyersCustom;
    }

    public void setSellerordersfrombuyersCustom(BuyerordersmanagementCustom sellerordersfrombuyersCustom) {
        this.sellerordersfrombuyersCustom = sellerordersfrombuyersCustom;
    }

}
