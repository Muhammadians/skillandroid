package com.skillsquared.app.models.viewOffers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ViewOffersModel {

    @SerializedName("abuyerrequest")
    @Expose
    private Abuyerrequest abuyerrequest;
    @SerializedName("asellerrequest")
    @Expose
    private List<Asellerrequest> asellerrequest = null;
    @SerializedName("ordercheck")
    @Expose
    private Integer ordercheck;
    @SerializedName("job_awarded_seller_id")
    @Expose
    private Object jobAwardedSellerId;

    public Abuyerrequest getAbuyerrequest() {
        return abuyerrequest;
    }

    public void setAbuyerrequest(Abuyerrequest abuyerrequest) {
        this.abuyerrequest = abuyerrequest;
    }

    public List<Asellerrequest> getAsellerrequest() {
        return asellerrequest;
    }

    public void setAsellerrequest(List<Asellerrequest> asellerrequest) {
        this.asellerrequest = asellerrequest;
    }

    public Integer getOrdercheck() {
        return ordercheck;
    }

    public void setOrdercheck(Integer ordercheck) {
        this.ordercheck = ordercheck;
    }

    public Object getJobAwardedSellerId() {
        return jobAwardedSellerId;
    }

    public void setJobAwardedSellerId(Object jobAwardedSellerId) {
        this.jobAwardedSellerId = jobAwardedSellerId;
    }
}
