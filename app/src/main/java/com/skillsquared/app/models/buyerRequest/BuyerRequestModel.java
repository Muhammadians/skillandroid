package com.skillsquared.app.models.buyerRequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BuyerRequestModel {

    @SerializedName("approve")
    @Expose
    private List<Approve> approve = null;
    @SerializedName("unapproved")
    @Expose
    private List<UnApproved> unapproved = null;
    @SerializedName("pending")
    @Expose
    private List<Pending> pending = null;

    public List<Approve> getApprove() {
        return approve;
    }

    public void setApprove(List<Approve> approve) {
        this.approve = approve;
    }

    public List<UnApproved> getUnapproved() {
        return unapproved;
    }

    public void setUnapproved(List<UnApproved> unapproved) {
        this.unapproved = unapproved;
    }

    public List<Pending> getPending() {
        return pending;
    }

    public void setPending(List<Pending> pending) {
        this.pending = pending;
    }

}
