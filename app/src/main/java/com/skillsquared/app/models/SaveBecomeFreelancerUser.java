package com.skillsquared.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SaveBecomeFreelancerUser {

    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("referal_code")
    @Expose
    private String referalCode;
    @SerializedName("timezone")
    @Expose
    private String timezone;
    @SerializedName("freelancer")
    @Expose
    private Boolean freelancer;
    @SerializedName("freelancer_id")
    @Expose
    private Integer freelancerId;
    @SerializedName("freelancerProfileSet")
    @Expose
    private Boolean freelancerProfileSet;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getReferalCode() {
        return referalCode;
    }

    public void setReferalCode(String referalCode) {
        this.referalCode = referalCode;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public Boolean getFreelancer() {
        return freelancer;
    }

    public void setFreelancer(Boolean freelancer) {
        this.freelancer = freelancer;
    }

    public Integer getFreelancerId() {
        return freelancerId;
    }

    public void setFreelancerId(Integer freelancerId) {
        this.freelancerId = freelancerId;
    }

    public Boolean getFreelancerProfileSet() {
        return freelancerProfileSet;
    }

    public void setFreelancerProfileSet(Boolean freelancerProfileSet) {
        this.freelancerProfileSet = freelancerProfileSet;
    }

}
