package com.skillsquared.app.models.viewOffers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Abuyerrequest {

    @SerializedName("buyer_id")
    @Expose
    private String buyerId;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("budget")
    @Expose
    private String budget;
    @SerializedName("delievry")
    @Expose
    private String delievry;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("buyerimage")
    @Expose
    private String buyerimage;
    @SerializedName("buyer_username")
    @Expose
    private String buyerUsername;

    public String getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(String buyerId) {
        this.buyerId = buyerId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBudget() {
        return budget;
    }

    public void setBudget(String budget) {
        this.budget = budget;
    }

    public String getDelievry() {
        return delievry;
    }

    public void setDelievry(String delievry) {
        this.delievry = delievry;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBuyerimage() {
        return buyerimage;
    }

    public void setBuyerimage(String buyerimage) {
        this.buyerimage = buyerimage;
    }

    public String getBuyerUsername() {
        return buyerUsername;
    }

    public void setBuyerUsername(String buyerUsername) {
        this.buyerUsername = buyerUsername;
    }

}
