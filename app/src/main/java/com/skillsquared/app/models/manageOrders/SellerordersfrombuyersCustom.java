package com.skillsquared.app.models.manageOrders;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SellerordersfrombuyersCustom {

    @SerializedName("aactive")
    @Expose
    private List<Aactive> aactive = null;
    @SerializedName("adelivered")
    @Expose
    private List<Adelivered> adelivered = null;
    @SerializedName("acompleted")
    @Expose
    private List<Acompleted> acompleted = null;
    @SerializedName("arevsion")
    @Expose
    private List<Arevision> arevsion = null;
    @SerializedName("adisputed")
    @Expose
    private List<Adisputed> adisputed = null;

    public List<Aactive> getAactive() {
        return aactive;
    }

    public void setAactive(List<Aactive> aactive) {
        this.aactive = aactive;
    }

    public List<Adelivered> getAdelivered() {
        return adelivered;
    }

    public void setAdelivered(List<Adelivered> adelivered) {
        this.adelivered = adelivered;
    }

    public List<Acompleted> getAcompleted() {
        return acompleted;
    }

    public void setAcompleted(List<Acompleted> acompleted) {
        this.acompleted = acompleted;
    }

    public List<Arevision> getArevsion() {
        return arevsion;
    }

    public void setArevsion(List<Arevision> arevsion) {
        this.arevsion = arevsion;
    }

    public List<Adisputed> getAdisputed() {
        return adisputed;
    }

    public void setAdisputed(List<Adisputed> adisputed) {
        this.adisputed = adisputed;
    }

}
