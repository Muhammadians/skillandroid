package com.skillsquared.app.models.viewOffers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SellerInfoModel {

    @SerializedName("freelancer_info")
    @Expose
    private FreelanceInfo freelancerInfo;

    public FreelanceInfo getFreelancerInfo() {
        return freelancerInfo;
    }

    public void setFreelancerInfo(FreelanceInfo freelancerInfo) {
        this.freelancerInfo = freelancerInfo;
    }

}
