package com.skillsquared.app.models.postAJob;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetCategoriesModelForPostAJob {

    @SerializedName("cat_id")
    @Expose
    private String catId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("subcat")
    @Expose
    private List<SubCatModelForPostAJob> subcat = null;

    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<SubCatModelForPostAJob> getSubcat() {
        return subcat;
    }

    public void setSubcat(List<SubCatModelForPostAJob> subcat) {
        this.subcat = subcat;
    }

}
