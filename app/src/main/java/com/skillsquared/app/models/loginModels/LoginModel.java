package com.skillsquared.app.models.loginModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.skillsquared.app.handler.RestApi;

public class LoginModel {

    @SerializedName("freelancer")
    @Expose
    private Boolean freelancer;
    @SerializedName("freelancerProfileSet")
    @Expose
    private Boolean freelancerProfileSet;
    @SerializedName("freelancer_id")
    @Expose
    private Integer freelancerId;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("skillSquaredUser")
    @Expose
    private LoginModelUser skillSquaredUser;

    public Boolean getFreelancer() {
        return freelancer;
    }

    public void setFreelancer(Boolean freelancer) {
        this.freelancer = freelancer;
    }

    public Integer getFreelancerId() {
        return freelancerId;
    }

    public void setFreelancerId(Integer freelancerId) {
        this.freelancerId = freelancerId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LoginModelUser getSkillSquaredUser() {
        return skillSquaredUser;
    }

    public void setSkillSquaredUser(LoginModelUser skillSquaredUser) {
        this.skillSquaredUser = skillSquaredUser;
    }

    public Boolean getFreelancerProfileSet() {
        return freelancerProfileSet;
    }

    public void setFreelancerProfileSet(Boolean freelancerProfileSet) {
        this.freelancerProfileSet = freelancerProfileSet;
    }
}