package com.skillsquared.app.models.sellerProfileModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Profileinfo {

    @SerializedName("online")
    @Expose
    private Integer online;
    @SerializedName("SellerProfileinfo")
    @Expose
    private SellerProfileInfo sellerProfileinfo;
    @SerializedName("gigs")
    @Expose
    private Gigs gigs;

    public Integer getOnline() {
        return online;
    }

    public void setOnline(Integer online) {
        this.online = online;
    }

    public SellerProfileInfo getSellerProfileinfo() {
        return sellerProfileinfo;
    }

    public void setSellerProfileinfo(SellerProfileInfo sellerProfileinfo) {
        this.sellerProfileinfo = sellerProfileinfo;
    }

    public Gigs getGigs() {
        return gigs;
    }

    public void setGigs(Gigs gigs) {
        this.gigs = gigs;
    }

}
