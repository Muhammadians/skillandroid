package com.skillsquared.app.models.buyerOrderDetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Status {

    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("payment_id")
    @Expose
    private String paymentId;
    @SerializedName("service_id")
    @Expose
    private String serviceId;
    @SerializedName("status_id")
    @Expose
    private String statusId;
    @SerializedName("statusTitle")
    @Expose
    private String statusTitle;
    @SerializedName("sellerName")
    @Expose
    private String sellerName;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getStatusId() {
        return statusId;
    }

    public void setStatusId(String statusId) {
        this.statusId = statusId;
    }

    public String getStatusTitle() {
        return statusTitle;
    }

    public void setStatusTitle(String statusTitle) {
        this.statusTitle = statusTitle;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

}
