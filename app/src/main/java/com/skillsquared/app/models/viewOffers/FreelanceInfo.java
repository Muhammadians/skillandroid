package com.skillsquared.app.models.viewOffers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FreelanceInfo {

    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("freelancer_title")
    @Expose
    private String freelancerTitle;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("language")
    @Expose
    private String language;
    @SerializedName("avgRating")
    @Expose
    private Integer avgRating;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("userImage")
    @Expose
    private String userImage;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFreelancerTitle() {
        return freelancerTitle;
    }

    public void setFreelancerTitle(String freelancerTitle) {
        this.freelancerTitle = freelancerTitle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Integer getAvgRating() {
        return avgRating;
    }

    public void setAvgRating(Integer avgRating) {
        this.avgRating = avgRating;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

}
