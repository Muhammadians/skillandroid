package com.skillsquared.app.models.modelsOnHome;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FeaturedGigsModel {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("service_title")
    @Expose
    private String serviceTitle;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("uniquekey")
    @Expose
    private String uniquekey;
    @SerializedName("freelancer_id")
    @Expose
    private String freelancerId;
    @SerializedName("servicekeyword")
    @Expose
    private String servicekeyword;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("category_name")
    @Expose
    private String categoryName;
    @SerializedName("current_status")
    @Expose
    private String currentStatus;
    @SerializedName("media")
    @Expose
    private String media;
    @SerializedName("service_username")
    @Expose
    private String serviceUsername;
    @SerializedName("user_image")
    @Expose
    private String userImage;
    @SerializedName("username")
    @Expose
    private String username;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getServiceTitle() {
        return serviceTitle;
    }

    public void setServiceTitle(String serviceTitle) {
        this.serviceTitle = serviceTitle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUniquekey() {
        return uniquekey;
    }

    public void setUniquekey(String uniquekey) {
        this.uniquekey = uniquekey;
    }

    public String getFreelancerId() {
        return freelancerId;
    }

    public void setFreelancerId(String freelancerId) {
        this.freelancerId = freelancerId;
    }

    public String getServicekeyword() {
        return servicekeyword;
    }

    public void setServicekeyword(String servicekeyword) {
        this.servicekeyword = servicekeyword;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }

    public String getMedia() {
        return media;
    }

    public void setMedia(String media) {
        this.media = media;
    }

    public String getServiceUsername() {
        return serviceUsername;
    }

    public void setServiceUsername(String serviceUsername) {
        this.serviceUsername = serviceUsername;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

}
