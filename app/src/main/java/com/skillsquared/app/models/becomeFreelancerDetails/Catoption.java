package com.skillsquared.app.models.becomeFreelancerDetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Catoption {

    @SerializedName("cat_id")
    @Expose
    private String catId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("subcat")
    @Expose
    private List<Subcat> subcat = null;

    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Subcat> getSubcat() {
        return subcat;
    }

    public void setSubcat(List<Subcat> subcat) {
        this.subcat = subcat;
    }

}
