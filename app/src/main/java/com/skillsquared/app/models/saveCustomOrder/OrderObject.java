package com.skillsquared.app.models.saveCustomOrder;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderObject {

    @SerializedName("service_id")
    @Expose
    private String serviceId;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("requirements")
    @Expose
    private String requirements;
    @SerializedName("work_duration")
    @Expose
    private String workDuration;
    @SerializedName("buyer_id")
    @Expose
    private Integer buyerId;
    @SerializedName("seller_id")
    @Expose
    private String sellerId;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("customorderid")
    @Expose
    private Integer customorderid;

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getRequirements() {
        return requirements;
    }

    public void setRequirements(String requirements) {
        this.requirements = requirements;
    }

    public String getWorkDuration() {
        return workDuration;
    }

    public void setWorkDuration(String workDuration) {
        this.workDuration = workDuration;
    }

    public Integer getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(Integer buyerId) {
        this.buyerId = buyerId;
    }

    public String getSellerId() {
        return sellerId;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public Integer getCustomorderid() {
        return customorderid;
    }

    public void setCustomorderid(Integer customorderid) {
        this.customorderid = customorderid;
    }
}
