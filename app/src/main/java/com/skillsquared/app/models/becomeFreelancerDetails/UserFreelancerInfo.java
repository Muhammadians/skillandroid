package com.skillsquared.app.models.becomeFreelancerDetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserFreelancerInfo {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("balance")
    @Expose
    private String balance;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("freelancer_title")
    @Expose
    private String freelancerTitle;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("availablity")
    @Expose
    private String availablity;
    @SerializedName("lang")
    @Expose
    private String lang;
    @SerializedName("cnic")
    @Expose
    private String cnic;
    @SerializedName("location_id")
    @Expose
    private String locationId;
    @SerializedName("facebbok")
    @Expose
    private String facebbok;
    @SerializedName("linked_in")
    @Expose
    private String linkedIn;
    @SerializedName("portfolio")
    @Expose
    private String portfolio;
    @SerializedName("google_plus")
    @Expose
    private String googlePlus;
    @SerializedName("twitter")
    @Expose
    private String twitter;
    @SerializedName("instagram")
    @Expose
    private String instagram;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("created_on")
    @Expose
    private String createdOn;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFreelancerTitle() {
        return freelancerTitle;
    }

    public void setFreelancerTitle(String freelancerTitle) {
        this.freelancerTitle = freelancerTitle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAvailablity() {
        return availablity;
    }

    public void setAvailablity(String availablity) {
        this.availablity = availablity;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getCnic() {
        return cnic;
    }

    public void setCnic(String cnic) {
        this.cnic = cnic;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getFacebbok() {
        return facebbok;
    }

    public void setFacebbok(String facebbok) {
        this.facebbok = facebbok;
    }

    public String getLinkedIn() {
        return linkedIn;
    }

    public void setLinkedIn(String linkedIn) {
        this.linkedIn = linkedIn;
    }

    public String getPortfolio() {
        return portfolio;
    }

    public void setPortfolio(String portfolio) {
        this.portfolio = portfolio;
    }

    public String getGooglePlus() {
        return googlePlus;
    }

    public void setGooglePlus(String googlePlus) {
        this.googlePlus = googlePlus;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }
}
