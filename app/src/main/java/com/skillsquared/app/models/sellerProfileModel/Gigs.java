package com.skillsquared.app.models.sellerProfileModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Gigs {

    @SerializedName("inactive_gigs_services")
    @Expose
    private Object inactiveGigsServices;
    @SerializedName("active_gigs_services")
    @Expose
    private List<ActiveGigsService> activeGigsServices = null;
    @SerializedName("pending_gigs_services")
    @Expose
    private Object pendingGigsServices;

    public Object getInactiveGigsServices() {
        return inactiveGigsServices;
    }

    public void setInactiveGigsServices(Object inactiveGigsServices) {
        this.inactiveGigsServices = inactiveGigsServices;
    }

    public List<ActiveGigsService> getActiveGigsServices() {
        return activeGigsServices;
    }

    public void setActiveGigsServices(List<ActiveGigsService> activeGigsServices) {
        this.activeGigsServices = activeGigsServices;
    }

    public Object getPendingGigsServices() {
        return pendingGigsServices;
    }

    public void setPendingGigsServices(Object pendingGigsServices) {
        this.pendingGigsServices = pendingGigsServices;
    }

}
