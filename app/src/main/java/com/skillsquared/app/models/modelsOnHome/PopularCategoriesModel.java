package com.skillsquared.app.models.modelsOnHome;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PopularCategoriesModel {

    @Expose
    @SerializedName("cat_id")
    String cat_id;

    @Expose
    @SerializedName("title")
    String title;

    @Expose
    @SerializedName("page_banner")
    String page_banner;

    public PopularCategoriesModel(String cat_id, String title, String page_banner) {
        this.cat_id = cat_id;
        this.title = title;
        this.page_banner = page_banner;
    }

    public String getCat_id() {
        return cat_id;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPage_banner() {
        return page_banner;
    }

    public void setPage_banner(String page_banner) {
        this.page_banner = page_banner;
    }
}
