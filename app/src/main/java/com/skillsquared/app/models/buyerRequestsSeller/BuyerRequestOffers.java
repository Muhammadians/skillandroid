package com.skillsquared.app.models.buyerRequestsSeller;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BuyerRequestOffers {

    @SerializedName("buyerequest")
    @Expose
    private List<BuyerRequestSeller> buyerequest = null;

    public List<BuyerRequestSeller> getBuyerequest() {
        return buyerequest;
    }

    public void setBuyerequest(List<BuyerRequestSeller> buyerequest) {
        this.buyerequest = buyerequest;
    }

}
