package com.skillsquared.app.models.buyerManageOrders.buyerManageOrdersCustom;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.skillsquared.app.models.buyerManageOrders.SellerServiceInfo;

public class Arevision_ {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("seller_offer_request_id")
    @Expose
    private String sellerOfferRequestId;
    @SerializedName("seller_id")
    @Expose
    private String sellerId;
    @SerializedName("payment_order_id")
    @Expose
    private String paymentOrderId;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("work_duration")
    @Expose
    private String workDuration;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("sellerfiles")
    @Expose
    private Object sellerfiles;
    @SerializedName("seller_service_info")
    @Expose
    private SellerServiceInfo sellerServiceInfo;
    @SerializedName("seller_name")
    @Expose
    private String sellerName;
    @SerializedName("seller_image")
    @Expose
    private String sellerImage;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSellerOfferRequestId() {
        return sellerOfferRequestId;
    }

    public void setSellerOfferRequestId(String sellerOfferRequestId) {
        this.sellerOfferRequestId = sellerOfferRequestId;
    }

    public String getSellerId() {
        return sellerId;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

    public String getPaymentOrderId() {
        return paymentOrderId;
    }

    public void setPaymentOrderId(String paymentOrderId) {
        this.paymentOrderId = paymentOrderId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getWorkDuration() {
        return workDuration;
    }

    public void setWorkDuration(String workDuration) {
        this.workDuration = workDuration;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Object getSellerfiles() {
        return sellerfiles;
    }

    public void setSellerfiles(Object sellerfiles) {
        this.sellerfiles = sellerfiles;
    }

    public SellerServiceInfo getSellerServiceInfo() {
        return sellerServiceInfo;
    }

    public void setSellerServiceInfo(SellerServiceInfo sellerServiceInfo) {
        this.sellerServiceInfo = sellerServiceInfo;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public String getSellerImage() {
        return sellerImage;
    }

    public void setSellerImage(String sellerImage) {
        this.sellerImage = sellerImage;
    }

}
