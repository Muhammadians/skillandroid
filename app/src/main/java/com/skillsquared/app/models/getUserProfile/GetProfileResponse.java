package com.skillsquared.app.models.getUserProfile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetProfileResponse {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("uerprofile")
    @Expose
    private List<UserProfile> uerprofile = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<UserProfile> getUerprofile() {
        return uerprofile;
    }

    public void setUerprofile(List<UserProfile> uerprofile) {
        this.uerprofile = uerprofile;
    }

}
