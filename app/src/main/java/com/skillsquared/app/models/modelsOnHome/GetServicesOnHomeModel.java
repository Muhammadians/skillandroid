package com.skillsquared.app.models.modelsOnHome;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetServicesOnHomeModel {

    @SerializedName("banners")
    @Expose
    private List<String> banners = null;
    @SerializedName("categories")
    @Expose
    private List<PopularCategoriesModel> popularServices = null;
    @SerializedName("featured_services")
    @Expose
    private List<FeaturedGigsModel> featuredServices = null;

    public List<String> getBanners() {
        return banners;
    }

    public void setBanners(List<String> banners) {
        this.banners = banners;
    }

    public List<PopularCategoriesModel> getPopularServices() {
        return popularServices;
    }

    public void setPopularServices(List<PopularCategoriesModel> popularServices) {
        this.popularServices = popularServices;
    }

    public List<FeaturedGigsModel> getFeaturedServices() {
        return featuredServices;
    }

    public void setFeaturedServices(List<FeaturedGigsModel> featuredServices) {
        this.featuredServices = featuredServices;
    }

}
