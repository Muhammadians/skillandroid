package com.skillsquared.app.models.manageOrders;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Acompleted {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("seller_offer_request_id")
    @Expose
    private String sellerOfferRequestId;
    @SerializedName("seller_id")
    @Expose
    private String sellerId;
    @SerializedName("payment_order_id")
    @Expose
    private String paymentOrderId;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("work_duration")
    @Expose
    private String workDuration;
    @SerializedName("seller_service_info")
    @Expose
    private String sellerServiceInfo;
    @SerializedName("buyer_name")
    @Expose
    private String buyerName;
    @SerializedName("buyer_image")
    @Expose
    private String buyerImage;
    @SerializedName("created_on")
    @Expose
    private String date;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSellerOfferRequestId() {
        return sellerOfferRequestId;
    }

    public void setSellerOfferRequestId(String sellerOfferRequestId) {
        this.sellerOfferRequestId = sellerOfferRequestId;
    }

    public String getSellerId() {
        return sellerId;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

    public String getPaymentOrderId() {
        return paymentOrderId;
    }

    public void setPaymentOrderId(String paymentOrderId) {
        this.paymentOrderId = paymentOrderId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getWorkDuration() {
        return workDuration;
    }

    public void setWorkDuration(String workDuration) {
        this.workDuration = workDuration;
    }

    public String getSellerServiceInfo() {
        return sellerServiceInfo;
    }

    public void setSellerServiceInfo(String sellerServiceInfo) {
        this.sellerServiceInfo = sellerServiceInfo;
    }

    public String getBuyerName() {
        return buyerName;
    }

    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    public String getBuyerImage() {
        return buyerImage;
    }

    public void setBuyerImage(String buyerImage) {
        this.buyerImage = buyerImage;
    }
}
