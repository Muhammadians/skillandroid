package com.skillsquared.app.handler;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.os.StrictMode;
import android.widget.ImageView;
import android.widget.Toast;

import com.skillsquared.app.utils.Constants;
import com.skillsquared.app.utils.Loading;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class ShareImage {

    public static void shareWhatsapp(ImageView imageView, Context context) {
        Uri bmpUri = getLocalBitmapUri(imageView);
        if (bmpUri != null) {
            Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
            whatsappIntent.setType("*/*");
            whatsappIntent.setPackage("com.whatsapp");
            whatsappIntent.putExtra(Intent.EXTRA_TEXT, Constants.k_shareText);
            whatsappIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
            try {
                StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                StrictMode.setVmPolicy(builder.build());
                context.startActivity(whatsappIntent);
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(context, "Whatsapp is not installed on this device", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(context, "Some went wrong.", Toast.LENGTH_SHORT).show();
        }
    }

    public static Uri getLocalBitmapUri(ImageView imageView) {
        Drawable drawable = imageView.getDrawable();
        Bitmap bmp = null;
        if (drawable instanceof BitmapDrawable){
            bmp = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        } else {
            return null;
        }
        Uri bmpUri = null;
        try {
            File file =  new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DOWNLOADS), "share_image_" + System.currentTimeMillis() + ".jpg");
            file.getParentFile().mkdirs();
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }

    public static void shareImage(ImageView imageView, Context context){
        Uri bmpUri = getLocalBitmapUri(imageView);
        if (bmpUri != null) {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("*/*");
            shareIntent.putExtra(Intent.EXTRA_TEXT,Constants.k_shareText);
            shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
            try {
                StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                StrictMode.setVmPolicy(builder.build());
                context.startActivity(shareIntent);
                Loading.cancel();
            } catch (android.content.ActivityNotFoundException ex) {
                Loading.cancel();
                Toast.makeText(context, "Cannot Share this image.", Toast.LENGTH_SHORT).show();
            }

        } else {
            Loading.cancel();
            Toast.makeText(context, "Cannot Share this image.", Toast.LENGTH_SHORT).show();
        }

    }

}
