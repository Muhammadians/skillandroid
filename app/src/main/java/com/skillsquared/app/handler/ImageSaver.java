package com.skillsquared.app.handler;

import android.os.Environment;

import java.io.File;

public class ImageSaver {

    public void makeDirectory() {
        File direct = new File(Environment.getExternalStorageDirectory()
                + "/ShairiAurIshq");
        if (!direct.exists()) {
            direct.mkdirs();
        }
    }

    public ImageSaver(){
        makeDirectory();
    }

}
