package com.skillsquared.app.handler;

import com.skillsquared.app.StringConverterFactory;
import com.skillsquared.app.models.AnalyticsModel;
import com.skillsquared.app.models.CategoriesModel;
import com.skillsquared.app.models.EarningsModel;
import com.skillsquared.app.models.GetFavServiceModel;
import com.skillsquared.app.models.NotificationResponse;
import com.skillsquared.app.models.PlaceOrderResponse;
import com.skillsquared.app.models.SaveBecomeFreelancerResponse;
import com.skillsquared.app.models.TermsPrivacyModel;
import com.skillsquared.app.models.UploadFileResponse;
import com.skillsquared.app.models.becomeFreelancerDetails.GetBecomeFreelanceDetails;
import com.skillsquared.app.models.becomeFreelancerDetails.GetStates;
import com.skillsquared.app.models.buyerManageOrders.BuyerManageOrderResponse;
import com.skillsquared.app.models.buyerOrderDetail.BuyerOrderDetailResponse;
import com.skillsquared.app.models.buyerRequest.BuyerRequest;
import com.skillsquared.app.models.buyerRequestsSeller.BuyerRequestModel;
import com.skillsquared.app.models.getUserProfile.GetProfileResponse;
import com.skillsquared.app.models.gigsDetailsModel.GigsServicesModel;
import com.skillsquared.app.models.loginModels.LoginModel;
import com.skillsquared.app.models.manageOrders.ManageOrdersModel;
import com.skillsquared.app.models.manageServices.ManageServicesModel;
import com.skillsquared.app.models.modelsOnHome.GetServicesOnHomeModel;
import com.skillsquared.app.models.orderDetail.OrderDetailResponse;
import com.skillsquared.app.models.payStack.PayStackBankList;
import com.skillsquared.app.models.paymentResponse.PaypalResponse;
import com.skillsquared.app.models.postAJob.GetCategoriesModelForPostAJob;
import com.skillsquared.app.models.postAJob.PostAJobResponse;
import com.skillsquared.app.models.saveCustomOrder.SaveCustomOrderResponse;
import com.skillsquared.app.models.sellerProfileModel.SellerProfileModel;
import com.skillsquared.app.models.signUpModels.SignUpModel;
import com.skillsquared.app.models.updateService.UpdateServiceResponse;
import com.skillsquared.app.models.updateUserProfile.UpdateUserProfileResponse;
import com.skillsquared.app.models.viewOffers.SellerInfoModel;
import com.skillsquared.app.models.viewOffers.ViewOffersModel;
import com.skillsquared.app.utils.Constants;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Query;

public class RestApi {

    public static Api api = null;

    public static Api getService() {
        if (api == null) {

            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            httpClient.readTimeout(15, TimeUnit.SECONDS);
            httpClient.connectTimeout(15, TimeUnit.SECONDS);
            httpClient.addInterceptor(logging.setLevel(HttpLoggingInterceptor.Level.BODY));

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.k_SERVER_IP)
                    .addConverterFactory(StringConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient.build())
                    .build();

            api = retrofit.create(Api.class);
        }
        return api;
    }

    public interface Api {

        /*SignUp*/
        @FormUrlEncoded
        @POST("signup")
        Call<SignUpModel> signUpCall(@Field("name") String name,
                                     @Field("email") String email,
                                     @Field("phone") String phone,
                                     @Field("password") String password,
                                     @Field("devicetype") String deviceType,
                                     @Field("device_id") String deviceId,
                                     @Field("timezone") String timeZone,
                                     @Field("become_freelancer") int becomeFreelancer
                                     );

        /*Login*/
        @FormUrlEncoded
        @POST("login")
        Call<LoginModel> loginCall(@Field("email") String email,
                                   @Field("password") String password,
                                   @Field("devicetype") String deviceType,
                                   @Field("device_id") String deviceId);

        /*Get Services*/
        @GET("homeData")
        Call<GetServicesOnHomeModel> getServices(
                @Header("accesstoken") String token
        );

        /*Get Sub Categories For Home Model*/
        @FormUrlEncoded
        @POST("getCategories")
        Call<CategoriesModel> getSubCategories(
                @Field("cat_id") String cat_id
        );

        /*Get Sub Sub Categories List After selecting
         * an option from home model sub categories*/
        @GET("getServices")
        Call<GigsServicesModel> getGigsList(
                @Header("accesstoken") String token,
                @Query("search") String search,
                @Query("cat_id") String cat_id,
                @Query("price") String price,
                @Query("statusfilter") String statusfilter,
                @Query("statusfilter") String pageNumber,
                @Query("radiusrange") String radius,
                @Query("latitude") String lat,
                @Query("longitude") String lon
        );

        /*Get Categories for post a job activity */
        @GET("getCatOptions")
        Call<List<GetCategoriesModelForPostAJob>> getCatForPostJob();

        /*Post A Job*/
        @FormUrlEncoded
        @POST("postBuyerRequest")
        Call<PostAJobResponse> createPost(
                @Header("accesstoken") String token,
                @Field("category_id") String category_id,
                @Field("description") String description,
                @Field("budget") int budget,
                @Field("delivery_time") String delivery_time
        );

        @GET("buyerManageRequest")
        Call<BuyerRequest> getBuyerRequest(
                @Header("accesstoken") String token
        );

        @FormUrlEncoded
        @POST("removesBuyerPostedRequest")
        Call<PostAJobResponse> removeBuyerRequest(
                @Header("accesstoken") String token,
                @Field("id") String id
        );

        @FormUrlEncoded
        @POST("removesellerrequest")
        Call<PostAJobResponse> removeSellerRequest(
                @Header("accesstoken") String token,
                @Field("offer_id") String id
        );

        @FormUrlEncoded
        @POST("offerReceived")
        Call<ViewOffersModel> getViewOffers(
                @Header("accesstoken") String token,
                @Field("request_id") String id
        );

        @FormUrlEncoded
        @POST("sellerInfoByFreelancerID")
        Call<SellerInfoModel> getSellerInfo(
                @Header("accesstoken") String token,
                @Field("freelancer_id") String id
        );

        @GET("analytics")
        Call<AnalyticsModel> getAnalytics(
                @Header("accesstoken") String token
        );

        @GET("getBuyerRequests")
        Call<BuyerRequestModel> getBuyerRequests(
                @Header("accesstoken") String token
        );

        @GET("earnings")
        Call<EarningsModel> getEarnings(
                @Header("accesstoken") String token
        );

        @Multipart
        @POST("createService")
        Call<PostAJobResponse> createService(
                @Header("accesstoken") String token,
                @Part("service_title") RequestBody title,
                @Part("category_id") RequestBody categoryId,
                @Part("description") RequestBody description,
                @Part("price") RequestBody price,
                @Part("delivery_time") RequestBody deliveryTime,
                @Part("service_banner\"; filename=\"myfile.jpg\" ") RequestBody file,
                @Part("additional_info") RequestBody additionalInfo,
                @Part("cat_level") RequestBody catLevel
        );

        @FormUrlEncoded
        @POST("createService")
        Call<PostAJobResponse> updateService(
                @Header("accesstoken") String token,
                @Field("service_id") String serviceId,
                @Field("service_title") String title,
                @Field("category_id") String categoryId,
                @Field("description") String description,
                @Field("price") String price,
                @Field("delivery_time") String deliveryTime,
                @Field("service_banner") String file,
                @Field("additional_info") String additionalInfo,
                @Field("cat_level") String catLevel
        );

        @GET("getSellerProfile")
        Call<SellerProfileModel> getUserProfile(
                @Header("accesstoken") String token
        );

        @GET("manageServices")
        Call<ManageServicesModel> getManageServices(
                @Header("accesstoken") String token
        );

        @GET("orders")
        Call<ManageOrdersModel> getOrders(
                @Header("accesstoken") String token
        );

        @FormUrlEncoded
        @POST("removeSellerService")
        Call<PostAJobResponse> removeService(
                @Header("accesstoken") String token,
                @Field("service_id") String id
        );

        @GET("buyerPlacedOrders")
        Call<BuyerManageOrderResponse> getBuyerManageOrders(
                @Header("accesstoken") String token
        );

        @FormUrlEncoded
        @POST("payBraintree")
        Call<PaypalResponse> getPaypalResponse(
                @Header("accesstoken") String token,
                @Field("nonce") String nonce,
                @Field("amount") String amount
        );

        @FormUrlEncoded
        @POST("makeFavouriteService")
        Call<SignUpModel> makeFavoriteService(
                @Header("accesstoken") String token,
                @Field("service_id") String serviceId
        );

        @GET("terms")
        Call<TermsPrivacyModel> getTerms();

        @GET("privacyPolicy")
        Call<TermsPrivacyModel> getPrivacyPolicy();

        @GET("getFavouriteServices")
        Call<GetFavServiceModel> getFavoriteServices(
                @Header("accesstoken") String token
        );

        @FormUrlEncoded
        @POST("placeOrder")
        Call<PlaceOrderResponse> placeOrder(
                @Header("accesstoken") String token,
                @Field("nonce") String nonce,
                @Field("amount") String amount,
                @Field("request_id") String requestId,
                @Field("payment_method") String paymentMethod
        );

        @FormUrlEncoded
        @POST("sendOffer")
        Call<SignUpModel> sendOffer(
                @Header("accesstoken") String token,
                @Field("service_id") String serviceId,
                @Field("buyer_request_id") String buyerRequestId,
                @Field("description") String description,
                @Field("price") String price,
                @Field("duration") String duration
        );

        @POST("psBankList")
        Call<PayStackBankList> getBankList(
                @Header("accesstoken") String token
        );

        @FormUrlEncoded
        @POST("paystackWithdrawRequest")
        Call<SignUpModel> sendPaystackWithdrawRequest(
                @Header("accesstoken") String token,
                @Field("email") String serviceId,
                @Field("amount") String buyerRequestId
        );

        @FormUrlEncoded
        @POST("psbanksetting")
        Call<SignUpModel> addBankDetails(
                @Header("accesstoken") String token,
                @Field("account_name") String accountName,
                @Field("bank_name") String bankName,
                @Field("bank_code") String bankCode,
                @Field("bank_account_no") String bankAccountNum
        );

        @FormUrlEncoded
        @POST("contactSupport")
        Call<SignUpModel> contactSupport(
                @Header("accesstoken") String token,
                @Field("name") String name,
                @Field("email") String email,
                @Field("subject") String subject,
                @Field("message") String message
        );

        @GET("getProfile")
        Call<GetProfileResponse> getUserDetails(
                @Header("accesstoken") String token
        );

        @Multipart
        @POST("updateProfile")
        Call<UpdateUserProfileResponse> editUserDetails(
                @Header("accesstoken") String token,
                @Part("name") RequestBody name,
                @Part("phone") RequestBody phone,
                @Part("address") RequestBody address,
                @Part("image\"; fileName=\"myFile.png\" ")RequestBody requestBodyFile
        );

        @GET("becomeafreelancer")
        Call<GetBecomeFreelanceDetails> getDetails(
                @Header("accesstoken") String token
        );

        @FormUrlEncoded
        @POST("get_cityStates")
        Call<List<GetStates>> getState(
                @Header("accesstoken") String token,
                @Field("hint") String hint,
                @Field("id") String id
        );

        @FormUrlEncoded
        @POST("saveBecomeFreelancer")
        Call<SaveBecomeFreelancerResponse> saveBecomeFreelancer(
                @Header("accesstoken") String token,
                @Field("freelancer_title") String freelancerTitle,
                @Field("category_id") String catId,
                @Field("availablity") String availability,
                @Field("description") String description,
                @Field("country") String country,
                @Field("state") String state,
                @Field("city") String city,
                @Field("identityfile") String file,
                @Field("facebbok") String facebbok,
                @Field("linked_in") String linked_in,
                @Field("google_plus") String google_plus,
                @Field("twitter") String twitter,
                @Field("instagram") String instagram,
                @Field("username") String username,
                @Field("language") String language
        );

        @Multipart
        @POST("uploadIdentity")
        Call<UploadFileResponse> uploadFile(
                @Header("accesstoken") String token,
                @Part("file\"; fileName=\"myFile.png\" ")RequestBody requestBodyFile
        );

        @FormUrlEncoded
        @POST("forgotPassword")
        Call<SignUpModel> resetPassword(
                @Field("email") String email
        );

        @FormUrlEncoded
        @POST("editservice")
        Call<UpdateServiceResponse> updateService(
                @Header("accesstoken") String token,
                @Field("service_id") String serviceId
        );

        @FormUrlEncoded
        @POST("getSellerProfileById")
        Call<SellerProfileModel> getFreelancerProfile(
                @Header("accesstoken") String token,
                @Field("freelancer_user_id") String freelancerId
        );

        @FormUrlEncoded
        @POST("orderDetail")
        Call<OrderDetailResponse> getOrderDetail(
                @Header("accesstoken") String token,
                @Field("order_id") String orderId
        );

        @FormUrlEncoded
        @POST("orderDetailBuyer")
        Call<BuyerOrderDetailResponse> getBuyerOrderDetail(
                @Header("accesstoken") String token,
                @Field("order_id") String orderId,
                @Field("type") String type
        );

        @FormUrlEncoded
        @POST("generetedisputeFromSeller")
        Call<SignUpModel> generateDispute(
                @Header("accesstoken") String token,
                @Field("order_id") String orderId,
                @Field("disputereason_id") String disputeReasonId,
                @Field("disputerequestmessage") String disputeRequestMessage,
                @Field("ordertype") String ordertype
        );

        @Multipart
        @POST("deliverWork")
        Call<SignUpModel> deliverWork(
                @Header("accesstoken") String token,
                @Part("order_id") RequestBody orderId,
                @Part("description") RequestBody disputeReasonId,
                @Part("file\"; filename=\"myfile.jpg\" ") RequestBody file
        );

        @FormUrlEncoded
        @POST("generetedisputeFromBuyer")
        Call<SignUpModel> generateDisputeFromBuyer(
          @Header("accesstoken") String token,
          @Field("disputereason_id") String disputeId,
          @Field("disputerequestmessage") String message,
          @Field("order_id") String orderId,
          @Field("ordertype") String orderType
        );

        @FormUrlEncoded
        @POST("buyerChangeOrderStatus")
        Call<SignUpModel> buyerChangeOrderStatus(
                @Header("accesstoken") String token,
                @Field("order_id") String orderId,
                @Field("status") String status
        );

        @FormUrlEncoded
        @POST("saveCustomOrder")
        Call<SaveCustomOrderResponse> saveCustomOrder(
                @Header("accesstoken") String token,
                @Field("service_id") String serviceId,
                @Field("price") String price,
                @Field("work_duration") String workDuration,
                @Field("requirements") String requirements
        );

        @FormUrlEncoded
        @POST("placeOrderCustom")
        Call<PlaceOrderResponse> placeCustomOrder(
                @Header("accesstoken") String token,
                @Field("nonce") String nonce,
                @Field("amount") String price,
                @Field("customorderid") Integer requestId,
                @Field("payment_method") String paymentMethod
        );

        @GET("getNotifications")
        Call<NotificationResponse> getNotifications(
                @Header("accesstoken") String token
        );

    }

}