package com.skillsquared.app.handler;

import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatSpinner;

import com.cometchat.pro.constants.CometChatConstants;
import com.cometchat.pro.core.Call;
import com.cometchat.pro.core.CometChat;
import com.cometchat.pro.exceptions.CometChatException;
import com.cometchat.pro.models.User;
import com.skillsquared.app.SkillSquared;
import com.skillsquared.app.cometChat.Activity.OneToOneChatActivity;
import com.skillsquared.app.cometChat.Contracts.StringContract;
import com.skillsquared.app.cometChat.Utils.CommonUtils;
import com.skillsquared.app.utils.Loading;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ApplicationHandler {

    private static final String IMAGE_DIRECTORY = "/Skillsquared";
    private static final int BUFFER_SIZE = 1024 * 2;

    public static void intent(final Class<? extends Activity> ActivityToOpen){
        Context context = SkillSquared.getAppContext();
        Intent intent = new Intent(context, ActivityToOpen);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public static void intent(final Class<? extends Activity> ActivityToOpen, String key, String putExtra){
        Context context = SkillSquared.getAppContext();
        Intent intent = new Intent(context, ActivityToOpen);
        intent.putExtra(key, putExtra);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public static boolean checkVal(String result){
        if (result.equals("Yes")){
            boolean answer = true;
            return answer;
        } else if (result.equals("No")){
            boolean answer = false;
            return answer;
        }
        return false;
    }

    public static boolean radioButtonValidation(RadioGroup radioGroup){
        return radioGroup.getCheckedRadioButtonId() != -1;
    }

    public static void toast(String message){
        Context context = SkillSquared.getAppContext();
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public static void arrayAdapter(int array, Spinner spinner){
        Context context = SkillSquared.getAppContext();
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(context,
                array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    public static void arrayAdapter(ArrayList<String> array, AppCompatSpinner spinner){
        Context context = SkillSquared.getAppContext();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context,
                android.R.layout.simple_spinner_item, array);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    public static String stringConverter(EditText editTextData){
        String value = editTextData.getText().toString().trim();
        return value;
    }

    public static String textViewGetText(TextView textView){
        String value = textView.getText().toString().trim();
        return value;
    }

    public static void setEditTextError(EditText editTextError, String error){
        editTextError.setError(error);
    }

    public static String currentDate() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        return dateFormat.format(date);
    }

    public static String currentTime() {
        DateFormat timeFormat = new SimpleDateFormat("hh:mm:ss");
        Date time = new Date();
        return timeFormat.format(time);
    }

    public static boolean isOnline() {
        Context context = SkillSquared.getAppContext();
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

    public static String getCurrentDateAndTime() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    public static void downloadFile(String uRl, Activity activity) {

        String folderName = "SkillSquared";
        String fileName = getCurrentDateAndTime();

        File folderDirect = new File(Environment.getExternalStorageDirectory()
                + "/"+folderName);

        Log.e("direct", folderDirect + "");

        if (!folderDirect.exists()) {
            folderDirect.mkdirs();
        }

        DownloadManager mgr = (DownloadManager) activity.getSystemService(Context.DOWNLOAD_SERVICE);
        Uri downloadUri = Uri.parse(uRl);
        DownloadManager.Request request = new DownloadManager.Request(
                downloadUri);

        request.setAllowedNetworkTypes(
                DownloadManager.Request.NETWORK_WIFI
                        | DownloadManager.Request.NETWORK_MOBILE)
                .setAllowedOverRoaming(false).setTitle("Image")
                .setDescription("SkillSquared")
                .setDestinationInExternalPublicDir("/"+folderName, fileName+".jpg");

        if (isOnline()){
            if (mgr != null) {
                mgr.enqueue(request);
                toast("Your file has been downloaded.");
            }
        } else {
            toast("File not saved. Please check your network connection");
        }
    }

    public static void hideStatusBar(Activity activity){
        activity.requestWindowFeature(Window.FEATURE_NO_TITLE);
        activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    public static void showSettingsDialog(final Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Need Permissions");
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.");
        builder.setPositiveButton("GOTO SETTINGS", (dialog, which) -> {
            dialog.cancel();
            openSettings(activity);
        });
        builder.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());
        builder.show();
    }

    public static void openSettings(Activity activity) {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", activity.getPackageName(), null);
        intent.setData(uri);
        activity.startActivityForResult(intent, 101);
    }

    public static String getRealPathFromURI(Uri uri, Activity activity) {
        String path = "";
        if (activity.getContentResolver() != null) {
            Cursor cursor = activity.getContentResolver().query(uri, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                path = cursor.getString(idx);
                cursor.close();
            }
        }
        return path;
    }

    public static String getFilePathFromURI(Context context, Uri contentUri) {
        //copy file and send new file path
        String fileName = getFileName(contentUri);
        File wallpaperDirectory = new File(
                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }
        if (!TextUtils.isEmpty(fileName)) {
            File copyFile = new File(wallpaperDirectory + File.separator + fileName);
            // create folder if not exists

            copy(context, contentUri, copyFile);
            return copyFile.getAbsolutePath();
        }
        return null;
    }

    public static String getFileName(Uri uri) {
        if (uri == null) return null;
        String fileName = null;
        String path = uri.getPath();
        int cut = path.lastIndexOf('/');
        if (cut != -1) {
            fileName = path.substring(cut + 1);
        }
        return fileName;
    }

    public static void copy(Context context, Uri srcUri, File dstFile) {
        try {
            InputStream inputStream = context.getContentResolver().openInputStream(srcUri);
            if (inputStream == null) return;
            OutputStream outputStream = new FileOutputStream(dstFile);
            copystream(inputStream, outputStream);
            inputStream.close();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int copystream(InputStream input, OutputStream output) throws Exception, IOException {
        byte[] buffer = new byte[BUFFER_SIZE];

        BufferedInputStream in = new BufferedInputStream(input, BUFFER_SIZE);
        BufferedOutputStream out = new BufferedOutputStream(output, BUFFER_SIZE);
        int count = 0, n = 0;
        try {
            while ((n = in.read(buffer, 0, BUFFER_SIZE)) != -1) {
                out.write(buffer, 0, n);
                count += n;
            }
            out.flush();
        } finally {
            try {
                out.close();
            } catch (IOException e) {
                Log.e(e.getMessage(), String.valueOf(e));
            }
            try {
                in.close();
            } catch (IOException e) {
                Log.e(e.getMessage(), String.valueOf(e));
            }
        }
        return count;
    }

    public static void cometChatCreateUser(String uid, String name, final Class<? extends Activity> ActivityToOpen, String message){

        User user = new User();
        user.setUid("11"+uid);
        user.setName(name);

        CometChat.createUser(user, StringContract.AppDetails.API_KEY, new CometChat.CallbackListener<User>() {
            @Override
            public void onSuccess(User user) {
                Log.e("createUser", user.toString());
                cometChatLogin(uid, ActivityToOpen, message);
            }

            @Override
            public void onError(CometChatException e) {
                Log.e("createUser", e.getMessage());
                cometChatLogin(uid, ActivityToOpen, message);
            }
        });
    }

    public static void cometChatCreateUser(String uid, String name, Activity activity, String message){

        User user = new User();
        user.setUid(uid);
        user.setName(name);

        CometChat.createUser(user, StringContract.AppDetails.API_KEY, new CometChat.CallbackListener<User>() {
            @Override
            public void onSuccess(User user) {
                Log.e("createUser", user.toString());
                String contactID = user.getUid();
                String contactName = user.getName();
                String userAvatar = user.getAvatar();

                Intent intent = new Intent(activity, OneToOneChatActivity.class);
                intent.putExtra(StringContract.IntentStrings.USER_ID, contactID);
                intent.putExtra(StringContract.IntentStrings.USER_AVATAR, userAvatar);
                intent.putExtra(StringContract.IntentStrings.USER_NAME, contactName);
                activity.startActivity(intent);
            }

            @Override
            public void onError(CometChatException e) {
                Log.e("createUser", e.getMessage());
            }
        });
    }

    public static void cometChatCreateUser(String uid, String name,Activity activity){

        User user = new User();
        user.setUid(uid);
        user.setName(name);

        CometChat.createUser(user, StringContract.AppDetails.API_KEY, new CometChat.CallbackListener<User>() {
            @Override
            public void onSuccess(User user) {
                Log.e("createUser", user.toString());
                com.cometchat.pro.core.Call call = new com.cometchat.pro.core.Call(uid, CometChatConstants.RECEIVER_TYPE_USER, CometChatConstants.CALL_TYPE_AUDIO);
                CometChat.initiateCall(call, new CometChat.CallbackListener<com.cometchat.pro.core.Call>() {
                    @Override
                    public void onSuccess(com.cometchat.pro.core.Call call) {
                        Loading.cancel();
                        CommonUtils.startCallIntent(activity, ((User) call.getCallReceiver()), call.getType(), true, call.getSessionId());
                    }

                    @Override
                    public void onError(CometChatException e) {
                        Loading.cancel();
                        Toast.makeText(activity, e.getMessage(), Toast.LENGTH_SHORT).show();
                        Log.e("application handler", "onError: "+e.getMessage());
                    }

                });
            }

            @Override
            public void onError(CometChatException e) {
                Loading.cancel();
                Log.e("createUser", e.getMessage());
            }
        });
    }

    public static void cometChatLogin(String uid, final Class<? extends Activity> ActivityToOpen, String message){
        String apiKey = StringContract.AppDetails.API_KEY; // Replace with your API Key.

        if (CometChat.getLoggedInUser() == null) {
            CometChat.login("11"+uid, apiKey, new CometChat.CallbackListener<User>() {

                @Override
                public void onSuccess(User user) {
                    Log.e("login user", "Login Successful : " + user.toString());
                    Loading.cancel();
                    toast(message);
                    intent(ActivityToOpen);
                }

                @Override
                public void onError(CometChatException e) {
                    Loading.cancel();
                    Log.e("login user", "Login failed with exception: " + e.getMessage());
                }
            });
        } else {
            Log.e("login user", "cometChatLogin: user already login");
            Loading.cancel();
            toast(message);
            intent(ActivityToOpen);
        }
    }

    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";
        // "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (!matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

}
